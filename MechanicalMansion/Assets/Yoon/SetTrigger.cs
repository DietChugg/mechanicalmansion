﻿using UnityEngine;
using System.Collections;

public class SetTrigger : MonoBehaviour 
{
	public string _trigger;
	
	public void Set () 
	{
		transform.GetComponent<Animator>().SetTrigger(_trigger);
	}
}
