﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

namespace SceneEventSystem
{
    public class SceneSelectable : MonoBehaviour
    {
        #region Public Member Variables
        [Tooltip("Disables the ablility to be clicked")]
        public bool interactable = true;
        [Tooltip("Blocks 2D events past this Object")]
        public bool blockRaycasts2D = false;
        [Tooltip("Blocks 3D events past this Object")]
        public bool blockRaycasts3D = false;
        #endregion

        #region ClickEvents
        public ClickEvent OnClick;                  //Called each time a completed click happens on this object

        //These two are hidden in the inspector because they aren't as useful
        //They can still come in handy though and are easily accessed in code
        [HideInInspector]
        public ClickEvent OnClickUp;                //Called each time a clickUp happens on this object (if the click isn't blocked)
        [HideInInspector]
        public ClickEvent OnClickDown;              //Called each time a clickDown happens on this object (if the click isn't blocked)
        #endregion

        #region Private Member Variables
        private bool _isActive;                     //This is set active if a clickUp event is recieved (makes it eligible for a completed click event)
        private bool _is3D;                         //This is used by the SceneEventSystem to determine which calls can be received by this object
        #endregion

        #region Properties
        public bool IsActive
        {
            get { return _isActive; }
            set { _isActive = value; }
        }
        
        public bool Is3D
        { 
            get { return _is3D; }
            set { _is3D = value; }
        }
        #endregion

        #region Override Methods
        void OnEnable()
        {
            _is3D = GetComponent<Collider>();
        }
        #endregion
    } 
}
