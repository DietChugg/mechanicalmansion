﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace SceneEventSystem
{
    public class SceneEventSystem : MonoBehaviour
    {
        #region Public Member Variables
        [Tooltip("The camera to base click inputs from. If left empty, the MainCamera will be used")]
        public Camera _eventCamera;
        [Tooltip("You can choose to send 3D events only, 2D events only, All events, or none.")]
        public EventType _eventType = EventType.EventsAll;
        #endregion

        #region Private Member Variables
        private List<SceneSelectable> _activeSelectables = new List<SceneSelectable>();     //This tracks all SceneSelectables that have become eligible for a Click event
        private bool _block3Dclicks;                                                        //At the end of the Click Method, this boolean tracks the ability to continue sending 3D events
        private bool _block2Dclicks;                                                        //At the end of the Click Method, this boolean tracks the ability to continue sending 2D events
        private Vector3 _screenPoint;                                                       //Stored in Click method to be sent with the ClickEventData
        #endregion

        #region Override Methods
        void OnEnable()
        {
            if (GetComponent<Camera>() != null)
                _eventCamera = GetComponent<Camera>();
            else
                _eventCamera = Camera.main;
        }

        void Update()
        {
            if (_eventCamera == null)
            {
                OnEnable();
                return;
            }

            if (Input.GetMouseButtonDown(0))
            {
                Click(ClickType.Down, Input.mousePosition);
            }

            if (Input.GetMouseButtonUp(0))
            {
                Click(ClickType.Up, Input.mousePosition);
            }
        }
        #endregion

        #region Public Methods
        /// <summary> 
        /// Performs a click at the screen point 
        /// </summary> 
        /// <param name="clickType">The type of click, either Up or Down </param> 
        /// <param name="screenPoint">Screen point to perform click</param> 
        public void Click(ClickType clickType, Vector3 screenPoint)
        {
            _screenPoint = screenPoint;
            List<SceneSelectable> currentSelectables = new List<SceneSelectable>();
            Ray mouseRay = _eventCamera.ScreenPointToRay(screenPoint);

            //Find all SceneSelectable objects that can be returned using 3D physics
            //and add them to currentSelectables
            if (_eventType == EventType.Events3D || _eventType == EventType.EventsAll)
            {
                List<RaycastHit> hits = Physics.RaycastAll(mouseRay, Mathf.Infinity, _eventCamera.cullingMask).ToList();
                foreach (RaycastHit hit in hits)
                {
                    if (hit.transform)
                        AddToCurrentSelectables(currentSelectables, hit.transform);
                }
            }

            //Find all SceneSelectable objects that can be returned using 2D physics
            //and add them to currentSelectables
            if (_eventType == EventType.Events2D || _eventType == EventType.EventsAll)
            {
                List<RaycastHit2D> hits = Physics2D.GetRayIntersectionAll(mouseRay, Mathf.Infinity, _eventCamera.cullingMask).ToList();
                foreach (RaycastHit2D hit in hits)
                {
                    if (hit.transform)
                        AddToCurrentSelectables(currentSelectables, hit.transform);
                }
            }

            //The Raycast Hit arrays may not have returned in an expected way
            //Also, both types of Raycast hit are potentially now combined in the currentSelectables list
            //These need to be sorted by their distance from the _eventCamera
            //in order for the block raycast variables on the SceneSelectable objects to work properly
            currentSelectables.Sort(delegate(SceneSelectable ss0, SceneSelectable ss1)
            {
                return (_eventCamera.transform.position - ss0.transform.position).sqrMagnitude.CompareTo
                ( (_eventCamera.transform.position - ss1.transform.position).sqrMagnitude );
            });

            //Call the events on all selectables in currentSelectables
            //Based on their order (distance from _eventCamera)
            //They will be checked against the block clicks bools that 
            //may be modified in the CallEvents() method
            foreach (SceneSelectable selectable in currentSelectables)
            {
                if(selectable.Is3D && !_block3Dclicks)
                    CallEvents(clickType, selectable);
                else if (!selectable.Is3D && !_block2Dclicks)
                    CallEvents(clickType, selectable);
            }

            //Reset the click blocking variables
            //These may have been changed inside the above foreach loop to disable clickTypes
            _block3Dclicks = false;
            _block2Dclicks = false;


            //Reset all _activeSelectables so that all SceneSelectable objects that were
            //eligible for an OnClick event, whether they received it or not, are now 
            //properly ready to respond to the next clickType
            if (clickType == ClickType.Up)
            {
                foreach (SceneSelectable selectable in _activeSelectables)
                    selectable.IsActive = false;
                _activeSelectables.Clear();
            }
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// If hitTransform has the SceneSelectable component, it is added to the list of CurrentSelectables
        /// </summary>
        /// <param name="currentSelectables">The list of CurrentSelectables</param>
        /// <param name="hitTransform">The list of CurrentSelectables</param>
        void AddToCurrentSelectables(List<SceneSelectable> currentSelectables, Transform hitTransform)
        {
            SceneSelectable sceneSelectable = hitTransform.GetComponent<SceneSelectable>();
            if (sceneSelectable != null && sceneSelectable.interactable)
                currentSelectables.Add(sceneSelectable);
        }

        /// <summary>
        /// Calls the appropriate Click Event on the SceneSelectable
        /// </summary>
        /// <param name="clickType">clickType helps determine the type of event to call</param>
        /// <param name="selectable">The SceneSelectable object to call events on</param>
        void CallEvents(ClickType clickType, SceneSelectable selectable)
        {
            //Create and initialize a new ClickEventData to send with the action
            ClickEventData ced = new ClickEventData() { sceneEventSystem = this, timeSent = Time.time, screenPointOfClick = _screenPoint };

            switch (clickType)
            {
                case ClickType.Up:
                    selectable.OnClickUp.Invoke(ced);
                    //If the selectable IsActive (has already received a clickDown event before another clickUp has occured),
                    if (selectable.IsActive)
                    {
                        selectable.IsActive = false;
                        selectable.OnClick.Invoke(ced);
                    }
                    break;
                case ClickType.Down:
                    //Set the Selectable's IsActive to true,
                    //This makes it eligible for an OnClick event on the next clickUp
                    selectable.IsActive = true;
                    selectable.OnClickDown.Invoke(ced);
                    //Add to _activeSelectables so that IsActive can be set back to false
                    //on the next clickUp if this selectable is not captured by that clickUp
                    _activeSelectables.Add(selectable);
                    break;
                default:
                    break;
            }

            //If this selectable blocks either 3D or 2D raycasts,
            //change the member variables to restrict functionality
            // in the foreach loop near the end of the Click() method
            if (selectable.blockRaycasts3D)
                _block3Dclicks = true;
            if (selectable.blockRaycasts2D)
                _block2Dclicks = true;
        }
        #endregion
    }

    #region Helper Enums
    public enum ClickType
    {
        Up,
        Down
    }

    public enum EventType
    {
        None,
        Events2D,
        Events3D,
        EventsAll
    }
    #endregion
}