﻿using UnityEngine.Events;
using UnityEngine;

namespace SceneEventSystem
{
    public class ClickEventData
    {
        /// <summary>
        /// The sceneEventSystem creating an instance of ClickEventData
        /// </summary>
        public SceneEventSystem sceneEventSystem;
        /// <summary>
        /// The screen position of the click
        /// </summary>
        public Vector3 screenPointOfClick;
        /// <summary>
        /// The Time.time of the creation of the ClickEventData
        /// </summary>
        public float timeSent;
        //... Oh there's lots of room here.
    }

    //This defines the type of Event used by the SceneEventSystem
    [System.Serializable]
    public class ClickEvent : UnityEvent<ClickEventData> { } 
}
