using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using StarterKit;

public class MaterialsStartColor : MonoBehaviourPlus 
{
    public List<Color> colors = new List<Color>();
	public new void OnEnable () 
	{
        for (int i = 0; i < colors.Count; i++)
        {
            GetComponent<Renderer>().materials[i].color = colors[i];
        }
	}

}
