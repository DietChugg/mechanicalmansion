using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using StarterKit;

public class AnimateScale : MonoBehaviourPlus
{
    public AnimationCurve animationCurve;

	public bool useRandomCurve;
	public List<AnimationCurve> curves = new List<AnimationCurve>();
 //   [HideInInspector]
    public bool isPlaying = false;
    public bool playOnEnable = true;
    public bool isPaused = false;
    public bool loops;
    public static SafeAction<AnimateScale> OnStartPlaying;
    public static SafeAction<AnimateScale> OnNewLoop;
    public static SafeAction<AnimateScale> OnFinishPlaying;
    public static SafeAction<AnimateScale> OnAnimationPause;
    bool stopPlaying;
	public float speed;
    public bool isGlobal;
    public float multiplier = 1f;

	new void OnEnable() 
	{
		(this as MonoBehaviourPlus).OnEnable();
		if(useRandomCurve)
			animationCurve = curves.GetRandom();
		StopAllCoroutines();
        if(playOnEnable)
            Play();
	}
	
	new void OnDisable() 
	{
		(this as MonoBehaviourPlus).OnDisable();
	}
	
	[ContextMenu("Play")]
    public void Play()
    {
		if(!isPlaying)
		{
			StartCoroutine(AnimateScaleValues(animationCurve.keys[0].time));
		}
    }

    public void Play(float startTime)
    {
		if(!isPlaying)
		{
			StartCoroutine(AnimateScaleValues(startTime));
		}
    }

	public void PlayIn(float time)
	{
		StartCoroutine(WaitToPlay(time));
	}

	public IEnumerator WaitToPlay(float time)
	{
		yield return new WaitForSeconds(time);
		if(!isPlaying)
		{
			Play ();
		}
	}

    public void Pause() 
    {
    	isPaused = true;
    }

    public void Resume() 
    {
    	isPaused = false;
    }
    
    public void Stop()
    {
        stopPlaying = true;
    }

	public override void OnLevelLoading (LoadingLevelData lld)
	{
		StopAllCoroutines();
		isPlaying = false;
		ApplyScale(animationCurve.keys.GetLast().time);
	}

	IEnumerator AnimateScaleValues(float startTime)
    {
        if(!isPlaying)
        {
            isPlaying = true;
			ApplyScale(startTime);
			yield return null;
            float curTime = startTime;
            OnStartPlaying.Call(this);
            while((curTime < animationCurve.keys.GetLast().time || loops) && !stopPlaying)
            {
				ApplyScale(curTime);
                
                curTime += Time.deltaTime * speed;
                if(loops)
                {
                    if(curTime >= animationCurve.keys.GetLast().time)
                    {
                        OnNewLoop.Call(this);
                        curTime = animationCurve.keys[0].time;
                    }
                }
                if(isPaused)
                {
                    OnAnimationPause.Call(this);
                }
                while(isPaused)
                {
                    yield return null;
                }

                yield return null;
            }
			ApplyScale(animationCurve.keys.GetLast().time);
            isPlaying = false;
            stopPlaying = false;
            OnFinishPlaying.Call(this);
        }
		yield return null;
    }

	void ApplyScale(float time)
	{
		if (isGlobal) 
		{
			transform.SetLossyScale(animationCurve.Evaluate(time) * multiplier);
		}
		else 
		{
			transform.SetLocalScale(animationCurve.Evaluate(time) * multiplier);
		}
	}

}
