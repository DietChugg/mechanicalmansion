using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayParticles : MonoBehaviour 
{
	public List<ParticleSystem> particleSystems = new List<ParticleSystem>();
	
	public void Play()
	{
		foreach(ParticleSystem ps in particleSystems)
			ps.Play();
	}
}
