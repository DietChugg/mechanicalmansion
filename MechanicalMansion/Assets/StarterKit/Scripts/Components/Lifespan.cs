using UnityEngine;
using System.Collections;

namespace StarterKit
{
    public class Lifespan : MonoBehaviour 
    {
    	public FloatClamp lifespan;
		public bool disablesObject = true;

    	void OnEnable () 
    	{
    		lifespan.Maximize();
    	}
    	
    	void Update () 
    	{
    		lifespan -= Time.deltaTime;
    		if(lifespan.IsAtMin())
    		{
				if(disablesObject)
					gameObject.SetActive(false);
				else
					Destroy(gameObject);
    		}
    	}
    }
}
