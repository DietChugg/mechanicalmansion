using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace StarterKit
{
    public class LoadScene : LifetimeBehaviour
    {
        public Scene scene;
        public LoadLevelOptions loadLevelOptions = LoadLevelOptions.LoadLevel;
        public bool loadRandomScene;
        public List<Scene> scenes;

        /// <summary>
        /// Legacy - This will be removed in the future. It remains for now for backward compatibillity
        /// </summary>
        public bool loadSceneOnEnable;
        
       
        new public string name;
        

        public void OnClick()
        {
            LoadLevel();
        }

        [ContextMenu("LoadLevel")]
        public void LoadLevel()
        {
            if (loadRandomScene)
            {
                scene = scenes.GetRandom();
            }
            if (name.IsNullOrEmpty())
            {
                name = scene.name;
            }
            switch (loadLevelOptions)
            {
                case LoadLevelOptions.LoadLevel:
                    ApplicationX.LoadLevel(name);
                    break;
                case LoadLevelOptions.LoadLevelAdditive:
                    ApplicationX.LoadLevelAdditive(name);
                    break;
                case LoadLevelOptions.LoadLevelAsync:
                    ApplicationX.LoadLevelAsync(name);
                    break;
                case LoadLevelOptions.LoadLevelAdditiveAsync:
                    ApplicationX.LoadLevelAdditiveAsync(name);
                    break;
                default:
                    break;
            }
        }

        void OnEnable()
        {
            if (loadSceneOnEnable)
            {
                LoadLevel();
            }
        }

        void Begin()
        {
            LoadLevel();
        }
    }
}