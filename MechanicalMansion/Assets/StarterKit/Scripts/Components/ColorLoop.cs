//© Waterford Institute 2014
//using UnityEditor;
using UnityEngine;
using System.Collections;

public class ColorLoop : MonoBehaviour
{
	#if UNITY_EDITOR
	[Comment("Loops the Color of the Material through the below Gradient","Use Up and Down Arrow to Start and Stop Looping.")]
	public string dummyString;
	#endif

	public Gradient grad;
	[Range(0.00000000001f, 60)]
	public float loopTime;

	private float evalTime;
	private bool isLooping;
	private bool canLoop;
	public bool playOnEnable = false;

	void OnEnable()
	{
		if(playOnEnable)
		{
			SetLooping(true);
		}
	}


	void Update()
	{
		if(Input.GetKeyDown(KeyCode.UpArrow))
			SetLooping(true);

		if(Input.GetKeyDown(KeyCode.DownArrow))
			SetLooping(false);
	}

	public void SetLooping(bool tf)
	{
		if(tf && !isLooping)
		{
			canLoop = true;
			StartCoroutine(GradientColorChange());
		}
		else if(!tf && isLooping)
			canLoop = false;
	}


	IEnumerator GradientColorChange()
	{
		isLooping = true;
		evalTime = 0;
		yield return null;

		while(evalTime <= loopTime)
		{
			float normalizedEvalTime = evalTime/loopTime;
			if(GetComponent<SpriteRenderer>())
			{
				GetComponent<SpriteRenderer>().color = grad.Evaluate(normalizedEvalTime);
			}
			this.transform.GetComponent<Renderer>().material.color = grad.Evaluate(normalizedEvalTime);
			evalTime += Time.deltaTime;
		
			yield return null;
		}
		isLooping = false;
		if(canLoop)
			StartCoroutine(GradientColorChange());
	}
}

///***************************
////SCRIPT COMMENT AND TOOLTIP
//***************************/
////Paste this at the bottom of your script, after your Class Declaration:
//#if UNITY_EDITOR
//
//public class CommentAttribute : PropertyAttribute 
//{
//	public readonly string tooltip;
//	public readonly string comment;
//	
//	public CommentAttribute( string comment, string tooltip ) 
//	{
//		this.tooltip = tooltip;
//		this.comment = comment;
//	}
//}
//
//[CustomPropertyDrawer(typeof(CommentAttribute))]
//public class CommentDrawer : PropertyDrawer 
//{
//	const int textHeight = 20;
//	
//	CommentAttribute commentAttribute { get { return (CommentAttribute)attribute; } }
//	
//	public override float GetPropertyHeight(SerializedProperty prop, GUIContent label) 
//	{
//		return textHeight;
//	}
//	
//	public override void OnGUI(Rect position, SerializedProperty prop, GUIContent label) 
//	{
//		EditorGUI.LabelField(position,new GUIContent(commentAttribute.comment,commentAttribute.tooltip));
//	}
//}
//
////Add this to the top of your class:
////[Comment("This is the description","And this is the tooltip")]
////public int thisIsADummy;
//
//#endif

