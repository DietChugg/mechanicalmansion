using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using StarterKit;

public class ScreenResolutionSwitcher : MonoBehaviourPlus 
{
	public int width;
	public int height;
	public bool isNewRes;

	new void OnEnable () 
	{
		(this as MonoBehaviourPlus).OnEnable();
		if(isNewRes)
			Screen.SetResolution (width,height,true);

		width = Screen.width;
		height = Screen.height;
		textMesh.text = "w: " + width + "h: " + height;
	}
	
	new void OnDisable () 
	{
		(this as MonoBehaviourPlus).OnDisable ();
	}
}
