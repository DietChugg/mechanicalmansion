using UnityEngine;
using System.Collections;

public class FollowMouse : MonoBehaviour 
{
	void OnEnable()
	{
		Update();
	}

	public void Update()
	{
		Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition); 
		this.transform.position = new Vector2(mousePos.x, mousePos.y);
	}
}
