using UnityEngine;
using System.Collections;

public class SnapToScreenSpace : MonoBehaviour 
{
	public Vector3 screenSpace;
	Vector3 adjustedSpace;
	Vector3 screenPos;
//	Vector3 mousePosition;
//	Vector3 mouseScreenPos;

	void Update () 
	{
		adjustedSpace = new Vector3(screenSpace.x * Screen.width, screenSpace.y * Screen.height,screenSpace.z);
		screenPos = Camera.main.ScreenToWorldPoint(adjustedSpace);
//		mousePosition = Input.mousePosition;
//		mouseScreenPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		if(transform.position != screenPos)
		{
			transform.position = screenPos;
            transform.SetZPosition (screenSpace.z);
		}
		
	}
}
