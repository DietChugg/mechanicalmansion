using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using StarterKit;

public class MoveTowardsTarget : MonoBehaviourPlus
{
    public Bool3 selectionAxis;
    public float speed;
    public Transform target;
	
	void Update() 
	{
        if(selectionAxis.IsAllMarkedAs(true))
        {
            transform.MoveTowards(target.position,speed * Time.deltaTime);
            return;
        }
        if(selectionAxis.x)
        {
            transform.MoveTowards(target.position,speed * Time.deltaTime,Axis.X);
        }
        if(selectionAxis.y)
        {
            transform.MoveTowards(target.position,speed * Time.deltaTime,Axis.Y);
        }
        if(selectionAxis.z)
        {
            transform.MoveTowards(target.position,speed * Time.deltaTime,Axis.Z);
        }
	}
}
