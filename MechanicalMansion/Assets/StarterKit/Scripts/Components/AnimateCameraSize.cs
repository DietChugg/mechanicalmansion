using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using StarterKit;

public class AnimateCameraSize : MonoBehaviourPlus
{
    public AnimationCurve animationCurve;
    protected bool isPlaying = false;
    public bool playOnEnable = true;
    public bool isPaused = false;
    public bool loops;
    public static SafeAction<Camera> OnStartPlaying;
    public static SafeAction<Camera> OnNewLoop;
    public static SafeAction<Camera> OnFinishPlaying;
    public static SafeAction<Camera> OnAnimationPause;
    bool stopPlaying;

	new void OnEnable() 
	{
		(this as MonoBehaviourPlus).OnEnable();
        if(playOnEnable)
            Play();
	}
	
	new void OnDisable() 
	{
		(this as MonoBehaviourPlus).OnDisable();
	}

	public void Play()
	{
		StartCoroutine(PlayAnimation());
	}

    public void Play(float startTime)
    {
        StartCoroutine(PlayAnimation(startTime));
    }
    
    public void Stop()
    {
        stopPlaying = true;
    }

    IEnumerator PlayAnimation()
    {
        StartCoroutine(PlayAnimation(animationCurve.keys[0].time));
        yield return null;
    }

	IEnumerator PlayAnimation(float startTime)
    {
        if(!isPlaying)
        {
            isPlaying = true;
            float curTime = startTime;
            OnStartPlaying.Call(GetComponent<Camera>());
            while((curTime < animationCurve.keys.GetLast().time || loops) && !stopPlaying)
            {
                if(GetComponent<Camera>().orthographic)
                    GetComponent<Camera>().orthographicSize = animationCurve.Evaluate(curTime);
                else
                    GetComponent<Camera>().fieldOfView = animationCurve.Evaluate(curTime);
                curTime += Time.deltaTime;
                if(loops)
                {
                    if(curTime >= animationCurve.keys.GetLast().time)
                    {
                        OnNewLoop.Call(GetComponent<Camera>());
                        curTime = animationCurve.keys[0].time;
                    }
                }
                if(isPaused)
                {
                    OnAnimationPause.Call(GetComponent<Camera>());
                }
                while(isPaused)
                {
                    yield return null;
                }

                yield return null;
            }
            animationCurve.Evaluate(1);
            isPlaying = false;
            stopPlaying = false;
            OnFinishPlaying.Call(GetComponent<Camera>());
        }
    }
}
