using UnityEngine;
using System.Collections;
using StarterKit;

public class LimitTimeInScene : MonoBehaviourPlus 
{
	public string levelToLoad;
	public float timeToWait;
//	bool exitedEarly = false;

	new void OnEnable () 
	{
		(this as MonoBehaviourPlus).OnEnable();
	}
	
	new void OnDisable () 
	{
		(this as MonoBehaviourPlus).OnDisable();
	}

	void Start () 
	{
//		Debug.Log ("LoadLevelAfterDelayStart");
		StartCoroutine(LoadLevelAfterDelay());
	}

	IEnumerator LoadLevelAfterDelay()
	{
//		Debug.Log ("LoadLevelAfterDelay");
		yield return new WaitForSeconds(timeToWait);
//		if(!exitedEarly)
//		Debug.Log ("Here We go again...");
		ApplicationX.LoadLevel(levelToLoad);
	}

//	public override void OnLevelLoading(LoadingLevelData lld)
//	{
//		exitedEarly = true;//
//	}

}
