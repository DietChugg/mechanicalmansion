using UnityEngine;
using System.Collections;
using StarterKit;

public class ActivateRandomChild : LifetimeBehaviour 
{
    public bool disableChildrenFirst = true;
	public override void Begin()
	{
        if(disableChildrenFirst)
            transform.GetKids().SetActive(false);
		transform.GetKids().ToList().GetRandom().gameObject.SetActive(true);
	}
}
