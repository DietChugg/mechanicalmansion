using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class TransformOffset
{
	public Transform transform;
	public Offset position;
	public Offset rotation;
	public Offset scale;

	public TransformOffset()
	{

	}

	public TransformOffset(Transform transform)
	{
		this.transform = transform;
	}

	[System.Serializable]
	public class Offset
	{
		public Space space = Space.Self;
		public Vector3 offset;
	}

	public void Apply()
	{
		Apply (transform);
	}

	public void Apply(Transform transform)
	{
		if (transform == null)
			return;
		transform.Translate(position.offset,position.space);
		transform.Rotate(rotation.offset,rotation.space);
		if(scale.space == Space.Self)
			transform.localScale = transform.localScale + scale.offset;
		else
			transform.SetLossyScale(transform.lossyScale + scale.offset);
	}
}
