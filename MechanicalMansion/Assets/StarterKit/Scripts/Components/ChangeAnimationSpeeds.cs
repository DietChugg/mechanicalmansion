using UnityEngine;
using System.Collections;
using StarterKit;

public class ChangeAnimationSpeeds : MonoBehaviour 
{
	public string animationClipName;
	public bool randomizeStartPoint;
	public bool randomizeSpeed;
	public FloatClamp speed;
	public AnimationCurve curve;
	float current = 0;
    //float animationSpeed;

	void OnEnable()
	{
		if (randomizeStartPoint)
			current = RandomX.value;
		if (randomizeSpeed)
			speed.Randomize ();
	}

	void Update () 
	{
		current += Time.deltaTime * speed.current;
		current = current %1;
		GetComponent<Animation>() [animationClipName].speed = curve.Evaluate (current);
        //animation [animationClipName].speed;//Debug
	}
}
