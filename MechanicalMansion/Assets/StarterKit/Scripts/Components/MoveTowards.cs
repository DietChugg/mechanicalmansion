﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System;
using System.Collections;
using System.Collections.Generic;
using StarterKit;

public class MoveTowards : LifetimeBehaviour 
{
    public MoveType moveType;
    public MoveTarget moveTarget;
    [Tooltip("Note: Once will stop behaviour from Running. OnComplete will call each Loop completion and on the To and From for PingPong")]
    public WrapMode wrapMode;
    public new Vector3 position;
    [Tooltip("Position Over Time Tool")]
    public AnimationCurve speedOverTime = new AnimationCurve(new Keyframe(0,0,0,0),new Keyframe(1,1,0,0));
    [Tooltip("Position Offset for position over time")]
    public AnimationCurve3 positionMod;
    public GameObject target;
    [Tooltip("Start this object at PingPongTarget OnBegin")]
    public bool snapToPingPongTarget;
    [Tooltip("After Going towards target, Where should we return to")]
    public GameObject pingPongTarget;
    [Tooltip("This is Time or Speed")]
    public float value = 1f;
    public UnityEvent OnComplete;

    [Header("Debug")]
    public GizmosShowType gizmosShowType;
    public Color color = Color.red;
    public float size = 1f;
    public AnimationCurve3 invertPositionMod;

	public IEnumerator Run()
	{
        Vector3 targetPos = GetTargetPosition();

        if (wrapMode == WrapMode.PingPong && snapToPingPongTarget)
            transform.position = pingPongTarget.transform.position;

        Vector3 startPosition = transform.position;
        bool playOnce = true;
        while (wrapMode == WrapMode.Loop || wrapMode == WrapMode.PingPong || playOnce)
        {
            if (wrapMode == WrapMode.Loop)
            {
                transform.position = startPosition;
            }
            if (moveType == MoveType.Speed)
            {
                if (target)
                {
                    while (transform.MoveTowards(target.transform.position, value * Time.deltaTime))
                    {
                        yield return null;
                    }
                }
                else 
                {
                    while (transform.MoveTowards(targetPos, value * Time.deltaTime))
                    {
                        yield return null;
                    }
                }
            }
            else
            {
                if (wrapMode == WrapMode.PingPong && target != null)
                    transform.MoveTowardsPointOverTime(pingPongTarget.transform, target.transform, value, speedOverTime, positionMod);
                else
                    transform.MoveTowardsPointOverTime(targetPos, value, speedOverTime, positionMod);
                yield return new WaitForSeconds(value);
                if (wrapMode == WrapMode.PingPong)
                {
                    OnComplete.Invoke();
                    invertPositionMod = positionMod.InverseCurveTimes();
                    if (target)
                        transform.MoveTowardsPointOverTime(target.transform, pingPongTarget.transform, value, speedOverTime, invertPositionMod);
                    else
                        transform.MoveTowardsPointOverTime(startPosition, value, speedOverTime, invertPositionMod);

                    yield return new WaitForSeconds(value);
                }
            }
            OnComplete.Invoke();
            yield return null;
            playOnce = false;
        }
	}

	public new void OnEnable () 
	{
        base.OnEnable();
	}

	public new void OnDisable () 
	{
        base.OnDisable();
	}

    public Vector3 GetTargetPosition()
    {
        switch (moveTarget)
        {
            case MoveTarget.GlobalPosition:
                return position;
            case MoveTarget.LocalPosition:
                return transform.position + position;
            case MoveTarget.TargetPosition:
                return target.transform.position;
            default:
                return Vector3.zero;
        }
    }

    public void OnDrawGizmosSelected()
    {
        if (gizmosShowType != GizmosShowType.OnDrawGizmosSelected)
        {
            return;
        }
        DrawGizmos();
    }

    public void OnDrawGizmos()
    {
        if (gizmosShowType != GizmosShowType.OnDrawGizmos)
        {
            return;
        }
        DrawGizmos();
    }

    public void DrawGizmos()
    {
        Gizmos.color = color;
        switch (moveTarget)
        {
            case MoveTarget.GlobalPosition:
                Gizmos.DrawWireSphere(position, size);
                break;
            case MoveTarget.LocalPosition:
                Gizmos.DrawWireSphere(transform.position + position, size);
                break;
            case MoveTarget.TargetPosition:
                if (target != null)
                {
                    Gizmos.DrawWireSphere(target.transform.position, size);
                }
                break;
            default:
                break;
        }
    }

    public override void Begin()
    {
        base.Begin();
        StartCoroutine(Run());
    }
}

public enum MoveType
{ 
    Time,
    Speed
}

public enum MoveTarget
{
    GlobalPosition,
    LocalPosition,
    TargetPosition
}
