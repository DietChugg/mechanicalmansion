using UnityEngine;
using System.Collections;

public class DeviceSleepTimeout : MonoBehaviour 
{

	public SleepTimeoutSettings SleepTimeoutSetting;
	public enum SleepTimeoutSettings{NeverSleep, SystemSetting, Custom};
    public int customValue = 120;

	void Start () 
	{
		switch (SleepTimeoutSetting)
		{
		case SleepTimeoutSettings.NeverSleep:
			Screen.sleepTimeout = UnityEngine.SleepTimeout.NeverSleep;
			break;
		case SleepTimeoutSettings.SystemSetting:
			Screen.sleepTimeout = SleepTimeout.SystemSetting;
			break;
        case SleepTimeoutSettings.Custom:
            Screen.sleepTimeout = customValue;
            break;
		}
	}
}
