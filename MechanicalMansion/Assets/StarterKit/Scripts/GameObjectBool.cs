using UnityEngine;
using System.Collections;

[System.Serializable]
public class GameObjectBool
{
	public GameObject gameObject;
	public bool value;

	public void SetActive()
	{
		gameObject.SetActive(value);
	}

}
