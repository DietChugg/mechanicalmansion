using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace StarterKit
{
    public struct SafeAction
    {
        public Action action;

        public void Call()
        {
            //        List<Delegate> delegates = new List<Delegate>(action.GetInvocationList());
            //TODO Look into way to check for bad subscriptions;
            //        for (int i = 0; i < delegates.Count; i++)
            //        {
            //            if(delegates[i].Target == null)
            //                delegates.RemoveAt(i);
            //            i--;
            //        }
            //        action. = delegates.ToArray();
            if (action != null) action();
        }

        public void Call(float delay)
        {
            CoroutineManager.Instance.StartCoroutine(DelayedCall(delay));
        }

        IEnumerator DelayedCall(float time)
        {
            yield return new WaitForSeconds(time);
            Call();
        }

        public static SafeAction operator +(SafeAction c1, Action c2)
        {
            c1.action += c2;
            return c1;
        }

        public static SafeAction operator -(SafeAction c1, Action c2)
        {
            c1.action -= c2;
            return c1;
        }

        public static implicit operator Action(SafeAction c1)
        {
            return c1.action;
        }
    }

    public struct SafeAction<T1>
    {
        public Action<T1> action;

        public void Call(T1 t1)
        {
            if (action != null) action(t1);
        }

        public void Call(float delay, T1 t1)
        {
            CoroutineManager.Instance.StartCoroutine(DelayedCall(delay, t1));
        }

        IEnumerator DelayedCall(float time, T1 t1)
        {
            yield return new WaitForSeconds(time);
            Call(t1);
        }

        public static SafeAction<T1> operator +(SafeAction<T1> c1, Action<T1> c2)
        {
            c1.action += c2;
            return c1;
        }

        public static SafeAction<T1> operator -(SafeAction<T1> c1, Action<T1> c2)
        {
            c1.action -= c2;
            return c1;
        }

        public static implicit operator Action<T1>(SafeAction<T1> c1)
        {
            return c1.action;
        }
    }

    public struct SafeAction<T1, T2>
    {
        public Action<T1, T2> action;

        public void Call(T1 t1, T2 t2)
        {
            if (action != null) action(t1, t2);
        }

        public void Call(float delay, T1 t1, T2 t2)
        {
            CoroutineManager.Instance.StartCoroutine(DelayedCall(delay, t1, t2));
        }

        IEnumerator DelayedCall(float time, T1 t1, T2 t2)
        {
            yield return new WaitForSeconds(time);
            Call(t1, t2);
        }

        public static SafeAction<T1, T2> operator +(SafeAction<T1, T2> c1, Action<T1, T2> c2)
        {
            c1.action += c2;
            return c1;
        }

        public static SafeAction<T1, T2> operator -(SafeAction<T1, T2> c1, Action<T1, T2> c2)
        {
            c1.action -= c2;
            return c1;
        }

        public static implicit operator Action<T1, T2>(SafeAction<T1, T2> c1)
        {
            return c1.action;
        }
    }

    public struct SafeAction<T1, T2, T3>
    {
        public Action<T1, T2, T3> action;

        public void Call(T1 t1, T2 t2, T3 t3)
        {
            if (action != null) action(t1, t2, t3);
        }

        public void Call(float delay, T1 t1, T2 t2, T3 t3)
        {
            CoroutineManager.Instance.StartCoroutine(DelayedCall(delay, t1, t2, t3));
        }

        IEnumerator DelayedCall(float time, T1 t1, T2 t2, T3 t3)
        {
            yield return new WaitForSeconds(time);
            Call(t1, t2, t3);
        }

        public static SafeAction<T1, T2, T3> operator +(SafeAction<T1, T2, T3> c1, Action<T1, T2, T3> c2)
        {
            c1.action += c2;
            return c1;
        }

        public static SafeAction<T1, T2, T3> operator -(SafeAction<T1, T2, T3> c1, Action<T1, T2, T3> c2)
        {
            c1.action -= c2;
            return c1;
        }

        public static implicit operator Action<T1, T2, T3>(SafeAction<T1, T2, T3> c1)
        {
            return c1.action;
        }
    }

    public struct SafeAction<T1, T2, T3, T4>
    {
        public Action<T1, T2, T3, T4> action;

        public void Call(T1 t1, T2 t2, T3 t3, T4 t4)
        {
            if (action != null) action(t1, t2, t3, t4);
        }

        public void Call(float delay, T1 t1, T2 t2, T3 t3, T4 t4)
        {
            CoroutineManager.Instance.StartCoroutine(DelayedCall(delay, t1, t2, t3, t4));
        }

        IEnumerator DelayedCall(float time, T1 t1, T2 t2, T3 t3, T4 t4)
        {
            yield return new WaitForSeconds(time);
            Call(t1, t2, t3, t4);
        }

        public static SafeAction<T1, T2, T3, T4> operator +(SafeAction<T1, T2, T3, T4> c1, Action<T1, T2, T3, T4> c2)
        {
            c1.action += c2;
            return c1;
        }

        public static SafeAction<T1, T2, T3, T4> operator -(SafeAction<T1, T2, T3, T4> c1, Action<T1, T2, T3, T4> c2)
        {
            c1.action -= c2;
            return c1;
        }

        public static implicit operator Action<T1, T2, T3, T4>(SafeAction<T1, T2, T3, T4> c1)
        {
            return c1.action;
        }
    }
}