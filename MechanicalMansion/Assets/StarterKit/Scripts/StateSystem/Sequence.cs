using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace StarterKit
{
	[System.Serializable]
		public class Step
		{
			public string name;	
			public FloatClamp timer = new FloatClamp(1f);
			public List<Behaviour> turnOnBehaviours;
			public List<Behaviour> turnOffBehaviours;
			public List<Behaviour> turnSwapBehaviours;
			public List<GameObject> turnOnGameObjects;
			public List<GameObject> turnOffGameObjects;
			public List<GameObject> turnSwapGameObjects;
		
			public bool ApplyStep(float time)
			{
				timer -= time;
				if(!timer.IsAtMin())
					return false;
				StepActivated ();
				timer.Maximize();
				return true;
			}

			public void StepActivated ()
			{
				////Debug.Log("Step Has been activated");
				foreach(Behaviour behave in turnOnBehaviours)
				{
					////Debug.Log(behave.enabled);
					behave.enabled = true;
				}
				foreach(Behaviour behaved in turnOffBehaviours)
				{
					behaved.enabled = false;
				}
				foreach(Behaviour behaveSwap in turnSwapBehaviours)
				{
					behaveSwap.enabled = !behaveSwap.enabled;
				}
				foreach(GameObject gObj in turnOnGameObjects)
				{
					gObj.SetActive(true);
				}
				foreach(GameObject gObj in turnOffGameObjects)
				{
					gObj.SetActive(false);
				}
				foreach(GameObject gObj in turnSwapGameObjects)
				{
					gObj.SetActive(!gObj.activeSelf);
				}
			}
		}

	public class Sequence : MonoBehaviour 
	{
		public Step enableStep;
		public List<Step> steps;
		public Step disableStep;
		public bool isLooping;
		public int currentStep = 0;
		public bool resetOnEnable = true;
		public bool resetOnDisable = true;
		bool hasEnabled;
		public bool debug;
		
		void OnEnable()
		{
			if(resetOnEnable)
			{
				currentStep = 0;
				if(debug)
					//Debug.Log("Step Called " + transform.name);
				enableStep.StepActivated();
				foreach(Step step in steps)
				{
					step.timer.Maximize();
				}
			}
			hasEnabled = true;
		}
		
		void OnDisable()
		{
			if(debug)
				//Debug.Log("DisAble Step called");
			if(resetOnDisable)
			{
				disableStep.StepActivated();
			}
			hasEnabled = false;
		}

		void Update () 
		{
			if(debug && currentStep < steps.Count)
					//Debug.Log("Current Step: " + steps[currentStep].name);
			if(!hasEnabled)
				return;
			if(currentStep >= steps.Count)
			{
				if(isLooping)
				{
					currentStep = 0;
				}
				else
				{
					enabled = false;
				}
				return;
			}
			if(steps[currentStep].ApplyStep(Time.deltaTime))
			{
				currentStep ++;
			}
		}
		
		public void Completed()
		{
			if(currentStep < steps.Count)
			{
				steps[currentStep].StepActivated ();
				steps[currentStep].timer.Maximize();
			}
			currentStep ++;
		}
	}
}
