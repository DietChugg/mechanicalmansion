using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections;
using System.Collections.Generic;

public class ToDo : SingletonScriptableObject<ToDo> 
{
	public List<string> items = new List<string>();
#if UNITY_EDITOR
	[MenuItem ("ScriptableObject/ToDo")]
	static void DrawHierarchyDataMenuItem () 
	{
		Selection.activeObject = (UnityEngine.Object)ToDo.Instance;
	}
#endif
}
