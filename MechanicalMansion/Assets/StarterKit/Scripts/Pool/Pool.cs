using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Pool
{
	public GameObject prefab;
	public bool extendable = true;
    public bool disablesOnSceneChange = true;
	public int startingPrefabs = 1;
    [HideInInspector]
	public List<GameObject> fishes = new List<GameObject>();
    public List<GameObject> Fishes
    {
        get
        {
            for (int i = 0; i < fishes.Count; i++) 
            {
                if (fishes[i] == null) 
                {
                    fishes.RemoveAt(i);
                    i--;
                }
            }
            return fishes;
        }
        set
        {
            fishes = value;
        }
    }
	GameObject parent;

	public void Init(GameObject newParent)
	{
		fishes.Clear();
		parent = newParent;
		AddPrefabsToPool(startingPrefabs);
	}

	public void AddPrefabsToPool(int numberToAdd)
	{
		for(int i = 0; i < numberToAdd; i ++)
		{
			AddPrefabToPool();
		}
	}

	public GameObject AddPrefabToPool()
	{
		GameObject newObj = (GameObject)GameObject.Instantiate(prefab);
		newObj.SetActive(false);
		newObj.transform.parent = parent.transform;
		DontDestroy(newObj);
		fishes.Add(newObj);
		return newObj;
	}

	public void DontDestroy(GameObject me)
	{
		MonoBehaviour.DontDestroyOnLoad(me);
		foreach(Transform kid in me.transform)
		{
			DontDestroy(kid.gameObject);
		}
	}

	public GameObject Request(GameObject source) 
	{
		GameObject request = null;
		foreach(GameObject fish in Fishes)
		{
			if(request == null)
			{
				if(!fish.activeInHierarchy && fish.activeSelf)
				{
					fish.SetActive(false);
				}
				if(fish.activeSelf == false)
				{
					fish.transform.position = source.transform.position;
					fish.transform.rotation = source.transform.rotation;
					fish.SetActive(true);
					request = fish;
				}
			}
		}
		if(extendable && request == null)
		{
			request = AddPrefabToPool();
			request.transform.position = source.transform.position;
			request.transform.rotation = source.transform.rotation;
			request.SetActive(true);
		}
		return request;
	}

	public GameObject Request() 
	{
		GameObject request = null;
		foreach(GameObject fish in Fishes)
		{
			if(request == null)
			{
				if(!fish.activeInHierarchy && fish.activeSelf)
				{
					fish.SetActive(false);
				}
				if(fish.activeSelf == false)
				{
					fish.SetActive(true);
					request = fish;
				}
			}
		}
		if(extendable && request == null)
		{
			request = AddPrefabToPool();
			request.SetActive(true);
		}
		return request;
	}
}
