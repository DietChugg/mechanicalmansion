using UnityEngine;
using System.Collections;
using System.ComponentModel;
using System.Collections.Generic;
using StarterKit;

public class PoolManager : Singleton<PoolManager> 
{
	public static List<Pool> pools = new List<Pool>();
	public List<Pool> showPools = new List<Pool>();
	public static Transform myTransform;

	public PoolManager()
	{
        //Debug.Log("Pool Manager Called".RTAqua());
		pools.Clear();
		pools.AddRange(PoolStart.Instance.pools.ToArray());
	}

//	public void Init()
//	{
//		//Does Nothing but builds the PoolManager Instance
//	}

    [RuntimeInitializeOnLoadMethod]
    static void AutoLoad()
    {
        if (Instance != null)
        {
            //AutoLoads the Singleton 
        }
    }

	new public void OnEnable()
	{
        (this as MonoBehaviourPlus).OnEnable();
		myTransform = transform;
		foreach(Pool pool in pools)
		{
            if(pool.prefab != null)
			    SetUpPool(pool);
            else
                Debug.LogWarning("An invalid Pools is currently in the Pool editor. Go to Edit/ProjectSettings/Pool and make sure all Pools have a valid Prefab Assigned");
		}
//        ApplicationX.OnLevelLoading += OnLevelLoading;
	}
	
    new public void OnDisable()
    {
        (this as MonoBehaviourPlus).OnDisable();
//        ApplicationX.OnLevelLoading -= OnLevelLoading;
    }

    override public void OnLevelLoaded(LoadingLevelData lld)
    {
		StartCoroutine(EmptyPoolAtEndOfFrame());
    }

	IEnumerator EmptyPoolAtEndOfFrame()
	{
		yield return new WaitForEndOfFrame();
		foreach (Pool pool in pools)
		{
			foreach(GameObject fish in pool.Fishes)
			{
				fish.transform.parent = transform;

//				for (int i = 0; i < fish.Get; i++) 
//				{
//
//
//				}

				if(fish.activeInHierarchy)
					fish.SetActive(!pool.disablesOnSceneChange);
			}
		}
	}

//	public void Refresh(GameObject fish)
//	{
//		foreach(Transform kid in fish.transform)
//		{
//			Refresh()
//		}
//	}

	public void Update()
	{
		#if UNITY_EDITOR
		showPools = pools;
		#endif
	}

	public static GameObject Request(GameObject requestedPrefab) 
	{
        if(requestedPrefab == null)
            Debug.LogException(new System.NullReferenceException("Requesting Null Prefab Breaks The Pool Manager. Don't do that!"));
        PoolManager.Instance.Init();
		foreach(Pool pool in pools)
		{
			if(pool.prefab == requestedPrefab)
			{
				return pool.Request();
			}
		}
		Pool newPool = new Pool();
		newPool.prefab = requestedPrefab;
		SetUpPool(newPool);
		pools.Add(newPool);
		return newPool.Request();
	}

	public static GameObject Request(GameObject requestedPrefab, GameObject requestor) 
	{
        if(requestedPrefab == null)
            Debug.LogException(new System.NullReferenceException("Requesting Null Prefab Breaks The Pool Manager. Don't do that!"));
        if(requestor == null)
            Debug.LogException(new System.NullReferenceException("Null Requestor Gives new Prefab Nowhere to go. Don't do that!"));
        PoolManager.Instance.Init();
		foreach(Pool pool in pools)
		{
			if(pool.prefab == requestedPrefab)
			{
				return pool.Request(requestor);
			}
		}
		Pool newPool = new Pool();
		newPool.prefab = requestedPrefab;
		SetUpPool(newPool);
		pools.Add(newPool);
		return newPool.Request(requestor);
	}

	public static void SetUpPool(Pool pool)
	{
        PoolManager.Instance.Init();
		GameObject newObj = new GameObject(pool.prefab.name);
        DontDestroyOnLoad(newObj);
		newObj.transform.parent = myTransform;
		pool.Init(newObj);
	}
}
