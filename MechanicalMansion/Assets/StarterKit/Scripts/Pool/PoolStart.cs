using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PoolStart : SingletonScriptableObject<PoolStart> 
{
	public List<Pool> pools = new List<Pool>();
}
