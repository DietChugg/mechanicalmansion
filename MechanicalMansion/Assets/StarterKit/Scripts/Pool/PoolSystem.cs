using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PoolSystem : MonoBehaviour 
{
	public static int id = 0;
    public int prefabsToBuild;
    public bool extendable;
    public List<Transform> poolListTransforms = new List<Transform>();

    [ContextMenu("Generate PoolLists")]
    void GeneratePoolLists()
    {
        foreach(Transform t in transform)
        {
            GameObject.DestroyImmediate(t.gameObject);
        }
        foreach(Transform poolList in poolListTransforms)
        {
            GameObject poolObj = new GameObject(poolList.name);
            poolObj.transform.position = transform.position;
            poolObj.transform.rotation = transform.rotation;
            poolObj.transform.parent = transform;
            PoolList poolListObj = (PoolList)poolObj.AddComponent<PoolList>();
            poolListObj.prefabsToBuild = prefabsToBuild;
            poolListObj.extendable = extendable;
            poolListObj.prefab = poolList;
        }
    }
	
	void OnEnable()
	{
		if(id == 0)
		{
			id ++;
			GenerateObjects ();
		}
		else
		{
			Destroy(gameObject);
		}
	}

	void GenerateObjects ()
	{
		GameObject.DontDestroyOnLoad(gameObject);
		BuildPrefabs(transform);
	}
	
	void BuildPrefabs(Transform node)
	{
		PoolList poolList = node.GetComponent<PoolList>();
		if(poolList)
			poolList.BuildPrefabs();
		foreach(Transform kid in node)
			BuildPrefabs(kid);
	}
	
	void OnDisable()
	{
		//Messenger.RemoveListener("OnLevelExit",OnLeavingLevel);
	}
	
	/*void OnLeavingLevel()
	{
		#if !UNITY_EDITOR
		////Debug.Log("Come Back To Me My Kids!");
		foreach(Transform kid in transform)
		{
			foreach(Transform grandKid in kid)
			{
				grandKid.GetComponent<PoolList>().ParentPool();
			}
		}
		#endif
	}*/
}
