#if UNITY_EDITOR
using UnityEditor;
using System;

public class PoolStart_MenuItem
{
	[MenuItem ("Edit/Project Settings/Pools")]
	static void InputPlusObjectSelect () 
	{
		PoolStart.Instance.Init();
		foreach(Pool pool in PoolStart.Instance.pools)
		{
			pool.Fishes.Clear();
		}
		Selection.activeObject = (UnityEngine.Object)PoolStart.Instance;
	}
}
#endif
