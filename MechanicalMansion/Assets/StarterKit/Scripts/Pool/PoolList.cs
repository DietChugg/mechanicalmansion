using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PoolList : MonoBehaviour 
{
	public List<Transform> pool = new List<Transform>();
	public Transform prefab;
	public int prefabsToBuild;
	public bool extendable;
	
	[ContextMenu("Name Self After Prefab")]
	public void NameSelf()
	{
		if(prefab)
		{
			transform.name = prefab.name;
		}
	}
	
	public void OnEnable()
	{
//		Messenger<Transform>.AddListener("Request"+prefab.name,RequestPrefab);
	}
	
	public void OnDisable()
	{
//		Messenger<Transform>.RemoveListener("Request"+prefab.name,RequestPrefab);
	}
	
	public void RequestPrefab(Transform sender)
	{
        foreach(Transform kid in pool)
		{
            if(!kid.GetActiveSelf())
			{
				kid.position = sender.position;
				kid.rotation = sender.rotation;
				kid.SetActive(true);
//				Messenger<Transform>.Broadcast("Recieve"+prefab.name,kid,MessengerMode.DONT_REQUIRE_LISTENER);
				return;
			}
		}

		if(extendable)
		{
			Transform fab = (Transform.Instantiate(prefab,transform.position,transform.rotation) as Transform);
			fab.parent = transform;
			pool.Add(fab);
			fab.position = sender.position;
			fab.rotation = sender.rotation;
			fab.SetActive(true);
//			Messenger<Transform>.Broadcast("Recieve"+prefab.name,fab,MessengerMode.DONT_REQUIRE_LISTENER);
		}
		
	}
	
	public void CanDrop(string senderName)
	{
		bool canDrop = false;
		for(int i = 0; i< pool.Count; i++)
		{
			if(pool[i].GetActiveSelf())
			{
				canDrop = true;
				i = pool.Count;
			}
		}
//		Messenger<bool>.Broadcast("CanDrop"+senderName,canDrop);
		if(canDrop)
		{
			
		}
	}
	
	public void SendList(string reciever)
	{
//		Messenger<Transform[]>.Broadcast("RecieveList"+reciever,pool.ToArray(),MessengerMode.DONT_REQUIRE_LISTENER);

	}
	
	public void BuildPrefabs()
	{
		pool.Clear();
		if(prefab)
		{
			for(int i = 0; i < prefabsToBuild; i ++)
			{
				Transform fab = (Transform.Instantiate(prefab,transform.position,transform.rotation) as Transform);
				fab.parent = transform;
				fab.gameObject.SetActive(false);
				pool.Add(fab);
			}
		}
	}
	
	public void ParentPool()
	{
		foreach(Transform kid in pool)
		{
			kid.parent = transform;
		}
	}
}
