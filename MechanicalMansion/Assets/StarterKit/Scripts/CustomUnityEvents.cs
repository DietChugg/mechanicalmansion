using UnityEngine;
using UnityEngine.Events;
using System.Collections;

[System.Serializable]
public class UnityIntEvent : UnityEvent<int> { }
[System.Serializable]
public class UnityIntIntEvent : UnityEvent<int, int> { }
[System.Serializable]
public class UnityIntIntIntEvent : UnityEvent<int, int, int> { }

[System.Serializable]
public class UnityFloatEvent : UnityEvent<float> { }
[System.Serializable]
public class UnityFloatFloatEvent : UnityEvent<float, float> { }
[System.Serializable]
public class UnityFloatFloatFloatEvent : UnityEvent<float, float, float> { }

[System.Serializable]
public class UnityObjectEvent : UnityEvent<object> { }

