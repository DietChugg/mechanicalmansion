using UnityEngine;
using System.Collections;
using StarterKit;

namespace StarterKit
{
    [System.Serializable]
    public class IntRange
    {
        public int min = 0;
        public int max = 1;

        public IntRange()
        {

        }

        public IntRange(int _max)
        {
            min = 0;
            max = _max;
        }

        public IntRange(int _min, int _max)
        {
            min = _min;
            max = _max;
            IsInRange(3, true);
        }

        public int GetRandom()
        {
            return RandomX.Range(min, max);
        }

        /// <summary>
        /// Returns distance between min and max
        /// </summary>
        /// <returns>distance between min and max</returns>
        public int Length()
        {
            return min.DistanceTo(max);
        }

        /// <summary>
        /// returns true if the value is between min and max. 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="includeMinMax">min and max values will return false if value matches them as long as this is set to false</param>
        /// <returns></returns>
        public bool IsInRange(int value, bool includeMinMax)
        {
            if ((value == min || value == max) && includeMinMax)
                return true;
            if (value < max && value > min)
                return true;
            return false;
        }

        /// <summary>
        /// returns true if the value is betwen or is min and max
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool IsInRange(int value)
        {
            return IsInRange(value, true);
        }

    }
}