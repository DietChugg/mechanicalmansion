using UnityEngine;
using System.Collections;
using StarterKit;

[System.Serializable]
public class FloatClamp
{
	public float min = 0;
	public float current = 0;
	public float max = 1;
    public float Range
    {
        get 
        {
            return min.DistanceTo(max).Abs(); 
        }
    }
	
	public FloatClamp()
	{
		
	}
	
	public FloatClamp(float _max)
	{
		min = 0;
		current = 0;
		max = _max;
	}
	
	public FloatClamp(float _min, float _current, float _max)
	{
		min = _min;
		current = _current;
		max = _max;
	}
	
	public static implicit operator float(FloatClamp c)
	{
        return c.current;
	}

    public static FloatClamp operator +(FloatClamp newSelf, float amount)
    {
        newSelf.Adjust(amount);
        return newSelf;
    }
	
	public static FloatClamp operator -(FloatClamp newSelf, float amount)
	{
		newSelf.Adjust(-amount);
		return newSelf;
	}
	
    /// <summary>
    /// Returns what percent of current is to Max based off Min
    /// </summary>
    /// <returns></returns>
	public float PercentageFull()
	{
		if(min <= 0 && max <= 0 && current <= 0)
		{
			float absMin = Mathf.Abs(min);
			float absMax = Mathf.Abs(max);
			float absCurrent = Mathf.Abs(current);
			return((absCurrent + absMin)/(absMax + absMin));
		}
		if(min <= 0)
		{
			float bumpedUpCurrent = current + Mathf.Abs(min);
			float bumpedUpMax = max + Mathf.Abs(min);
			return bumpedUpCurrent/bumpedUpMax;
		}
		float bumpedDownCurrent = current - min;
		float bumpedDownMax = max - min;
		return bumpedDownCurrent/bumpedDownMax;
	}
	
    /// <summary>
    /// Adjusts current by an amount while clamping current between min and max (Both inclusively aka. current can equal min or max when done)
    /// </summary>
    /// <param name="amount"></param>
	public void Adjust(float amount)
	{
		current += amount;
		if(current > max)
			current = max;
		if(current < min)
			current = min;
	}
	
    /// <summary>
    /// Sets current to value while clamping current between min and max (Both inclusively aka. current can equal min or max when done)
    /// </summary>
    /// <param name="amount"></param>
	public void AdjustTo(float amount)
	{
		current = amount;
		if(current > max)
			current = max;
		if(current < min)
			current = min;
	}

    /// <summary>
    /// Sets current to value while clamping current between min and max (Both inclusively aka. current can equal min or max when done)
    /// </summary>
    /// <param name="amount"></param>
    public void Set(float amount)
    {
        current = amount;
        if (current > max)
            current = max;
        if (current < min)
            current = min;
    }

    /// <summary>
    /// Sets current to value of a percentage of the min and max values while clamping current between min and max (Both inclusively aka. current can equal min or max when done)
    /// </summary>
    /// <param name="amount"></param>
    public void AdjustToPercent(float amount)
    {
        amount = amount.Clamp01();
        current = ((max - min) * amount)+min;
        if(current > max)
            current = max;
        if(current < min)
            current = min;
    }

    /// <summary>
    /// Get a percentage at a given value based off the FloatClamp's min max values
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
	public float GetPercentAt(float value)
	{
		float result = 0f;
		float prevCurrent = current;
		AdjustTo(value);
		result = PercentageFull();
		current = prevCurrent;
		return result;
	}

    /// <summary>
    /// Get a value at a given percentage based off the FloatClamp's min max values
    /// </summary>
    /// <param name="percent"></param>
    /// <returns></returns>
	public float GetValueAt(float percent)
	{
		return ((max - min) * percent)+min;
	}
	
    /// <summary>
    /// returns true when current equals max
    /// </summary>
    /// <returns></returns>
	public bool IsAtMax()
	{
		return current == max;
	}
	
    /// <summary>
    /// returns true when current equals min
    /// </summary>
    /// <returns></returns>
	public bool IsAtMin()
	{
		return current == min;
	}
	
    /// <summary>
    /// Sets current to maximum value
    /// </summary>
	public void Maximize()
	{
		current = max;
	}
	
    /// <summary>
    /// Sets current to minimum value
    /// </summary>
	public void Minimize()
	{
		current = min;
	}
	
    /// <summary>
    /// Sets current to a random value within this FloatClamp's min and max range
    /// </summary>
	public void Randomize()
	{
		current = RandomX.Range(min,max);
	}
}
