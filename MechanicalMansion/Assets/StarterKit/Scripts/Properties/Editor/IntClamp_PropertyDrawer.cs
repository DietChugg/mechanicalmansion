////@CustomEditor (IntLimit)
//#if UNITY_EDITOR
//using System;
//using System.Collections;
//using UnityEditor;
//using UnityEngine;

////[CanEditMultipleObjects]
//[CustomPropertyDrawer (typeof(IntClamp))]
//public class IntClamp_PropertyDrawer : PropertyDrawer 
//{
//    public float heightOffset = 40f;
//    // Draw the property inside the given rect
//    public Rect positionSize = new Rect(0,0,20f,20f);
    
//    public SerializedProperty maxProp;
//    public SerializedProperty currentProp;
//    public SerializedProperty minProp;
	
//    public void OnEnable()
//    {
		
//    }
	
//    public override float GetPropertyHeight (SerializedProperty property, GUIContent label)
//    {
//        return heightOffset;
//    }
    
    
//    public override void OnGUI (Rect position, SerializedProperty property, GUIContent label) 
//    {
//        maxProp = property.FindPropertyRelative("max");
//        currentProp = property.FindPropertyRelative("current");
//        minProp = property.FindPropertyRelative("min");
		
//        // Using BeginProperty / EndProperty on the parent property means that
//        // prefab override logic works on the entire property.
//        if(Selection.transforms.Length > 1)
//        {
//            EditorGUI.BeginProperty (position, label, maxProp);
//                EditorGUI.showMixedValue = maxProp.hasMultipleDifferentValues;
//                EditorGUI.BeginChangeCheck();
//                int temMax = EditorGUI.IntField (new Rect(position.x + (position.width * .85f), position.y + 18f, 50f, 18f), property.FindPropertyRelative ("max").intValue);
//                if(EditorGUI.EndChangeCheck ()) 
//                {
//                    //Debug.Log("cHANGE 	Occured");
//                    property.FindPropertyRelative ("max").intValue = temMax;
//                }
//            EditorGUI.EndProperty ();
			
//            EditorGUI.BeginProperty (position, label, currentProp);
//                bool cannotEdit = maxProp.hasMultipleDifferentValues | minProp.hasMultipleDifferentValues;
//                if(!cannotEdit)
//                {
//                    EditorGUI.showMixedValue = currentProp.hasMultipleDifferentValues;
//                EditorGUI.BeginChangeCheck();
//                int tempCurrent = EditorGUI.IntSlider(new Rect(position.x, position.y, position.width, 18f), new GUIContent(property.name), property.FindPropertyRelative("current").intValue, property.FindPropertyRelative("min").intValue, property.FindPropertyRelative("max").intValue );
//                if(EditorGUI.EndChangeCheck ()) 
//                {
//                    //Debug.Log("cHANGE 	Occured");
//                    property.FindPropertyRelative("current").intValue = tempCurrent;
//                }
				
				
				
//                //property.FindPropertyRelative("current").intValue = EditorGUI.Slider(new Rect(position.x, position.y, position.width, 18f), new GUIContent(property.name), property.FindPropertyRelative("current").intValue, property.FindPropertyRelative("min").intValue, property.FindPropertyRelative("max").intValue );
//                }
//            EditorGUI.EndProperty ();
			
//            EditorGUI.BeginProperty (position, label, minProp);

//                EditorGUI.showMixedValue = minProp.hasMultipleDifferentValues;
//                EditorGUI.BeginChangeCheck();
//                int temMin = EditorGUI.IntField (new Rect(position.x + (position.width * .6f), position.y + 18f, 50f, 18f), property.FindPropertyRelative ("min").intValue);
//                if(EditorGUI.EndChangeCheck ()) 
//                {
//                    //Debug.Log("cHANGE 	Occured");
//                    property.FindPropertyRelative ("min").intValue = temMin;
//                }
			
//            EditorGUI.EndProperty ();
			
//                //int multiEditCur = EditorGUI.Slider(new Rect(position.x, position.y, position.width, 18f), new GUIContent(property.name), property.FindPropertyRelative("current").intValue, property.FindPropertyRelative("min").intValue, property.FindPropertyRelative("max").intValue );
		        
//                //property.FindPropertyRelative("current").intValue = //This is putting up the word min
//                EditorGUI.LabelField (new Rect(position.x + (position.width * .5f), position.y + 18f, position.width, 18f), "Min");
//                //This is where the min int adjustment is at
//                //property.FindPropertyRelative ("min").intValue = EditorGUI.IntField (new Rect(position.x + (position.width * .6f), position.y + 18f, 50f, 18f), property.FindPropertyRelative ("min").intValue);
//                //This is putting up the word max
//                EditorGUI.LabelField (new Rect(position.x + (position.width * .75f), position.y + 18f, position.width, 18f), "Max");
//                //This is where the min int adjustment is at
//                //property.FindPropertyRelative ("max").intValue = EditorGUI.IntField (new Rect(position.x + (position.width * .85f), position.y + 18f, 50f, 18f), property.FindPropertyRelative ("max").intValue);
//            return;
//        }
//        EditorGUI.BeginProperty (position, label, property);
//            //This is making the slider
		
//            property.FindPropertyRelative("current").intValue = EditorGUI.IntSlider(new Rect(position.x, position.y, position.width, 18f), new GUIContent(property.name), property.FindPropertyRelative("current").intValue, property.FindPropertyRelative("min").intValue, property.FindPropertyRelative("max").intValue );
//            //This is putting up the word min
//            EditorGUI.LabelField (new Rect(position.x + (position.width * .5f), position.y + 18f, position.width, 18f), "Min");
//            //This is where the min int adjustment is at
//            property.FindPropertyRelative ("min").intValue = EditorGUI.IntField (new Rect(position.x + (position.width * .6f), position.y + 18f, 50f, 18f), property.FindPropertyRelative ("min").intValue);
//            //This is putting up the word max
//            EditorGUI.LabelField (new Rect(position.x + (position.width * .75f), position.y + 18f, position.width, 18f), "Max");
//            //This is where the min int adjustment is at
//            property.FindPropertyRelative ("max").intValue = EditorGUI.IntField (new Rect(position.x + (position.width * .85f), position.y + 18f, 50f, 18f), property.FindPropertyRelative ("max").intValue);
//            //if(oldMax != property.FindPropertyRelative ("max").intValue)
//            //	oldMax = property.FindPropertyRelative ("max").intValue;
//            //if(oldMin != property.FindPropertyRelative ("min").intValue)
//            //	oldMin = property.FindPropertyRelative ("min").intValue;
//            //if(oldCurrent != property.FindPropertyRelative ("current").intValue)
//            //	oldCurrent = property.FindPropertyRelative ("current").intValue;
//        EditorGUI.EndProperty ();
//    }
//}
//#endif
