//@CustomEditor (FloatLimit)
#if UNITY_EDITOR
using System;
using System.Collections;
using UnityEditor;
using UnityEngine;

//[CanEditMultipleObjects]
[CustomPropertyDrawer (typeof(FloatRange))]
public class FloatRange_PropertyDrawer : PropertyDrawer 
{
    public float heightOffset = 20f;
    // Draw the property inside the given rect
    public Rect positionSize = new Rect(0,0,20f,20f);
    
	public SerializedProperty maxProp;
	public SerializedProperty minProp;
	
	public void OnEnable()
	{
		
	}
	
    public override float GetPropertyHeight (SerializedProperty property, GUIContent label)
    {
    	return heightOffset;
    }
    
    
    public override void OnGUI (Rect position, SerializedProperty property, GUIContent label) 
	{
        maxProp = property.FindPropertyRelative("max");
		minProp = property.FindPropertyRelative("min");
		
        if(Selection.transforms.Length > 1)
		{
			EditorGUI.BeginProperty (position, label, maxProp);
				EditorGUI.showMixedValue = maxProp.hasMultipleDifferentValues;
				EditorGUI.BeginChangeCheck();
                float temMax = EditorGUI.FloatField(new Rect(position.x + (position.width * .7f), position.y, (position.width * .1f), 18f), property.FindPropertyRelative("max").floatValue);
				if(EditorGUI.EndChangeCheck ()) 
				{
					property.FindPropertyRelative ("max").floatValue = temMax;
    			}
			EditorGUI.EndProperty ();
			
			EditorGUI.BeginProperty (position, label, minProp);

				EditorGUI.showMixedValue = minProp.hasMultipleDifferentValues;
				EditorGUI.BeginChangeCheck();
                float temMin = EditorGUI.FloatField(new Rect(position.x + (position.width * .5f), position.y, (position.width * .1f), 18f), property.FindPropertyRelative("min").floatValue);
				if(EditorGUI.EndChangeCheck ()) 
				{
					property.FindPropertyRelative ("min").floatValue = temMin;
    			}
			
			EditorGUI.EndProperty ();

            EditorGUI.LabelField(new Rect(position.x,
            position.y,
            position.width, 18f), new GUIContent(property.name));

		        EditorGUI.LabelField (new Rect(position.x + (position.width * .4f), position.y, position.width, 18f), "Min");

		        EditorGUI.LabelField (new Rect(position.x + (position.width * .6f), position.y, position.width, 18f), "Max");

			return;
		}
        EditorGUI.BeginProperty (position, label, property);

        //property.FindPropertyRelative("current").floatValue = EditorGUI.Slider(new Rect(position.x, position.y, position.width, 18f), new GUIContent(property.name), property.FindPropertyRelative("current").floatValue, property.FindPropertyRelative("min").floatValue, property.FindPropertyRelative("max").floatValue);

        EditorGUI.LabelField(new Rect(position.x, 
            position.y,
            position.width, 18f), new GUIContent(property.name));


	        EditorGUI.LabelField (new Rect(position.x + (position.width * .5f), position.y, position.width, 18f), "Min");
			property.FindPropertyRelative ("min").floatValue = EditorGUI.FloatField (new Rect(position.x + (position.width * .6f), position.y , 50f, 18f), property.FindPropertyRelative ("min").floatValue);
	        EditorGUI.LabelField (new Rect(position.x + (position.width * .75f), position.y, position.width, 18f), "Max");
			property.FindPropertyRelative ("max").floatValue = EditorGUI.FloatField (new Rect(position.x + (position.width * .85f), position.y, 50f, 18f), property.FindPropertyRelative ("max").floatValue);

		EditorGUI.EndProperty ();
    }
}
#endif
