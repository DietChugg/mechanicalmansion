//@CustomEditor (FloatLimit)
#if UNITY_EDITOR
using System;
using System.Collections;
using UnityEditor;
using UnityEngine;

//[CanEditMultipleObjects]
[CustomPropertyDrawer (typeof(FloatClamp))]
public class FloatClamp_PropertyDrawer : PropertyDrawer 
{
    public float heightOffset = 40f;
    // Draw the property inside the given rect
    public Rect positionSize = new Rect(0,0,20f,20f);
    
	public SerializedProperty maxProp;
	public SerializedProperty currentProp;
	public SerializedProperty minProp;
	
	public void OnEnable()
	{
		
	}
	
    public override float GetPropertyHeight (SerializedProperty property, GUIContent label)
    {
    	return heightOffset;
    }
    
    
    public override void OnGUI (Rect position, SerializedProperty property, GUIContent label) 
	{
        maxProp = property.FindPropertyRelative("max");
		currentProp = property.FindPropertyRelative("current");
		minProp = property.FindPropertyRelative("min");
		
		// Using BeginProperty / EndProperty on the parent property means that
        // prefab override logic works on the entire property.
        if(Selection.transforms.Length > 1)
		{
			EditorGUI.BeginProperty (position, label, maxProp);
				EditorGUI.showMixedValue = maxProp.hasMultipleDifferentValues;
				EditorGUI.BeginChangeCheck();
				float temMax = EditorGUI.FloatField (new Rect(position.x + (position.width * .85f), position.y + 18f, 50f, 18f), property.FindPropertyRelative ("max").floatValue);
				if(EditorGUI.EndChangeCheck ()) 
				{
					//Debug.Log("cHANGE 	Occured");
					property.FindPropertyRelative ("max").floatValue = temMax;
    			}
			EditorGUI.EndProperty ();
			
			EditorGUI.BeginProperty (position, label, currentProp);
				bool cannotEdit = maxProp.hasMultipleDifferentValues | minProp.hasMultipleDifferentValues;
				if(!cannotEdit)
				{
					EditorGUI.showMixedValue = currentProp.hasMultipleDifferentValues;
				EditorGUI.BeginChangeCheck();
				float tempCurrent = EditorGUI.Slider(new Rect(position.x, position.y, position.width, 18f), new GUIContent(property.name), property.FindPropertyRelative("current").floatValue, property.FindPropertyRelative("min").floatValue, property.FindPropertyRelative("max").floatValue );
				if(EditorGUI.EndChangeCheck ()) 
				{
					//Debug.Log("cHANGE 	Occured");
					property.FindPropertyRelative("current").floatValue = tempCurrent;
    			}
				
				
				
				//property.FindPropertyRelative("current").floatValue = EditorGUI.Slider(new Rect(position.x, position.y, position.width, 18f), new GUIContent(property.name), property.FindPropertyRelative("current").floatValue, property.FindPropertyRelative("min").floatValue, property.FindPropertyRelative("max").floatValue );
				}
			EditorGUI.EndProperty ();
			
			EditorGUI.BeginProperty (position, label, minProp);

				EditorGUI.showMixedValue = minProp.hasMultipleDifferentValues;
				EditorGUI.BeginChangeCheck();
				float temMin = EditorGUI.FloatField (new Rect(position.x + (position.width * .6f), position.y + 18f, 50f, 18f), property.FindPropertyRelative ("min").floatValue);
				if(EditorGUI.EndChangeCheck ()) 
				{
					//Debug.Log("cHANGE 	Occured");
					property.FindPropertyRelative ("min").floatValue = temMin;
    			}
			
			EditorGUI.EndProperty ();
			
				//float multiEditCur = EditorGUI.Slider(new Rect(position.x, position.y, position.width, 18f), new GUIContent(property.name), property.FindPropertyRelative("current").floatValue, property.FindPropertyRelative("min").floatValue, property.FindPropertyRelative("max").floatValue );
		        
				//property.FindPropertyRelative("current").floatValue = //This is putting up the word min
		        EditorGUI.LabelField (new Rect(position.x + (position.width * .5f), position.y + 18f, position.width, 18f), "Min");
		        //This is where the min float adjustment is at
				//property.FindPropertyRelative ("min").floatValue = EditorGUI.FloatField (new Rect(position.x + (position.width * .6f), position.y + 18f, 50f, 18f), property.FindPropertyRelative ("min").floatValue);
		        //This is putting up the word max
		        EditorGUI.LabelField (new Rect(position.x + (position.width * .75f), position.y + 18f, position.width, 18f), "Max");
		        //This is where the min float adjustment is at
				//property.FindPropertyRelative ("max").floatValue = EditorGUI.FloatField (new Rect(position.x + (position.width * .85f), position.y + 18f, 50f, 18f), property.FindPropertyRelative ("max").floatValue);
			return;
		}
        EditorGUI.BeginProperty (position, label, property);
	        //This is making the slider
		
	        property.FindPropertyRelative("current").floatValue = EditorGUI.Slider(new Rect(position.x, position.y, position.width, 18f), new GUIContent(property.name), property.FindPropertyRelative("current").floatValue, property.FindPropertyRelative("min").floatValue, property.FindPropertyRelative("max").floatValue );
	        //This is putting up the word min
	        EditorGUI.LabelField (new Rect(position.x + (position.width * .5f), position.y + 18f, position.width, 18f), "Min");
	        //This is where the min float adjustment is at
			property.FindPropertyRelative ("min").floatValue = EditorGUI.FloatField (new Rect(position.x + (position.width * .6f), position.y + 18f, 50f, 18f), property.FindPropertyRelative ("min").floatValue);
	        //This is putting up the word max
	        EditorGUI.LabelField (new Rect(position.x + (position.width * .75f), position.y + 18f, position.width, 18f), "Max");
	        //This is where the min float adjustment is at
			property.FindPropertyRelative ("max").floatValue = EditorGUI.FloatField (new Rect(position.x + (position.width * .85f), position.y + 18f, 50f, 18f), property.FindPropertyRelative ("max").floatValue);
        	//if(oldMax != property.FindPropertyRelative ("max").floatValue)
			//	oldMax = property.FindPropertyRelative ("max").floatValue;
			//if(oldMin != property.FindPropertyRelative ("min").floatValue)
			//	oldMin = property.FindPropertyRelative ("min").floatValue;
			//if(oldCurrent != property.FindPropertyRelative ("current").floatValue)
			//	oldCurrent = property.FindPropertyRelative ("current").floatValue;
		EditorGUI.EndProperty ();
    }
}
#endif
