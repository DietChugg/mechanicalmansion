using UnityEngine;
using System.Collections;
using StarterKit;

[System.Serializable]
public class IntClamp
{
	public int min = 0;
	public int current = 0;
	public int max = 1;
    public int Range
    {
        get 
        {
            return min.DistanceTo(max).Abs(); 
        }
    }
	
	public IntClamp()
	{
		
	}
	
	public IntClamp(int _max)
	{
		min = 0;
		current = 0;
		max = _max;
	}
	
	public IntClamp(int _min, int _current, int _max)
	{
		min = _min;
		current = _current;
		max = _max;
	}
	
	public static implicit operator int(IntClamp c)
	{
        return c.current;
	}

    public static IntClamp operator +(IntClamp newSelf, int amount)
    {
        newSelf.Adjust(amount);
        return newSelf;
    }
	
	public static IntClamp operator -(IntClamp newSelf, int amount)
	{
		newSelf.Adjust(-amount);
		return newSelf;
	}
	
    /// <summary>
    /// Returns what percent of current is to Max based off Min
    /// </summary>
    /// <returns></returns>
	public int PercentageFull()
	{
		if(min <= 0 && max <= 0 && current <= 0)
		{
			int absMin = Mathf.Abs(min);
			int absMax = Mathf.Abs(max);
			int absCurrent = Mathf.Abs(current);
			return((absCurrent + absMin)/(absMax + absMin));
		}
		if(min <= 0)
		{
			int bumpedUpCurrent = current + Mathf.Abs(min);
			int bumpedUpMax = max + Mathf.Abs(min);
			return bumpedUpCurrent/bumpedUpMax;
		}
		int bumpedDownCurrent = current - min;
		int bumpedDownMax = max - min;
		return bumpedDownCurrent/bumpedDownMax;
	}
	
    /// <summary>
    /// Adjusts current by an amount while clamping current between min and max (Both inclusively aka. current can equal min or max when done)
    /// </summary>
    /// <param name="amount"></param>
	public void Adjust(int amount)
	{
		current += amount;
		if(current > max)
			current = max;
		if(current < min)
			current = min;
	}
	
    /// <summary>
    /// Sets current to value while clamping current between min and max (Both inclusively aka. current can equal min or max when done)
    /// </summary>
    /// <param name="amount"></param>
	public void AdjustTo(int amount)
	{
		current = amount;
		if(current > max)
			current = max;
		if(current < min)
			current = min;
	}

    /// <summary>
    /// Sets current to value while clamping current between min and max (Both inclusively aka. current can equal min or max when done)
    /// </summary>
    /// <param name="amount"></param>
    public void Set(int amount)
    {
        current = amount;
        if (current > max)
            current = max;
        if (current < min)
            current = min;
    }

    /// <summary>
    /// Sets current to value of a percentage of the min and max values while clamping current between min and max (Both inclusively aka. current can equal min or max when done)
    /// </summary>
    /// <param name="amount"></param>
    public void AdjustToPercent(float amount)
    {
        amount = amount.Clamp01();
        current = (int)(((float)(max - min) * amount)+(float)min);
        if(current > max)
            current = max;
        if(current < min)
            current = min;
    }

    /// <summary>
    /// Get a percentage at a given value based off the IntClamp's min max values
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
	public int GetPercentAt(int value)
	{
		int result = 0;
		int prevCurrent = current;
		AdjustTo(value);
		result = PercentageFull();
		current = prevCurrent;
		return result;
	}

    /// <summary>
    /// Get a value at a given percentage based off the IntClamp's min max values
    /// </summary>
    /// <param name="percent"></param>
    /// <returns></returns>
	public int GetValueAt(int percent)
	{
		return ((max - min) * percent)+min;
	}
	
    /// <summary>
    /// returns true when current equals max
    /// </summary>
    /// <returns></returns>
	public bool IsAtMax()
	{
		return current == max;
	}
	
    /// <summary>
    /// returns true when current equals min
    /// </summary>
    /// <returns></returns>
	public bool IsAtMin()
	{
		return current == min;
	}
	
    /// <summary>
    /// Sets current to maximum value
    /// </summary>
	public void Maximize()
	{
		current = max;
	}
	
    /// <summary>
    /// Sets current to minimum value
    /// </summary>
	public void Minimize()
	{
		current = min;
	}
	
    /// <summary>
    /// Sets current to a random value within this IntClamp's min and max range
    /// </summary>
	public void Randomize()
	{
		current = RandomX.Range(min,max);
	}
}
