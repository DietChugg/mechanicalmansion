using UnityEngine;
using System.Collections;
using StarterKit;

[System.Serializable]
public class FloatRange
{
	public float min = 0;
	public float max = 1;
	
	public FloatRange()
	{
		
	}
	
	public FloatRange(float _max)
	{
		min = 0;
		max = _max;
	}
	
	public FloatRange(float _min, float _max)
	{
		min = _min;
		max = _max;
	}

    public float Length()
    {
        return min.DistanceTo(max);
    }

	public float GetRandom()
	{
		return RandomX.Range(min,max);
	}

	public bool IsInRange(float value, bool includeMinMax)
	{
		if((value == min || value == max ) && includeMinMax)
			return true;
		if(value < max && value > min)
			return true;
		return false;
	}

	public bool IsInRange(float value)
	{
		return IsInRange(value,true);
	}

}
