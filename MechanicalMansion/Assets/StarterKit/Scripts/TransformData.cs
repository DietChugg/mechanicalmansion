﻿using UnityEngine;
using System;
using System.Collections;

[Serializable]
public class TransformData
{
    public Vector3 position;
    public Quaternion rotation;
    public Vector3 scale;
    public Space defaultSpace;

    public void ApplyTo(Transform transform, Space space)
    {
        if (space == Space.World)
        {
            transform.position = position;
            transform.rotation = rotation;
            transform.SetLossyScale(scale);
        }
        else
        {
            transform.localPosition = position;
            transform.localRotation = rotation;
            transform.localScale = scale;
        }
    }

    public void SaveFrom(Transform transform)
    {
        SaveFrom(transform, defaultSpace);
    }

    public void SaveFrom(GameObject gameObject)
    {
        SaveFrom(gameObject.transform, defaultSpace);
    }

    public void SaveFrom(Transform transform, Space space)
    {
        if (space == Space.World)
        {
            position = transform.position;
            rotation = transform.rotation;
            scale = transform.lossyScale;
        }
        else
        {
            position = transform.localPosition;
            rotation = transform.localRotation;
            scale = transform.localScale;
        }
    }

    public void ApplyTo(Transform transform)
    {
        this.ApplyTo(transform, defaultSpace);
    }

    public void ApplyTo(GameObject gameObject)
    {
        this.ApplyTo(gameObject.transform, defaultSpace);
    }

    public void ApplyTo(GameObject gameObject, Space space)
    {
        this.ApplyTo(gameObject.transform, space);
    }
}
