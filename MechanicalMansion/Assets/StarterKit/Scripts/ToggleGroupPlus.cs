﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using StarterKit;

namespace UnityEngine.UI
{
    [AddComponentMenu("Toggle Group Plus", 100)]
    public class ToggleGroupPlus : UIBehaviour
    {
        public AudioClip audioClip;

        [Range(0,1f)]
        public float volume = 1f;
        [Range(0, 1f)]
        public float spatialBlend = 1f;

        public bool cutOffAudioOnSelection;
        AudioSource audioSource;
        [SerializeField]
        private bool m_AllowSwitchOff = false;
        public bool allowSwitchOff { get { return m_AllowSwitchOff; } set { m_AllowSwitchOff = value; } }
        public ToggleGroupEvent OnActiveToggleChange;

        private List<TogglePlus> m_Toggles = new List<TogglePlus>();

        protected ToggleGroupPlus()
        { }

        private void ValidateToggleIsInGroup(TogglePlus toggle)
        {
            if (toggle == null || !m_Toggles.Contains(toggle))
                throw new ArgumentException(string.Format("Toggle {0} is not part of ToggleGroup {1}", toggle, this));
        }

        public void NotifyToggleOn(TogglePlus toggle)
        {
            //Debug.Log("NotifyToggleOn");
            ValidateToggleIsInGroup(toggle);

            // disable all toggles in the group
            for (var i = 0; i < m_Toggles.Count; i++)
            {
                if (m_Toggles[i] == toggle)
                    continue;

                m_Toggles[i].isOn = false;
            }

            if (audioClip)
            {
                if(Application.isPlaying)
                {
                    if (audioSource && cutOffAudioOnSelection)
                    {
                        audioSource.Stop();
                        Destroy(audioSource.gameObject);
                    }
                    audioSource = AudioSourceX.PlayClipAtPoint(audioClip, volume);
                    audioSource.spatialBlend = spatialBlend;
                }
            }

            OnActiveToggleChange.Invoke(toggle);
        }

        public void UnregisterToggle(TogglePlus toggle)
        {
            if (m_Toggles.Contains(toggle))
                m_Toggles.Remove(toggle);
        }

        public void RegisterToggle(TogglePlus toggle)
        {
            if (!m_Toggles.Contains(toggle))
                m_Toggles.Add(toggle);
        }

        public bool AnyTogglesOn()
        {
            return m_Toggles.Find(x => x.isOn) != null;
        }

        public IEnumerable<TogglePlus> ActiveToggles()
        {
            return m_Toggles.Where(x => x.isOn);
        }

        public void SetAllTogglesOff()
        {
            bool oldAllowSwitchOff = m_AllowSwitchOff;
            m_AllowSwitchOff = true;

            for (var i = 0; i < m_Toggles.Count; i++)
                m_Toggles[i].isOn = false;

            m_AllowSwitchOff = oldAllowSwitchOff;
        }
    }

    [System.Serializable]
    public class ToggleGroupEvent : UnityEvent<TogglePlus> { }
}
