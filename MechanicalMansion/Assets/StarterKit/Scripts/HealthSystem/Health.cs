using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using StarterKit;

namespace StarterKit.LifeSystem
{
public class Health : MonoBehaviourPlus 
{
    public IntClamp health = new IntClamp(0,5,5);
    public bool enableWithFullHealth;
    bool canTakeDamage = true;
    public bool canCollectHealth = false;
    public bool destoryOnNoHealth = true;
    public bool disableOnNoHealth = true;
    public bool hasDied = false;
    public SafeAction<int, DamageInflictor> OnTakeDamage;
    public SafeAction<int> OnRecoveredHealth;
    public SafeAction OnDeath;
    public Animator animator;
    public bool invincible;
    public bool overrideRecoverTime;
    public float newRecoverTime;

    public bool onlyDropWhenAbovePlayer;
    public bool dropOnDeath;
    public GameObject[] drops;
    public bool useRendererCheck;
    public Renderer renderer;
    private bool canCollectHeart;

    public IEnumerator Start()
	{
		yield return null;
	}

    public void OnEnable()
    {
        if (enableWithFullHealth)
        {
            health.Maximize();
        }
        if (animator != null)
            animator.SetInteger("Health", this.health);
    }

    //public new void OnEnable ()
    //{
    //    (this as MonoBehaviourPlus).OnEnable();
    //    CharacterController2D cc2D = GetComponent<CharacterController2D>();
    //    if (cc2D)
    //    {
    //        cc2D.onEnter += OnEnter;
    //    }
    //}

    //void OnEnter(Character2DHit obj)
    //{
    //    if (obj == null || obj.transform == null)
    //        return;
    //    DamageInflictor damageInflictor = obj.transform.GetComponent<DamageInflictor>();
    //    if (damageInflictor != null)
    //    {
    //        if(damageInflictor.damageType == damageType || damageInflictor.damageType == PlayerType.All)
    //            TakeDamage(damageInflictor.damage, damageInflictor.recoverTime);
    //    }
    //}


    //public void OnCollisionEnter(Collision other)
    //{
    //    OnHitEnter(other.collider);
    //}

    public void OnCollisionEnter2D(Collision2D other)
    {
        if (renderer != null && (renderer.isVisible || !useRendererCheck))
            OnHitEnter2D(other.collider);
    }

    public void OnCollisionStay2D(Collision2D other)
    {
        if (renderer != null && (renderer.isVisible || !useRendererCheck))
            OnHitEnter2D(other.collider);
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (renderer != null && (renderer.isVisible || !useRendererCheck))
            OnHitEnter2D(other);
    }

    public void OnTriggerStay2D(Collider2D other)
    {
        if (renderer != null && (renderer.isVisible || !useRendererCheck))
            OnHitEnter2D(other);
    }

    public void OnTriggerExit2D(Collider2D other)
    {
        //OnHitEnter(other);
    }

    //public void OnParticleCollision(GameObject other)
    //{
    //    DamageInflictor damageInflictor = other.transform.GetComponent<DamageInflictor>();
    //    Shield shield = transform.GetComponent<Shield>();
    //    Health health = transform.GetComponent<Health>();
    //    if (health == null)
    //        return;
    //    int damageToDeal = damageInflictor.damage;
    //    //If the thing I am to damage has shields
    //    if (shield != null)
    //        damageToDeal = shield.TakeDamage(damageInflictor.damage);
    //    if (damageToDeal > 0)
    //    {
    //        health.TakeDamage(damageToDeal, damageInflictor.recoverTime);
    //        OnTakeDamage.Call(damageToDeal, damageInflictor);
    //    }
    //}

    public void RestoreAllHealth()
    {
        health.Maximize();
        if (animator != null)
        {
            animator.SetInteger("Health", this.health);
            animator.SetTrigger("Recovered");
        }
            

    }

    public void RestoreHealth(int restore)
    {
        health.Adjust(restore);
        if (animator != null)
        {
            animator.SetInteger("Health", this.health);
            animator.SetTrigger("Recovered");
        }
            
    }

    public void OnHitEnter(Collider other)
    {
        DamageCheck(other.gameObject);
        HeatlhCheck(other.transform.GetComponent<Heart>());
    }

    public void OnHitEnter2D(Collider2D other)
    {
        DamageCheck(other.gameObject);
        HeatlhCheck(other.transform.GetComponent<Heart>());
    }

    public void DamageCheck(GameObject other)
    {
        DamageInflictor damageInflictor = other.transform.GetComponent<DamageInflictor>();
        if (damageInflictor == null)
            return;
        
        if (damageInflictor.damage <= 0 || !canTakeDamage || invincible || hasDied || !damageInflictor.canDamage || !transform.CompareLayerMask(damageInflictor.hitMask))
        {
            damageInflictor.HitCompletion();
            return;
        }
        if (!damageInflictor.isDeathZone)
        {
            if (transform.name == "Main Player") 
                Debug.Log("DAMAGE: ".RTRed() + damageInflictor.damage);
            TakeDamage(damageInflictor.damage, damageInflictor.recoverTime);
        }
        else
        {
            if (transform.name == "Main Player") 
                Debug.Log("DEATH ZONE ENTERED".RTRed());
            TakeDamage(health.current + 1, damageInflictor.recoverTime);
            Stats.Instance().Health = 0;
        }
            

        if (transform.name == "Main Player") 
        {
            //Debug.Log(other.transform.name, other.transform);
            Stats.Instance().Health--;

            if (health.current != Stats.Instance().Health)
                health.current = Stats.Instance().Health;
            Debug.Log("Stats.Instance().Health: ".RTYellow() + Stats.Instance().Health);
            Debug.Log("Health Script Health: ".RTYellow() + health.current);

        }
        if(animator != null)
            animator.SetInteger("Health", this.health);

        if (!health.IsAtMin())
        {
            OnTakeDamage.Call(damageInflictor.damage, damageInflictor);
        }
        else
        {
            if (animator != null)
                animator.SetTrigger("Death");
            hasDied = true;
            if (dropOnDeath)
            {
                GameObject toDrop = drops.ToList().GetRandom();
                if (toDrop != null)
                {
                    if (onlyDropWhenAbovePlayer)
                    {
                        if (MainCharacterController.Instance.transform.position.y < transform.position.y)
                        {
                            toDrop.Instantiate(transform.position, transform.rotation);
                        }
                    }
                    else
                    {
                        toDrop.Instantiate(transform.position, transform.rotation);
                    }
                } 
            }
            
        }

        damageInflictor.HitCompletion();
        
        
    }

    public void Update()
    {
        canCollectHeart = true;
    }

    public void HeatlhCheck(Heart heart)
    {
        if (heart == null || !canCollectHealth || !canCollectHeart)
            return;
        canCollectHeart = false;
        health.Adjust(1);
        Stats.Instance().Health++;
        

        if (health.current != Stats.Instance().Health)
            health.current = Stats.Instance().Health;

        Debug.Log("Gained Health".RTGreen() + Stats.Instance().Health);

        Debug.Log("Stats.Instance().Health: ".RTYellow() + Stats.Instance().Health);
        Debug.Log("Health Script Health: ".RTYellow() + health.current);
        heart.collectedFX.Instantiate(heart.transform.position, heart.rotation);
        Destroy(heart.gameObject);
        OnRecoveredHealth.Call(1);
    }

    //public new void OnDisable () 
    //{
    //    (this as MonoBehaviourPlus).OnDisable();
    //}
	
	public void TakeDamage (int damage, float recoverTime) 
	{
        health -= damage;
        
        if (health.IsAtMin())
        {
            hasDied = true;
            OnDeath.Call();
        }

        if (health.IsAtMin() && disableOnNoHealth)
        {
            gameObject.SetActive(false);
        }

        if (health.IsAtMin() && destoryOnNoHealth)
            Destroy(gameObject);

        if (overrideRecoverTime)
            recoverTime = newRecoverTime;

        if (gameObject.activeSelf)
            StartCoroutine(OuchWait(recoverTime));
	}

    IEnumerator OuchWait(float ouchTime)
    {
        canTakeDamage = false;
        yield return new WaitForSeconds(ouchTime);
        canTakeDamage = true;
    }
}
}
