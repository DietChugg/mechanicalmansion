using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

namespace StarterKit.LifeSystem
{
    //[RequireComponent(typeof(Collider2D))]
    public class DamageInflictor : MonoBehaviourPlus 
    {
        public int damage;
        public float recoverTime;
        public Vector2 kickback;
        public bool canDamage = true;
        public LayerMask hitMask;
        public float lifeSpan = .35f;
        public AnimationCurve kickbackCurve;
        public AnimationCurve blendPlayerControls; 
        [Space(10f)]
        public bool overrideKickbackDirection;
        public bool isRightKickBack;
        public bool destoryAfterDamageSent;
        public GameObject destoryFX;
        public bool useRendererCheck = true;
        public Renderer renderer;
        public bool isDeathZone = false;

        //public PlayerType damageType;

        //CharacterController2D cc2D;
        //// Use this for initialization
        //void OnEnable()
        //{
        //    cc2D = GetComponent<CharacterController2D>();
        //    if (cc2D) 
        //        cc2D.onEnter += OnEnter;
        //}

        //void OnDisable()
        //{
        //    if (cc2D)
        //        cc2D.onEnter -= OnEnter;
        //}

        //// Update is called once per frame
        //public void OnEnter(Character2DHit other)
        //{
        //    //Debug.Log("Source Damage: " + transform.name);

        //    //Debug.Log("Damaging: " + other.transform.name);
        //    Health health = other.transform.GetComponent<Health>();
        //    Shield shield = other.transform.GetComponent<Shield>();
        //    if (health == null)
        //        return;
        //    int damageToDeal = damage;
        //    //If the thing I am to damage has shields
        //    if (shield != null)
        //        damageToDeal = shield.TakeDamage(damage);
        //    health.TakeDamage(damageToDeal, recoverTime);
        //}



        public void OnCollisionEnter2D(Collision2D other)
        {
            if (renderer != null && (renderer.isVisible || !useRendererCheck))
                OnTriggerEnter2D(other.collider);
        }

        public void OnCollisionStay2D(Collision2D other)
        {
            if (renderer != null && (renderer.isVisible || !useRendererCheck)) 
                OnTriggerStay2D(other.collider);
        }

        //public void OnTriggerEnter(Collider other)
        //{
        //    OnHitEnter(other);
        //}

        //public void OnTriggerEnter2D(Collider2D other)
        //{
        //    OnHitEnter2D(other);
        //}

        public void OnTriggerEnter2D(Collider2D other)
        {
            if (!enabled || (renderer != null && !renderer.isVisible && useRendererCheck))
                return;
            //Debug.Log(transform.name + " Detected Collision With: " + other.name);
            if (other.transform.GetComponent<Health>() != null)
            {
                //Debug.Log(transform.name + " Detected Collision With: " + other.name);
                other.transform.GetComponent<Health>().OnTriggerEnter2D(transform.GetComponent<Collider2D>());
            }
        }

        public void OnTriggerStay2D(Collider2D other)
        {
            if (!enabled || (renderer != null && !renderer.isVisible && useRendererCheck))
                return;
            //Debug.Log(transform.name + " Detected Collision With: " + other.name);
            if (other.transform.GetComponent<Health>() != null)
            {
                //Debug.Log(transform.name + " Detected Collision With: " + other.name);
                other.transform.GetComponent<Health>().OnTriggerStay2D(transform.GetComponent<Collider2D>());
            }
        }
           


        //public void OnHitEnter(Collider other)
        //{
        //    Shield shield = other.transform.GetComponent<Shield>();
        //    Health health = other.transform.GetComponent<Health>();
        //    if (health == null)
        //        return;
        //    int damageToDeal = damage;
        //    //If the thing I am to damage has shields
        //    if (shield != null)
        //        damageToDeal = shield.TakeDamage(damage);
        //    health.TakeDamage(damageToDeal, recoverTime);
        //}

        //public void OnHitEnter2D(Collider2D other)
        //{
        //    Shield shield = other.transform.GetComponent<Shield>();
        //    Health health = other.transform.GetComponent<Health>();
        //    if (health == null)
        //        return;
        //    int damageToDeal = damage;
        //    //If the thing I am to damage has shields
        //    if (shield != null)
        //        damageToDeal = shield.TakeDamage(damage);
        //    health.TakeDamage(damageToDeal, recoverTime);
        //}

        public void HitCompletion()
        {
            if (destoryAfterDamageSent)
            {
                if (destoryFX != null)
                    destoryFX.Instantiate(transform.position, transform.rotation);
                Destroy(gameObject);
            }
        }
    }

    public enum PlayerType
    { 
        None,
        All,
        Player,
        Enemy,
    }
}
