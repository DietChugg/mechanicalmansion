using UnityEngine;
using System.Collections;
using System.ComponentModel;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class SingletonPrefab<T> : ScriptableObject where T : ScriptableObject
{
	protected static T instance;
	public static T Instance
	{
        get
        {
            if(instance == null)
            {
                System.Type myType = typeof(T);
				UnityEngine.Object o = Resources.Load(myType.FullName,typeof(T));
                if(o != null)
                {
                    instance = (T)o;
                }
                else
                {
                    instance = CreateAsset();
                }
                //GameObject prefabTemplate = new GameObject(myType.FullName);
#if UNITY_EDITOR
                MonoScript script = MonoScript.FromScriptableObject(instance as ScriptableObject);

                string scriptPath = AssetDatabase.GetAssetPath(script);
                string resourcesFolderPath = scriptPath.Substring(0, scriptPath.Length - ("/" + myType.FullName + ".cs").Length);
#if UNITY_5
                if (!AssetDatabase.IsValidFolder(resourcesFolderPath + "/Resources"))
                {
                    AssetDatabase.CreateFolder(resourcesFolderPath, "Resources");
                }
#else
		        if(!System.IO.Directory.Exists(resourcesFolderPath + "/Resources"))
		        {
			        AssetDatabase.CreateFolder(resourcesFolderPath ,"Resources");
		        }
#endif
#endif
            }
            return instance;
        }
	}

    public GameObject prefab;

    protected GameObject _gameObject;
    public GameObject gameObject
    {
        get
        {
            if (_gameObject == null)
            {
                GameObject singletonFolder = GameObject.Find("_Singletons");
                if (singletonFolder == null)
                {
                    singletonFolder = new GameObject("_Singletons");
                    DontDestroyOnLoad(singletonFolder);
                }
                if (prefab == null)
                {
                    System.Type myType = typeof(T);
                    string resourcePath = myType.FullName + "Prefab";
                    if (Resources.Load(resourcePath) == null)
                    {
                        Debug.Log("Nothing at: " + resourcePath);    
                    }
                    prefab = Resources.Load(resourcePath, typeof(GameObject)) as GameObject;
                }

                _gameObject = (GameObject)GameObject.Instantiate(prefab) as GameObject;
                DontDestroyOnLoad(_gameObject);
                _gameObject.transform.parent = singletonFolder.transform;
            }
            return _gameObject;
        }
    }

    #if UNITY_EDITOR
    public void SetUpPrefab()
    {
        Debug.Log("SettingUpPrefab");
        System.Type myType = typeof(T);
        MonoScript script = MonoScript.FromScriptableObject(instance as ScriptableObject);
        string scriptPath = AssetDatabase.GetAssetPath(script);
        string resourcesFolderPath = scriptPath.Substring(0, scriptPath.Length - ("/" + myType.FullName + ".cs").Length);

        if (prefab == null)
        {
            string path = resourcesFolderPath + "/Resources/" + myType.FullName + ".prefab";
            //PrefabUtility.CreateEmptyPrefab(resourcesFolderPath + "/Resources/" + myType.FullName + ".prefab");
            GameObject newObject = new GameObject(myType.FullName);
            prefab = PrefabUtility.CreatePrefab(path, newObject);
            DestroyImmediate(newObject);
            //AssetDatabase.CreateAsset(prefab, resourcesFolderPath + "/Resources/");
            //AssetDatabase.SaveAssets();
        }
    }
    # endif

    public virtual void Init()
    {
    }


    public static T CreateAsset()
    {
        
        instance = CreateInstance<T>();//new InputPlus();
        #if UNITY_EDITOR
		System.Type myType = typeof(T);
         AssetDatabase.CreateAsset(instance, "Assets/"+myType.FullName+".asset");
		MonoScript script = MonoScript.FromScriptableObject(instance as ScriptableObject);

		string scriptPath = AssetDatabase.GetAssetPath(script);
		string resourcesFolderPath = scriptPath.Substring(0,scriptPath.Length - ("/" + myType.FullName + ".cs").Length);

#if UNITY_5
		if(!AssetDatabase.IsValidFolder(resourcesFolderPath + "/Resources"))
		{
			AssetDatabase.CreateFolder(resourcesFolderPath ,"Resources");
		}
#else
		if(!System.IO.Directory.Exists(resourcesFolderPath + "/Resources"))
		{
			AssetDatabase.CreateFolder(resourcesFolderPath ,"Resources");
		}

#endif
		AssetDatabase.MoveAsset("Assets/" + myType.FullName + ".asset",
		                        resourcesFolderPath + "/Resources/" + myType.FullName + ".asset");
        AssetDatabase.SaveAssets();
		AssetDatabase.Refresh();
        #endif
//        Debug.Log("There is a Scriptable Object in your Resources folder called "+ myType.FullName);
        return instance;
    }



	public static T CreateAssetWithName(string name)
	{
		
		instance = CreateInstance<T>();//new InputPlus();
		#if UNITY_EDITOR
		System.Type myType = typeof(T);
		//		MonoScript script = MonoScript.((myType as ScriptableObject));
		//		Debug.Log(AssetDatabase.GetAssetPath(script));
		AssetDatabase.CreateAsset(instance, "Assets/"+name+".asset");
		MonoScript script = MonoScript.FromScriptableObject(instance as ScriptableObject);
		
		
		//Build Proper Path
		
		
		string scriptPath = AssetDatabase.GetAssetPath(script);
		string resourcesFolderPath = scriptPath.Substring(0,scriptPath.Length - ("/" + myType.FullName + ".cs").Length);
		//		Debug.Log(resourcesFolderPath);
		
		#if UNITY_5
		if(!AssetDatabase.IsValidFolder(resourcesFolderPath + "/Resources"))
		{
			AssetDatabase.CreateFolder(resourcesFolderPath ,"Resources");
		}
		#else
		if(!System.IO.Directory.Exists(resourcesFolderPath + "/Resources"))
		{
			AssetDatabase.CreateFolder(resourcesFolderPath ,"Resources");
		}
		
		#endif
		AssetDatabase.MoveAsset("Assets/" + name + ".asset",
		                        resourcesFolderPath + "/Resources/" + name + ".asset");
		AssetDatabase.SaveAssets();
		AssetDatabase.Refresh();
		#endif
		//        Debug.Log("There is a Scriptable Object in your Resources folder called "+ myType.FullName);
		return instance;
	}
}


