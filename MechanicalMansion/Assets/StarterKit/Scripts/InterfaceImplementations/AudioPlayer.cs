﻿using UnityEngine;
using System.Collections;

public class AudioPlayer : LifetimeBehaviour, IPlayer 
{
    AudioSource audioSource;
    bool firstPlay = false;
    public bool stopsPlaying;

    new void Start()
    {
        audioSource = GetComponent<AudioSource>();
        if (audioSource == null)
            enabled = false;
        base.Start();
    }

    public bool IsPlaying
    {
        get
        {
            return audioSource.isPlaying;
        }
        set
        {
            if (value)
            {
                if (!audioSource.isPlaying)
                {
                    audioSource.Play();
                }
                    
            }
            else
            {
                if (audioSource.isPlaying)
                {
                    audioSource.Stop();
                }
            }
        }
    }

    public bool Loops
    {
        get
        {
            return audioSource.loop;
        }
        set
        {
             audioSource.loop = value;
        }
    }

    public event System.EventHandler OnCompleted;

    public void Play()
    {
        audioSource.Play();
        IsPlaying = true;
    }

    public void Stop()
    {
        audioSource.Stop();
        IsPlaying = false;
    }

    public void Pause()
    {
        audioSource.Pause();
    }

    public void Resume()
    {
        audioSource.UnPause();
    }

    public override void Begin()
    {
        if (!stopsPlaying)
            Play();
        else
            Stop();
    }
}
