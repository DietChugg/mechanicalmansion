﻿using UnityEngine;
using System.Collections;

public class AnimationPlayer : LifetimeBehaviour, IPlayer
{
    Animation _animation;
    [SerializePrivateVariables]
    string animationClipName;
    float pauseTime;
    WrapMode prevWrapMode;

    new void Start()
    {
        _animation = GetComponent<Animation>();
        if (!_animation)
        {
            enabled = false;
            return;        
        }
        prevWrapMode = _animation[animationClipName].wrapMode;
        base.Start();
    }

    public bool IsPlaying
    {
        get
        {
            return _animation.isPlaying;
        }
        set
        {
            if (value)
            {
                if (!_animation.isPlaying)
                    _animation.Play();
            }
            else
            {
                if (_animation.isPlaying)
                    _animation.Stop();
            }
        }
    }

    public bool Loops
    {
        get
        {
            return _animation[animationClipName].wrapMode == WrapMode.Loop;
        }
        set
        {
            if (value)
            {
                prevWrapMode = _animation[animationClipName].wrapMode;
                _animation[animationClipName].wrapMode = WrapMode.Loop;
            }
            else
            {

            }
            
        }
    }

    public event System.EventHandler OnCompleted;

    public void Play()
    {
        _animation.Play(animationClipName);
    }

    public void Stop()
    {
        _animation.Stop(animationClipName);
        pauseTime = 0f;
    }

    public void Pause()
    {
        pauseTime = _animation[animationClipName].time;
        _animation.Stop(animationClipName);
    }

    public void Resume()
    {
        _animation.Play(animationClipName);
        _animation[animationClipName].time = pauseTime;
    }

}
