﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Object = UnityEngine.Object;

public class ActivateIPlayers : LifetimeBehaviour 
{
    public float gapTime;
    public enum ActivationMethod
    {
        PickOneAtRandom,
        AllAtOnce,
        Staggered,

    }
    public ActivationMethod activationMethod;
    [Serializable]
    public class IPlayerData
    {
        public Object iPlayer;
        public float gap;
    }

    public List<IPlayerData> iPlayerDatas = new List<IPlayerData>();
    

    public override void Begin()
    {
        Debug.Log("Begin");
        IPlayer player;
        switch (activationMethod)
        {
            case ActivationMethod.PickOneAtRandom:
                player = iPlayerDatas.GetRandom().iPlayer as IPlayer;
                if (player != null)
                    player.Play();
                break;
            case ActivationMethod.AllAtOnce:
                for (int i = 0; i < iPlayerDatas.Count; i++)
                {
                    player = iPlayerDatas[i].iPlayer as IPlayer;
                    if (player != null)
                        player.Play();
                }
                break;
            case ActivationMethod.Staggered:
                StartCoroutine(PlayStaggered());
                break;
            default:
                break;
        }
    }

    [ContextMenu("Play")]
    public void Play()
    {
        Begin();
    }

    [ContextMenu("Pause")]
    public void Pause()
    { 
        
    }

    [ContextMenu("Stop")]
    public void Stop()
    {

    }

    [ContextMenu("Resume")]
    public void Resume()
    {

    }

    IEnumerator PlayStaggered()
    {
        IPlayer player;
        for (int i = 0; i < iPlayerDatas.Count; i++)
        {
            player = iPlayerDatas[i].iPlayer as IPlayer;
            if (player != null)
            {
                player.Play();
                yield return new WaitForSeconds(gapTime + iPlayerDatas[i].gap);
            }
        }
        yield return null;
    }
}
