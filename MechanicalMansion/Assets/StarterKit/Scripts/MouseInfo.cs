using UnityEngine;
using System.Collections;
using StarterKit;

public class MouseInfo : MonoBehaviourPlus 
{
	public Vector2 mousePositionDownScreenSpace;
	public Vector2 mousePositionScreenSpace;
	public Vector2 mousePositionUpScreenSpace;
	public Vector3 mousePositionWorldSpace;
	public bool mousePresent;
	public bool mouse0Down;
	public bool mouse0Held;
	public bool mouse0Up;
	public Bounds clickBounds;
	public Vector3 mouse0DownPoint;
	public Vector3 mouse0HeldPoint;

//	public bool mouse1Down;
//	public bool mouse1Held;
//	public bool mouse1Up;
//
//	public bool mouse2Down;
//	public bool mouse2Held;
//	public bool mouse2Up;

	void Update () 
	{
		mousePositionScreenSpace = Input.mousePosition;
		mousePositionWorldSpace = GetComponent<Camera>().ScreenToWorldPoint(Input.mousePosition);
		mousePresent = Input.mousePresent;
		mouse0Down = Input.GetMouseButtonDown(0);
		mouse0Held = Input.GetMouseButton(0);
		mouse0Up = Input.GetMouseButtonUp(0);

		if(mouse0Down)
		{
			mousePositionDownScreenSpace = Input.mousePosition;
			mouse0DownPoint = mousePositionWorldSpace;
		}
		if(Input.GetMouseButton(0))
		{
			mouse0HeldPoint = mousePositionWorldSpace;
		}

		if(Input.GetMouseButtonUp(0))
		{
			mouse0DownPoint = Vector3.zero;
			mouse0HeldPoint = Vector3.zero;
			mousePositionUpScreenSpace = Input.mousePosition;
		}
		clickBounds = BoundsX.Bounds(mouse0DownPoint,mouse0HeldPoint);
//		transform.Add
	}

	void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.red;
		if(mouse0Held)
		{
			Gizmos.DrawLine(clickBounds.min,clickBounds.max);
			Gizmos.DrawWireSphere(clickBounds.center,3);
			Gizmos.DrawWireSphere(clickBounds.min,1);
			Gizmos.DrawWireSphere(clickBounds.max,1);
//			Gizmos.DrawLine(clickBounds.max);
		}

	}
}
