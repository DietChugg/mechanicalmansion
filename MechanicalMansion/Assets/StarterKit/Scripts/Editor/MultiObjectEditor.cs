#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System;
using System.Reflection;

public static class MultiObjectEditor 
{
	//Overload for Basic Types (String, Int(non-slider), float(non-slider), Color, etc)
	public static void Property(SerializedProperty serializedOBJ, string name, params GUILayoutOption[] options)
	{
		Property(serializedOBJ, name, Color.black, false, 0f, 0f, options);
	}

	//Overload for AnimationCurves
	public static void Property(SerializedProperty serializedOBJ, string name, Color animCurveColor, params GUILayoutOption[] options)
	{
		Property(serializedOBJ, name, animCurveColor, false, 0f, 0f, options);
	}

	//Overload for Ints and floats with sliders
	public static void Property(SerializedProperty serializedOBJ, string name, bool isSlider, float sliderMin, float sliderMax, params GUILayoutOption[] options)
	{
		Property(serializedOBJ, name, Color.black, isSlider, sliderMin, sliderMax, options);
	}

	public static void Property(SerializedProperty serializedOBJ, string name, Color animCurveColor, bool isSlider, float sliderMin, float sliderMax, params GUILayoutOption[] options)
	{
		EditorGUI.showMixedValue = serializedOBJ.hasMultipleDifferentValues;
		EditorGUI.BeginChangeCheck();
		
		switch(serializedOBJ.type)
		{
		case "bool":
			bool boolChange = EditorGUILayout.Toggle(name, serializedOBJ.boolValue, options);
			if(EditorGUI.EndChangeCheck())
				serializedOBJ.boolValue = boolChange;
			break;
		case "int":
			if(!isSlider)
			{
				int intChange = EditorGUILayout.IntField(name, serializedOBJ.intValue, options);
				if(EditorGUI.EndChangeCheck())
					serializedOBJ.intValue = intChange;
			}
			else
			{
				int intChange = EditorGUILayout.IntSlider(name, serializedOBJ.intValue, (int)sliderMin, (int)sliderMax, options);
				if(EditorGUI.EndChangeCheck())
					serializedOBJ.intValue = intChange;
			}
			break;
		case "float":
			if(!isSlider)
			{
				float floatChange = EditorGUILayout.FloatField(name, serializedOBJ.floatValue, options);
				if(EditorGUI.EndChangeCheck())
					serializedOBJ.floatValue = floatChange;
			}
			else
			{
				float floatChange = EditorGUILayout.Slider(name, serializedOBJ.floatValue, sliderMin, sliderMax, options);
				if(EditorGUI.EndChangeCheck())
					serializedOBJ.floatValue = floatChange;
			}
			break;
		case "string":
			string stringChange = EditorGUILayout.TextField(name, serializedOBJ.stringValue, options);
			if(EditorGUI.EndChangeCheck())
				serializedOBJ.stringValue = stringChange;
			break;
		case "Color":
			Color colorChange = EditorGUILayout.ColorField(name, serializedOBJ.colorValue, options);
			if(EditorGUI.EndChangeCheck())
				serializedOBJ.colorValue = colorChange;
			break;
		case "AnimationCurve":
			AnimationCurve animationCurveChange = EditorGUILayout.CurveField(name, serializedOBJ.animationCurveValue, animCurveColor, new Rect(0,0,0,0), options);
			if(EditorGUI.EndChangeCheck())
				serializedOBJ.animationCurveValue = animationCurveChange;
			break;
		case "Enum":
			EditorGUILayout.PropertyField(serializedOBJ, options);
			EditorGUI.EndChangeCheck();
			break;
		case "Gradient":
			EditorGUILayout.PropertyField(serializedOBJ, options);
			EditorGUI.EndChangeCheck();
			break;
		case "Vector3":
			Vector3 vector3Change = EditorGUILayout.Vector3Field(name,serializedOBJ.vector3Value, options);
			if(EditorGUI.EndChangeCheck())
				serializedOBJ.vector3Value = vector3Change;
			break;
		default:
			EditorGUILayout.PropertyField(serializedOBJ, options);
			EditorGUI.EndChangeCheck();
			EditorGUILayout.HelpBox(serializedOBJ.name + " is rendering Default", MessageType.Warning);
			break;
		}
	}

	public static void Button(SerializedObject serializedOBJ, string name, Type type, string method, params GUILayoutOption[] options)
	{
		if(GUILayout.Button(name, options))
		{
			for (int i = 0; i < serializedOBJ.targetObjects.Length; i++) 
			{
				MethodInfo[] methodInfo = type.GetMethods();
				
				foreach (MethodInfo info in methodInfo)
				{
					if (info.Name == method && info.GetParameters().Length < 1)
					{
						info.Invoke(serializedOBJ.targetObjects[i], null);
					}
				}
			}
		}
	}

}
#endif
