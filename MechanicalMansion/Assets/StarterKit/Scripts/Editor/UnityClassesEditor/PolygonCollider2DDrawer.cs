//#if UNITY_EDITOR
//using UnityEngine;
//using System;
//using System.Collections;
//using System.Collections.Generic;
//using UnityEditor;
//
//[CanEditMultipleObjects]
//[CustomEditor(typeof(PolygonCollider2D))]
//public class PolygonCollider2DDrawer : Editor
//{
//
//	public override void OnInspectorGUI ()
//	{
//		if(targets.Length > 1)
//		{
//			DrawDefaultInspector();
//			return;
//		}
//		PolygonCollider2D polygonCollider2D = (target as PolygonCollider2D);
//		if(polygonCollider2D.GetComponent<Rigidbody2D>() == null)
//		{
//			EditorGUILayout.BeginHorizontal();
//			if(GUILayout.Button(AssetPreview.GetMiniTypeThumbnail(typeof(Rigidbody2D)) as Texture2D,GUILayout.MaxHeight(20)))
//				polygonCollider2D.gameObject.AddComponent<Rigidbody2D>();
//
//			EditorGUILayout.EndHorizontal();
//		}
//		DrawDefaultInspector();
//	}
//}
//#endif
