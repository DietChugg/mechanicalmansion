//#if UNITY_EDITOR
//using UnityEngine;
//using System;
//using System.Collections;
//using System.Collections.Generic;
//using UnityEditor;
//
//[CanEditMultipleObjects]
//[CustomEditor(typeof(Rigidbody2D))]
//public class Rigidbody2DDrawer : Editor
//{
//
//	public override void OnInspectorGUI ()
//	{
//		if(targets.Length > 1)
//		{
//			DrawDefaultInspector();
//			return;
//		}
//		Rigidbody2D rigidbody2D = (target as Rigidbody2D);
//		if(rigidbody2D.GetComponent<Collider2D>() == null)
//		{
//			EditorGUILayout.BeginHorizontal();
//			if(GUILayout.Button(AssetPreview.GetMiniTypeThumbnail(typeof(BoxCollider2D)) as Texture2D,GUILayout.MaxHeight(20)))
//				rigidbody2D.gameObject.AddComponent<BoxCollider2D>();
//
//			if(GUILayout.Button(AssetPreview.GetMiniTypeThumbnail(typeof(CircleCollider2D)) as Texture2D,GUILayout.MaxHeight(20)))
//				rigidbody2D.gameObject.AddComponent<CircleCollider2D>();
//
//			if(GUILayout.Button(AssetPreview.GetMiniTypeThumbnail(typeof(PolygonCollider2D)) as Texture2D,GUILayout.MaxHeight(20)))
//				rigidbody2D.gameObject.AddComponent<PolygonCollider2D>();
//
//			if(GUILayout.Button(AssetPreview.GetMiniTypeThumbnail(typeof(EdgeCollider2D)) as Texture2D,GUILayout.MaxHeight(20)))
//				rigidbody2D.gameObject.AddComponent<EdgeCollider2D>();
//
//			EditorGUILayout.EndHorizontal();
//		}
//		DrawDefaultInspector();
//	}
//}
//#endif
