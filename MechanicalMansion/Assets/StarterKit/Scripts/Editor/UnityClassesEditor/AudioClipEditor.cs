//#if UNITY_EDITOR && UNITY_4_6
//using UnityEngine;
//using UnityEditor;
//using UnityEditorInternal;
//using System;
//using System.Collections;
//using System.Collections.Generic;
//
//namespace StarterKit
//{
//
//	[CustomEditor(typeof(AudioClip)),CanEditMultipleObjects]
//	public class AudioClipEditor : Editor
//	{
//
//		public static bool isLocal = true;
//
//		public static SerializedProperty audioFormat;
//		public static SerializedProperty audioType;
//		public static SerializedProperty is3D;
//		public static SerializedProperty loadType;
//		public static SerializedProperty useHardware;
//		public static SerializedProperty compressionSize;
//
//
//		AudioImporterFormat format;
//		bool isSelected3D;
//		bool isSelectedForceMono;
//		bool isSelectedHardwareDecoding;
//		bool isSelectedGaplessLooping;
//		AudioImporterLoadType selectedLoadType = AudioImporterLoadType.CompressedInMemory;
//		int selectedCompression;
//
//
//		public void OnEnable()
//		{
//			if(Selection.objects.Length <= 1)
//				return;
//			foreach (SerializedProperty prop in serializedObject.GetIterator())
//			{
//				Debug.Log(prop.name);
//				Debug.Log(prop.type.RTBlue());
//			}
//			audioFormat = serializedObject.FindProperty("m_Format");
//			audioType = serializedObject.FindProperty("m_Type");
//			useHardware = serializedObject.FindProperty("m_UseHardware");
//			is3D = serializedObject.FindProperty("m_3D");
//			compressionSize = serializedObject.FindProperty("m_AudioData");//.FindPropertyRelative("size");
//			Debug.Log(compressionSize.type);
////			selectedCompression = compressionSize.intValue;
//			format = (AudioImporterFormat)Enum.ToObject(typeof(AudioImporterFormat),audioFormat.intValue);
//			selectedLoadType = (AudioImporterLoadType)Enum.ToObject(typeof(AudioImporterLoadType),audioType.intValue);
//			isSelected3D = is3D.boolValue;
//		}
//
//
//		public override void OnInspectorGUI ()
//		{
//			if(Selection.objects.Length <= 1)
//				return;
//			GUI.enabled = true;
//			EditorGUILayout.LabelField("Adding Multi Clip Editing");
////			AudioImporterFormat audioFormat = AudioImporterFormat.Native;
//			format = (AudioImporterFormat)EditorGUILayout.EnumPopup("Audio Format",(Enum)format);
//			isSelected3D = EditorGUILayout.Toggle("3D Sound",isSelected3D);
//			isSelectedForceMono = EditorGUILayout.Toggle("Force to mono",isSelectedForceMono);
//
//			selectedLoadType = (AudioImporterLoadType)EditorGUILayout.EnumPopup("LoadType",(Enum)selectedLoadType);
//
//			isSelectedHardwareDecoding = EditorGUILayout.Toggle("Hardware Decoding",isSelectedHardwareDecoding);
//			isSelectedGaplessLooping = EditorGUILayout.Toggle("Gapless looping",isSelectedGaplessLooping);
//			selectedCompression = EditorGUILayout.IntSlider("Compression (kbps)",selectedCompression,45,500);
//
//
//			EditorGUILayout.BeginHorizontal();
//			EditorGUILayout.Space();
//			if(GUILayout.Button("Apply"))
//			{
//				ChangeAudioImportSettings.SelectedToggle3DSoundSettings(isSelected3D);
//				ChangeAudioImportSettings.SelectedToggleForceToMonoSettings(isSelectedForceMono);
//				ChangeAudioImportSettings.enableHardwareDecoding(isSelectedHardwareDecoding);
//				ChangeAudioImportSettings.SelectedToggleCompressionSettings(format);
//				ChangeAudioImportSettings.SelectedToggleDecompressOnLoadSettings(selectedLoadType);
//				ChangeAudioImportSettings.SelectedSetCompressionBitrate(selectedCompression*1000f);
////				serializedObject.ApplyModifiedProperties();
//			}
//			EditorGUILayout.EndHorizontal();
//		}
//
//
//
//
//	}
//}
//#endif
