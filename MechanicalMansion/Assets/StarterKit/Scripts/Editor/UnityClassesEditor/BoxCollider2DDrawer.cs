//#if UNITY_EDITOR
//using UnityEngine;
//using System;
//using System.Collections;
//using System.Collections.Generic;
//using UnityEditor;
//
//[CanEditMultipleObjects]
//[CustomEditor(typeof(BoxCollider2D))]
//public class BoxCollider2DDrawer : Editor
//{
//
//	public override void OnInspectorGUI ()
//	{
//		if(targets.Length > 1)
//		{
//			DrawDefaultInspector();
//			return;
//		}
//		BoxCollider2D boxCollider2D = (target as BoxCollider2D);
//		if(boxCollider2D.GetComponent<Rigidbody2D>() == null)
//		{
//			EditorGUILayout.BeginHorizontal();
//			if(GUILayout.Button(AssetPreview.GetMiniTypeThumbnail(typeof(Rigidbody2D)) as Texture2D,GUILayout.MaxHeight(20)))
//				boxCollider2D.gameObject.AddComponent<Rigidbody2D>();
//
//			EditorGUILayout.EndHorizontal();
//		}
//		DrawDefaultInspector();
//	}
//}
//#endif
