//#if UNITY_EDITOR
//using UnityEngine;
//using System;
//using System.Collections;
//using System.Collections.Generic;
//using UnityEditor;
//
//[CanEditMultipleObjects]
//[CustomEditor(typeof(EdgeCollider2D))]
//public class EdgeCollider2DDrawer : Editor
//{
//
//	public override void OnInspectorGUI ()
//	{
//		if(targets.Length > 1)
//		{
//			DrawDefaultInspector();
//			return;
//		}
//		EdgeCollider2D edgeCollider2D = (target as EdgeCollider2D);
//		if(edgeCollider2D.GetComponent<Rigidbody2D>() == null)
//		{
//			EditorGUILayout.BeginHorizontal();
//			if(GUILayout.Button(AssetPreview.GetMiniTypeThumbnail(typeof(Rigidbody2D)) as Texture2D,GUILayout.MaxHeight(20)))
//				edgeCollider2D.gameObject.AddComponent<Rigidbody2D>();
//
//			EditorGUILayout.EndHorizontal();
//		}
//		DrawDefaultInspector();
//	}
//}
//#endif
