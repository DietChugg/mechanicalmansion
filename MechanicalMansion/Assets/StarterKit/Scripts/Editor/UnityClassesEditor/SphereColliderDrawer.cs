//#if UNITY_EDITOR
//using UnityEngine;
//using System;
//using System.Collections;
//using System.Collections.Generic;
//using UnityEditor;
//
//[CanEditMultipleObjects]
//[CustomEditor(typeof(SphereCollider))]
//public class SphereColliderDrawer : Editor
//{
//
//	public override void OnInspectorGUI ()
//	{
//		if(targets.Length > 1)
//		{
//			DrawDefaultInspector();
//			return;
//		}
//		SphereCollider sphereCollider = (target as SphereCollider);
//		if(sphereCollider.GetComponent<Rigidbody>() == null)
//		{
//			EditorGUILayout.BeginHorizontal();
//			if(GUILayout.Button(AssetPreview.GetMiniTypeThumbnail(typeof(Rigidbody)) as Texture2D,GUILayout.MaxHeight(20)))
//				sphereCollider.gameObject.AddComponent<Rigidbody>();
//
//			EditorGUILayout.EndHorizontal();
//		}
//		DrawDefaultInspector();
//	}
//}
//#endif
