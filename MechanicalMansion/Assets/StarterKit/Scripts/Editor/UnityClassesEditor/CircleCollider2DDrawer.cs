#if UNITY_EDITOR
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

[CanEditMultipleObjects]
[CustomEditor(typeof(CircleCollider2D))]
public class CircleCollider2DDrawer : Editor
{

	public override void OnInspectorGUI ()
	{
		if(targets.Length > 1)
		{
			DrawDefaultInspector();
			return;
		}
		CircleCollider2D circleCollider2D = (target as CircleCollider2D);
		if(circleCollider2D.GetComponent<Rigidbody2D>() == null)
		{
			EditorGUILayout.BeginHorizontal();
			if(GUILayout.Button(AssetPreview.GetMiniTypeThumbnail(typeof(Rigidbody2D)) as Texture2D,GUILayout.MaxHeight(20)))
				circleCollider2D.gameObject.AddComponent<Rigidbody2D>();

			EditorGUILayout.EndHorizontal();
		}
		DrawDefaultInspector();
	}
}
#endif
