#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using System;
using System.Collections;
using System.Collections.Generic;

namespace StarterKit
{

[CustomEditor(typeof(Transform))]
public class TransformEditor : Editor
{

	public static bool isLocal = true;


//		SerializedProperty localPosition;
//
//		public void OnEnable()
//		{
//			localPosition = serializedObject.FindProperty("m_LocalPosition");
//		}



	public override void OnInspectorGUI ()
	{
//		if(Selection.transforms.Length > 1)
//		{
//				EditorGUILayout.BeginHorizontal();
//
////				foreach (SerializedProperty prop in serializedObject.GetIterator())
////				{
////					Debug.Log(prop.name);
////				}
////				return;
//
//				SerializedProperty posX = localPosition.FindPropertyRelative("x");
//				EditorGUI.showMixedValue = posX.hasMultipleDifferentValues;
//				EditorGUI.BeginChangeCheck();
//				posX.floatValue = EditorGUILayout.FloatField("X",posX.floatValue);
//
//
//				EditorGUILayout.Toggle(posX.);
//
////				float y = EditorGUILayout.FloatField("Y",localPosition.vector3Value.y);
////				float z = EditorGUILayout.FloatField("Z",localPosition.vector3Value.z);
//
//				if(EditorGUI.EndChangeCheck())
//				{
//					Debug.Log("Value Changed...");
//					serializedObject.ApplyModifiedProperties();
//				}
//				EditorGUILayout.EndHorizontal();
//
//				return;
//		}



		
		Transform trans = target as Transform;
		Undo.RecordObject(trans, "Transform Changes");
        EditorGUIUtility.labelWidth = 15f;

		Vector3 pos;
		Vector3 rot;
		Vector3 scale;

		EditorGUILayout.BeginHorizontal();


		if(GUILayout.Button("Swap Space Mode"))
		{
			isLocal = !isLocal;
		}
		if(isLocal)
			GUILayout.Label("Space Mode: Local");
		else
			GUILayout.Label("Space Mode: Global");
		EditorGUILayout.Space();
		EditorGUILayout.EndHorizontal();
		GUILayout.Space(5f);
		// Position
		EditorGUILayout.BeginHorizontal();
		{
			if (DrawButton("P", "Reset Position", IsResetPositionValid(trans), 20f))
			{
				if(isLocal)
					trans.localPosition = Vector3.zero;
				else
					trans.position = Vector3.zero;
			}
			if(isLocal)
				pos = DrawVector3(trans.localPosition, 0);
			else
				pos = DrawVector3(trans.position, 0);
		}
		EditorGUILayout.EndHorizontal();

		// Rotation
		EditorGUILayout.BeginHorizontal();
		{
			if (DrawButton("R", "Reset Rotation", IsResetRotationValid(trans), 20f))
			{
				if(isLocal)
					trans.localEulerAngles = Vector3.zero;
				else
					trans.eulerAngles = Vector3.zero;
			}
			if(isLocal)
				rot = DrawVector3(trans.localEulerAngles, 0);
			else
				rot = DrawVector3(trans.eulerAngles, 0);
		}
		EditorGUILayout.EndHorizontal();

		// Scale
		EditorGUILayout.BeginHorizontal();
		{
			if (DrawButton("S", "Reset Scale", IsResetScaleValid(trans), 20f))
			{
				if(isLocal)
					trans.localScale = Vector3.one;
				else
					trans.SetLossyScale(Vector3.one);
			}
			if(isLocal)
				scale = DrawVector3(trans.localScale, 1);
			else
				scale = DrawVector3(trans.lossyScale, 1);
		}
		EditorGUILayout.EndHorizontal();

		// If something changes, set the transform values
		if (GUI.changed)
		{
			if(isLocal)
			{
				trans.localPosition		= Validate(pos);
				trans.localEulerAngles	= Validate(rot);
				trans.localScale		= Validate(scale);
			}
			else
			{
				trans.position		= Validate(pos);
				trans.eulerAngles	= Validate(rot);
				trans.SetLossyScale(Validate(scale));
			}
		}
	}

	/// <summary>
	/// Helper function that draws a button in an enabled or disabled state.
	/// </summary>

	static bool DrawButton (string title, string tooltip, bool enabled, float width)
	{
		if (enabled)
		{
			// Draw a regular button
			return GUILayout.Button(new GUIContent(title, tooltip), GUILayout.Width(width));
		}
		else
		{
			// Button should be disabled -- draw it darkened and ignore its return value
			Color color = GUI.color;
			GUI.color = new Color(1f, 1f, 1f, 0.25f);
			GUILayout.Button(new GUIContent(title, tooltip), GUILayout.Width(width));
			GUI.color = color;
			return false;
		}
	}

	/// <summary>
	/// Helper function that draws a field of 3 floats.
	/// </summary>

	static Vector3 DrawVector3 (Vector3 value, float defaultValue)
	{
		GUILayoutOption opt = GUILayout.MinWidth(30f);
		value.x = EditorGUILayout.FloatField("X", value.x, opt);
        if (DrawButton(defaultValue.ToString(), "Reset X", value.x != defaultValue, 20f))
        {
            value.x = defaultValue;
        }
		value.y = EditorGUILayout.FloatField("Y", value.y, opt);
        if (DrawButton(defaultValue.ToString(), "Reset X", value.y != defaultValue, 20f))
        {
            value.y = defaultValue;
        }
		value.z = EditorGUILayout.FloatField("Z", value.z, opt);
        if (DrawButton(defaultValue.ToString(), "Reset Z", value.z != defaultValue, 20f))
        {
            value.z = defaultValue;
        }
		return value;
	}

	/// <summary>
	/// Helper function that determines whether its worth it to show the reset position button.
	/// </summary>

    static bool IsResetFloatValid (Transform targetTransform)
    {
        Vector3 v = targetTransform.localPosition;
        return (v.x != 0f || v.y != 0f || v.z != 0f);
    }

	static bool IsResetPositionValid (Transform targetTransform)
	{
		Vector3 v = targetTransform.localPosition;
		return (v.x != 0f || v.y != 0f || v.z != 0f);
	}

	/// <summary>
	/// Helper function that determines whether its worth it to show the reset rotation button.
	/// </summary>

	static bool IsResetRotationValid (Transform targetTransform)
	{
		Vector3 v = targetTransform.localEulerAngles;
		return (v.x != 0f || v.y != 0f || v.z != 0f);
	}

	/// <summary>
	/// Helper function that determines whether its worth it to show the reset scale button.
	/// </summary>

	static bool IsResetScaleValid (Transform targetTransform)
	{
		Vector3 v = targetTransform.localScale;
		return (v.x != 1f || v.y != 1f || v.z != 1f);
	}

	/// <summary>
	/// Helper function that removes not-a-number values from the vector.
	/// </summary>

	static Vector3 Validate (Vector3 vector)
	{
		vector.x = float.IsNaN(vector.x) ? 0f : vector.x;
		vector.y = float.IsNaN(vector.y) ? 0f : vector.y;
		vector.z = float.IsNaN(vector.z) ? 0f : vector.z;
		return vector;
	}
}
}
#endif
