#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(Dropper))]
public class EditorDropper : Editor 
{
	public Dropper drop;
	public float scrollAmount = 0f;
	public Vector3 lastSpotDown;
	
	public void OnEnable()
	{
	    drop = (Dropper)target;
	    SceneView.onSceneGUIDelegate += DropperUpdate;
	}
	 
	void DropperUpdate(SceneView sceneview)
	{
		if(!drop.dropmodeActive)
		{
			if(drop.toDrop)
				GameObject.DestroyImmediate(drop.toDrop.gameObject);
			//drop.camPosition = SceneView.lastActiveSceneView.pivot;
			
			return;
		}
		
		GameObject[] newSelections = new GameObject[1];
		newSelections[0] = drop.gameObject;
		Selection.objects = newSelections;
		
		Event e = Event.current;
		if(drop.toDrop == null && drop.prefabs[drop.index] !=null )
		{
			drop.toDrop = (Transform.Instantiate(drop.prefabs[drop.index],Vector3.zero,drop.transform.rotation)as Transform);
			drop.toDrop.Rotate(drop.rotationOffset);
		}
		sceneview.orthographic = true;
		//sceneview.MoveToView(drop.transform);
		//sceneview.camera.transform.LookAt(new Vector3(0,0,100));
		if(drop.toDrop && e.type == EventType.MouseMove)
		{
			Vector3 dropPoint = sceneview.camera.ScreenToWorldPoint(new Vector3(e.mousePosition.x,sceneview.camera.pixelHeight - e.mousePosition.y,-sceneview.camera.transform.position.z));
			drop.toDrop.position = dropPoint;
			//Debug.Log(dropPoint);
		}
		
		
		if(e.type == EventType.MouseDown)
		{
			if(e.button == 0)
			{
				//lastSpotDown = drop.camPosition;
			}
			
			if(e.button == 0)
			{
				Vector3 dropPoint = sceneview.camera.ScreenToWorldPoint(new Vector3(e.mousePosition.x,sceneview.camera.pixelHeight - e.mousePosition.y,-sceneview.camera.transform.position.z));
				Transform newObj = (Transform)GameObject.Instantiate(drop.prefabs[drop.index],new Vector3(dropPoint.x,dropPoint.y,0),drop.transform.rotation);
				newObj.Rotate(drop.rotationOffset);
				Undo.RegisterCreatedObjectUndo(newObj.gameObject, "Create " + newObj.name); 
				if(e.modifiers == EventModifiers.Control)
				{
					RaycastHit hit;
					if(newObj.GetComponent<Collider>())
					{
						if(Physics.SphereCast(newObj.transform.position,newObj.GetComponent<Collider>().bounds.extents.x,-Vector3.up, out hit))
						{
							float yOffset = newObj.GetComponent<Collider>().bounds.extents.y;
							newObj.transform.position = new Vector3(hit.point.x,hit.point.y + yOffset,hit.point.z);
						}
					}
				}
			}
		}
		
		if(Event.current.type == EventType.MouseDrag)
		{
			if(Event.current.button == 2)
			{
				float distanceAway = Mathf.Abs(drop.transform.position.z - SceneView.lastActiveSceneView.size)/555;
				drop.camPosition += new Vector3(-e.delta.x*distanceAway,e.delta.y*distanceAway,0);
			}
		}
		
		drop.toDrop.rotation = Quaternion.Euler(drop.rotationOffset);
		
		if(e.type == EventType.ScrollWheel)
		{
			if(e.modifiers == EventModifiers.Control)
			{
				scrollAmount += e.delta.y;
				if(scrollAmount > drop.sensitivity)
				{
					scrollAmount = 0f;
					drop.index ++;
					if(drop.loops)
					{
						drop.index = drop.index % drop.prefabs.Count;
					}
					else
					{
						drop.index = Mathf.Clamp(drop.index, 0, drop.prefabs.Count-1);
					}
					Transform newSelection = (Transform.Instantiate(drop.prefabs[drop.index],drop.toDrop.position,drop.toDrop.rotation)as Transform);
					newSelection.Rotate(drop.rotationOffset);
					GameObject.DestroyImmediate(drop.toDrop.gameObject);
					drop.toDrop = newSelection;
				}
				else if(scrollAmount < -drop.sensitivity)
				{
					scrollAmount = 0f;
					drop.index --;
					if(drop.loops)
					{
						if(drop.index < 0)
							drop.index = drop.prefabs.Count-1;
					}
					else
					{
						drop.index = Mathf.Clamp(drop.index, 0, drop.prefabs.Count-1);
					}
					Transform newSelection = (Transform.Instantiate(drop.prefabs[drop.index],drop.toDrop.position,drop.toDrop.rotation)as Transform);
					newSelection.Rotate(drop.rotationOffset);
					GameObject.DestroyImmediate(drop.toDrop.gameObject);
					drop.toDrop = newSelection;
				}
			}
			else
			{
				SceneView.lastActiveSceneView.size += e.delta.y;
				//drop.camPosition = new Vector3(drop.camPosition.x,drop.camPosition.y,drop.camPosition.z + drop.moveSpeed);
				//sceneview.camera.orthographicSize += e.delta.y * 10;
			}

		}
		
		SceneView.lastActiveSceneView.pivot = drop.camPosition;
		SceneView.lastActiveSceneView.rotation = Quaternion.Euler(new Vector3(0,0,0));
		SceneView.lastActiveSceneView.Repaint();
		Event.current.Use();
	}
	
	public override void OnInspectorGUI () 
	{
		DrawDefaultInspector();
	}
}
#endif
