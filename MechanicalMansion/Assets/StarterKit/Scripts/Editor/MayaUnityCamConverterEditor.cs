#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System;
using System.Collections;

[CustomEditor(typeof(MayaUnityCamConverter)), CanEditMultipleObjects]
public class MayaUnityCamConverterEditor : Editor 
{

	public override void OnInspectorGUI()
	{
		serializedObject.Update();

		DrawDefaultInspector();

		EditorGUILayoutX.SuperSpace(2);
		GUILayout.BeginHorizontal();
		MultiObjectEditor.Button(serializedObject, "Convert", typeof(MayaUnityCamConverter), "Convert", GUILayout.Height(40));
		GUILayout.EndHorizontal();

		serializedObject.ApplyModifiedProperties ();
	}
}
#endif
