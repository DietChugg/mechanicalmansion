﻿using UnityEngine;
using UnityEditor;
using UnityEditor.UI;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using StarterKit;

[CustomEditor(typeof(TogglePlus), true)]
[CanEditMultipleObjects]
public class TogglePlusEditor : SelectableEditor
{
    SerializedProperty m_OnValueChangedProperty;
    //SerializedProperty m_OnClickDownProperty;
    SerializedProperty m_TransitionProperty;
    SerializedProperty m_GraphicProperty;
    SerializedProperty m_GroupProperty;
    SerializedProperty m_IsOnProperty;
    //SerializedProperty gameObjectsProperty;

    protected override void OnEnable()
    {
        base.OnEnable();

        m_TransitionProperty = serializedObject.FindProperty("toggleTransition");
        m_GraphicProperty = serializedObject.FindProperty("graphic");
        m_GroupProperty = serializedObject.FindProperty("m_Group");
        m_IsOnProperty = serializedObject.FindProperty("m_IsOn");
        m_OnValueChangedProperty = serializedObject.FindProperty("onValueChanged");
        //m_OnClickDownProperty = serializedObject.FindProperty("onClickedDown");
        //gameObjectsProperty = serializedObject.FindProperty("gameObjects");
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        EditorGUILayout.Space();

        serializedObject.Update();
        EditorGUILayout.PropertyField(m_IsOnProperty);
        EditorGUILayout.PropertyField(m_TransitionProperty);
        EditorGUILayout.PropertyField(m_GraphicProperty);
        EditorGUILayout.PropertyField(m_GroupProperty);

        EditorGUILayout.Space();

        // Draw the event notification options
        EditorGUILayout.PropertyField(m_OnValueChangedProperty);
        //if (EditorGUILayout.PropertyField(gameObjectsProperty))
        //{
        //    gameObjectsProperty.arraySize = EditorGUILayout.IntField("Size: ", gameObjectsProperty.arraySize);
        //    for (int i = 0; i < gameObjectsProperty.arraySize; i++)
        //    {
        //        EditorGUILayout.PropertyField(gameObjectsProperty.GetArrayElementAtIndex(i));
        //    }
        //}

        //if (m_OnClickDownProperty == null)
        //{
        //    Debug.Log("Not A Things");
        //}

        //EditorGUILayout.PropertyField(m_OnClickDownProperty);

        serializedObject.ApplyModifiedProperties();
    }
}