﻿using UnityEngine;
using UnityEditor;
//This file must be in Assets\Editor\

namespace StarterKit
{
    [CustomPropertyDrawer(typeof(Tag))]
    public class TagDrawer : PropertyDrawer
    {
        int tagIndex = 0;
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            string[] tagNames = UnityEditorInternal.InternalEditorUtility.tags;

            EditorGUI.BeginProperty(position, label, property);

            EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

            SerializedProperty levelIndexProp = property.FindPropertyRelative("tag");
            tagIndex = EditorGUI.Popup(position, label.text, tagIndex, tagNames);
            levelIndexProp.stringValue = tagNames[tagIndex];

            EditorGUI.EndProperty();
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return base.GetPropertyHeight(property, label);
        }
    } 
}