﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

namespace StarterKit
{

    public class FolderForeachSubfolder_MenuItem
    {

        [MenuItem("Assets/Create/FolderForeachSubfolder", false, 11)]
        public static void ScriptableObjectTemplateMenuItem()
        {
            CreateFolderInAllChildren_Window window = (CreateFolderInAllChildren_Window)EditorWindow.GetWindow(typeof(CreateFolderInAllChildren_Window), false, "SOT");
            window.ShowPopup();
            EditorWindow.FocusWindowIfItsOpen<CreateFolderInAllChildren_Window>();

        }
    }

    public class CreateFolderInAllChildren_Window : EditorWindow
    {
        string folderName = "";

        void OnGUI()
        {
            EditorGUILayout.TextArea("This supports Folders within Folders ie. folderName can be Cake/Icing/Color and will create any needed folders");
            folderName = EditorGUILayout.TextField("Folder Name: ",folderName);
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Submit"))
	        {
            string path = AssetDatabase.GetAssetPath(Selection.activeObject);
            string[] subFolderPaths = AssetDatabase.GetSubFolders(path);
            
            for (int i = 0; i < subFolderPaths.Length; i++)
            {
                float percentComplete = (float)i / (float)subFolderPaths.Length;
                EditorUtility.DisplayProgressBar("Creating Folders","subFolderPath: " + (i+1), percentComplete);


                string[] split = folderName.Split(new char[] { '/' });

                if (split.Length > 1)
                {
                    for (int j = 0; j < split.Length; j++)
                    {
                        string subFolderSplit = "";
                        string subFolderSplitMinusNewFolder = "";
                        string newFolder = split[j];

                        //Debug.Log("New Folder: ".RTAqua() + newFolder);

                        for (int k = 0; k <= j; k++)
                        {
                            subFolderSplit += split[k] + "/";
                            if(k != j)
                                subFolderSplitMinusNewFolder += split[k] + "/";

                            
                        }

                        //Debug.Log("subFolderSplit: " + subFolderSplit);
                        //Debug.Log("subFolderSplitMinusNewFolder: " + subFolderSplitMinusNewFolder);


                        if (subFolderSplit.IsNotNullOrEmpty() && subFolderSplitMinusNewFolder.IsNotNullOrEmpty())
                        {
                            //Debug.Log("No Empty String Found");
                            string finalPathWithNewFolder = subFolderPaths[i] + "/" + subFolderSplit.InsetFromEnd(1);
                            //Debug.Log("finalPathWithNewFolder: ".RTOrange() + finalPathWithNewFolder);
                            string finalPathWithoutNewFolder = subFolderPaths[i] + "/" + subFolderSplitMinusNewFolder.InsetFromEnd(1);
                            //Debug.Log("finalPathWithoutNewFolder: ".RTOrange() + finalPathWithoutNewFolder);
                            //Debug.Log("newFolder: ".RTOrange() + newFolder);


                            if (!AssetDatabase.IsValidFolder(finalPathWithNewFolder))
                                AssetDatabase.CreateFolder(finalPathWithoutNewFolder, newFolder);
                        }
                        else
                        {
                            string finalPathWithNewFolder = subFolderPaths[i] + "/" + newFolder;
                            //Debug.Log("finalPathWithNewFolder: ".RTOrange() + finalPathWithNewFolder);
                            string finalPathWithoutNewFolder = subFolderPaths[i];
                            //Debug.Log("finalPathWithoutNewFolder: ".RTOrange() + finalPathWithoutNewFolder);
                            //Debug.Log("newFolder: ".RTOrange() + newFolder);


                            if (!AssetDatabase.IsValidFolder(finalPathWithNewFolder))
                                AssetDatabase.CreateFolder(finalPathWithoutNewFolder, newFolder);
                        }

                        //if (subFolderSplit.Length > 0)
                        //    subFolderSplit = subFolderSplit.InsetFromEnd(1);
                        //if (subFolderSplitMinusNewFolder.Length > 0)
                        //    subFolderSplitMinusNewFolder = subFolderSplitMinusNewFolder.InsetFromEnd(1);
                        
                        //Debug.Log(subFolderSplit);



                        //if (subFolderSplit.IsNotNullOrEmpty())
                        //{
                        //    if (!AssetDatabase.IsValidFolder(subFolderPaths[i] + "/" + subFolderSplit))
                        //        AssetDatabase.CreateFolder(subFolderPaths[i] + "/" + subFolderSplitMinusNewFolder, newFolder);
                        //}
                        
                        
                    }
                }
                else
                {
                    if (!AssetDatabase.IsValidFolder(subFolderPaths[i] + "/" + folderName))
                        AssetDatabase.CreateFolder(subFolderPaths[i], folderName);
                }
                


                
            }
            EditorUtility.ClearProgressBar();
                Close();
	        }
            if (GUILayout.Button("Cancel"))
            {
                Close();
            }
            GUILayout.EndHorizontal();
        }
    } 
}