#if UNITY_EDITOR
using System;
using System.Collections; 
using UnityEditor;
using UnityEngine;

//[CanEditMultipleObjects]
[CustomPropertyDrawer (typeof(char))]
public class CharDrawer : PropertyDrawer
{
    public float heightOffset = 20f;
    // Draw the property inside the given rect
    public Rect positionSize = new Rect(0, 0, 20f, 20f);
    public SerializedProperty charProp;
    
    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return heightOffset;
    }
    
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        //char cha= 'd';

//        Debug.Log(property.propertyType);
        //charProp = property.FindPropertyRelative("value");
        int indentlevel = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        if (Selection.transforms.Length > 1)
        {
            EditorGUI.LabelField(new Rect(position.x, position.y, position.width, position.height),"Multi Not Yet Supported");
            return;
        }

        EditorGUI.LabelField(new Rect(position.x, position.y, position.width, position.height),"Single Not Yet Supported");

        //property.stringValue = EditorGUI.TextField(new Rect(position.x, position.y, position.x + position.width, position.height),"Char Value", property.stringValue);


        EditorGUI.indentLevel = indentlevel;
    }
}
#endif
