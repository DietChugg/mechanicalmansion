#if UNITY_EDITOR
using System;
using System.Collections;
using UnityEditor;
using UnityEngine;

//[CanEditMultipleObjects]
[CustomPropertyDrawer (typeof(Bool3))]
public class Bool3Drawer : PropertyDrawer 
{
    public float heightOffset = 20f;
    // Draw the property inside the given rect
    public Rect positionSize = new Rect(0,0,20f,20f);
    
    public SerializedProperty x;
    public SerializedProperty y;
    public SerializedProperty z;
    
    public void OnEnable()
    {
        
    }
    
    public override float GetPropertyHeight (SerializedProperty property, GUIContent label)
    {
        return heightOffset;
    }
    
    
    public override void OnGUI (Rect position, SerializedProperty property, GUIContent label) 
    {
        x = property.FindPropertyRelative("x");
        y = property.FindPropertyRelative("y");
        z = property.FindPropertyRelative("z");
        //EditorGUI.indentLevel
        // Using BeginProperty / EndProperty on the parent property means that
        // prefab override logic works on the entire property.
        if (Selection.transforms.Length > 1)
        {
            EditorGUI.LabelField(new Rect(position.x, position.y, position.width, position.height),"Multi Not Yet Supported");
            return;
        }
        EditorGUI.BeginProperty (position, label, property);

        EditorGUI.LabelField (new Rect(position.x, position.y, position.width, 18f), "X: ");
        x.boolValue = EditorGUI.Toggle(new Rect(position.x + (position.width * .1666f), position.y, position.x + (position.width * .125f), position.height), x.boolValue);

        EditorGUI.LabelField (new Rect(position.x + (position.width * .3333f), position.y, position.width, 18f), "Y: ");
        y.boolValue = EditorGUI.Toggle (new Rect((position.width * .5f), position.y,position.x + (position.width * .125f), position.height), y.boolValue);

        EditorGUI.LabelField (new Rect(position.x + (position.width * .6666f), position.y, position.width, 18f), "Z: ");
        z.boolValue = EditorGUI.Toggle (new Rect((position.width * .8333f), position.y, position.x + (position.width* .2f), position.height), z.boolValue);

        EditorGUI.EndProperty ();
    }
}
#endif
