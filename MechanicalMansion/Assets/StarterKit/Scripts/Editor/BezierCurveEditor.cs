#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System;
using System.Collections;

[CustomEditor(typeof(BezierCurve)), CanEditMultipleObjects]
public class BezierCurveEditor : Editor 
{
	SerializedProperty spacing;
	SerializedProperty t;
	SerializedProperty resample;
	SerializedProperty resampleDistance;
	SerializedProperty drawHandles;
	SerializedProperty handleColor;
	SerializedProperty drawCurve;
	SerializedProperty pathColor;
	SerializedProperty resampleColor;
	
	void OnEnable()
	{
		spacing = serializedObject.FindProperty("spacing");
		t = serializedObject.FindProperty("t");
		resample = serializedObject.FindProperty("resample");
		resampleDistance = serializedObject.FindProperty("resampleDistance");
		drawHandles = serializedObject.FindProperty("drawHandles");
		handleColor = serializedObject.FindProperty("handleColor");
		drawCurve = serializedObject.FindProperty("drawCurve");
		pathColor = serializedObject.FindProperty("pathColor");
		resampleColor = serializedObject.FindProperty("resampleColor");
	}

	public override void OnInspectorGUI()
	{
//		DrawDefaultInspector();
//		return;


		serializedObject.Update();
		UnityEngine.Object obj = EditorGUILayout.ObjectField("Script",MonoScript.FromMonoBehaviour(target as MonoBehaviour),typeof(MonoScript),false);
		if(obj != null && obj != target)
		{
//			(target as MonoBehaviour).gameObject.AddComponent<obj.GetType()>();
		}

		EditorGUILayoutX.SuperSpace(2);

		//BEZ CURVE
		EditorGUILayout.HelpBox("   Bez Curve", MessageType.None);
		MultiObjectEditor.Property(spacing, "Spacing", true, 0.01f, 0.25f);
		MultiObjectEditor.Property(t, "T", true, 0.01f, 1f);

		EditorGUILayoutX.SuperSpace(5);

		//RESAMPLING
		EditorGUILayout.HelpBox("   Resampling", MessageType.None);
		MultiObjectEditor.Property(resample, "Resample");
		MultiObjectEditor.Property(resampleDistance, "Resample Distance");

		EditorGUILayoutX.SuperSpace(5);

		//DEBUG OPTIONS
		EditorGUILayout.HelpBox("   Debug Options", MessageType.None);
		MultiObjectEditor.Property(drawHandles, "Draw Handles");
		MultiObjectEditor.Property(handleColor, "Handle Color");
		MultiObjectEditor.Property(drawCurve, "Draw Curve");
		MultiObjectEditor.Property(pathColor, "Path Color");
		MultiObjectEditor.Property(resampleColor, "Resample Color");

		EditorGUILayoutX.SuperSpace(5);

		//CONTROL POINTS
		EditorGUILayout.HelpBox("   Control Points", MessageType.None);
		GUILayout.BeginHorizontal();
		MultiObjectEditor.Button(serializedObject, "Add Point", typeof(BezierCurve), "AddPoint");
		MultiObjectEditor.Button(serializedObject, "Clear Points", typeof(BezierCurve), "ClearPoints");
		GUILayout.EndHorizontal();

		serializedObject.ApplyModifiedProperties ();
	}

}
#endif

