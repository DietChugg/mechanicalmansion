#if UNITY_EDITOR
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using System.Collections;
using StarterKit;

[CustomEditor(typeof(IntSelector)), CanEditMultipleObjects]
public class IntSelectorEditor : Editor 
{
	SerializedProperty minValue;
    SerializedProperty currentValue;
    SerializedProperty maxValue;
    SerializedProperty decrementButton;
    SerializedProperty intDisplay;
    SerializedProperty incrementButton;
    SerializedProperty OnValueChange;

    void OnEnable()
    {
        minValue = serializedObject.FindProperty("minValue");
        currentValue = serializedObject.FindProperty("currentValue");
        maxValue = serializedObject.FindProperty("maxValue");
        decrementButton = serializedObject.FindProperty("decrementButton");
        intDisplay = serializedObject.FindProperty("intDisplay");
        incrementButton = serializedObject.FindProperty("incrementButton");
        OnValueChange = serializedObject.FindProperty("OnValueChange");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        UnityEngine.Object obj = EditorGUILayout.ObjectField("Script", MonoScript.FromMonoBehaviour(target as MonoBehaviour), typeof(MonoScript), false);
        if (obj)
        {

        }

        EditorGUILayoutX.SuperSpace(2);

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Min", GUILayout.MinWidth(50f));
        EditorGUILayout.LabelField("Current", GUILayout.MinWidth(50f));
        EditorGUILayout.LabelField("Max", GUILayout.MinWidth(50f));
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        minValue.intValue = EditorGUILayout.IntField(minValue.intValue, GUILayout.MinWidth(50f));
        currentValue.intValue = EditorGUILayout.IntField(currentValue.intValue, GUILayout.MinWidth(50f));
        maxValue.intValue = EditorGUILayout.IntField(maxValue.intValue, GUILayout.MinWidth(50f));
        EditorGUILayout.EndHorizontal();

        EditorGUILayoutX.SuperSpace(1);

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("[-]", GUILayout.MinWidth(50f));
        EditorGUILayout.LabelField("[#]", GUILayout.MinWidth(50f));
        EditorGUILayout.LabelField("[+]", GUILayout.MinWidth(50f));
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        decrementButton.objectReferenceValue = EditorGUILayout.ObjectField(decrementButton.objectReferenceValue, typeof(Button), true, GUILayout.MinWidth(50f));
        intDisplay.objectReferenceValue = EditorGUILayout.ObjectField(intDisplay.objectReferenceValue, typeof(Text), true, GUILayout.MinWidth(50f));
        incrementButton.objectReferenceValue = EditorGUILayout.ObjectField(incrementButton.objectReferenceValue, typeof(Button), true, GUILayout.MinWidth(50f));
        EditorGUILayout.EndHorizontal();

        EditorGUILayoutX.SuperSpace(2);

        EditorGUILayout.HelpBox("Assign Methods to OnValueChange(Int32) as 'Dynamic Int'", MessageType.Info);
        EditorGUILayout.PropertyField(OnValueChange);

        serializedObject.ApplyModifiedProperties();
    }
	
}
#endif
