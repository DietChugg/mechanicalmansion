#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using System.Collections;
 
public class DeleteMyPlayerPrefs
{ 
	[MenuItem("Tools/DeleteMyPlayerPrefs")] 
	static void DeleteMyPlayerPrefss() 
	{ 
		PlayerPrefs.DeleteAll();
	} 
}
#endif
