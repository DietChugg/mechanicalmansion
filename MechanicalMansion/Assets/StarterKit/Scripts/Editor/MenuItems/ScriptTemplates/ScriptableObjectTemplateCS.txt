﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections;
public class #ClassName# : SingletonScriptableObject<#ClassName#> 
{
	
#if UNITY_EDITOR
	[MenuItem ("ScriptableObject/#ClassName#")]
	static void DrawHierarchyDataMenuItem () 
	{
		Selection.activeObject = (UnityEngine.Object)#ClassName#.Instance;
	}
#endif
}
