#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;

namespace StarterKit
{

	public class ScriptableObjectTemplate_MenuItem
	{

		[MenuItem("Assets/Create/ScriptTemplates/ScriptableObject",false,9)]
		public static void ScriptableObjectTemplateMenuItem()
		{
			ScritpableObject_Window window = (ScritpableObject_Window)EditorWindow.GetWindow (typeof (ScritpableObject_Window), false, "SOT");
			window.ShowPopup();
			EditorWindow.FocusWindowIfItsOpen<ScritpableObject_Window>();

		}
	}

	public static class Recompiler
	{
		public static bool hasCompiled = false;
		static Recompiler()
		{
			Debug.Log("I just happened");
			hasCompiled = true;
		}
	}

	public class ScritpableObject_Window : EditorWindow
	{
		public string className = "";
		public string path = "Empty";
		public bool createScriptableObject = true;
		public bool autoLoadScript = true;
		public float waitForImport = 2f;
		public string filetypeExtension = "";
		public bool hasSubmitted = false;

		public enum ScriptType
		{
			cs,
			js
		}

		public ScriptType scriptType;
	    
		void Update()
		{
			if(hasSubmitted)
			{
				Recompiler.hasCompiled = false;
				if(createScriptableObject)
				{
#if UNITY_5
					if(!AssetDatabase.IsValidFolder(path + "Resources"))
#endif
						AssetDatabase.CreateFolder(path.Substring(0,path.Length-1),"Resources");
					ScriptableObject scriptableObject = ScriptableObject.CreateInstance(className);
					AssetDatabase.CreateAsset(scriptableObject, path + "Resources/" +className+".asset");
					AssetDatabase.SaveAssets();

					if(autoLoadScript)
					{
						MonoScript newScript = MonoScript.FromScriptableObject(scriptableObject);// (MonoScript)AssetDatabase.LoadAssetAtPath(path+className+scriptTypeName);
						AssetDatabase.OpenAsset(newScript);
					}
				}
				Close ();
	        }
	    }
	    
	    void OnGUI()
		{

			if(hasSubmitted)
			{
				GUILayout.Label("Generating Content");
				return;
			}

			if(Selection.activeObject == null)
			{
				GUILayout.Label("Select A Spot in Your Project Folder");
				return;
			}


			GUILayout.BeginHorizontal();
			GUILayout.Label("Path");

			if (IsAssetAFolder(Selection.activeObject))
			{
				path = AssetDatabase.GetAssetPath(Selection.activeObject.GetInstanceID()) + "/";
			}
			else
			{
				string partialPath = AssetDatabase.GetAssetPath(Selection.activeObject.GetInstanceID()).Replace(Selection.activeObject.name,"");
				string[] split = partialPath.Split(new char[]{'.'});

				path = split[0];
			}

			GUILayout.Label(path);
			GUILayout.EndHorizontal();

			scriptType = (ScriptType)EditorGUILayout.EnumPopup("Script Type: ",scriptType);
			filetypeExtension = scriptType == ScriptType.cs ? ".cs" : ".js";

			GUILayout.BeginHorizontal();
			if(scriptType != ScriptType.cs)
			{
				GUILayout.Label("JS Not Yet Supported");
				return;
			}
			GUILayout.EndHorizontal();


			GUILayout.BeginHorizontal();
			GUILayout.Label("Script Name:");
			className = GUILayout.TextField(className);
			GUILayout.EndHorizontal();

			GUILayout.BeginHorizontal();
			createScriptableObject = GUILayout.Toggle(createScriptableObject,"Create Scriptable Object?");
			GUILayout.EndHorizontal();

			if(createScriptableObject)
			{
				GUILayout.BeginHorizontal();
				autoLoadScript = GUILayout.Toggle(autoLoadScript,"Auto Load Script?");
				GUILayout.EndHorizontal();
			}

			GUILayout.BeginHorizontal();
			if(GUILayout.Button("Cancel"))
			{
				Close();
			}
			if(GUILayout.Button("Ok") || Input.GetKeyDown(KeyCode.Return))
			{
				List<string> lines = new List<string>();
				StreamReader reader;
				if(scriptType == ScriptType.cs)
					reader = new StreamReader(
						"Assets/StarterKit/Scripts/Editor/MenuItems/ScriptTemplates/ScriptableObjectTemplateCS.txt");
				else
					reader = new StreamReader(
						"Assets/StarterKit/Scripts/Editor/MenuItems/ScriptTemplates/ScriptableObjectTemplateJS.txt");
				while(!reader.EndOfStream)
					lines.Add(reader.ReadLine());
				reader.Close();

				StreamWriter writer = new StreamWriter(path + className + filetypeExtension);

				for (int i = 0; i < lines.Count; i++) 
				{
					writer.WriteLine(lines[i].Replace("#ClassName#",className));
				}
				writer.Close();
				Recompiler.hasCompiled = false;
				AssetDatabase.ImportAsset(path + className + filetypeExtension, ImportAssetOptions.ForceUpdate | ImportAssetOptions.ImportRecursive);
				hasSubmitted = true;

			}
			GUILayout.EndHorizontal();
		}

		private static bool IsAssetAFolder(UnityEngine.Object obj)
		{
			string path = "";
			
			if (obj == null){
				return false;
			}
			
			path = AssetDatabase.GetAssetPath(obj.GetInstanceID());
			
			if (path.Length > 0)
			{
				if (Directory.Exists(path))
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			
			return false;
		}

	}

}
#endif
