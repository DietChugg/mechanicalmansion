#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Collections;
 
public class SnapToGrid_MenuItem
{
    [MenuItem ("GameObject/Snap to Grid &s")]
    static void MenuSnapToGrid()
    {
        Transform[] transforms = Selection.GetTransforms(SelectionMode.TopLevel | SelectionMode.OnlyUserModifiable);
 
        float gridx = 1.0f;
        float gridy = 1.0f;
        float gridz = 1.0f;

		float gridxOffset = .5f;
		float gridyOffset = 0f;
		float gridzOffset = 0f;
 
        foreach (Transform transform in transforms)
        {
            Vector3 newPosition = transform.position;
			newPosition.x = gridxOffset+ Mathf.Round(newPosition.x / gridx) * gridx;
			newPosition.y = gridyOffset+Mathf.Round(newPosition.y / gridy) * gridy;
			newPosition.z = gridzOffset+Mathf.Round(newPosition.z / gridz) * gridz;
            transform.position = newPosition;
        }
    }
}
#endif
