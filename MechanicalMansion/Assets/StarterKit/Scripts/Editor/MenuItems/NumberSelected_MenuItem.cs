#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using System.Collections;
 
public class NumberSelected_MenuItem
{ 
	[MenuItem("Tools/NumberSelected &n")] 
	static void DeleteMyPlayerPrefss() 
	{ 
		Undo.RecordObjects(Selection.gameObjects,"Numbered Objects");
		for (int i = 0; i < Selection.gameObjects.Length; i++) 
		{
			int length = Selection.gameObjects[i].name.Length;

			if(length < 3)
			{
				Selection.gameObjects[i].name += i.ToString("D3");
			}
			else
			{
				int result = -1;
				string numberString = Selection.gameObjects[i].name.Substring(length-3);
				string nameString = Selection.gameObjects[i].name.Substring(0,length-3);
				if(int.TryParse(numberString,out result))
				{
					Selection.gameObjects[i].name = nameString + i.ToString("D3");
				}
				else
				{
					Selection.gameObjects[i].name += i.ToString("D3");
				}
			}
		}
	} 
}
#endif
