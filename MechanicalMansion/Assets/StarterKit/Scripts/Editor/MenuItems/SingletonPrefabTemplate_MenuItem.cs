#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;

namespace StarterKit
{
    //TODO: IMPLEMENT CREATION OF SINGLETON PREFAB 
    public class SingletonPrefabTemplate_MenuItem
	{

		[MenuItem("Assets/Create/ScriptTemplates/SingletonPrefab",false,11)]
		public static void ScriptableObjectTemplateMenuItem()
		{
            SingletonPrefab_Window window = (SingletonPrefab_Window)EditorWindow.GetWindow(typeof(SingletonPrefab_Window), false, "SOT");
			window.ShowPopup();
            EditorWindow.FocusWindowIfItsOpen<SingletonPrefab_Window>();

		}
	}

	public static class RecompilerPrefab
	{
		public static bool hasCompiled = false;
        static RecompilerPrefab()
		{
			hasCompiled = true;
		}
	}

	public class SingletonPrefab_Window : EditorWindow
	{
		public string className = "";
		public string path = "Empty";
		public bool autoLoadScript = true;
		public float waitForImport = 2f;
		public string filetypeExtension = "";
		public bool hasSubmitted = false;
	    
		void Update()
		{
			if(Recompiler.hasCompiled && hasSubmitted)
			{
                EditorUtility.DisplayProgressBar("Generating Content", "Recompile Complete", .8f);
				Recompiler.hasCompiled = false;

#if UNITY_5
					if(!AssetDatabase.IsValidFolder(path + "Resources"))
#endif
						AssetDatabase.CreateFolder(path.Substring(0,path.Length-1),"Resources");
					ScriptableObject scriptableObject = ScriptableObject.CreateInstance(className);

                    Debug.Log("SettingUpPrefab");

                    GameObject newObject = new GameObject(className);
                    PrefabUtility.CreatePrefab(path + "Resources/" + className + "Prefab.prefab", newObject);
                    DestroyImmediate(newObject);

                    EditorUtility.DisplayProgressBar("Generating Content", "Creating Asset", .83f);
					AssetDatabase.CreateAsset(scriptableObject, path + "Resources/" +className+".asset");
                    EditorUtility.DisplayProgressBar("Generating Content", "Saving Asset", .83f);
					AssetDatabase.SaveAssets();

					if(autoLoadScript)
					{
                        EditorUtility.DisplayProgressBar("Generating Content", "Loading Up Script Editor", .9f);
						MonoScript newScript = MonoScript.FromScriptableObject(scriptableObject);// (MonoScript)AssetDatabase.LoadAssetAtPath(path+className+scriptTypeName);
						AssetDatabase.OpenAsset(newScript);
					}
                    EditorUtility.ClearProgressBar();
				Close ();
	        }
	    }
	    
	    void OnGUI()
		{

			if(hasSubmitted)
			{
				GUILayout.Label("Generating Content");
				return;
			}

			if(Selection.activeObject == null)
			{
				GUILayout.Label("Select A Spot in Your Project Folder");
				return;
			}


			GUILayout.BeginHorizontal();
			GUILayout.Label("Path");

			if (IsAssetAFolder(Selection.activeObject))
			{
				path = AssetDatabase.GetAssetPath(Selection.activeObject.GetInstanceID()) + "/";
			}
			else
			{
				string partialPath = AssetDatabase.GetAssetPath(Selection.activeObject.GetInstanceID()).Replace(Selection.activeObject.name,"");
				string[] split = partialPath.Split(new char[]{'.'});

				path = split[0];
			}

			GUILayout.Label(path);
			GUILayout.EndHorizontal();

            //scriptType = (ScriptType)EditorGUILayout.EnumPopup("Script Type: ",scriptType);
            filetypeExtension = ".cs";

            //GUILayout.BeginHorizontal();
            //if(scriptType != ScriptType.cs)
            //{
            //    GUILayout.Label("JS Not Yet Supported");
            //    return;
            //}
            //GUILayout.EndHorizontal();


			GUILayout.BeginHorizontal();
			GUILayout.Label("Script Name:");
			className = GUILayout.TextField(className);
			GUILayout.EndHorizontal();

            //GUILayout.BeginHorizontal();
            //createScriptableObject = GUILayout.Toggle(createScriptableObject,"Create Scriptable Object?");
            //GUILayout.EndHorizontal();

            //if(createScriptableObject)
            //{
            //    GUILayout.BeginHorizontal();
            //    autoLoadScript = GUILayout.Toggle(autoLoadScript,"Auto Load Script?");
            //    GUILayout.EndHorizontal();
            //}

            GUILayout.BeginHorizontal();
            autoLoadScript = GUILayout.Toggle(autoLoadScript, "Auto Load Script?");
            GUILayout.EndHorizontal();

			GUILayout.BeginHorizontal();
			if(GUILayout.Button("Cancel"))
			{
				Close();
			}
			if(GUILayout.Button("Ok") || Input.GetKeyDown(KeyCode.Return))
			{
                EditorUtility.DisplayProgressBar("Generating Content", "Init", 0f);
				List<string> lines = new List<string>();
				StreamReader reader;
					reader = new StreamReader(
                        "Assets/StarterKit/Scripts/Editor/MenuItems/ScriptTemplates/SingletonPrefabTemplateCS.txt");

				while(!reader.EndOfStream)
					lines.Add(reader.ReadLine());
				reader.Close();



                EditorUtility.DisplayProgressBar("Generating Content", "Writing Script", .1f);
				StreamWriter writer = new StreamWriter(path + className + filetypeExtension);

				for (int i = 0; i < lines.Count; i++) 
				{
					writer.WriteLine(lines[i].Replace("#ClassName#",className));
				}
				writer.Close();
				Recompiler.hasCompiled = false;
                EditorUtility.DisplayProgressBar("Generating Content", "Importing Asset", .25f);
				AssetDatabase.ImportAsset(path + className + filetypeExtension, ImportAssetOptions.ForceUpdate | ImportAssetOptions.ImportRecursive);
                EditorUtility.DisplayProgressBar("Generating Content", "Refreshing AssetDataBase", .4f);
                AssetDatabase.Refresh();
				hasSubmitted = true;
                EditorUtility.DisplayProgressBar("Generating Content", "Waiting On Recompile", .5f);

			}
			GUILayout.EndHorizontal();
		}

		private static bool IsAssetAFolder(UnityEngine.Object obj)
		{
			string path = "";
			
			if (obj == null){
				return false;
			}
			
			path = AssetDatabase.GetAssetPath(obj.GetInstanceID());
			
			if (path.Length > 0)
			{
				if (Directory.Exists(path))
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			
			return false;
		}

	}

}
#endif
