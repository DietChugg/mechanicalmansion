#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;

public class AssetLocPrinter
{
    [MenuItem ("Tools/Print Asset Location %j")]
    static void CopyAssetLocations() {
        string path=string.Empty;
        foreach (Object o in Selection.objects)
        {
            path += "\n" + AssetDatabase.GetAssetPath(o);
        }
        Debug.Log(path);
        EditorGUIUtility.systemCopyBuffer = path;
    }
}
#endif
