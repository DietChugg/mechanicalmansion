#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
 
/// <summary>
/// Extends the Transform context menu to provide support for copying, pasting
/// and pushing position/rotation/scale data around.
/// 
/// Code by Zach Aikman - zachaikman@gmail.com
/// </summary>
public class TransformContextMenu: EditorWindow
{
    private class TransformClipboard
    {
//        public Vector3      position;
//        public Quaternion   rotation;
//        public Vector3      scale;
 
        public bool isPositionSet = false;
        public bool isRotationSet = false;
        public bool isScaleSet = false;
    }
 	static int skip;
//    private static TransformClipboard clipboard = new TransformClipboard();

	[MenuItem("CONTEXT/Transform/Apply Prefab", false, 209)]
	static void ApplyPrefab()
	{
		Undo.RecordObject(Selection.activeTransform, "Apply Prefab");
		GameObject selectedGameObject = Selection.activeGameObject;
		PrefabType selectedPrefabType = PrefabUtility.GetPrefabType(selectedGameObject);
		GameObject parentGameObject = selectedGameObject.transform.root.gameObject;
		UnityEngine.Object prefabParent = PrefabUtility.GetPrefabParent(selectedGameObject);

		if (selectedPrefabType == PrefabType.PrefabInstance) 
		{
			PrefabUtility.ReplacePrefab(parentGameObject, prefabParent,
			                            ReplacePrefabOptions.ConnectToPrefab);
		}
	}
	
	#region Count
	[MenuItem("CONTEXT/Transform/Count Scene Objects", false, 300)]
    static public void CountSceneObjects()
	{
		int count = 0;  
		int activeCount = 0;
		object[] allObjects = Resources.FindObjectsOfTypeAll(typeof(GameObject));
	    foreach(object thisObject in allObjects)
		{
	    	if (((GameObject) thisObject))
	    		count ++;
			if((((GameObject) thisObject).activeInHierarchy))
				activeCount ++;
		}
		Debug.Log("Objects in Scene is:" + count +" "+  EditorApplication.currentScene);
		Debug.Log("Active Objects in Scene is:" + activeCount +" "+ EditorApplication.currentScene);
	}
	
	[MenuItem("CONTEXT/Transform/Count Kids", false, 301)]
	static public void CountKids()
	{
		int totalCount = 0;
		totalCount = CountKidsRecursive(Selection.activeTransform,false);
		totalCount --;
		int totalCountRecursive = 0;
		totalCountRecursive = CountKidsRecursive(Selection.activeTransform,true);
		totalCountRecursive --;
		Debug.Log("Total Kids Recursive: " + totalCountRecursive);
		Debug.Log("Total Kids Non-Recursive: " + totalCount);
	}
	
	static private int CountKidsRecursive(Transform counter, bool isRecursive)
	{
		int count = 1;
		foreach(Transform kid in counter)
		{
			if(isRecursive)
				count += CountKidsRecursive(kid, isRecursive);
			else
				count ++;
		}
		return count;
	}
	#endregion
	
	#region ResetKids
	[MenuItem("CONTEXT/Transform/Reset Kids LocalPosition", false, 400)]
    static void ResetKidsLocalPosition()
	{
		foreach(Transform transform in Selection.transforms)
			ResetKidsLocalPositionRecursive (transform, false);
	}
	
	[MenuItem("CONTEXT/Transform/Reset Kids LocalPosition Recursive", false, 401)]
	static void ResetKidsLocalPositionRecurse()
	{
		foreach(Transform transform in Selection.transforms)
			ResetKidsLocalPositionRecursive (transform, true);
	}

	static void ResetKidsLocalPositionRecursive (Transform transform, bool isRecursive)
	{
		foreach(Transform kid in transform)
    	{
    		kid.localPosition = Vector3.zero;
			if(isRecursive)
			{
				ResetKidsLocalPositionRecursive(kid,isRecursive);
			}
    	}
	}
	#endregion
	
	#region SelectKids
	[MenuItem("CONTEXT/Transform/Select Kids", false, 500)]
    static void SelectKids()
	{
		//Debug.Log(skip);
		if(skip > 0 && Selection.transforms.Length != 1)
		{
			skip --;
			return;
		}
		skip = Selection.transforms.Length;
		if(Selection.transforms.Length == 1)
			skip = 0;
		//List<Transform> tList = new List<Transform>();
		List<GameObject> goList = new List<GameObject>();
		//foreach(Transform transform in Selection.transforms)
		//{
			//Debug.Log("I am: " + transform.name);
			//tList.AddRange(SelectKidsRecursive (transform, false));
		//}
		
		//foreach(Transform t in tList)
		//{
		//	goList.Add(t.gameObject);
		//}
		
		//Selection.objects = goList.ToArray();
		foreach(Transform transform in Selection.transforms)
		{
			goList.AddRange(SelectKidsRecursive (transform, false));
		}
		Selection.objects = goList.ToArray();
		EditorApplication.RepaintHierarchyWindow();
	}
	
	[MenuItem("CONTEXT/Transform/Select Kids Recursive", false, 501)]
	static void SelectKidsRecurse()
	{
		//Debug.Log(skip);
		if(skip > 0 && Selection.transforms.Length != 1)
		{
			skip --;
			return;
		}
		skip = Selection.transforms.Length;
		if(Selection.transforms.Length == 1)
			skip = 0;
		List<GameObject> goList = new List<GameObject>();
		/*foreach(Transform transform in Selection.transforms)
		{
			//Debug.Log("I am: " + transform.name);
			tList.AddRange(SelectKidsRecursive (transform, true));
		}
		
		foreach(Transform t in tList)
		{
			goList.Add(t.gameObject);
		}
		
		Selection.objects = goList.ToArray();*/
		foreach(Transform transform in Selection.transforms)
		{
			goList.AddRange(SelectKidsRecursive (transform, true));
		}
		Selection.objects = goList.ToArray();
		EditorApplication.RepaintHierarchyWindow();
	}

    [MenuItem("CONTEXT/Transform/Select Kids Random", false, 500)]
    static void SelectKidsRandom()
    {
        //Debug.Log(skip);
        if(skip > 0 && Selection.transforms.Length != 1)
        {
            skip --;
            return;
        }
        skip = Selection.transforms.Length;
        if(Selection.transforms.Length == 1)
            skip = 0;
        //List<Transform> tList = new List<Transform>();
        List<GameObject> goList = new List<GameObject>();
        //foreach(Transform transform in Selection.transforms)
        //{
        //Debug.Log("I am: " + transform.name);
        //tList.AddRange(SelectKidsRecursive (transform, false));
        //}
        
        //foreach(Transform t in tList)
        //{
        //  goList.Add(t.gameObject);
        //}
        
        //Selection.objects = goList.ToArray();
        foreach(Transform transform in Selection.transforms)
        {
            goList.AddRange(SelectKidsRecursive (transform, false));
        }

        for (int i = 0; i < goList.Count; i++) 
        {
            Random.seed ++;
            if(Random.value >= .5f)
            {
                goList.RemoveAt(i);
                i--;
            }
        }

        Selection.objects = goList.ToArray();
        EditorApplication.RepaintHierarchyWindow();
    }

    

	static GameObject[] SelectKidsRecursive (Transform transform, bool isRecursive)
	{
		//List<Transform> kidsToSearch = new List<Transform>();
		List<GameObject> kidsToAdd = new List<GameObject>();
		
		if(isRecursive)
		{
			Transform[] justTKids = transform.GetComponentsInChildren<Transform>(true);
			//Debug.Log(justTKids.Length);
			GameObject[] justKids = new GameObject[justTKids.Length];
			for(int i = 0; i < justTKids.Length; i ++)
				justKids[i] = justTKids[i].gameObject;
			return justKids;
		}
		
		foreach(Transform kid in transform)
		{
			kidsToAdd.Add(kid.gameObject);
		}
		
		/*kidsToSearch.AddRange(transform.GetComponentsInChildren<Transform>(true));
		foreach(Transform kid in transform.GetComponentsInChildren<Transform>(true))
		{
			kidsToAdd.Add(kid.gameObject);
		}
		while(kidsToSearch.Count > 0)
		{
			Transform[] newKids = kidsToSearch[0].GetComponentsInChildren<Transform>(true);
			kidsToSearch.AddRange(newKids);
			foreach(Transform kid in newKids)
			{
				kidsToAdd.Add(kid.gameObject);
			}
			kidsToSearch.RemoveAt(0);
		}*/
		
		
		/*foreach(Transform kid in transform)
    	{
    		tArray.Add(kid);
			//Debug.Log("Added: " + kid.name);
			if(isRecursive)
			{
				tArray.AddRange(SelectKidsRecursive(kid,isRecursive, tArray));
			}
    	}*/
		
		return kidsToAdd.ToArray();
	}
	#endregion
	
	
	public void OnHierarchyChange()
	{
		Debug.Log("Called");
		SceneView.RepaintAll();
		Debug.Log("Called");
	}
}
#endif
