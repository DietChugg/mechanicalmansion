#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Collections;
using UnityEngine.UI;

public class ButtonContextMenu 
{

	[MenuItem("CONTEXT/Button/Set GameObject Name To Image Name", false, 20)]
	static void RenameToImageName()
	{
		for (int i = 0; i < Selection.gameObjects.Length; i++) 
		{
			Selection.gameObjects[i].name = Selection.gameObjects[i].transform.GetComponent<Image>().sprite.name;
		}
	}

	[MenuItem("CONTEXT/Button/Set GameObject Name To Text Name", false, 11)]
	static void RenameToTextName()
	{
		for (int i = 0; i < Selection.gameObjects.Length; i++) 
		{
			Selection.gameObjects[i].name = Selection.gameObjects[i].transform.Find("Text").GetComponent<Text>().text;
		}
	}

}
#endif
