#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Collections;

public static class TransformMenu
{

	// Use this for initialization
	[MenuItem("CONTEXT/Transform/SnapToGrid", false, 152)]
	public static void SnapToGrid () 
	{
		Debug.Log("Snap");
		foreach (Transform transform in Selection.transforms)
		{
			transform.position = new Vector3(Mathf.RoundToInt(transform.position.x),Mathf.RoundToInt(transform.position.y),Mathf.RoundToInt(transform.position.z));
		}
	}
}
#endif
