#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
 
public class AnimationContextMenu: EditorWindow
{
	[MenuItem("CONTEXT/Animation/Play Clip", false, 209)]
	static void PlayClip()
	{
		Undo.RecordObject(Selection.activeTransform, "Play Clip");
		GameObject selectedGameObject = Selection.activeGameObject;
		selectedGameObject.GetComponent<Animation>().Play(selectedGameObject.GetComponent<Animation>().clip.name);
	}
	
	public void OnHierarchyChange()
	{
		SceneView.RepaintAll();
	}
}
#endif
