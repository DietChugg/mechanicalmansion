#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;

public class TextMeshContextMenu : EditorWindow 
{

	[MenuItem("CONTEXT/TextMesh/SetNameToText", false, 150)]
	static void SetNameToText()
	{
		foreach(Transform selection in Selection.transforms)
		{
			selection.name = selection.GetComponent<TextMesh>().text;
		}
	}

	[MenuItem("CONTEXT/TextMesh/SetTextToName", false, 151)]
	static void SetTextToName()
	{
		foreach(Transform selection in Selection.transforms)
		{
			selection.GetComponent<TextMesh>().text = selection.name;
		}
	}
}
#endif
