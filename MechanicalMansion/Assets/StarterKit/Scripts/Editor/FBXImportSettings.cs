using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections;
public class FBXImportSettings : SingletonScriptableObject<FBXImportSettings> 
{
	public float importScale = 0.01f;
	
#if UNITY_EDITOR
	[MenuItem ("Edit/Project Settings/FBXImportSettings")]
	static void DrawHierarchyDataMenuItem () 
	{
		Selection.activeObject = (UnityEngine.Object)FBXImportSettings.Instance;
	}
#endif
}
