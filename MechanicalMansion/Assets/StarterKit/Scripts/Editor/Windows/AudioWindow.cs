#if UNITY_4
using UnityEngine;
using System.Collections;
using UnityEditor;

public class AudioWindow : EditorWindow
{
	string myString = "Hello World";
	Rect showRect;
	AudioClip clip;
	GUIContent content;
	Texture2D audioTexture;
	bool generateNewTexture;
    [MenuItem ("Window/Chugg/AudioWindow")]
    static void Init () 
	{
		AudioWindow window = (AudioWindow)EditorWindow.GetWindow (typeof (AudioWindow));
		if(window)
		{
			
		}
    }
    
    void OnGUI () 
	{
        GUILayout.Label ("Base Settings", EditorStyles.boldLabel);
            myString = EditorGUILayout.TextField ("Text Field", myString);
		showRect = EditorGUILayout.RectField(showRect);
		//InputPlus.testWord = myString;
		clip = (AudioClip)EditorGUILayout.ObjectField(clip,typeof(AudioClip),false);
		generateNewTexture = EditorGUILayout.Toggle("Generate New AudioTeture",generateNewTexture);

		if(generateNewTexture)
		{
			generateNewTexture = false;
			audioTexture = AudioWaveform(clip,(int)(EditorWindow.focusedWindow.maxSize.x-EditorWindow.focusedWindow.minSize.x),80,Color.green);
			content = new GUIContent(audioTexture);
		}

		if(content != null)
			EditorGUILayout.LabelField(content);


//		PlayerPrefs.SetString("StaticInput",myString);
//		Debug.Log("Called InputPlusGUI");
    }

	public static Texture2D AudioWaveform(AudioClip aud, int width, int height, Color color)
	{
		
		int step = Mathf.CeilToInt((aud.samples * aud.channels) / width);
		float[] samples = new float[aud.samples * aud.channels];
		
		//workaround to prevent the error in the function getData
		//when Audio Importer loadType is "compressed in memory"
		string path = AssetDatabase.GetAssetPath(aud);
		AudioImporter audioImporter = AssetImporter.GetAtPath(path) as AudioImporter;
		AudioClipLoadType audioLoadTypeBackup = audioImporter.loadType;
		audioImporter.loadType = AudioClipLoadType.DecompressOnLoad;
		AssetDatabase.ImportAsset(path);
		
		//getData after the loadType changed
		aud.GetData(samples, 0);
		
		//restore the loadType (end of workaround)
		audioImporter.loadType = audioLoadTypeBackup;
		AssetDatabase.ImportAsset(path);
		
		Texture2D img = new Texture2D(width, height, TextureFormat.RGBA32, false);
		
		Color[] xy = new Color[width * height];
		for (int x = 0; x < width * height; x++)
		{
			xy[x] = new Color(0, 0, 0, 0);
		}
		
		img.SetPixels(xy);
		
		int i = 0;
		while (i < width)
		{
			int barHeight = Mathf.CeilToInt(Mathf.Clamp(Mathf.Abs(samples[i * step]) * height, 0, height));
			int add = samples[i * step] > 0 ? 1 : -1;
			for (int j = 0; j < barHeight; j++)
			{
				img.SetPixel(i, Mathf.FloorToInt(height / 2) - (Mathf.FloorToInt(barHeight / 2) * add) + (j * add), color);
			}
			++i;
			
		}
		
		img.Apply();
		return img;
	}
}
#endif
