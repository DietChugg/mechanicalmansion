#if UNITY_EDITOR
using UnityEngine;
using System.Collections;
using UnityEditor;

public class SampleWindow : EditorWindow
{
	string myString = "Hello World";
    bool groupEnabled;
    bool myBool = true;
    float myFloat = 1.23f;
    
    [MenuItem ("Window/SampleWindow")]
    static void Init () 
	{
		SampleWindow window = (SampleWindow)EditorWindow.GetWindow (typeof (SampleWindow));
		if(window)
		{
			
		}
    }
    
    void OnGUI () 
	{
        GUILayout.Label ("Base Settings", EditorStyles.boldLabel);
            myString = EditorGUILayout.TextField ("Text Field", myString);
        
        groupEnabled = EditorGUILayout.BeginToggleGroup ("Optional Settings", groupEnabled);
            myBool = EditorGUILayout.Toggle ("Toggle", myBool);
            myFloat = EditorGUILayout.Slider ("Slider", myFloat, -3, 3);
        EditorGUILayout.EndToggleGroup ();
    }
	
	void Awake()
	{
		Debug.Log("Called Awake");
	}
	
	void Start()
	{
		Debug.Log("Called Start");
	}
	
	void Update()
	{
		Debug.Log("Called Update");
	}
}
#endif
