using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using StarterKit;

public class WrapClassInNameSpace_Window : EditorWindow 
{
    static WrapClassInNameSpace_Window wrapperWindow;
    string nameSpace = "";
	[MenuItem("Tools/Wrap Classes Into Namespace")]
	static void Init () 
    {
        wrapperWindow = new WrapClassInNameSpace_Window();
        wrapperWindow.ShowPopup();
	}
	
	void OnGUI () 
    {
        nameSpace = EditorGUILayout.TextField("nameSpace: ", nameSpace);
        if (GUILayout.Button("Pressed") || Event.current.keyCode == KeyCode.Return)
        {
            for (int i = 0; i < Selection.objects.Length; i++)
            {
                string path = Application.dataPath.Substring(0, Application.dataPath.Length - 6) + AssetDatabase.GetAssetPath(Selection.objects[i]);
                Debug.Log(path);
                string[] lines = StreamReaderX.GetTextLines(path);

                //bool insideComment = false;
                List<string> linesList = lines.ToList();
                for (int j = 0; j < linesList.Count; j++)
                {
                    Debug.Log(linesList[j]);
                    if(linesList[j].Contains("class"))
                    {
                        Debug.Log("Has Class");
                        linesList.Insert(j, "{");
                        linesList.Insert(j, "namespace " + nameSpace);
                        linesList.Add("}");  
                        j = linesList.Count;
                    }
                      
                }
                

                StreamReaderX.SetTextLines(path,linesList.ToArray());
                AssetDatabase.ImportAsset(path, ImportAssetOptions.ForceUpdate | ImportAssetOptions.ImportRecursive);
            }
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
            if(wrapperWindow != null)
                wrapperWindow.Close();
        }
	}
}
