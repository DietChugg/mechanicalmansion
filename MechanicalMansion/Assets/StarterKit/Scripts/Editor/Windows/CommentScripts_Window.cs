using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using StarterKit;

public class CommentScripts_Window : EditorWindow 
{
    static CommentScripts_Window wrapperWindow;
    bool toggleOn = true;
	[MenuItem("Tools/Comment Scripts")]
	static void Init () 
    {
        wrapperWindow = new CommentScripts_Window();
        wrapperWindow.ShowPopup();
	}
	
	void OnGUI () 
    {
        toggleOn = EditorGUILayout.Toggle("Toggle Comments: ", toggleOn);
        if (GUILayout.Button("Pressed") || Event.current.keyCode == KeyCode.Return)
        {
            for (int i = 0; i < Selection.objects.Length; i++)
            {
                string path = Application.dataPath.Substring(0, Application.dataPath.Length - 6) + AssetDatabase.GetAssetPath(Selection.objects[i]);
                Debug.Log(path);
                string[] lines = StreamReaderX.GetTextLines(path);

                //bool insideComment = false;
                List<string> linesList = lines.ToList();
                for (int j = 0; j < linesList.Count; j++)
                {
                    if (toggleOn)
                    {
                        linesList[j] = "//" + linesList[j];
                    }
                    else
                    {
                        if (linesList[j].Substring(0, 2) == "//")
                        {
                            linesList[j] = linesList[j].TrimStart(new char[] { '/', '/' });
                        }
                    }
                      
                }
                

                StreamReaderX.SetTextLines(path,linesList.ToArray());
                AssetDatabase.ImportAsset(path, ImportAssetOptions.ForceUpdate | ImportAssetOptions.ImportRecursive);
            }
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
            if(wrapperWindow != null)
                wrapperWindow.Close();
        }
	}
}
