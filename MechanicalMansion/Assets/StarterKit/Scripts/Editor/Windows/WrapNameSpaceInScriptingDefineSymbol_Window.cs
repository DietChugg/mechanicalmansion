using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using StarterKit;

public class WrapNameSpaceInScriptingDefineSymbol_Window : EditorWindow 
{
    static WrapNameSpaceInScriptingDefineSymbol_Window wrapperWindow;
    string nameSpace = "";
    string scriptingDefineSymbol = "";
    [MenuItem("Tools/Wrap Namespace Into ScriptingDefineSymbol")]
	static void Init () 
    {
        wrapperWindow = new WrapNameSpaceInScriptingDefineSymbol_Window();
        wrapperWindow.ShowPopup();
	}
	
	void OnGUI () 
    {
        nameSpace = EditorGUILayout.TextField("nameSpace: ", nameSpace);
        scriptingDefineSymbol = EditorGUILayout.TextField("scriptingDefineSymbol: ", scriptingDefineSymbol);
        if (GUILayout.Button("Pressed") || Event.current.keyCode == KeyCode.Return)
        {
            //Attempts to Find All Scripts In Project WIP
            //Object[] objects = AssetDatabaseX.FindAllCSScripts(".cs");
            //Debug.Log(Application.dataPath);
            //Debug.Log(objects.Length);

            //for (int i = 0; i < objects.Length; i++)
            //{
            //    Debug.Log(objects[i]);
            //}

            for (int i = 0; i < Selection.objects.Length; i++)
            {
                bool hasNamespace = false;
                string path = Application.dataPath.Substring(0, Application.dataPath.Length - 6) + AssetDatabase.GetAssetPath(Selection.objects[i]);
                Debug.Log(path);
                string[] lines = StreamReaderX.GetTextLines(path);

                //bool insideComment = false;
                List<string> linesList = lines.ToList();
                for (int j = 0; j < linesList.Count; j++)
                {
                    if(linesList[j].Contains("namespace " + nameSpace))
                    {
                        hasNamespace = true;
                        j = linesList.Count;
                    }
                      
                }
                if (hasNamespace)
                {
                    Debug.Log("Has Class");
                    linesList.Insert(0,"#if " + scriptingDefineSymbol);
                    linesList.Add("#endif");
                }

                StreamReaderX.SetTextLines(path,linesList.ToArray());
                AssetDatabase.ImportAsset(path, ImportAssetOptions.ForceUpdate | ImportAssetOptions.ImportRecursive);
            }

            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
            if(wrapperWindow != null)
                wrapperWindow.Close();
        }
	}
}
