#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(SnappingAssetDropper))]
public class SnappingAssetDropperEditor : Editor 
{
	SnappingAssetDropper sad;
	bool mouseDown;
	int prevIndex = 0;
	float scrollAmount = 0;

	void OnEnable()
	{
		sad = (SnappingAssetDropper)target;
		SceneView.onSceneGUIDelegate += EventThing;
		if(sad.snap == null)
		{
			sad.snap = new GameObject("Snap");

		}
	}

	void OnDisable()
	{
		SceneView.onSceneGUIDelegate -= EventThing;
	}

	void OnInspectorUpdate()
	{

		DrawDefaultInspector();
	}

	void RemoveEmptySpots ()
	{
		if (sad.deleteAllSpots) {
			sad.deleteAllSpots = false;
			for (int i = 0; i < sad.spots.Count; i++) {
				Undo.DestroyObjectImmediate (sad.spots [i].transform.gameObject);
			}
			sad.spots.Clear ();
		}
	}

	void EventThing (SceneView sceneView) 
	{
		if (!sad.eatEvents)
		{	
			if(sad.previewAsset)
				if(sad.previewAsset.gameObject.activeSelf)
					sad.previewAsset.SetActive(false);
			return;
		}
		if(sad.previewAsset)
		{
			if(!sad.previewAsset.gameObject.activeSelf)
				sad.previewAsset.SetActive(true);
			sad.previewAsset.hideFlags = HideFlags.HideInHierarchy;
		}

		RemoveEmptySpots ();

		sceneView.orthographic = true;

		if (sad.index < 0)
			sad.index = 0;
		if (sad.index > sad.assetGroups.Count - 1)
			sad.index = sad.assetGroups.Count - 1;

		for(int i = 0; i < sad.spots.Count; i++)
		{
			if(sad.spots[i].transform == null)
			{
				sad.spots.RemoveAt(i);
				i--;
			}
		}

		if (sad.previewAsset == null)
		{
			sad.previewAsset = (Transform)PrefabUtility.InstantiatePrefab (sad.TransformAtIndex());
			sad.previewAsset.rotation = Quaternion.Euler(sad.rotationOffset);
		}
		else if(prevIndex != sad.AssetGroupAtIndex().index)
		{
			//Debug.Log("The Id Has Changed");
			Undo.DestroyObjectImmediate(sad.previewAsset.gameObject);
			sad.previewAsset = (Transform)PrefabUtility.InstantiatePrefab (sad.TransformAtIndex());
			sad.previewAsset.rotation = Quaternion.Euler(sad.rotationOffset);
		}
		else
		{
			if(sad.previewAsset.rotation != Quaternion.Euler(sad.rotationOffset))
				sad.previewAsset.rotation = Quaternion.Euler(sad.rotationOffset);
			if(Event.current.type == EventType.MouseMove || Event.current.type == EventType.MouseDrag)
			{
				sad.previewAsset.transform.position = sceneView.camera.ScreenToWorldPoint(new Vector3(Event.current.mousePosition.x,sceneView.camera.pixelHeight -Event.current.mousePosition.y,-sceneView.pivot.z));
				sad.previewAsset.transform.position = new Vector3(Mathf.Round(sad.previewAsset.transform.position.x),Mathf.Round(sad.previewAsset.transform.position.y),sad.transform.position.z+1);
			}
			Vector3[] points = new Vector3[]
			{
				(sad.previewAsset.position + Vector3.up*.5f + Vector3.right*-.5f),
				(sad.previewAsset.position + Vector3.up*.5f + Vector3.right*.5f),
				(sad.previewAsset.position + Vector3.up*-.5f + Vector3.right*.5f),
				(sad.previewAsset.position + Vector3.up*-.5f + Vector3.right*-.5f)
			};
			Handles.DrawSolidRectangleWithOutline(points,Color.clear,Color.yellow);

			if(Event.current.type == EventType.MouseDown)
			{
				mouseDown = true;
			}

			if(Event.current.type == EventType.MouseUp)
			{
				mouseDown = false;
			}
			#region DropTilesDown
			if(Event.current.type == EventType.MouseDown || (Event.current.type == EventType.MouseDrag && mouseDown))
			{
				if(Event.current.button == 1)
				{
					bool canPlace = true;
					Spot oldSpot = null;
					foreach(Spot spot in sad.spots)
					{
						if(spot.coordinates == (Vector2)sad.previewAsset.transform.position)
						{
								Undo.DestroyObjectImmediate(spot.transform.gameObject);
								oldSpot = spot;
						}
					}
					if(oldSpot != null && canPlace)
					{
						//warnings are dumb
					}
				}


				if(Event.current.button == 0)
				{
					bool canPlace = true;
					Spot oldSpot = null;
					foreach(Spot spot in sad.spots)
					{
						if(spot.coordinates == (Vector2)sad.previewAsset.transform.position
						   && spot.name == sad.AssetGroupAtIndex().name)
						{
							if(spot.transform.name != sad.previewAsset.transform.name)
							{
								Undo.DestroyObjectImmediate(spot.transform.gameObject);
								oldSpot = spot;
								//Debug.Log("Swap Found");
							}
							else
							{
								canPlace = false;
								//Debug.Log("Match Found");
							}
						}
					}

					if(oldSpot != null)
					{
						sad.spots.Remove(oldSpot);
					}

					if(canPlace)
					{
						//Debug.Log("Can Place");

						Transform newTransform = (Transform)PrefabUtility.InstantiatePrefab(sad.TransformAtIndex());
						newTransform.position = sad.previewAsset.transform.position + Vector3.forward*-1;
						newTransform.rotation = Quaternion.Euler(sad.rotationOffset);
						Spot spot = new Spot(sad.AssetGroupAtIndex().name,newTransform,sad.previewAsset.transform.position);
						Undo.RegisterCreatedObjectUndo (newTransform.gameObject, "Created Tile");
						sad.spots.Add(spot);
						spot.transform.parent = sad.snap.transform;
                        spot.transform.SetZPosition(sad.snap.transform.position.z);
					}
				}
				Selection.activeObject = sad.gameObject;
			}
			#endregion
		}

		if(Event.current.type == EventType.ScrollWheel)
		{
			if(Event.current.modifiers == EventModifiers.Shift)
			{
				scrollAmount += Event.current.delta.x;
				if(scrollAmount > sad.sensitivity)
				{
					scrollAmount = 0f;
					sad.index ++;
					if(sad.loops)
					{
						sad.index = sad.index % sad.assetGroups.Count;
					}
					else
					{
						sad.index = Mathf.Clamp(sad.index, 0, sad.assetGroups.Count-1);
					}

				}
				else if(scrollAmount < -sad.sensitivity)
				{
					scrollAmount = 0f;
					sad.index --;
					if(sad.loops)
					{
						if(sad.index < 0)
							sad.index = sad.assetGroups.Count-1;
					}
					else
					{
						sad.index = Mathf.Clamp(sad.index, 0, sad.assetGroups.Count-1);
					}
				}
				ResetPreviewAsset();
			}

			if(Event.current.modifiers == EventModifiers.Alt)
			{
				scrollAmount += Event.current.delta.y;
				if(scrollAmount > sad.sensitivity)
				{
					scrollAmount = 0f;
					sad.AssetGroupAtIndex().index ++;
					if(sad.loops)
					{
						sad.AssetGroupAtIndex().index = sad.AssetGroupAtIndex().index % sad.AssetGroupAtIndex().prefabs.Count;
					}
					else
					{
						sad.AssetGroupAtIndex().index = Mathf.Clamp(sad.AssetGroupAtIndex().index, 0, sad.AssetGroupAtIndex().prefabs.Count-1);
					}
				}
				else if(scrollAmount < -sad.sensitivity)
				{
					scrollAmount = 0f;
					sad.AssetGroupAtIndex().index --;
					if(sad.loops)
					{
						if(sad.AssetGroupAtIndex().index < 0)
							sad.AssetGroupAtIndex().index = sad.AssetGroupAtIndex().prefabs.Count-1;
					}
					else
					{
						sad.AssetGroupAtIndex().index = Mathf.Clamp(sad.AssetGroupAtIndex().index, 0, sad.AssetGroupAtIndex().prefabs.Count-1);
					}
				}
				ResetPreviewAsset();
			}
			else
			{
				SceneView.lastActiveSceneView.size += Event.current.delta.y * 3f;
			}
		}
		if(Event.current.type == EventType.MouseDrag)
		{
			if(Event.current.button == 2)
			{
				float distanceAway = Mathf.Abs(sad.transform.position.z - SceneView.lastActiveSceneView.size)/555;
				sad.camPosition += new Vector3(-Event.current.delta.x*distanceAway,Event.current.delta.y*distanceAway,0);
			}
		}
		prevIndex = sad.AssetGroupAtIndex().index;
		SceneView.lastActiveSceneView.pivot = sad.camPosition;
		SceneView.lastActiveSceneView.rotation = Quaternion.RotateTowards(SceneView.lastActiveSceneView.rotation,Quaternion.Euler(new Vector3(0,0,0)),1.30f);
		SceneView.lastActiveSceneView.Repaint();
		EndEvent();
	}

	void ResetPreviewAsset()
	{
		Vector3 pos = sad.previewAsset.position;
		Undo.DestroyObjectImmediate(sad.previewAsset.gameObject);
		sad.previewAsset = (Transform)PrefabUtility.InstantiatePrefab (sad.TransformAtIndex());
		sad.previewAsset.position = pos;
		sad.previewAsset.rotation = Quaternion.Euler(sad.rotationOffset);
	}

	void EndEvent()
	{
		int controlID = GUIUtility.GetControlID (FocusType.Passive);
		
		if (Event.current.type == EventType.Layout) 
		{
			HandleUtility.AddDefaultControl (controlID);
		}
		Event.current.Use();
	}
}
#endif
