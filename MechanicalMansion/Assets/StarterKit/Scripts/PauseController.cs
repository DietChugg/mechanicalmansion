using UnityEngine;
using System;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using StarterKit;

public class PauseController : Singleton<PauseController>
{
    public static Action<bool> OnPause;
    public float timeSpentPaused;
    public float lastPauseLength;

    bool _isPaused;
    float _timeScale = 1f;

    static Dictionary<AudioSource, float> pausedAudio = new Dictionary<AudioSource,float>();

    public void Awake()
    {
        Time.timeScale = 1f;
    }

    public bool IsPaused
    {
        get
        {
            return _isPaused;
        }
        set
        {
            if (_isPaused == value)
                return;
            _isPaused = value;
            if (OnPause != null)
            {
                OnPause(_isPaused);
            }
            if (_isPaused)
            {
                _timeScale = Time.timeScale;
                Time.timeScale = 0;
                StartCoroutine(TrackPauseTime());
                AudioSource[] audioSources = UnityEngine.Object.FindObjectsOfType<AudioSource>();
                for (int i = 0; i < audioSources.Length; i++)
                {
                    if (audioSources[i].isPlaying)
                    {
                        pausedAudio.Add(audioSources[i], audioSources[i].time);
                        audioSources[i].Stop();
                    }
                }
            }
            else
            {
                Time.timeScale = _timeScale;
                foreach (KeyValuePair<AudioSource,float> keyValuePair in pausedAudio)
                {
                    keyValuePair.Key.time = keyValuePair.Value;
                    keyValuePair.Key.Play();
                }
                pausedAudio.Clear();
            }
        }
    }

    public IEnumerator TrackPauseTime()
    {
        //Stopwatch stopwatch = new Stopwatch();
        //stopwatch.Start();
        lastPauseLength = 0f;
        while (_isPaused)
        {
            timeSpentPaused += Time.unscaledDeltaTime;
            lastPauseLength += Time.unscaledDeltaTime;
            yield return null;
        }
        //stopwatch.Stop();
        //timeSpentPaused += (float)(stopwatch.Elapsed.TotalMilliseconds / 1000d);
        //lastPauseLength = (float)(stopwatch.Elapsed.TotalMilliseconds / 1000d);
        //UnityEngine.Debug.Log("Time SPent Paused: " + timeSpentPaused);
        yield return null;
    }

}
