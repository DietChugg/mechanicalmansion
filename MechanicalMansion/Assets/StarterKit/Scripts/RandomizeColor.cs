using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RandomizeColor : MonoBehaviour 
{
	public List<Color> colors = new List<Color>();

	void OnEnable () 
	{
		GetComponent<Renderer>().material.color = colors.GetRandom();
	}
	
}
