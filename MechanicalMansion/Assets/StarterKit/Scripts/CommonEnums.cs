using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class CommonEnums
{
	  
}

public enum Axis
{
    X,
    Y,
    Z
}

public enum UnityLifetime
{
    None,
    Awake,
    Start,
    OnEnable,
    OnDisable,
    FixedUpdate,
    FixedUpdateCoRoutine,
    Update,
    UpdateCoRoutine,
    LateUpdate,
    OnGUI,
    WaitTillEndOfFrame
}

public enum GizmosShowType
{ 
    None,
    OnDrawGizmos,
    OnDrawGizmosSelected
}


