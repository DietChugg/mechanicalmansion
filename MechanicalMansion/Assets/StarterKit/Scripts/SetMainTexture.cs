using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SetMainTexture : MonoBehaviour 
{
	[Header("  (Defaults to renderer.material)")]
	public Material material;
	[Space(10)]
	public List<Texture2D> textures = new List<Texture2D>();
	public float textureIndex;

	public bool awake;
	public bool start;
	public bool onEnable;
	public bool update;


	void Awake () 
	{
		if(awake)
			Set();
	}

	void Start () 
	{
		if(start)
			Set();
	}

	void OnEnable () 
	{
		if(onEnable)
			Set();
	}

	void Update () 
	{
		if(update)
			Set();
	}

	void Set()
	{
		if(!material)
			material = this.GetComponent<Renderer>().material;
		
		material.mainTexture = textures[(int)textureIndex];
	}
}
