using UnityEngine;
using System.Collections;
using System.Collections.Generic;
//using UnityEditor;
using System.Reflection;

public class BezierCurve : MonoBehaviour 
{
	
	//[Header("   Bez Curve")]
	[Range(0.01f, 0.25f)]
	public float spacing = 0.1f;
	//[HideInInspector]
	public List<Transform> 		controlPoints = new List<Transform>();
	public List<List<Transform>> bezCurvesList = new List<List<Transform>>(); 
	public List<Vector3> 		bezPoints = new List<Vector3>();
	[Range(0.0f, 1f)]
	public float 				t;

	//[Header("   Resampling")]
	public bool 				resample;
	public float				resampleDistance;
	public List<Vector3> 		resPoints = new List<Vector3>();

	//[Header("   Drawing")]
	public bool 				drawHandles = true;
	public Color				handleColor = Color.red;
	public bool 				drawCurve = true;
	public Color				pathColor = Color.green;
	public Color				resampleColor = Color.yellow;

	[ContextMenu("Add Point")]
	public void AddPoint()
	{
		if(controlPoints.Count >= 1)
			AddPoint(controlPoints[(controlPoints.Count-1)].position + Vector3.right);
		else
			AddPoint(transform.position);
	}
	
	public void AddPoint(Vector3 position)
	{
		if(controlPoints.Count == 0)
		{
			GameObject newObj = new GameObject("P"+controlPoints.Count);
			newObj.transform.position = position;
			controlPoints.Add(newObj.transform);
			newObj.transform.parent = transform;
		}

		for(int i = 0; i < 3; i++)
		{
			GameObject newObj = new GameObject("P"+controlPoints.Count);
			newObj.transform.position = position;
			controlPoints.Add(newObj.transform);
			newObj.transform.parent = transform;
		}
		controlPoints[controlPoints.Count-3].parent = controlPoints[controlPoints.Count-4];
		controlPoints[controlPoints.Count-2].parent = controlPoints[controlPoints.Count-1];
	}
	
	//[ContextMenu("Clear Points")]
	public void ClearPoints()
	{
		for(int i = 0; i < controlPoints.Count; i++)
		{
			if(controlPoints[i] != null)
				DestroyImmediate(controlPoints[i].gameObject);
		}
		
		controlPoints.Clear();
		bezCurvesList.Clear();
	}

	public void OnEnable()
	{
		BuildBez(false);
	}

	public void OnDrawGizmos()
	{
		if(Application.isPlaying)
			return;
		
		BuildBez(true);
	}

	[ContextMenu("Build Bez")]
	public void BuildBez()
	{
		BuildBez(false);
	}

	public void BuildBez(bool drawGizmos)
	{
		for(int i = 0; i < controlPoints.Count; i++)
		{
			if(controlPoints[i] == null)
			{
				controlPoints.RemoveAt(i);
				i--;
			}
		}
		
		#region BEZLISTGROUPING
		if(controlPoints.Count < 2)
			return;

		spacing = Mathf.Clamp(spacing,.01f,1f);
		bezCurvesList = new List<List<Transform>>();
		List<Transform> pointGroup = new List<Transform>();
		for(int i = 0; i < controlPoints.Count; i++)
		{
			pointGroup.Add(controlPoints[i]);
			if(pointGroup.Count == 4)
			{
				bezCurvesList.Add(pointGroup);
				pointGroup = new List<Transform>();
				pointGroup.Clear();
				pointGroup.Add(controlPoints[i]);
			}
		}
		if(pointGroup.Count > 1)
		{
			bezCurvesList.Add(pointGroup);
		}
		
		pointGroup = new List<Transform>();
		pointGroup.Clear();
		#endregion
		
		#region DRAWHANDLES
		if(drawGizmos && drawHandles)
		{
			Gizmos.color = handleColor;
			for(int i = 1; i < controlPoints.Count; i++)
			{
				if(i%3 != 2)
					Gizmos.DrawLine(controlPoints[i-1].position,controlPoints[i].position);
			}
		}
		#endregion
		
		#region DRAWBEZIERCURVE
		if(drawGizmos)
			Gizmos.color = pathColor;
		bezPoints.Clear();
		if(resample)
			resPoints.Clear();
		for(int i =0; i < bezCurvesList.Count; i++)
		{
			float f =0;
			for(float j = spacing; j < 1; j += spacing)
			{
				if(drawGizmos && drawCurve)
					Gizmos.DrawLine(GetTPoint(bezCurvesList[i],j - spacing),GetTPoint(bezCurvesList[i],j));
				bezPoints.Add(GetTPoint(bezCurvesList[i],j - spacing));
				f = j;
			}
			
			if(drawGizmos)
				Gizmos.DrawLine(GetTPoint(bezCurvesList[i],f),GetTPoint(bezCurvesList[i],1));
			bezPoints.Add(GetTPoint(bezCurvesList[i],f));
			if(resample)
			{
				if(resampleDistance <= 0)
					resampleDistance = .1f;
				Vector3[] newSample = EquadistanceResample(bezPoints.Count,resampleDistance,bezCurvesList[i]);
				resPoints.AddRange(newSample);

				//Hopefully cleaning up that stupid flickering endcap in GLTexturedQuads
				if(i < bezCurvesList.Count - 1)
					resPoints.RemoveLast();
			}
		}
		bezPoints.Add(GetTPoint(bezCurvesList[bezCurvesList.Count-1],1));
		if(resample)
		{
			resPoints.Add(GetTPoint(bezCurvesList[bezCurvesList.Count-1],1));
			
			if(drawGizmos && drawCurve)
			{
				Gizmos.color = resampleColor;
				for(int i = 0; i < resPoints.Count-1; i++)
				{
					int next = i+1;
					Gizmos.DrawLine(resPoints[i],resPoints[next]);
				}
			}
		}
		#endregion
		
	}

	public Vector3 GetTPoint(List<Transform> lPoints, float t)
	{
		t = Mathf.Clamp01(t);
		Vector3 calculatedPoint = new Vector3();
		int n = lPoints.Count -1;
		for(int i = 0; i < lPoints.Count; i++)
		{
			calculatedPoint.Set(
				calculatedPoint.x + (Factorial(n)/(Factorial(i)*Factorial(n-i)))*Mathf.Pow(t,i)*Mathf.Pow(1-t,n-i)*lPoints[i].position.x,
				calculatedPoint.y + (Factorial(n)/(Factorial(i)*Factorial(n-i)))*Mathf.Pow(t,i)*Mathf.Pow(1-t,n-i)*lPoints[i].position.y,
				calculatedPoint.z + (Factorial(n)/(Factorial(i)*Factorial(n-i)))*Mathf.Pow(t,i)*Mathf.Pow(1-t,n-i)*lPoints[i].position.z);
		}
		return calculatedPoint;
	}

	public Vector3 GetTPoint(List<Vector3> lPoints, float t)
	{
		t = Mathf.Clamp01(t);
		Vector3 calculatedPoint = new Vector3();
		int n = lPoints.Count -1;
		for(int i = 0; i < lPoints.Count; i++)
		{
			calculatedPoint.Set(
				calculatedPoint.x + (Factorial(n)/(Factorial(i)*Factorial(n-i)))*Mathf.Pow(t,i)*Mathf.Pow(1-t,n-i)*lPoints[i].x,
				calculatedPoint.y + (Factorial(n)/(Factorial(i)*Factorial(n-i)))*Mathf.Pow(t,i)*Mathf.Pow(1-t,n-i)*lPoints[i].y,
				calculatedPoint.z + (Factorial(n)/(Factorial(i)*Factorial(n-i)))*Mathf.Pow(t,i)*Mathf.Pow(1-t,n-i)*lPoints[i].z);
		}
		return calculatedPoint;
	}

	public Vector3[] EquadistanceResample(int originalSampleSize, float worldDistance, List<Vector3> bezCurve)
	{
		int n = originalSampleSize;
		float s = 1.0f/(n-1);
		float[] tt = new float[n];
		Vector3[] PP = new Vector3[n];
		float[] cc = new float[n];

		for (int i = 0 ; i < n ; i++) 
		{
			tt[i] = i*s;
			PP[i] = GetTPoint(bezCurve,tt[i]);
			if (i > 0) cc[i] = Vector3.Distance(PP[i], PP[i-1]);
		}

		//Find Distances
		float totalDistance = 0f;
		for (int i = 0; i < PP.Length-1; i ++) 
		{
			int next = i+1;
			totalDistance += Vector3.Distance(PP[i],PP[next]);
		}
		if (worldDistance <= 0)
			worldDistance = 0.1f;
		int totalNewPoints = (int)(totalDistance/worldDistance);

		// Get fractional arclengths along polyline
		float[] dd = new float[n];
		dd[0] = 0;
		for (int i = 1; i < n; i++) dd[i] = dd[i-1] + cc[i];
		for (int i = 1; i < n; i++) dd[i] = dd[i]/dd[n-1];
		
		// Number of points to place on curve
		int m = totalNewPoints;
		float step = 1.0f/(m-1);
		Vector3[] QQ = new Vector3[m];
		
		for (int r = 0 ; r < m ; r++)
		{
			float d = r*step;
			int i = FindIndex(dd, d);
			float u = (d - dd[i]) / (dd[i+1] - dd[i]);
			float t = (i + u)*s;
			QQ[r] = GetTPoint(bezCurve,t);
		}
		return QQ;
	}

	public Vector3[] EquadistanceResample(int originalSampleSize, float worldDistance, List<Transform> bezCurve)
	{
		List<Vector3> coords = new List<Vector3>();
		for (int i = 0; i < bezCurve.Count; i++) 
		{
			coords.Add(bezCurve[i].position);
		}
		return EquadistanceResample (originalSampleSize, worldDistance, coords);
	}

	public float CalcLength()
	{
		return CalcLength(bezPoints);
	}

	public float CalcLength(List<Vector3> lPoints)
	{
		float distance = 0;
		for(int i = 1; i < lPoints.Count; i++)
		{
			distance += Vector3.Distance(lPoints[i-1],lPoints[i]);
		}
		return distance;
	}

	public static int FindIndex(float[] dd, float d)
	{
		int i = 0;
		for (int j = 0 ; j < dd.Length ; j++)
		{
			if (d > dd[j]) i = j;
		}
		return i;
	}

	int Factorial (int n)
	{ 
	    if(n==0) return(1);
	 	return (n * Factorial(n-1) );
	}
}
