using UnityEngine;
using System.Collections;
using System.Collections.Generic;

#region String Keys
[System.Serializable]
public class SDictionaryStringGameObject : SerializableDictionary<string,GameObject>{}

[System.Serializable]
public class SDictionaryStringTransform : SerializableDictionary<string,Transform>{}

[System.Serializable]
public class SDictionaryStringString : SerializableDictionary<string,string>{}

[System.Serializable]
public class SDictionaryStringInt : SerializableDictionary<string,int>{}

[System.Serializable]
public class SDictionaryStringFloat : SerializableDictionary<string,float>{}
#endregion

#region Int Keys
[System.Serializable]
public class SDictionaryIntGameObject : SerializableDictionary<int,GameObject>{}

[System.Serializable]
public class SDictionaryIntTransform : SerializableDictionary<int,Transform>{}

[System.Serializable]
public class SDictionaryIntString : SerializableDictionary<int,string>{}

[System.Serializable]
public class SDictionaryIntInt : SerializableDictionary<int,int>{}

[System.Serializable]
public class SDictionaryIntFloat : SerializableDictionary<int,float>{}
#endregion

