using UnityEngine;
using System.Collections;

public class SwapActiveGameObject : MonoBehaviour 
{
	public void OnPressed () 
	{
		gameObject.SetActive(!gameObject.activeSelf);
	}
}
