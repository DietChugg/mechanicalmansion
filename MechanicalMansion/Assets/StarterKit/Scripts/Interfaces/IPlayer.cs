﻿using UnityEngine;
using System;
using System.Collections;

public interface IPlayer 
{
    bool IsPlaying { get; set; }
    bool Loops { get; set; }
    event EventHandler OnCompleted;
    void Play();
    void Stop();
    void Pause();
    void Resume();
}
