using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using StarterKit;

public class TraverseBezCurve : MonoBehaviourPlus 
{
	public BezierCurve bez;
	public Vector3 offset;
	public float speed;
	[Range(0,1f)]
	public float travelPercent;
	int startIndex = 0, lastIndex;
	public bool loop;
	public float nextLoopWait = 3;
	public SafeAction OnTravelBezComplete;
	public SafeAction OnTravelBezLoop;


	new void OnEnable () 
	{
		(this as MonoBehaviourPlus).OnEnable();
		StartCoroutine( TravelBezCurve() );
	}
	
	new void OnDisable () 
	{
		(this as MonoBehaviourPlus).OnDisable ();
		StopAllCoroutines();
	}

	public void GoToStartPosition()
	{
		transform.position = bez.resPoints[startIndex]+offset;
	}

	public IEnumerator TravelBezCurve() 
	{
		while(loop)
		{
			lastIndex = (int)(bez.resPoints.Count * travelPercent);
			GoToStartPosition();
			int goToIndex = 0;
			while(transform.position != bez.resPoints[lastIndex]+offset)
			{
				yield return null;
				float stepDistance = speed * Time.deltaTime;
				Vector3 startPos = transform.position;

				while(stepDistance > 0 && transform.position != bez.resPoints[lastIndex]+offset)
				{
					transform.MoveTowards(bez.resPoints[goToIndex]+offset, stepDistance);
					stepDistance -= transform.DistanceTo(startPos);
					if(transform.position == bez.resPoints[goToIndex]+offset)
						goToIndex++;
				}

//
//				while(transform.MoveTowards(bez.resPoints[goToIndex]+offset, speed * Time.deltaTime))
//				{
//					yield return null;
//				}
//				goToIndex++;
			}
			yield return new WaitForSeconds(nextLoopWait);
			OnTravelBezLoop.Call();
		}
		OnTravelBezComplete.Call();
	}
}
