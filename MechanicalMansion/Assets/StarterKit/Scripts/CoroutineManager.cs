using UnityEngine;
using System.Collections;

public class CoroutineManager : Singleton<CoroutineManager>
{
    [RuntimeInitializeOnLoadMethod]
    static void AutoLoad()
    {
        if (Instance != null)
        {
            //AutoLoads the Singleton 
        }
    }

    public new Coroutine StartCoroutine(IEnumerator coroutine)
    {
        return (this as MonoBehaviour).StartCoroutine(coroutine);
    }

    public new void StopAllCoroutines()
    {
        base.StopAllCoroutines();
    }
    
}
