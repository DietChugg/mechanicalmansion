#if UNITY_5
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace StarterKit
{
public class Vsync : MonoBehaviour 
{
	public Text state;
	public Slider slider;

	public void OnEnable()
	{
		slider.value = QualitySettings.vSyncCount;
		OnValueChanged();
	}

	public void OnValueChanged()
	{
		Settings.currentOptions.vSyncCount = (int)(slider.value);

		switch(Settings.currentOptions.vSyncCount)
		{
		case 0:
			state.text = "Don't Sync";
			break;
		case 1:
			state.text = "Every VBlank";
			break;
		case 2:
			state.text = "Every Second VBlank";
			break;
		}
	}
}
}
#endif

