#if UNITY_5
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace StarterKit
{
public class FullScreen : MonoBehaviour 
{
	public void OnEnable()
	{
		GetComponent<Toggle>().isOn = Screen.fullScreen;
	}
	
	public void OnValueChanged (UnityEngine.Object newValue) 
	{
		Settings.currentOptions.fullscreen = (newValue as Toggle).isOn;
	}
}
}
#endif

