#if UNITY_5
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace StarterKit
{
    public class AntiAliasing : MonoBehaviour 
    {
	    public Text state;
	    public Slider slider;
	
	    public void OnEnable()
	    {
		    switch(QualitySettings.antiAliasing)
		    {
		    case 0:
			    slider.value = 0;
			    break;
		    case 2:
			    slider.value = 1;
			    break;
		    case 4:
			    slider.value = 2;
			    break;
		    case 8:
			    slider.value = 3;
			    break;
		    }

		    OnValueChanged();
	    }
	
	    public void OnValueChanged()
	    {
		    switch((int)(slider.value))
		    {
		    case 0:
			    Settings.currentOptions.antiAliasing = 0;
			    state.text = "Disabled";
			    break;
		    case 1:
			    Settings.currentOptions.antiAliasing = 2;
			    state.text = "2x Multi Sampling";
			    break;
		    case 2:
			    Settings.currentOptions.antiAliasing = 4;
			    state.text = "4x Multi Sampling";
			    break;
		    case 3:
			    Settings.currentOptions.antiAliasing = 8;
			    state.text = "8x Multi Sampling";
			    break;
		    }
	    }
    }
}
#endif
