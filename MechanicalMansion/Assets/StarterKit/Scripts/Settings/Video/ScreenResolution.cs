#if UNITY_5
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace StarterKit
{
public class ScreenResolution : MonoBehaviour 
{
	public Text state;
	public Slider slider;
	
	public void OnEnable()
	{
		slider.maxValue = Screen.resolutions.Length;

		for(int i = 0; i < Screen.resolutions.Length; i ++)
		{
			if(Screen.currentResolution.height == Screen.resolutions[i].height &&
			   Screen.currentResolution.width == Screen.resolutions[i].width)
				slider.value = i;
		}
		OnValueChanged();
	}
	
	public void OnValueChanged()
	{
//		Screen.SetResolution(Screen.resolutions[(int)slider.value].width,Screen.resolutions[(int)slider.value].height,Screen.fullScreen);

		Settings.currentOptions.currentResolution = Screen.resolutions[(int)slider.value];
		state.text = Screen.resolutions[(int)slider.value].width.ToString() + " x " + Screen.resolutions[(int)slider.value].height.ToString();
	}
}
}
#endif

