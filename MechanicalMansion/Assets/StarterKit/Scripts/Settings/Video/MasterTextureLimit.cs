#if UNITY_5
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace StarterKit
{
public class MasterTextureLimit : MonoBehaviour 
{
	public Text state;
	public Slider slider;
	
	public void OnEnable()
	{
		slider.value = QualitySettings.masterTextureLimit;
		OnValueChanged();
	}
	
	public void OnValueChanged()
	{
		Settings.currentOptions.masterTextureLimit = (int)(slider.value);
		
		switch(Settings.currentOptions.masterTextureLimit)
		{
		case 0:
			state.text = "Full Res";
			break;
		case 1:
			state.text = "Half Res";
			break;
		case 2:
			state.text = "Quarter Res";
			break;
//		case 3:
//			state.text = "Eighth Res";
//			break;  
		}
	}
}
}
#endif
