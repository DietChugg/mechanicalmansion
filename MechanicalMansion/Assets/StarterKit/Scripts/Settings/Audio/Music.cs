#if UNITY_5
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace StarterKit
{
    public class Music : MonoBehaviour
    {
        public Text state;
        public Slider slider;
        float current;

        public void OnEnable()
        {
            SliderInfo.OnEndDragCalled += Reset;
            SliderInfo.OnBeginDragCalled += OnPointerDownCalled;
            //		SliderInfo.OnDrag += Reset;
            slider.value = AudioMixerData.Instance.musicVolume;
            OnValueChanged();
        }

        public void OnDisable()
        {
            SliderInfo.OnEndDragCalled -= Reset;
            SliderInfo.OnBeginDragCalled -= OnPointerDownCalled;
            //		SliderInfo.OnDrag -= Reset;
        }

        public void OnPointerDownCalled(Slider slider, PointerEventData eventData)
        {
            current = AudioMixerData.Instance.musicVolume;
        }

        void Reset(Slider slider, PointerEventData eventData)
        {
            Debug.Log("Reset");
            AudioMixerData.Instance.musicVolume = current;
            AudioMixerData.Instance.UpdateAudioVolumeLevels();
        }

        public void OnValueChanged()
        {
            Settings.currentOptions.musicVolumePercent = slider.value;
            state.text = (Settings.currentOptions.musicVolumePercent * 100).ToString("0.00") + "%";
            AudioMixerData.Instance.UpdateAudioVolumeLevels();
        }


    } 
}
#endif
