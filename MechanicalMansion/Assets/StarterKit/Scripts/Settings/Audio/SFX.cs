#if UNITY_5
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace StarterKit
{
public class SFX : MonoBehaviour 
{
	public Text state;
	public Slider slider;
	
	public void OnEnable()
	{
		slider.value = AudioMixerData.Instance.sfxVolume;
		OnValueChanged();
	}
	
	public void OnValueChanged()
	{
		Settings.currentOptions.sfxVolumePercent = slider.value;
		state.text = (Settings.currentOptions.sfxVolumePercent * 100).ToString("0.00") + "%";
	}
}

}
#endif
