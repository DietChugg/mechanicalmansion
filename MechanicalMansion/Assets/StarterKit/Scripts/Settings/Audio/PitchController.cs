#if UNITY_5
using UnityEngine;
using System.Collections;

namespace StarterKit
{
public class PitchController : MonoBehaviour 
{
	public AnimationCurve curve;
	float current = 0;

	void Update () 
	{
		current += Time.deltaTime;
		current = current%Mathf.Max(curve.GetCurveTimeLength(),.01f);
		GetComponent<AudioSource>().outputAudioMixerGroup.audioMixer.SetFloat("Music Pitch",curve.Evaluate(current));
	}
}

}
#endif
