using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace StarterKit
{
    public class SliderInfo : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler
    {
        public static SafeAction<Slider, PointerEventData> OnBeginDragCalled;
        public static SafeAction<Slider, PointerEventData> OnDragCalled;
        public static SafeAction<Slider, PointerEventData> OnEndDragCalled;

        public void OnBeginDrag(PointerEventData eventData)
        {
            OnBeginDragCalled.Call(GetComponent<Slider>(), eventData);
        }

        public void OnDrag(PointerEventData eventData)
        {
            OnDragCalled.Call(GetComponent<Slider>(), eventData);
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            OnEndDragCalled.Call(GetComponent<Slider>(), eventData);
        }
    }

}