#if UNITY_5
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ApplySettings : MonoBehaviour 
{
	public void OnStart()
	{

	}

	public void OnClick () 
	{
		Settings.Apply();
		GetComponent<Button>().interactable = false;
	}

	public void ChangeOccured()
	{
//		Debug.Log(Settings.currentOptions.Equals(Settings.prevOptions));
		GetComponent<Button>().interactable = !(Settings.currentOptions.Equals(Settings.prevOptions));
	}

}
#endif
