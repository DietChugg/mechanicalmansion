using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections;
using System.Collections.Generic;

public class SpritePrefabMaker : MonoBehaviour 
{
	public string path;
	public List<Sprite> sprites;


	[ContextMenu("Make Sprites Into Prefabs")]
	void MakePrefabs () 
	{
		for(int i = 0; i < sprites.Count; i++)
		{
			print(i);
			GameObject newPrefab = new GameObject(sprites[i].name);
			newPrefab.transform.localScale = new Vector3(6.25f,6.25f,1f);
			SpriteRenderer spriteRenderer = newPrefab.AddComponent<SpriteRenderer>() as SpriteRenderer;
			spriteRenderer.sprite = sprites[i];
            #if UNITY_EDITOR
			PrefabUtility.CreatePrefab(path+sprites[i].name+".prefab",newPrefab);
            #endif
		}
	}
}
