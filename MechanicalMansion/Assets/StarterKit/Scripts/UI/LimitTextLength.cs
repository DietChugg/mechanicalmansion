#if UNITY_5
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using StarterKit;

[ExecuteInEditMode]
public class LimitTextLength : MonoBehaviourPlus 
{
	public int maxCharacters;

	void Update () 
	{
		if(maxCharacters < 0)
			maxCharacters = 0;
		if(maxCharacters != 0)
		{
			Text text = GetComponent<Text>();
			text.text = text.text.Substring(0,Mathf.Min((int)maxCharacters,text.text.Length));
		}
	}
}
#endif
