using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using StarterKit;

public class ToggleGameObjectsOnPressed : MonoBehaviourPlus
{
	public List<GameObject> gameObjects = new List<GameObject>();
	public void OnPressed()
	{
		gameObjects.ToArray().ToggleActive();
	}

    public void OnClicked()
    {
        gameObjects.ToArray().ToggleActive();
    }

    public void OnValueChanged(bool toggle)
    {
        gameObjects.ToArray().SetActive(toggle);
    }
}
