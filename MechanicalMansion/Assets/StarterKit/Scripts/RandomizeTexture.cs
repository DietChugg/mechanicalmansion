using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using StarterKit;

public class RandomizeTexture : MonoBehaviourPlus 
{
	public List<Texture> textures = new List<Texture>();

	new void OnEnable () 
	{
		GetComponent<Renderer>().material.SetTexture("_MainTex",textures.GetRandom());
	}
}
