using UnityEngine;
using System.Collections;

public static class DebugColor
{
	public const string AQUA = "aqua";
	public const string BLACK = "black";
	public const string BLUE = "blue";
	public const string BROWN = "brown";
	public const string CYAN = "cyan";
	public const string DARKBLUE = "darkblue";
	public const string FUCHSIA = "fuchsia";
	public const string GREEN = "green";
	public const string GREY = "grey";
	public const string LIGHTBLUE = "lightblue";
	public const string LIME = "lime";
	public const string MAGENTA = "magenta";
	public const string MAROON = "maroon";
	public const string NAVY = "navy";
	public const string OLIVE = "olive";
	public const string ORANGE = "orange";
	public const string PURPLE = "purple";
	public const string RED = "red";
	public const string SILVER = "silver";
	public const string TEAL = "teal";
	public const string WHITE = "white";
	public const string YELLOW = "yellow";
}
