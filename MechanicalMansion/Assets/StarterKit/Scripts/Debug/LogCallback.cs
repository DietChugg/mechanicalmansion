#if UNITY_5
using UnityEngine;
using System.Collections;

public class LogCallback : MonoBehaviour
{
	public string output = "";
	public string stack = "";

	void OnEnable ()
	{ 
		Application.logMessageReceived += HandleLog;
	}

	void OnDisable ()
	{
		Application.logMessageReceived -= HandleLog;
	}

	void HandleLog (string logString, string stackTrace, LogType type)
	{
		output = logString;
		stack = stackTrace;
	}
}
#endif
