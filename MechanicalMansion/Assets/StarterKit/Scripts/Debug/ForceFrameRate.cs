using UnityEngine;
using System.Collections;

public class ForceFrameRate : MonoBehaviour
{
#if UNITY_EDITOR
    public int frameRate;

	[ContextMenu("Update")]
	void Awake()
	{
        Application.targetFrameRate = frameRate;
	}
#endif
}
