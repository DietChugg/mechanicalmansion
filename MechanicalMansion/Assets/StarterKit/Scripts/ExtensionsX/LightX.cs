using UnityEngine;
using System.Collections;

public static class LightX
{
	#region Constants
	#endregion
	
	#region StaticMethods
	#endregion
	
	#region ExtensionMethods
	public static void Flicker(this Light me, int freq)
	{
		if(Random.Range(0,freq) == 0)
		{
			me.enabled = !me.enabled;
		}
	}

	public static void Pulse(this Light me, float speed, float min, float max)
	{
		me.intensity = min + Mathf.PingPong(Time.time*speed,max-min);
	}
	#endregion
}
