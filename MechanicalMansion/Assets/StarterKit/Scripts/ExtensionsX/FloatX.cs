using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class FloatX 
{
	public static void SetToNearest(this float me, float one, float two)
	{
		if(Mathf.Abs(me - one) < Mathf.Abs(me - two))
			me = one;
		me = two;
	}

	public static void SetToNearest(this float me, params float[] testValues)
	{
		float clostest = testValues[0];
		for(int i = 0; i < testValues.Length; i++)
		{
			if(Mathf.Abs(me - testValues[i]) < Mathf.Abs(me - clostest))
			{
				clostest = testValues[i];
			}
		}
		me = clostest;
	}

	public static bool IsBetween(float current, float min, float max)
	{
		if(min < current && current < max)
			return true;
		return false;
	}

    public static bool IsNaN(this float me)
    {
        return float.IsNaN(me);
    }

    public static bool IsBetween(this float me, float min, float max, bool isInclusive)
    {
        if (min < me && me < max)
            return true;
        if (min == me && me == max && isInclusive)
            return true;
        return false;
    }

    public static float Max(this float me, params float[] values)
    {
        List<float> checkValues = new List<float>();
        checkValues.Add(me);
        checkValues.AddRange(values);
        return Mathf.Max(checkValues.ToArray());
    }

    public static float Min(this float me, params float[] values)
    {
        List<float> checkValues = new List<float>();
        checkValues.Add(me);
        checkValues.AddRange(values);
        return Mathf.Min(checkValues.ToArray());
    }

    public static float DistanceTo(this float me, float other)
    {
        float maxVal = me.Abs().Max(other.Abs());
        float minVal = me.Abs().Min(other.Abs());
        bool isSameSign = (me.Sign() == other.Sign());

        if(isSameSign)
            return maxVal - minVal;
        else
            return me.Abs() + other.Abs();
    }

    public static float Abs(this float me)
    {
        return Mathf.Abs(me);
    }

    public static float Sign(this float me)
    {
        return Mathf.Sign(me);
    }

    public static float HighCut(this float me, float high)
    {
        if(me > high)
            return high;
        return me;
    }
    
    public static float LowCut(this float me, float low)
    {
        if(me < low)
            return low;
        return me;
    }

    public static float MoveTowards(this float me, float target, float delta)
    {
        return Mathf.MoveTowards(me, target, delta);
    }

    public static float Clamp(this float me, int min, int max)
    {
        return Mathf.Clamp(me,min,max);
    }

    public static float Clamp(this float me, float min, float max)
    {
        return Mathf.Clamp(me, min, max);
    }

	public static float Clamp01(this float me)
	{
		return Mathf.Clamp01(me);
	}

    public static float Randomize(this float me)
    {
        Random.seed++;
        if (me > 0)
        {
            return Random.Range(0, me);
        }
        if (me < 0)
        {
            return Random.Range(me, 0);
        }
        return 0;
    }
}
