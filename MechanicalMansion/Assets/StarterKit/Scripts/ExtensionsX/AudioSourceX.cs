using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using StarterKit;

public static class AudioSourceX 
{

    public static AudioSource PlayClipAtPoint(AudioClip clip)
    {
       return AudioSourceX.PlayClipAtPoint(clip, Vector3.zero);
    }

    public static AudioSource PlayClipAtPoint(AudioClip clip, Vector3 vector3)
    {
        return AudioSourceX.PlayClipAtPoint(clip, Vector3.zero, 1f);
    }

    public static AudioSource PlayClipAtPoint(AudioClip clip, float volume)
    {
        return AudioSourceX.PlayClipAtPoint(clip, Vector3.zero, volume, 1f);
    }

    public static AudioSource PlayClipAtPoint(AudioClip clip, Vector3 vector3, float volume)
    {
        return AudioSourceX.PlayClipAtPoint(clip, vector3, volume, 1f);
    }

    public static AudioSource PlayClipAtPoint(AudioClip clip, Vector3 vector3, float volume, float pitch)
    {
        GameObject newObject = new GameObject("Play Clip At Point");
        newObject.transform.position = vector3;
        AudioSource audioSource = newObject.AddComponent<AudioSource>() as AudioSource;
        audioSource.clip = clip;
        audioSource.volume = volume;
        audioSource.pitch = pitch;
        if (vector3 == Vector3.zero)
            audioSource.spatialBlend = 0f;
        audioSource.Play();
        newObject.Destroy(audioSource.clip.length);
        return audioSource;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="clips"></param>
    /// <param name="vector3"></param>
    /// <param name="volume"></param>
    /// <param name="pitch">Additional Delay between each clip<</param>
    public static void PlayClipsAtPoint(AudioClip[] clips, Vector3 vector3, float volume, float pitch, float delay)
    {
        CoroutineManager.Instance.StartCoroutine(PlayClips(clips, vector3, volume, pitch, delay));
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="clips"></param>
    /// <param name="vector3"></param>
    /// <param name="volume"></param>
    /// <param name="pitch"></param>
    /// <param name="delay">Additional Delay between each clip</param>
    /// <returns></returns>
    public static IEnumerator PlayClips(AudioClip[] clips, Vector3 vector3, float volume, float pitch, float delay)
    {
        for (int i = 0; i < clips.Length; i++)
        {
            PlayClipAtPoint(clips[i],vector3,volume,pitch);
            yield return new WaitForSeconds(delay + clips[i].length);
        }
        yield return null;
    }

}
