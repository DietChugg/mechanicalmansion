﻿using UnityEngine;
using System.Collections;

namespace StarterKit
{
    public static class SpriteRendererX
    {

        // Use this for initialization
        public static void FadeSprite(this SpriteRenderer me, AnimationCurve fadeCurve, bool fadeIn, float time)
        {
            CoroutineManager.Instance.StartCoroutine(FadeSpriteRoutine(me, fadeCurve, fadeIn, time));
        }

        // Update is called once per frame
        public static IEnumerator FadeSpriteRoutine(SpriteRenderer spriteRenderer, AnimationCurve fadeCurve, bool fadeIn, float time)
        {
            float curTime = 0f;
            while (curTime != time)
            {
                float percent = curTime / time;
                if (!fadeIn)
                    percent = 1 - percent;
                spriteRenderer.color = spriteRenderer.SetA(fadeCurve.Evaluate(percent));
                curTime += Time.deltaTime;
                if (curTime > time)
                    curTime = time;
                yield return null;
            }
            yield return null;
        }
    }

}