#if UNITY_EDITOR
using UnityEditor;

public static class EditorGUILayoutX 
{
	public static void SuperSpace(int spaces)
	{
		for(int i = 0; i < spaces; i++)
			EditorGUILayout.Space();
	}
}
#endif
