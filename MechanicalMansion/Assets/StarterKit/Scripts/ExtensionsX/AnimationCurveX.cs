using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public static class AnimationCurveX
{
    public static float GetCurveTimeLength(this AnimationCurve me)
    {
        if (me.keys.Length == 0)
        {
            return 0;
        }
        return me.keys[0].time.DistanceTo(me.keys.GetLast().time);
    }

    public static AnimationCurve Normalized(this AnimationCurve me)
    {
        Keyframe[] normalizedKeys = new Keyframe[me.keys.Length];
        float totalLength = me.GetCurveTimeLength();
        for (int i = 0; i < normalizedKeys.Length; i++)
        {
            normalizedKeys[i] = new Keyframe((me.keys[i].time - me.keys[0].time)/totalLength,me.keys[i].value,
                me.keys[i].inTangent,me.keys[i].outTangent);
        }
        return new AnimationCurve(normalizedKeys);
    }

    public static AnimationCurve GetInverseTimeOfCurve(this AnimationCurve me)
    {
        AnimationCurve normalizedMe = me.Normalized();
        Keyframe[] inverseKeys = new Keyframe[normalizedMe.keys.Length];


        for (int i = 0; i < inverseKeys.Length; i++)
        {
            Keyframe lastKey = normalizedMe.keys[(normalizedMe.keys.Length - 1) - i];
            Keyframe thisKey = normalizedMe.keys[i];
            Keyframe keyFrame = new Keyframe(1 - thisKey.time, thisKey.value, -thisKey.outTangent, -thisKey.inTangent);
            inverseKeys[i] = keyFrame;
        }
        AnimationCurve inverseCurve = new AnimationCurve(inverseKeys);
        return inverseCurve;
    }
}
