using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

public static class GameObjectX
{
	#region Constants
	#endregion
	
	#region StaticMethods
    /// <summary>
    /// Given a Prefab Array this will create one of each prefab at the same position and rotation
    /// </summary>
    /// <param name="prefabs"></param>
    /// <param name="position"></param>
    /// <param name="rotation"></param>
    /// <returns></returns>
    public static GameObject[] MultiInstantiate(GameObject[] prefabs, Vector3 position, Quaternion rotation)
    {
        GameObject[] instances = new GameObject[prefabs.Length];
        for (int i = 0; i < prefabs.Length; i++)
		{
            instances[i] = (GameObject)GameObject.Instantiate(prefabs[i], position, rotation);
		}
        return instances;
    }

    public static GameObject[] MultiInstantiate(GameObject[] prefabs, Vector3 position, Quaternion rotation, 
        Axis useColliderBounds, bool isCenterPivot)
    {
        return MultiInstantiate(prefabs, position, rotation,
        useColliderBounds, isCenterPivot, 0f);
    }

    public static GameObject Instantiate(this GameObject me)
    {
        return me.Instantiate(me.transform.position, me.transform.rotation);
    }

    public static GameObject Instantiate(this GameObject me, Vector3 position, Quaternion rotation)
    {
        return (GameObject)GameObject.Instantiate(me, position, rotation);
    }
    

    public static GameObject[] MultiInstantiate(GameObject[] prefabs, Vector3 position, Quaternion rotation, 
        Axis useColliderBounds, bool isCenterPivot, float padding)
    {
        GameObject[] instances = MultiInstantiate(prefabs, position, rotation);

        Bounds[] bounds = new Bounds[instances.Length];
        for (int i = 0; i < instances.Length; i++)
        {
            bounds[i] = instances[i].GetComponent<Collider>().bounds;
        }
        Vector3 startPosition = position;
        if (isCenterPivot)
        {
            float totalWidth = 0;
            switch (useColliderBounds)
            {
                case Axis.X:
                    totalWidth = bounds.GetTotalWidth();
                    totalWidth += padding * (bounds.Length-1).Max(0);
                    startPosition.x -= totalWidth/2;
                    break;
                case Axis.Y:
                    totalWidth = bounds.GetTotalHeight();
                    totalWidth += padding * (bounds.Length - 1).Max(0);
                    startPosition.y -= totalWidth/2;
                    break;
                case Axis.Z:
                    totalWidth = bounds.GetTotalDepth();
                    totalWidth += padding * (bounds.Length - 1).Max(0);
                    startPosition.z -= totalWidth/2;
                    break;
                default:
                    break;
            }
        }


        float totalDistance = 0;
        for (int i = 0; i < instances.Length; i++)
        {
            switch (useColliderBounds)
            {
                case Axis.X:
                    float width = instances[i].GetComponent<BoxCollider>().bounds.size.x;
                    //Debug.Log(width);
                    totalDistance += width;
                    instances[i].transform.position = startPosition + Vector3.right * (totalDistance - width/2) + Vector3.right * padding * i;
                    break;
                case Axis.Y:
                    float height = instances[i].GetComponent<BoxCollider>().bounds.size.x;
                    totalDistance += height;
                    instances[i].transform.position = startPosition + Vector3.up * (totalDistance - height / 2) + Vector3.up * padding * i;
                    break;
                case Axis.Z:
                    float depth = instances[i].GetComponent<BoxCollider>().bounds.size.x;
                    totalDistance += depth;
                    instances[i].transform.position = startPosition + Vector3.forward * (totalDistance - depth / 2) + Vector3.forward * padding * i;
                    break;
                default:
                    break;
            }
        }
        return instances;
    }

	#endregion
	
	#region ExtensionMethods
      
    public static T CopyComponent<T>(this GameObject me, T original) where T : Component
	{
		System.Type type = original.GetType();
		Component copy = me.AddComponent(type);
		System.Reflection.FieldInfo[] fields = type.GetFields();
		foreach (System.Reflection.FieldInfo field in fields)
		{
			field.SetValue(copy, field.GetValue(original));
		}
		return copy as T;
	}

    public static void DestoryKids(this GameObject me)
    {
        Transform[] kids = me.transform.GetKids();
        for (int i = 0; i < kids.Length; i++)
        {
            GameObject.Destroy(kids[i].gameObject);
        }
    }

#if UNITY_EDITOR
    public static void DestoryKidsImmediate(this GameObject me)
    {
        Transform[] kids = me.transform.GetKids();
        for (int i = 0; i < kids.Length; i++)
        {
            GameObject.DestroyImmediate(kids[i].gameObject);
        }
    }
#endif

    public static GameObject[] GetKids(this GameObject me)
    {
        Transform[] kids = me.transform.GetKids();
        return kids.ToGameObjectArray();
    }

//	public static void CopyPrefab(this GameObject me, GameObject prefab)
//	{
//
//	}
//
//	void CopyPrefabData(GameObject source)
//	{
//        foreach(Transform kid in source.transform)
//		{
//			CopyPrefabData
//		}
//    }

	public static Component[] GetAllComponents(this GameObject me)
	{
		List<Component> components = new List<Component>();
		foreach(var component in me.GetComponents<Component>())
		{
			components.Add(component);
		}
		return components.ToArray();
	}

	public static T AddComponent<T>(this GameObject go, T toAdd) where T : Component
	{
		return go.AddComponent<T>().GetCopyOf(toAdd) as T;
	}


	public static T[] FindSceneComponentsOfType<T>(this GameObject me)
	{
		return UnityEngine.Object.FindObjectsOfType(typeof(T)) as T[];
	}

    public static T GetSafeComponent<T>(this GameObject obj) where T: MonoBehaviour
    {
        T component = obj.GetComponent<T>();
        
        if(component == null)
        {
            Debug.LogError("Expected to find component of type " + typeof(T) + " but found none", obj);
        }
        return component;
    }

    public static GameObject FindByID(int instanceId)
    {
//        Transform[] allTransforms = GameObject.FindObjectsOfType<Transform>();
        GameObject[] go = Object.FindObjectsOfType (typeof(GameObject)) as GameObject[];
        
        for (int i = 0; i < go.Length; i++)
        {
            if(go[i].GetInstanceID() == instanceId)
                return go[i];
        }
        return null;
    }

    public static GameObject[] FindAllGameObjects()
    {
		return Resources.FindObjectsOfTypeAll (typeof(GameObject)) as GameObject[];
    }

    public static int HeritageCount(this GameObject me)
    {
        int heritageCount = 0;
        GameObject currentAdult = me.GetParent();
        while(currentAdult != null)
        {
            heritageCount++;
            currentAdult = currentAdult.GetParent();
        }
        return heritageCount;
    }

    public static GameObject GetParent(this GameObject me)
    {
        if(me == null)
            return null;
        if(me.transform.parent != null)
            return me.transform.parent.gameObject;
        return null;
    }

    public static void SetParentTo(this GameObject me, GameObject newParent)
    {
        if(me == null)
            return;
        if(newParent == null)
            me.transform.parent = null;
        else
            me.transform.parent = newParent.transform;
    }

    static public T GetOrAddComponent<T> (this Component child) where T: Component {
        T result = child.GetComponent<T>();
        if (result == null) {
            result = child.gameObject.AddComponent<T>();
        }
        return result;
    }
	#endregion

	#region AlsoInTransformX

	public static void Reset(this GameObject me)
	{
		me.transform.position = Vector3.zero;
		me.transform.rotation = Quaternion.identity;
		me.transform.SetLossyScale(Vector3.one);
	}
	
	public static void Reset(this GameObject me, bool isLocal)
	{
		if(!isLocal)
		{
			me.Reset ();
			return;
		}
		me.transform.localPosition = Vector3.zero;
		me.transform.localRotation = Quaternion.identity;
		me.transform.localScale = Vector3.one;
	}
	
	public static void MoveToMouse(this GameObject me)
	{
		if(Camera.main == null)
		{
			Debug.LogError("Camera is Null. Make sure you have a Main Camera in the scene or that you have assinged a camera properly");
			return;
		}
		me.transform.MoveToMouse(Camera.main);
	}
	
	public static bool IsInFrontOfMe(this GameObject me, GameObject other)
	{
		if(Camera.main == null)
		{
			Debug.LogError("I'm null and will always return false");
			return false;
		}
		Vector3 directionToOther = me.DirectionTo(other);
		float dot = Vector3.Dot(me.transform.forward, directionToOther);
		if(dot >= 0)
			return true;
		else
			return false;
	}
	
	public static Vector3 OffsetFromMouse(this GameObject me)
	{
		if(Camera.main == null)
		{
			Debug.LogError("Camera is Null. Make sure you have a Main Camera in the scene or that you have assinged a camera properly");
			return Vector3.zero;
		}
		return me.transform.OffsetFromMouse(Camera.main);
	}
	
	public static Vector3 OffsetFromMouse(this GameObject me, Camera targetCamera)
	{
		if(targetCamera == null)
		{
			return me.transform.OffsetFromMouse();
		}
		
		Vector3 startPos = me.transform.position;
		me.transform.MoveToMouse(targetCamera);
		Vector3 finalPos = me.transform.position;
		me.transform.position = startPos;
		return startPos - finalPos;
		
		//        Vector3 meInScreenSpace = targetCamera.WorldToScreenPoint(me.transform.position);
		//
		////        Vector3 heading = me.transform.position - targetCamera.transform.position;
		////        float depth = Vector3.Dot(heading, targetCamera.transform.forward);
		//        Vector3 mouseScreenPos = Input.mousePosition;
		//        Vector3 offsetInScreenSpace = meInScreenSpace - mouseScreenPos;
		////        mouseScreenPos.z = depth;
		//        offsetInScreenSpace.z = me.transform.DistanceTo(targetCamera.transform);
		//        Vector3 offsetInWorldSpace = targetCamera.ScreenToWorldPoint(offsetInScreenSpace);
		//        return me.transform.position - offsetInWorldSpace;
	}
	
	public static void MoveToMouse(this GameObject me, Camera targetCamera)
	{
		if(targetCamera == null)
		{
			me.transform.MoveToMouse();
			return;
		}
		Vector3 heading = me.transform.position - targetCamera.transform.position;
		float depth = Vector3.Dot(heading, targetCamera.transform.forward);
		Vector3 mouseScreenPos = Input.mousePosition;
		mouseScreenPos.z = depth;
		Vector3 mouseWorldPosition = targetCamera.ScreenToWorldPoint(mouseScreenPos);
		me.transform.position = new Vector3(mouseWorldPosition.x, mouseWorldPosition.y, mouseWorldPosition.z);
	}
	
	public static bool CompareLayerMask(this GameObject me, LayerMask mask)
	{
		if((1<<me.layer & mask.value) != 0)
		{
			return true;
		}
		return false;
	}
	
	public static Vector3 Vector2As3(this GameObject me, Vector2 v2)
	{
		return new Vector3(v2.x,v2.y,me.transform.position.z);
	}
	
	public static void SetV2(this GameObject me, float x, float y)
	{
		me.transform.SetV2(new Vector2(x,y));
	}
	
	public static void SetV2(this GameObject me, Vector2 v2)
	{
		if(me == null)
		{
			return;
		}
		me.transform.position = new Vector3(v2.x,v2.y,me.transform.position.z);
	}
	
	public static Vector3 PositionWithXAt(this GameObject me, float x)
	{
		return new Vector3(x,me.transform.position.y,me.transform.position.z);
	}
	
	public static Vector3 PositionWithYAt(this GameObject me, float y)
	{
		return new Vector3(me.transform.position.x,y,me.transform.position.z);
	}
	
	public static Vector3 PositionWithZAt(this GameObject me, float z)
	{
		return new Vector3(me.transform.position.x,me.transform.position.y,z);
	}
	
	public static void SetLossyScale(this GameObject me, Vector3 newScale)
	{
		if(me == null)
		{
			return;
		}
		Transform parent = me.transform.parent;
		me.transform.parent = null;
		me.transform.localScale = newScale;
		me.transform.parent = parent;
	}
	
	public static void SetLossyScale(this GameObject me, float x, float y, float z)
	{
		if(me == null)
		{
			return;
		}
		me.transform.SetLossyScale(new Vector3(x,y,z));
	}
	
	/// <summary>
	/// Sets the X position.
	/// </summary>
	/// <param name="me">Me.</param>
	/// <param name="x">The x coordinate.</param>
	public static void SetXPosition(this GameObject me, float x)
	{
		if(me == null)
		{
			return;
		}
		me.transform.position = new Vector3(x,me.transform.position.y,me.transform.position.z);
	}
	
	/// <summary>
	/// Sets the Y position.
	/// </summary>
	/// <param name="me">Me.</param>
	/// <param name="y">The y coordinate.</param>
	public static void SetYPosition(this GameObject me, float y)
	{
		if(me == null)
		{
			return;
		}
		me.transform.position = new Vector3(me.transform.position.x,y,me.transform.position.z);
	}
	
	/// <summary>
	/// Sets the Z position.
	/// </summary>
	/// <param name="me">Me.</param>
	/// <param name="z">The z coordinate.</param>
	public static void SetZPosition(this GameObject me, float z)
	{
		if(me == null)
		{
			return;
		}
		me.transform.position = new Vector3(me.transform.position.x,me.transform.position.y,z);
	}
	
	/// <summary>
	/// Sets the local X position.
	/// </summary>
	/// <param name="me">Me.</param>
	/// <param name="x">The x coordinate.</param>
	public static void SetLocalXPosition(this GameObject me, float x)
	{
		if(me == null)
		{
			return;
		}
		me.transform.localPosition = new Vector3(x,me.transform.localPosition.y,me.transform.localPosition.z);
	}
	
	/// <summary>
	/// Sets the local Y position.
	/// </summary>
	/// <param name="me">Me.</param>
	/// <param name="y">The y coordinate.</param>
	public static void SetLocalYPosition(this GameObject me, float y)
	{
		if(me == null)
		{
			return;
		}
		me.transform.localPosition = new Vector3(me.transform.localPosition.x,y,me.transform.localPosition.z);
	}
	
	/// <summary>
	/// Sets the local Z position.
	/// </summary>
	/// <param name="me">Me.</param>
	/// <param name="z">The z coordinate.</param>
	public static void SetLocalZPosition(this GameObject me, float z)
	{
		if(me == null)
		{
			return;
		}
		me.transform.localPosition = new Vector3(me.transform.localPosition.x,me.transform.localPosition.y,z);
	}
	
	/// <summary>
	/// Sets the X rotation.
	/// </summary>
	/// <param name="me">Me.</param>
	/// <param name="x">The x coordinate.</param>
	public static void SetXRotation(this GameObject me, float x)
	{
		if(me == null)
		{
			return;
		}
		me.transform.rotation = Quaternion.Euler(x,me.transform.rotation.eulerAngles.y,me.transform.rotation.eulerAngles.z);
	}
	
	/// <summary>
	/// Sets the Y rotation.
	/// </summary>
	/// <param name="me">Me.</param>
	/// <param name="y">The y coordinate.</param>
	public static void SetYRotation(this GameObject me, float y)
	{
		if(me == null)
		{
			return;
		}
		me.transform.rotation = Quaternion.Euler(me.transform.rotation.eulerAngles.x,y,me.transform.rotation.eulerAngles.z);
	}
	
	/// <summary>
	/// Sets the Z rotation.
	/// </summary>
	/// <param name="me">Me.</param>
	/// <param name="z">The z coordinate.</param>
	public static void SetZRotation(this GameObject me, float z)
	{
		if(me == null)
		{
			return;
		}
		me.transform.rotation = Quaternion.Euler(me.transform.rotation.eulerAngles.x,me.transform.rotation.eulerAngles.y,z);
	}
	
	/// <summary>
	/// Sets the X local scale.
	/// </summary>
	/// <param name="me">Me.</param>
	/// <param name="x">The x coordinate.</param>
	public static void SetXLocalScale(this GameObject me, float x)
	{
		if(me == null)
		{
			return;
		}
		me.transform.localScale = new Vector3(x,me.transform.localScale.y,me.transform.localScale.z);
	}
	
	/// <summary>
	/// Sets the local scale for all three axis.
	/// </summary>
	/// <param name="me">Me.</param>
	/// <param name="x">The x coordinate.</param>
	public static void SetLocalScale(this GameObject me, float all)
	{
		if(me == null)
		{
			return;
		}
		me.transform.localScale = new Vector3(all,all,all);
	}
	
	/// <summary>
	/// Sets the Y local scale.
	/// </summary>
	/// <param name="me">Me.</param>
	/// <param name="y">The y coordinate.</param>
	public static void SetYLocalScale(this GameObject me, float y)
	{
		if(me == null)
		{
			return;
		}
		me.transform.localScale = new Vector3(me.transform.localScale.x,y,me.transform.localScale.z);
	}
	
	/// <summary>
	/// Sets the Z local scale.
	/// </summary>
	/// <param name="me">Me.</param>
	/// <param name="z">The z coordinate.</param>
	public static void SetZLocalScale(this GameObject me, float z)
	{
		if(me == null)
		{
			return;
		}
		me.transform.localScale = new Vector3(me.transform.localScale.x,me.transform.localScale.y,z);
	}
	
	/// <summary>
	/// Sets the lossy scale.
	/// </summary>
	/// <param name="me">Me.</param>
	/// <param name="all">All coordinates.</param>
	public static void SetLossyScale(this GameObject me, float all)
	{
		if(me == null)
		{
			return;
		}
		me.transform.SetLossyScale(all,all,all);
	}
	
	/// <summary>
	/// Sets the X lossy scale.
	/// </summary>
	/// <param name="me">Me.</param>
	/// <param name="x">The x coordinate.</param>
	public static void SetXLossyScale(this GameObject me, float x)
	{
		if(me == null)
		{
			return;
		}
		me.transform.SetLossyScale(x,me.transform.lossyScale.y,me.transform.lossyScale.z);
	}
	
	/// <summary>
	/// Sets the Y lossy scale.
	/// </summary>
	/// <param name="me">Me.</param>
	/// <param name="y">The y coordinate.</param>
	public static void SetYLossyScale(this GameObject me, float y)
	{
		if(me == null)
		{
			return;
		}
		me.transform.SetLossyScale(me.transform.lossyScale.x,y,me.transform.lossyScale.z);
	}
	
	/// <summary>
	/// Sets the Z lossy scale.
	/// </summary>
	/// <param name="me">Me.</param>
	/// <param name="z">The z coordinate.</param>
	public static void SetZLossyScale(this GameObject me, float z)
	{
		if(me == null)
		{
			return;
		}
		me.transform.SetLossyScale(me.transform.lossyScale.x,me.transform.lossyScale.y,z);
	}
	
	/// <summary>
	/// Directions to target.
	/// </summary>
	/// <returns>The direction.</returns>
	/// <param name="me">Me.</param>
	/// <param name="to">To.</param>
	public static Vector3 DirectionTo(this GameObject me, GameObject target)
	{
		if(me == null)
		{
			return Vector3.zero;
		}
		return (Vector3.Normalize((target.transform.position-me.transform.position)/Vector3.Magnitude((target.transform.position-me.transform.position))));
	}
	
	/// <summary>
	/// Directions to targetPosition.
	/// </summary>
	/// <returns>The direction.</returns>
	/// <param name="me">Me.</param>
	/// <param name="targetPosition">Target position.</param>
	public static Vector3 DirectionTo(this GameObject me, Vector3 targetPosition)
	{
		if(me == null)
		{
			return Vector3.zero;
		}
		return (Vector3.Normalize((targetPosition-me.transform.position)/Vector3.Magnitude((targetPosition-me.transform.position))));
	}
	
	public static Vector3 DirectionFrom(this GameObject me, GameObject _from)
	{
		if(me == null)
		{
			return Vector3.zero;
		}
		return (Vector3.Normalize((me.transform.position-_from.transform.position)/Vector3.Magnitude((me.transform.position - _from.transform.position))));
	}
	
	public static Vector3 DirectionFrom(this GameObject me, Vector3 _from)
	{
		if(me == null)
		{
			return Vector3.zero;
		}
		return (Vector3.Normalize((me.transform.position-_from)/Vector3.Magnitude((me.transform.position-_from))));
	}
	
	public static void CenterOnChildren(this GameObject me)
	{
		if(me == null)
		{
			//Debug.LogError("Cannot MoveTowards because Transform is null");
			return;
		}
		Vector3 averagePos = Vector3.zero;
		int count = 0;
		foreach(Transform kid in me.transform)
		{
			averagePos += kid.position;
			kid.parent = null;
			count ++;
		}
		if(count == 0)
			return;
		averagePos /= count;
		me.transform.position = averagePos;
		foreach(Transform kid in me.transform)
		{
			kid.parent = me.transform;
		}
	}
	
	public static bool CompareTags(this GameObject me, string[] tags)
	{
		if(me == null)
		{
			return false;
		}
		foreach(string tag in tags)
		{
			if(me.CompareTag(tag))
				return true;
		}
		return false;
	}

	public static void SetActive(this GameObject me, bool isActive)
	{
		if(me == null)
		{
			return;
		}
		me.SetActive(isActive);
	}

    public static void SetKidsActive(this GameObject me, bool isActive)
    {
        if (me == null)
        {
            return;
        }
        me.GetKids().SetActive(isActive);
    }
	
	public static bool GetActiveInHiearchy(this GameObject me)
	{
		if(me == null)
		{
			return false;
		}
		return me.activeInHierarchy;
	}
	
	public static bool GetActiveSelf(this GameObject me)
	{
		if(me == null)
		{
			return false;
		}
		return me.activeSelf;
	}
	
	public static int GetLayer(this GameObject me)
	{
		if(me == null)
		{
			return LayerMask.NameToLayer("Default");
		}
		return me.layer;
	}
	
	public static void SetLayer(this GameObject me, int newLayer)
	{
		if(me == null)
		{
			return;
		}
		me.layer = newLayer;
	}
	
	public static bool LocalScaleTowards(this GameObject me, Vector3 targetLocalScale, float speed)
	{
		if(me == null)
		{
			return false;
		}
		if(me.transform.localScale == targetLocalScale || Vector3.Distance(me.transform.localScale,targetLocalScale) < speed)
		{
			me.transform.localScale = targetLocalScale;
			return false;
		}
		me.transform.localScale = Vector3.MoveTowards(me.transform.localScale, targetLocalScale, speed);
		return true;
	}
	
	public static bool MoveTowards(this GameObject me, Vector3 targetPos, float speed)
	{
		if(me == null)
		{
			return false;
		}
		if(me.transform.position == targetPos || me.transform.DistanceTo(targetPos) < speed)
		{
			me.transform.position = targetPos;
			return false;
		}
		me.transform.position = Vector3.MoveTowards(me.transform.position, targetPos, speed);
		return true;
	}
	
	public static bool MoveTowards(this GameObject me, Vector3 targetPos, float speed, Axis axis)
	{
		switch (axis)
		{
		case Axis.X: 
			targetPos = new Vector3(targetPos.x,me.transform.position.y,me.transform.position.z);
			break;
		case Axis.Y: 
			targetPos = new Vector3(me.transform.position.x,targetPos.y,me.transform.position.z);
			break;
		case Axis.Z: 
			targetPos = new Vector3(me.transform.position.x,me.transform.position.y,targetPos.z);
			break;
		}
		
		return me.transform.MoveTowards(targetPos,speed);
	}
	
	public static bool MoveTowards(this GameObject me, float x, float y, float z, float speed)
	{
		return me.MoveTowards(new Vector3(x,y,z),speed);
	}
	
	public static bool MoveTowards(this GameObject me, GameObject targetPos, float speed)
	{
		return me.MoveTowards(targetPos.transform.position,speed);
	}
	
	public static bool MoveTowards(this GameObject me, GameObject targetPos, float speed, Axis axis)
	{
		return me.MoveTowards(targetPos.transform.position,speed, axis);
	}
	
	public static bool MoveTowards(this GameObject me, float x, float y, float z, float speed, Axis axis)
	{
		return me.MoveTowards(new Vector3(x,y,z),speed, axis);
	}
	
	public static bool MoveTowardsX(this GameObject me, float x, float speed) 
	{
		return me.MoveTowards(new Vector3(x,me.transform.position.y,me.transform.position.z),speed, Axis.X);
	}
	
	public static bool MoveTowardsX(this GameObject me, Vector3 targetPos, float speed, Axis axis)
	{
		return me.MoveTowards(targetPos,speed, Axis.X);
	}
	
	public static bool MoveTowardsX(this GameObject me, GameObject targetPos, float speed, Axis axis)
	{
		return me.MoveTowards(targetPos.transform.position,speed, Axis.X);
	}
	
	public static bool MoveTowardsY(this GameObject me, float y, float speed) 
	{
		return me.MoveTowards(new Vector3(me.transform.position.x,y,me.transform.position.z),speed, Axis.X);
	}
	
	public static bool MoveTowardsY(this GameObject me, Vector3 targetPos, float speed, Axis axis)
	{
		return me.MoveTowards(targetPos,speed, Axis.Y);
	}
	
	public static bool MoveTowardsY(this GameObject me, GameObject targetPos, float speed, Axis axis)
	{
		return me.MoveTowards(targetPos.transform.position,speed, Axis.Y);
	}
	
	public static bool MoveTowardsZ(this GameObject me, float z, float speed) 
	{
		return me.MoveTowards(new Vector3(me.transform.position.x,me.transform.position.y,z),speed, Axis.X);
	}
	
	public static bool MoveTowardsZ(this GameObject me, Vector3 targetPos, float speed, Axis axis)
	{
		return me.MoveTowards(targetPos,speed, Axis.Z);
	}
	
	public static bool MoveTowardsZ(this GameObject me, GameObject targetPos, float speed, Axis axis)
	{
		return me.MoveTowards(targetPos.transform.position,speed, Axis.Z);
	}
	
	public static bool LerpTowards(this GameObject me, float x, float y, float z, float speed)
	{
		return me.LerpTowards(new Vector3(x,y,z),speed);
	}
	
	public static bool LerpTowards(this GameObject me, GameObject targetPos, float speed)
	{
		return me.LerpTowards(targetPos.transform.position,speed,.01f);
	}
	
	public static bool LerpTowards(this GameObject me, Vector3 targetPos, float speed)
	{
		return me.LerpTowards(targetPos,speed,.01f);
	}
	
	public static bool LerpTowards(this GameObject me, GameObject targetPos, float speed, float closeEnough)
	{
		return me.LerpTowards(targetPos.transform.position,speed,closeEnough);
	}
	
	public static bool LerpTowards(this GameObject me, Vector3 targetPos, float speed, float closeEnough)
	{
		if(me == null)
		{
			Debug.LogWarning("Nothing to Lerp Towards with");
			return false;
		}
		if(me.DistanceTo(targetPos) < closeEnough)
		{
			me.transform.position = targetPos;
			return false;
		}
		else
		{
			me.transform.position = Vector3.Lerp(me.transform.position, targetPos, speed);
		}
		return true;
	}
	
	public static bool RotateTowards(this GameObject me, Quaternion targetRot, float speed)
	{
		me.transform.rotation = Quaternion.RotateTowards(me.transform.rotation, targetRot, speed);
		return me.transform.rotation == targetRot;
	}
	
	public static bool RotateTowards(this GameObject me, Quaternion targetRot, float speed, Axis axis)
	{
		switch (axis)
		{
		case Axis.X: 
			targetRot = Quaternion.Euler(new Vector3(me.transform.position.x,targetRot.eulerAngles.y,targetRot.eulerAngles.z));
			break;
		case Axis.Y: 
			targetRot = Quaternion.Euler(new Vector3(targetRot.eulerAngles.x,me.transform.position.y,targetRot.eulerAngles.z));
			break;
		case Axis.Z: 
			targetRot = Quaternion.Euler(new Vector3(targetRot.eulerAngles.x,targetRot.eulerAngles.y,me.transform.position.z));
			break;
		}
		return me.RotateTowards(targetRot,speed);  
	}
	
	public static bool RotateTowardsX(this GameObject me, Quaternion targetRot, float speed)
	{
		return me.RotateTowards(targetRot,speed,Axis.X);  
	}
	
	public static bool RotateTowardsY(this GameObject me, Quaternion targetRot, float speed)
	{
		return me.RotateTowards(targetRot,speed,Axis.Y);  
	}
	
	public static bool RotateTowardsZ(this GameObject me, Quaternion targetRot, float speed)
	{
		return me.RotateTowards(targetRot,speed,Axis.Z);  
	}
	
	public static bool SmoothLookAt(this GameObject me, Vector3 targetPos, float speed)
	{
		if(targetPos - me.transform.position != Vector3.zero)
		{
			Quaternion rotation = Quaternion.LookRotation(targetPos - me.transform.position);
			me.transform.rotation = Quaternion.Slerp(me.transform.rotation, rotation, speed);
			if(me.transform.rotation == rotation)
				return true;
		}
		return false;
	}
	
	public static bool SmoothLookAt(this GameObject me, Vector3 targetPos, float speed, Axis axis)
	{
		switch (axis)
		{
		case Axis.X: 
			targetPos = new Vector3(me.transform.position.x,targetPos.y,targetPos.z);
			break;
		case Axis.Y: 
			targetPos = new Vector3(targetPos.x,me.transform.position.y,targetPos.z);
			break;
		case Axis.Z: 
			targetPos = new Vector3(targetPos.x,targetPos.y,me.transform.position.z);
			break;
		}
		return me.SmoothLookAt(targetPos,speed);  
	}
	
	public static bool SmoothLookAtX(this GameObject me, Vector3 targetPos, float speed)
	{
		return me.SmoothLookAt(targetPos,speed,Axis.X);  
	}
	
	public static bool SmoothLookAtY(this GameObject me, Vector3 targetPos, float speed)
	{
		return me.SmoothLookAt(targetPos,speed,Axis.Y);  
	}
	
	public static bool SmoothLookAtZ(this GameObject me, Vector3 targetPos, float speed)
	{
		return me.SmoothLookAt(targetPos,speed,Axis.Z);  
	}
	
	public static float DistanceTo(this GameObject me, Vector3 targetPos)
	{
		if(me == null)
		{
			return -1;
		}
		return Vector3.Distance(me.transform.position, targetPos);	
	}

	public static float DistanceTo(this GameObject me, GameObject target)
	{
		if(me == null)
		{
			return -1;
		}
		return Vector3.Distance(me.transform.position, target.transform.position);	
	}

	public static float DistanceTo(this GameObject me, Transform target)
	{
		if(me == null)
		{
			return -1;
		}
		return Vector3.Distance(me.transform.position, target.position);	
	}
	
	public static Vector3 GetClosestPosition(this GameObject me, params Vector3[] possiblePositions)
	{
		if(me == null)
		{
			return Vector3.zero;
		}
		int finalPosition = 0;
		for(int i = 1; i < possiblePositions.Length; i++)
		{
			if(me.DistanceTo(possiblePositions[i]) < me.DistanceTo(possiblePositions[finalPosition]))
			{
				finalPosition = i;
			}
		}
		return possiblePositions[finalPosition];	
	}
	
	public static Vector3 GetClosestPosition(this GameObject me, params GameObject[] possiblePositions)
	{
		if(me == null)
		{
			return Vector3.zero;
		}
		int finalPosition = 0;
		for(int i = 1; i < possiblePositions.Length; i++)
		{
			if(me.DistanceTo(possiblePositions[i].transform.position) < me.DistanceTo(possiblePositions[finalPosition].transform.position))
			{
				finalPosition = i;
			}
		}
		return possiblePositions[finalPosition].transform.position;	
	}
	
	public static void SmoothLookAt(this GameObject me, GameObject targetPos, float speed)
	{
		if(me == null)
		{
			return;
		}
		if(targetPos.transform.position - me.transform.position != Vector3.zero)
		{
			Quaternion rotation = Quaternion.LookRotation(targetPos.transform.position - me.transform.position);
			me.transform.rotation = Quaternion.Slerp(me.transform.rotation, rotation, speed);
		}
	}
	
	public static void LookTowards(this GameObject me, Vector3 targetPos, float speed)
	{
		if(me == null)
		{
			return;
		}
		if(targetPos - me.transform.position != Vector3.zero)
		{
			Quaternion rotation = Quaternion.LookRotation(targetPos - me.transform.position);
			me.transform.rotation = Quaternion.RotateTowards(me.transform.rotation, rotation, speed);	
		}
	}
	
	public static void LookTowards(this GameObject me, GameObject targetPos, float speed)
	{
		if(me == null)
		{
			return;
		}
		if(targetPos.transform.position - me.transform.position != Vector3.zero)
		{
			Quaternion rotation = Quaternion.LookRotation(targetPos.transform.position - me.transform.position);
			me.transform.rotation = Quaternion.RotateTowards(me.transform.rotation, rotation, speed);
		}
	}

	public static void SetChildrenActive(this GameObject me, bool active)
	{
		SetChildrenActiveRecursive(me.transform,active,false);
	}
	
	public static void SetChildrenActive(this GameObject me, bool active, bool isRecursive)
	{
		SetChildrenActiveRecursive(me.transform,active,isRecursive);
	}
	
	public static void SetChildrenActiveRecursive(Transform parent, bool activity, bool isRecursive)
	{
		foreach(Transform kid in parent)
		{
			kid.SetActive(activity);
			if(isRecursive)
			{
				SetChildrenActiveRecursive(kid,activity,isRecursive);
			}
		}
	}

	#endregion
}
