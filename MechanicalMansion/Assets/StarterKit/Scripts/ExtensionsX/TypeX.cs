using UnityEngine;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;

public static class TypeX
{
	public static T DeepClone<T>(T obj)
	{
		using (var ms = new MemoryStream())
		{
			var formatter = new BinaryFormatter();
			formatter.Serialize(ms, obj);
			ms.Position = 0;
			return (T)formatter.Deserialize(ms);
		}
	}

	public static bool IsIn<T>(this T source, params T[] list)
	{
		return list.ToList().Contains(source);
	}
}
