﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace StarterKit
{
    public static class InputX
    {
        /// <summary>
        /// Returns false if no Keycode was pressed, otherwise outs the result
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        public static KeyCode[] GetKeysDown() 
        {
            return GetKey(Input.GetKeyDown);
	    }

        /// <summary>
        /// Returns false if no Keycode was released, otherwise outs the result
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        public static KeyCode[] GetKeysUp()
        {
            return GetKey(Input.GetKeyUp);
        }

        /// <summary>
        /// Returns false if no Keycode is held, otherwise outs the result
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        public static KeyCode[] GetKeys()
        {
            return GetKey(Input.GetKey);
        }


        static KeyCode[] GetKey(Func<KeyCode,bool> inputMethod)
        {
            List<KeyCode> pressedCodes = new List<KeyCode>();
            foreach (KeyCode keyCode in EnumX.GetValues<KeyCode>())
            {
                if (inputMethod(keyCode))
                {
                    pressedCodes.Add(keyCode);
                }
            }
            return pressedCodes.ToArray();
        }
    }

}