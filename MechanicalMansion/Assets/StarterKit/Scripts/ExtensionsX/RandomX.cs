using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace StarterKit
{
	public static class RandomX 
	{
		public static System.Random randomGenerator = new System.Random();

        /// <summary>
        /// Retruns a random Number between 0 and 1
        /// </summary>
		public static float value
		{
			get
			{
				int percent = randomGenerator.Next(0,1000000001);
				return (float)(percent)/1000000000f;
			}
		}

        /// <summary>
        /// returns Random range between min and max values. Min value is Inclusive and Max value is not. ie. Range(0,5) will return a random numbers between of 0 and 5 except it will never be 5 exactly
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
		public static float  Range (float min, float max) 
		{
			float range = min.DistanceTo(max);
			return min + (value * range);
		}
		
        /// <summary>
        /// returns Random range between min and max values. Min value is Inclusive and Max value is not. ie. Range(0,5) will return a random number of 0,1,2,3,4 but never 5
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
		public static int  Range (int min, int max) 
		{
			return randomGenerator.Next(min,max);
		}

		/// <summary>
		/// Get Random Charactor of upper and lower case
		/// </summary>
		public static char Character()
		{
			List<char> chars = new List<char>();
			for (char i = 'a'; i <= 'z'; i++) 
			{
				chars.Add(i);
				chars.Add(i.ToString().ToUpper().ToCharArray()[0]);
			}
			return chars.GetRandom();
		}

		/// <summary>
		/// Get Random Charactor of only upper or lower case
		/// </summary>
		/// <param name="isUpperOnly">If set to <c>true</c> is upper only.</param>
		public static char Character(bool isUpperOnly)
		{
			return Character(isUpperOnly, null);
		}

		/// <summary>
		/// Get Random Charactor of only upper or lower case
		/// </summary>
		/// <param name="isUpperOnly">If set to <c>true</c> is upper only.</param>
		/// <param name="excludeList">Exclude list.</param>
		public static char Character(bool isUpperOnly, char[] excludeList)
		{
			List<char> chars = new List<char>();
			for (char i = 'a'; i <= 'z'; i++) 
			{
				if(excludeList != null)
				{
					if(!excludeList.ToList().Contains(i.ToString().ToUpper().ToCharArray()[0])
					   && !excludeList.ToList().Contains(i.ToString().ToCharArray()[0]))
					{
						if(!isUpperOnly)
							chars.Add(i);
						if(isUpperOnly)
							chars.Add(i.ToString().ToUpper().ToCharArray()[0]);
					}
				}
				else
				{
					if(!isUpperOnly)
						chars.Add(i);
					if(isUpperOnly)
						chars.Add(i.ToString().ToUpper().ToCharArray()[0]);
				}
			}
			return chars.GetRandom();
		}

        /// <summary>
        /// Gets a random character of the Alphabet
        /// </summary>
        /// <param name="excludeList"></param>
        /// <returns></returns>
		public static char Character(char[] excludeList)
		{
			return Character(excludeList, true);
		}

        /// <summary>
        /// Gets a random character of the Alphabet excluding characters from the excludeList. if excludesOtherCase is false it will be from both Upper and LowerCase
        /// Otherwise it will always be uppercase.
        /// </summary>
        /// <param name="excludeList"></param>
        /// <param name="excludeOtherCase"></param>
        /// <returns></returns>
		public static char Character(char[] excludeList, bool excludeOtherCase)
		{
			List<char> chars = new List<char>();
			for (char i = 'a'; i <= 'z'; i++) 
			{
				if(excludeList != null)
				{
					if(excludeOtherCase)
					{
						if(!excludeList.ToList().Contains(i.ToString().ToUpper().ToCharArray()[0])
						   && !excludeList.ToList().Contains(i.ToString().ToCharArray()[0]))
						{
							chars.Add(i);
							chars.Add(i.ToString().ToUpper().ToCharArray()[0]);
						}
					}
					else
					{
						if(!excludeList.ToList().Contains(i.ToString().ToUpper().ToCharArray()[0]))
						{
							chars.Add(i.ToString().ToUpper().ToCharArray()[0]);
						}
						
						if(!excludeList.ToList().Contains(i.ToString().ToCharArray()[0]))
						{
							chars.Add(i);
						}
					}
				}
				else
				{
						chars.Add(i);
						chars.Add(i.ToString().ToUpper().ToCharArray()[0]);
				}
			}
			return chars.GetRandom();
		}
	}
}
