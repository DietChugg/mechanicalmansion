using UnityEngine;
//using System;
using System.Collections;
using System.Collections.Generic;
using StarterKit;

public static class ListX 
{
	public static T GetRandom<T>(this List<T> list)
	{
        if(list.Count == 0)
            return default(T);
		Random.seed ++;
		if(list.Count == 0)
			Debug.Log ("List is zero!");
		return list[RandomX.Range(0,list.Count)];
	}

    public static T GetRandomFromRange<T>(this List<T> list, int index, int count)
    {
        if(list.Count == 0)
            return default(T);
        Random.seed ++;
        if(list.Count == 0)
            Debug.Log ("List is zero!");

        if(list.IsIndexInRange(index) || list.IsIndexInRange(index + count))
            Debug.LogException(new System.ArgumentOutOfRangeException(list.ToString(),list,"GetRandomFromRange index and/or count size is out of range"));

        return list[RandomX.Range(index,index+count)];
    }

    public static T GetLast<T>(this List<T> list)
    {
		if(list.Count > 0)
			return list[list.Count-1];
		return default(T);
    }

	public static bool Contains<T>(this List<T> list, int index)
	{
		if(index < list.Count && index >=0)
			return true;
		return false;
	}

	public static int GetFirstIndexOf<T>(this List<T> list, T item)
	{
		for (int i = 0; i < list.Count; i++) 
		{
			if(item.Equals(list[i]))
				return i;
		}
		return -1;
	}

    public static void AddIfDoesNotContain<T>(this List<T> list, T newItem)
    {
        if(!list.Contains(newItem))
            list.Add(newItem);
    }

	public static int ContainCount<T>(this List<T> list, T newItem)
	{
		int count = 0;
		for (int i = 0; i < list.Count; i++) 
		{
			if(list[i].Equals(newItem))
				count++;
		}
		return count;
	}

    public static T RemoveLast<T>(this List<T> list)
    {
        T last = list.GetLast();
        list.RemoveAt(list.Count-1);
        return last;
    }

    public static void ReplaceRange<T>(this List<T> list, T[] newRange)
    {
        list.Clear();
        list.AddRange(newRange);
    }

	public static void RemoveDuplicates<T>(this List<T> list)
	{
		List<T> duplicateFreeList = new List<T>();
		for (int i = 0; i < list.Count; i++)
		{
			if(!duplicateFreeList.Contains(list[i]))
				duplicateFreeList.Add(list[i]);
		}
		list.Clear();
		list.AddRange(duplicateFreeList.ToArray());
	}

	public static void RemoveNulls<T>(this List<T> list)
	{
		List<T> nullFreeList = new List<T>();
		for (int i = 0; i < list.Count; i++)
		{
			if(!EqualityComparer<T>.Default.Equals(list[i], default(T)))
				nullFreeList.Add(list[i]);
		}
		list.Clear();
		list.AddRange(nullFreeList.ToArray());
	}

    public static bool IsIndexInRange<T>(this List<T> list, int index)
    {
        if(index < 0 || index >= list.Count)
            return false;
        return true;
    }

	public static bool IsAllValuesTheSame<T>(this List<T> list)
	{
		List<T> copyList = new List<T>();
		copyList.AddRange(list.ToArray());
		if(copyList.Count == 1 || copyList.Count == 0)
			return true;

		T item = copyList[0];
		copyList.RemoveAt(0);
		for (int i = 0; i < copyList.Count; i++)
		{
			if(!EqualityComparer<T>.Default.Equals(item, copyList[i]))
				return false;
		}
		return true;
	}

	public static int GetRandomInt<T>(this List<T> list)
	{
		Random.seed ++;
		return Random.Range(0,list.Count);
	}

	public static List<T> Shuffle<T>(this List<T> list)
	{
		List<T> shuffledList = new List<T>();
		while(list.Count > 0)
		{
			int randomPick = list.GetRandomInt();
			shuffledList.Add(list[randomPick]);
			list.RemoveAt(randomPick);
		}
		return shuffledList;
	}
}
