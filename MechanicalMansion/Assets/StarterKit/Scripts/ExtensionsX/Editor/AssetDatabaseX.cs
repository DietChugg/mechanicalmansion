﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public static class AssetDatabaseX
{
    /// <summary>
    /// Returns all Objects in the Project Folder of Unity of type
    /// </summary>
    /// <param name="type">ie ".js" or ".cs"</param>
    /// <returns></returns>
    //public static Object[] FindAllCSScripts(string type)
    //{
    //    //string[] allPaths = AssetDatabase.FindAssets("t:Script");
    //    //Debug.Log(allPaths.Length);
    //    //List<Object> matchingTypeObjects = new List<Object>();
    //    //for (int i = 0; i < allPaths.Length; i++)
    //    //{
    //    //    Debug.Log(allPaths[i]);
    //    //    if (allPaths[i].EndsWith(type))
    //    //    {
    //    //        Debug.Log("Match Found");
    //    //        matchingTypeObjects.Add(AssetDatabase.LoadAssetAtPath(allPaths[i],typeof(MonoScript)));
    //    //    }
    //    //}
    //    return GetAssetsAtFolder(Application.dataPath, true);
    //}

    //public static Object[] GetAssetsAtFolder(string folderPath, bool isRecursive)
    //{ 
    //    List<Object> assets = new List<Object>();

    //    assets.AddRange(AssetDatabaseX.GetAtPath<MonoScript>(folderPath));
    //    Debug.Log(assets.Count);
    //    if(isRecursive)
    //    {
    //        string[] paths = Directory.GetDirectories(folderPath);
    //        for (int i = 0; i < paths.Length; i++)
    //        {
    //            assets.AddRange(AssetDatabaseX.GetAssetsAtFolder(paths[i],true));
    //        }
    //    }
          
    //    return assets.ToArray();
    //}

    //public static Object[] GetAssetsAtFolder(string folderPath)
    //{
    //    return GetAssetsAtFolder(folderPath,false);
    //}


    /// <summary>
    /// Get an array of all of a type at a path
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="path"></param>
    /// <returns></returns>
    public static T[] GetAtPath<T>(string path)
    {

        ArrayList al = new ArrayList();
        string[] fileEntries = Directory.GetFiles(path);
        foreach (string fileName in fileEntries)
        {
            int index = fileName.LastIndexOf("/");
            string localPath = "Assets/" + path;

            if (index > 0)
                localPath += fileName.Substring(index);

            Object t = AssetDatabase.LoadAssetAtPath(localPath, typeof(T));

            if (t != null)
                al.Add(t);
        }
        T[] result = new T[al.Count];
        for (int i = 0; i < al.Count; i++)
            result[i] = (T)al[i];

        return result;
    }

    /// <summary>
    /// Will give you a full or local path to an object in Project Folder
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="isFullPath"></param>
    /// <returns></returns>
    public static string GetPath(Object obj, bool isFullPath)
    {
        if (isFullPath)
        {
            return AssetDatabase.GetAssetPath(obj);
        }
        else
        {
            return Application.dataPath.InsetFromEnd(6) + AssetDatabase.GetAssetPath(obj);
        }
    }

    /// <summary>
    /// This will work with a local path ie "Assets/item.png" as well as a full path ie "C:/Users/Micheael/Assets/item.png" 
    /// </summary>
    /// <param name="path"></param>
    /// <param name="assetType"></param>
    /// <returns></returns>
    public static UnityEngine.Object GetAsset(string path, System.Type assetType)
    {
        Object obj = AssetDatabase.LoadAssetAtPath(path,assetType);
        if(obj != null)
            return obj;
        return AssetDatabase.LoadAssetAtPath(path.InsetFromStart(Application.dataPath.Length - 6),assetType);
    }

    /// <summary>
    /// Returns all file names or paths of filetype in the Unity Project Folder
    /// </summary>
    /// <param name="fileType"></param>
    /// <param name="isNameNotPath"></param>
    /// <returns></returns>
    public static string[] FindAllProjectFolderFiles(string fileType, FindFileOptions findFileOptions)
    { 
        DirectoryInfo directoryInfo = new DirectoryInfo(Application.dataPath);
        return FindFiles(fileType, directoryInfo, findFileOptions);
    }

    static string[] FindFiles(string fileType, DirectoryInfo directoryInfo, FindFileOptions findFileOptions)
    {
        List<string> matchingFiles = new List<string>();
        DirectoryInfo[] directoryInfos = directoryInfo.GetDirectories();
        for (int i = 0; i < directoryInfos.Length; i++)
        {
            matchingFiles.AddRange(FindFiles(fileType, directoryInfos[i], findFileOptions));
        }
        FileInfo[] files = directoryInfo.GetFiles();
        for (int i = 0; i < files.Length; i++)
        {
            if (files[i].Name.EndsWith(fileType))
            {
                switch (findFileOptions)
                {
                    case FindFileOptions.FullFilePath:
                        matchingFiles.Add(files[i].DirectoryName + fileType);
                        break;
                    case FindFileOptions.FilePathWithoutFiletype:
                        matchingFiles.Add(files[i].DirectoryName);
                        break;
                    case FindFileOptions.FullFileName:
                        matchingFiles.Add(files[i].Name);
                        break;
                    case FindFileOptions.FileNameWithoutFiletype:
                        matchingFiles.Add(files[i].Name.InsetFromEnd(fileType.Length));
                        break;
                    default:
                        break;
                }
            }
        }
        return matchingFiles.ToArray();
    }

    //public static Object[] GetAssetsAtFolder(string folderPath, bool isRecursive, Object[] recursiveObjs)
    //{ 
    //    List<Object> assets = new List<Object>();

    //    assets.AddRange(AssetDatabase.LoadAllAssetsAtPath(folderPath));

    //    if(isRecursive)
          
    //    string[] folders = 
    //}
}

public enum FindFileOptions
{
    FullFilePath,
    FilePathWithoutFiletype,
    FullFileName,
    FileNameWithoutFiletype
}
	
#endif