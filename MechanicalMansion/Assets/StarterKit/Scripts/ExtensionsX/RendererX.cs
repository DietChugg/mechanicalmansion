using UnityEngine;
using System.Collections;

public static class RendererX 
{
	#region Constants
	#endregion
	
	#region StaticMethods
	#endregion
	
	#region ExtensionMethods
	public static bool IsVisibleFrom(this Renderer renderer, Camera camera)
	{
		Plane[] planes = GeometryUtility.CalculateFrustumPlanes(camera);
		return GeometryUtility.TestPlanesAABB(planes, renderer.bounds);
	}

    public static Color SetR(this Renderer me, float value)
    {
        return new Color(value,me.material.color.g,me.material.color.b,me.material.color.a);
    }

    public static Color SetG(this Renderer me, float value)
    {
        return new Color(me.material.color.r,value,me.material.color.b,me.material.color.a);
    }

    public static Color SetB(this Renderer me, float value)
    {
        return new Color(me.material.color.r,me.material.color.g,value,me.material.color.a);
    }

    public static Color SetA(this Renderer me, float value)
    {
        return new Color(me.material.color.r,me.material.color.g,me.material.color.b,value);
    }
	#endregion
}
