using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace StarterKit
{
	public static class Vector3ArrayX
	{
		public static float[] GetAxisValues(this Vector3[] me, Axis axis)
		{
			List<float> floats = new List<float>(); 
			for (int i = 0; i < me.Length; i++) 
			{
				switch(axis)
				{
				case Axis.X: 
					floats.Add(me[i].x);
					break;
				case Axis.Y: 
					floats.Add(me[i].y);
					break;
				case Axis.Z: 
					floats.Add(me[i].z);
					break;
				}
			}
			return floats.ToArray();
		}

		public static float[] GetXValues(this Vector3[] me)
		{
			return me.GetAxisValues(Axis.X);
		}

		public static float[] GetYValues(this Vector3[] me)
		{
			return me.GetAxisValues(Axis.Y);
		}

		public static float[] GetZValues(this Vector3[] me)
		{
			return me.GetAxisValues(Axis.Z);
		}

		public static Vector2[] AsVector2Array(this Vector3[] me)
		{
			List<Vector2> convertedPoints = new List<Vector2>();
			for (int i = 0; i < me.Length; i++) 
			{
				convertedPoints.Add(me[i]);
			}
			return convertedPoints.ToArray();
		}

		public static Vector3 ClosestPointTo(this Vector3[] me, Vector3 comparePoint)
		{
			int closestIndex = 0;
			for (int i = 0; i < me.Length; i++) 
			{
				if(me[i].Distance(comparePoint) < me[closestIndex].Distance(comparePoint))
				{
					closestIndex = i;
				}
			}
			return me[closestIndex];
		}

		public static int ClosestIndexTo(this Vector3[] me, Vector3 comparePoint)
		{
			int closestIndex = 0;
			for (int i = 0; i < me.Length; i++) 
			{
				if(me[i].Distance(comparePoint) < me[closestIndex].Distance(comparePoint))
				{
					closestIndex = i;
				}
			}
			return closestIndex;
		}

	}
}
