using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace StarterKit
{
    public static class CharX
    {
        #region Chars in a-z and A-Z range
        /// <summary>
        /// <para>Returns the upper case version of an a-z char.</para>
        /// <para>If not a-z, the returned char is unchanged.</para>
        /// </summary>
        /// <param name="me"></param>
        /// <returns>char</returns>
        public static char ToUpper(this char me)
        {
            if (me.IsLowerCaseAlpha())
                me = (char)((int)me - 32);
            else
                Debug.LogWarning("Char was not Lower-Case Alpha");

            return me;
        }

        /// <summary>
        /// <para>Returns the lower case version of an A-Z char.</para>
        /// <para>If not A-Z, the returned char is unchanged.</para>
        /// </summary>
        /// <param name="me"></param>
        /// <returns>char</returns>
        public static char ToLower(this char me)
        {
            if (me.IsUpperCaseAlpha())
                me = (char)((int)me + 32);
            else
                Debug.LogWarning("Char was not Upper-Case Alpha");

            return me;
        }

        /// <summary>
        /// <para>Returns the next char in alphabetic order. At the end of a-z, A is returned. At the end of A-Z, a is returned.</para>
        /// <para>If not a-z or A-Z, the returned char is unchanged.</para>
        /// </summary>
        /// <param name="me"></param>
        /// <returns>char</returns>
        public static char NextAlphaChar(this char me)
        {
            return me.NextAlphaChar(false);
        }

        /// <summary>
        /// <para>Returns the next char in alphabetic order.</para>
        /// <para>If not a-z or A-Z, the returned char is unchanged.</para>
        /// </summary>
        /// <param name="me"></param>
        /// <param name="stayInCase"> 
        /// <para>If true: </para>
        /// <para>At the end of A-Z, A is returned. At the end of a-z, a is returned.</para>
        /// <para>If false: </para>
        /// <para>At the end of a-z, A is returned. At the end of A-Z, a is returned.</para>
        /// </param>
        /// <returns>char</returns>
        public static char NextAlphaChar(this char me, bool stayInCase)
        {
            if (me.IsAlpha())
            {
                if (stayInCase)
                {
                    if (me == 'z')
                        me = 'a';
                    else if (me == 'Z')
                        me = 'A';
                    else
                        me++;
                }
                else
                {
                    if (me == 'z')
                        me = 'A';
                    else if (me == 'Z')
                        me = 'a';
                    else
                        me++;
                }
            }
            else
            {
                Debug.LogWarning("Char was not a-z or A-Z range, returned value is unchanged!");
            }

            return me;
        }

        /// <summary>
        /// Returns a random char between a-z and A-Z.
        /// </summary>
        /// <returns>char</returns>
        public static char RandomAlphaChar()
        {
            char c;

            int upperCase = Random.Range(0, 2);
            if (upperCase > 0)
                c = RandomUpperCaseAlphaChar();
            else
                c = RandomLowerCaseAlphaChar();

            return c;
        }

        /// <summary>
        /// Returns a random char between A-Z.
        /// </summary>
        /// <returns>char</returns>
        public static char RandomUpperCaseAlphaChar()
        {
            return (char)Random.Range(65, 91);
        }

        /// <summary>
        /// Returns a random char between a-z.
        /// </summary>
        /// <returns>char</returns>
        public static char RandomLowerCaseAlphaChar()
        {
            return (char)Random.Range(97, 123);
        }

        /// <summary>
        /// Returns true if char is an upper case letter between A-Z.
        /// </summary>
        /// <param name="me"></param>
        /// <returns>bool</returns>
        public static bool IsUpperCaseAlpha(this char me)
        {
            int validAlphaCheck = (int)me;
            return validAlphaCheck >= 65 && validAlphaCheck <= 90;
        }

        /// <summary>
        /// Returns true if char is an lower case letter between a-z.
        /// </summary>
        /// <param name="me"></param>
        /// <returns>bool</returns>
        public static bool IsLowerCaseAlpha(this char me)
        {
            int validAlphaCheck = (int)me;
            return validAlphaCheck >= 97 && validAlphaCheck <= 122;
        }

        /// <summary>
        /// Returns true if char is a valid letter between a-z or A-Z.
        /// </summary>
        /// <param name="me"></param>
        /// <returns>bool</returns>
        public static bool IsAlpha(this char me)
        {
            return me.IsUpperCaseAlpha() || me.IsLowerCaseAlpha();
        }
        #endregion

        #region Chars in 0-9 range

        /// <summary>
        /// Returns true if char is an int between 0-9.
        /// </summary>
        /// <param name="me"></param>
        /// <returns>bool</returns>
        public static bool IsInt(this char me)
        {
            int validAlphaCheck = (int)me;
            return validAlphaCheck >= 48 && validAlphaCheck <= 57;
        }

        /// <summary>
        /// Returns a random char between 0-9.
        /// </summary>
        /// <returns>char</returns>
        public static char RandomIntChar()
        {
            return (char)Random.Range(48, 58);
        }

        /// <summary>
        /// <para>Returns the next char in numerical order.</para>
        /// <para>Values loop back to 0 after 9.</para>
        /// <para>If not 0-9, the returned char is unchanged.</para>
        /// </summary>
        /// <param name="me"></param>
        /// <returns>char</returns>
        public static char NextIntChar(this char me)
        {
            if (me.IsInt())
            {
                if (me == '9')
                    me = '0';
                else
                    me++;
            }
            else
            {
                Debug.LogWarning("Char was not in 0-9 range, returned value is unchanged!");
            }

            return me;
        }

        #endregion
    }
}
