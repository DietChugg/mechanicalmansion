using UnityEngine;
using System.Collections;

public static class IntArrayX
{

	public static int Min(this int[] me)
	{
		if(me == null || me.Length == 0)
		{
			Debug.LogError("I'm Nonexistant");
			return 0;
		}

		int min = me[0];
		for (int i = 0; i < me.Length; i++) 
		{
			min = Mathf.Min(min,me[i]);
		}
		return min;
	}

	public static int Max(this int[] me)
	{
		if(me == null || me.Length == 0)
		{
			Debug.LogError("I'm Nonexistant");
			return 0;
		}
		
		int max = me[0];
		for (int i = 0; i < me.Length; i++) 
        {
			max = Mathf.Max(max,me[i]);
        }
		return max;
    }

	public static int Center(this int[] me)
	{
		return (me.Max () + me.Min ())/2;
	}
}
