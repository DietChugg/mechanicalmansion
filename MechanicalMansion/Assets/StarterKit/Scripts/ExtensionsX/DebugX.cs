using UnityEngine;
using System.Collections;
using System;

[Obsolete("Using DebugX doesn't link back to code... Avoid it!")]
public static class DebugX 
{
    public static void Log(object message, UnityEngine.Object context, string color)
    {
        Debug.Log("<color=" + color + ">" + message + "</color>",context);
    }

    public static void Log(object message, string color)
    {
        Debug.Log("<color=" + color + ">" + message + "</color>");
    }

    public static void Log(object message, UnityEngine.Object context)
    {
        Debug.Log(message,context);
    }

    public static void Log(object message)
    {
        System.Console.WriteLine(message);
    }
}
