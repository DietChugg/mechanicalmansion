using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public static class GameObjectArrayX 
{
    /// <summary>
    /// Call a method that takes a gameObject as a parameter on all gameObjects of this array.
    /// </summary>
    /// <param name="me"></param>
    /// <param name="action"></param>
    public static void ApplyMethod(this GameObject[] me, Action<GameObject> action)
    {
        for (int i = 0; i < me.Length; i++)
        {
            if (action != null)
	        {
		        action(me[i]);
	        }
        }
    }

    /// <summary>
    /// Returns a transform array of the gameObject array
    /// </summary>
    /// <param name="me"></param>
    /// <returns></returns>
    public static Transform[] ToTransformArray(this GameObject[] me)
    {
        Transform[] gos = new Transform[me.Length];
        for (int i = 0; i < me.Length; i++)
        {
            gos[i] = me[i].transform;
        }
        return gos;
    }


    /// <summary>
    /// Grab components of a type from all gameObjects
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="me"></param>
    /// <returns></returns>
	public static T[] GetComponents<T>(this GameObject[] me) where T: Component
	{
		List<T> components = new List<T>();
		for (int i = 0; i < me.Length; i++) 
		{
			T component = me[i].GetComponent<T>();
			if(component != null)
				components.Add(component);
		}
		return components.ToArray();
	}

    /// <summary>
    /// Finds GameObject With Name if one exists. Returns null otherwise
    /// </summary>
    /// <param name="me"></param>
    /// <param name="name"></param>
    /// <returns></returns>
    public static GameObject GetGameObjectWithName(this GameObject[] me, string name)
    {
        for (int i = 0; i < me.Length; i++)
        {
            if (me[i].name == name)
            {
                return me[i];
            }
        }
        return null;
    }

    public static Vector3 GetCenterPosition(this GameObject[] me)
    {
        return me.ToTransformArray().GetCenterPosition();
    }

    /// <summary>
    /// Sets all gameObjects active or inactive
    /// </summary>
    /// <param name="me"></param>
    /// <param name="value"></param>
	public static void SetActive(this GameObject[] me, bool value)
	{
		for (int i = 0; i < me.Length; i++) 
		{
			me[i].SetActive(value);
		}
	}

    /// <summary>
    /// Adds a Compoent of type T to all gameobjects
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="me"></param>
    /// <returns></returns>
    public static T[] AddComponent<T>(this GameObject[] me)where T : Component
    {
        T[] newComponents = new T[me.Length];
        for (int i = 0; i < me.Length; i++)
        {
            newComponents[i] = me[i].AddComponent<T>();
        }
        return newComponents;
    }


    /// <summary>
    /// Sets all gameObjects parent to newParent
    /// </summary>
    /// <param name="me"></param>
    /// <param name="newParent"></param>
    /// <param name="worldPositionStays"></param>
    public static void SetParent(this GameObject[] me, GameObject newParent, bool worldPositionStays = false)
    {
        for (int i = 0; i < me.Length; i++)
        {
            me[i].transform.SetParent(newParent.transform,worldPositionStays);
        }
    }

    /// <summary>
    /// Sets all gameObjects parent to newParent
    /// </summary>
    /// <param name="me"></param>
    /// <param name="newParent"></param>
    /// <param name="worldPositionStays"></param>
    public static void SetParent(this GameObject[] me, Transform newParent, bool worldPositionStays = false)
    {
        for (int i = 0; i < me.Length; i++)
        {
            me[i].transform.SetParent(newParent, worldPositionStays);
        }
    }

    /// <summary>
    /// Toggle Active Added
    /// </summary>
    /// <param name="me"></param>
    public static void ToggleActive(this GameObject[] me)
    {
        for (int i = 0; i < me.Length; i++)
        {
            me[i].SetActive(!me[i].activeSelf);
        }
    }
}
