using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using StarterKit;

public static class RendererArrayX 
{
    public static void SetEnabled(this Renderer[] me, bool value)
    {
        for (int i = 0; i < me.Length; i++)
        {
            me[i].enabled = value;
        }
    }
}
