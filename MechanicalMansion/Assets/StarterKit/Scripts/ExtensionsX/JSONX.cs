#if JSON
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

public static class JSONX
{

    public static JSONObject ToJSON(System.Object classInstance)
    {
        JSONObject jsonObj = new JSONObject();

        Type myObjectType = classInstance.GetType();
        FieldInfo[] fieldInfo = myObjectType.GetFields();
        bool showWarning = false;
        for (int i = 0; i < fieldInfo.Length; i++)
        {

            //Debug.Log(fieldInfo[i].Name.ToString().RTGreen());
            //Debug.Log(fieldInfo[i].FieldType);
            //Debug.Log(fieldInfo[i].GetValue(classInstance));
            switch (fieldInfo[i].FieldType.ToString())
            {
                case("System.Int32"):
		            jsonObj.AddField(fieldInfo[i].Name, (int)(fieldInfo[i].GetValue(classInstance)));
                    break;
                case ("System.Single"):
                    jsonObj.AddField(fieldInfo[i].Name, (float)(fieldInfo[i].GetValue(classInstance)));
                break;
                case ("System.String"):
                    jsonObj.AddField(fieldInfo[i].Name, (string)(fieldInfo[i].GetValue(classInstance)));
                break;
                case ("System.Boolean"):
                    jsonObj.AddField(fieldInfo[i].Name, (bool)(fieldInfo[i].GetValue(classInstance)));
                break;



                case ("System.Int32[]"):
                    int[] ints = (int[])(fieldInfo[i].GetValue(classInstance));
                    JSONObject intArray = new JSONObject(JSONObject.Type.ARRAY);
                    if(ints != null)
                    { 
                        for (int j = 0; j < ints.Length; j++)
			            {
                            intArray.Add(ints[j]);
			            }
                    }
                    jsonObj.AddField(fieldInfo[i].Name, intArray);
                    
                break;
                case ("System.Single[]"):
                    float[] floats = (float[])(fieldInfo[i].GetValue(classInstance));
                    JSONObject floatArray = new JSONObject(JSONObject.Type.ARRAY);
                    if(floats != null)
                    { 
                        for (int j = 0; j < floats.Length; j++)
			            {
                            floatArray.Add(floats[j]);
			            }
                    }
                    jsonObj.AddField(fieldInfo[i].Name, floatArray);
                break;
                case ("System.String[]"):
                    string[] strings = (string[])(fieldInfo[i].GetValue(classInstance));
                    JSONObject stringArray = new JSONObject(JSONObject.Type.ARRAY);
                    if(strings != null)
                    { 
                        for (int j = 0; j < strings.Length; j++)
			            {
                            stringArray.Add(strings[j]);
			            }
                    }
                    jsonObj.AddField(fieldInfo[i].Name, stringArray);
                break;
                case ("System.Boolean[]"):
                    bool[] bools = (bool[])(fieldInfo[i].GetValue(classInstance));
                    JSONObject boolArray = new JSONObject(JSONObject.Type.ARRAY);
                    if(bools != null)
                    { 
                        for (int j = 0; j < bools.Length; j++)
			            {
                            boolArray.Add(bools[j]);
			            }
                    }
                    jsonObj.AddField(fieldInfo[i].Name, boolArray);
                break;

                default:
                if (fieldInfo[i].FieldType.GetConstructors() != null && fieldInfo[i].FieldType.GetConstructors().Length > 0)
                {
                    jsonObj.AddField(
                    fieldInfo[i].Name,
                    (JSONObject)(JSONX.ToJSON(fieldInfo[i].GetValue(classInstance)))
                    );
                }
                else
                {
                    showWarning = true;
                }
                break;
            }

        }

        if (showWarning)
        {
            Debug.LogWarning("All Data was not tranferred from the classes. Only int, float bool, and string data can be converted. Manually Transfer all other data to JSON");
        }

        return jsonObj;
    }

    public static object FromJSON(JSONObject jsonObj, Type myObjectType)
    {
        object instance = Activator.CreateInstance(myObjectType, new object[] { });
        //Search Through Types ect.

        //Type myObjectType = typeof(T);
        FieldInfo[] fieldInfo = myObjectType.GetFields();
        bool showWarning = false;

        for (int i = 0; i < fieldInfo.Length; i++)
        {
            switch (fieldInfo[i].FieldType.ToString())
            {
                case("System.Int32"):
                    fieldInfo[i].SetValue(instance,(int)jsonObj.GetField(fieldInfo[i].Name).n);
                    break;
                case ("System.Single"):
                    fieldInfo[i].SetValue(instance, jsonObj.GetField(fieldInfo[i].Name).f);
                    break;
                case ("System.String"):
                    fieldInfo[i].SetValue(instance, jsonObj.GetField(fieldInfo[i].Name).str);
                    break;
                case ("System.Boolean"):
                    fieldInfo[i].SetValue(instance, jsonObj.GetField(fieldInfo[i].Name).b);
                    break;

                case ("System.Int32[]"):
                    JSONObject intArray = jsonObj.GetField(fieldInfo[i].Name);
                    List<int> intList = new List<int>();
                    for (int j = 0; j < intArray.list.Count; j++)
                    {
                        intList.Add((int)intArray.list[j].n);
                    }
                    fieldInfo[i].SetValue(instance, (int[])intList.ToArray());
                    break;

                case ("System.Single[]"):
                    JSONObject floatArray = jsonObj.GetField(fieldInfo[i].Name);
                    List<float> floatList = new List<float>();
                    for (int j = 0; j < floatArray.list.Count; j++)
                    {
                        floatList.Add(floatArray.list[j].f);
                    }
                    fieldInfo[i].SetValue(instance, (float[])floatList.ToArray());
                    break;

                case ("System.String[]"):
                    JSONObject stringArray = jsonObj.GetField(fieldInfo[i].Name);
                    List<string> stringList = new List<string>();
                    for (int j = 0; j < stringArray.list.Count; j++)
                    {
                        stringList.Add(stringArray.list[j].str);
                    }
                    fieldInfo[i].SetValue(instance, (string[])stringList.ToArray());
                    break;

                case ("System.Boolean[]"):
                    JSONObject boolArray = jsonObj.GetField(fieldInfo[i].Name);
                    List<bool> boolList = new List<bool>();
                    for (int j = 0; j < boolArray.list.Count; j++)
                    {
                        boolList.Add(boolArray.list[j].b);
                    }
                    fieldInfo[i].SetValue(instance, (bool[])boolList.ToArray());
                    break;

                default:
                    if (fieldInfo[i].FieldType.GetConstructors() != null && fieldInfo[i].FieldType.GetConstructors().Length > 0)
                    {
                        fieldInfo[i].SetValue(
                            instance,
                            JSONX.FromJSON(
                                jsonObj.GetField(fieldInfo[i].Name),
                                fieldInfo[i].FieldType
                            ));
                    }
                    else
                    {
                        showWarning = true;
                    }
                    break;
            }
            if (showWarning)
            {
                Debug.LogWarning("All Data was not tranferred to the resulting object. Only int, float bool, and string data can be converted. Manually Transfer all other data to the object");
            }
            //Debug.Log(fieldInfo[i].Name.ToString().RTGreen());
            //Debug.Log(fieldInfo[i].FieldType);
            //Debug.Log(fieldInfo[i].GetValue(classInstance));
            //if (fieldInfo[i].FieldType.ToString() == "System.Int32")
            //{
            //    fieldInfo[i].SetValue(instance,-19284);
            //}



                
        }

        return instance;
    }

    public static object FromJSON(string jsonString, Type myObjectType)
    {
        JSONObject jsonObj = new JSONObject(jsonString);
        return FromJSON(jsonObj, myObjectType);
    }

}
#endif
