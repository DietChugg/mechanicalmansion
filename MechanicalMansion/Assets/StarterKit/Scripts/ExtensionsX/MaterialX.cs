using UnityEngine;
using System.Collections;

public static class MaterialExtensions 
{
	public static void SetA(this Material me, float a)
	{
		if(me == null)
		{
			//Debug.LogError("Cannot MoveTowards because Transform is null");
			return;
		}
		me.color = new Color(me.color.r,me.color.g,me.color.b,a);
	}
	
	public static void SetR(this Material me, float r)
	{
		if(me == null)
		{
			//Debug.LogError("Cannot MoveTowards because Transform is null");
			return;
		}
		me.color = new Color(r,me.color.g,me.color.b,me.color.a);
	}
	
	public static void SetG(this Material me, float g)
	{
		if(me == null)
		{
			//Debug.LogError("Cannot MoveTowards because Transform is null");
			return;
		}
		me.color = new Color(me.color.r,g,me.color.b,me.color.a);
	}
	
	public static void SetB(this Material me, float b)
	{
		if(me == null)
		{
			//Debug.LogError("Cannot MoveTowards because Transform is null");
			return;
		}
		me.color = new Color(me.color.r,me.color.g,b,me.color.a);
	}
}
