using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections;
using System.Collections.Generic;
using StarterKit;

public static class TransformX
{
    #region Constants
    #endregion

    #region StaticMethods

    public static Vector3 PointOnLine(Transform point1, Transform point2, float d)
    {
        return Vector2X.PointOnLine(point1.position, point2.position, d);
    }
    #endregion

    #region ExtensionMethods

    public static void LookAt2D(this Transform me, Transform target, float angleOffset)
    {
        me.rotation = QuaternionX.LookAtRotation2D(me, target, angleOffset);
    }

    public static void LookAt2D(this Transform me, Transform target)
    {
        me.LookAt2D(target, 0);
    }

    public static void DestoryKids(this Transform me)
    {
        Transform[] kids = me.GetKids();
        for (int i = 0; i < kids.Length; i++)
        {
            GameObject.Destroy(kids[i].gameObject);
        }
    }

#if UNITY_EDITOR
    public static void DestoryKidsImmediate(this Transform me)
    {
        Transform[] kids = me.GetKids();
        for (int i = 0; i < kids.Length; i++)
        {
            GameObject.DestroyImmediate(kids[i].gameObject);
        }
    }
#endif

    public static void Set(this Transform me, TransformData transformData)
    {
        transformData.ApplyTo(me);
    }

    public static string GetPath(this Transform me)
    {
        string path = me.name;
        Transform currentTransform = me;
        while (currentTransform.parent != null)
        {
            path = currentTransform.parent.name + "/" + path;
            currentTransform = currentTransform.parent;
        }
        return path;
    }

    public static void Reset(this Transform me)
    {
        me.position = Vector3.zero;
        me.rotation = Quaternion.identity;
        me.SetLossyScale(Vector3.one);
    }

    public static void Reset(this Transform me, bool isLocal)
    {
        if (!isLocal)
        {
            me.Reset();
            return;
        }
        me.localPosition = Vector3.zero;
        me.localRotation = Quaternion.identity;
        me.localScale = Vector3.one;
    }

    public static void MoveToMouse(this Transform me)
    {
        if (me == null)
        {
            Debug.LogError("Camera is Null. Make sure you have a Main Camera in the scene or that you have assinged a camera properly");
            return;
        }
        me.MoveToMouse(Camera.main);
        //		me.GetKids(
    }

    public static void MoveToMouse(this Transform me, float distanceOut)
    {
        me.MoveToMouse(Camera.main, distanceOut);
    }

    public static void MoveToMouse(this Transform me, Camera targetCamera)
    {
        if (targetCamera == null)
        {
            me.MoveToMouse();
            return;
        }
        Vector3 heading = me.position - targetCamera.transform.position;
        float depth = Vector3.Dot(heading, targetCamera.transform.forward);
        Vector3 mouseScreenPos = Input.mousePosition;
        mouseScreenPos.z = depth;
        Vector3 mouseWorldPosition = targetCamera.ScreenToWorldPoint(mouseScreenPos);
        me.position = new Vector3(mouseWorldPosition.x, mouseWorldPosition.y, mouseWorldPosition.z);
    }

    public static void MoveToMouse(this Transform me, Camera targetCamera, float distanceOut)
    {
        if (targetCamera == null)
        {
            me.MoveToMouse();
            return;
        }
        Vector3 heading = me.position - targetCamera.transform.position;
        float depth = Vector3.Dot(heading, targetCamera.transform.forward);
        Vector3 mouseScreenPos = Input.mousePosition;
        mouseScreenPos.z = depth;
        mouseScreenPos.z = distanceOut;
        Vector3 mouseWorldPosition = targetCamera.ScreenToWorldPoint(mouseScreenPos);
        me.position = new Vector3(mouseWorldPosition.x, mouseWorldPosition.y, mouseWorldPosition.z);
    }

    //	public static 

    public static Renderer[] GetRenderers(this Transform me)
    {
        if (me == null)
        {
            Debug.LogError("Camera is Null. Make sure you have a Main Camera in the scene or that you have assinged a camera properly");
            return null;
        }
        Transform[] allKids = me.GetKids(true);
        List<Renderer> renderers = new List<Renderer>();
        for (int i = 0; i < allKids.Length; i++)
        {
            if (allKids[i].GetComponent<Renderer>())
                renderers.Add(allKids[i].GetComponent<Renderer>());
        }
        return renderers.ToArray();
    }

    public static Renderer[] GetRenderers(this Transform me, bool includeSelf)
    {
        if (me == null)
        {
            Debug.LogError("Camera is Null. Make sure you have a Main Camera in the scene or that you have assinged a camera properly");
            return null;
        }
        Transform[] allKids = me.GetKids(true);
        List<Renderer> renderers = new List<Renderer>();
        if (includeSelf)
            renderers.Add(me.GetComponent<Renderer>());
        for (int i = 0; i < allKids.Length; i++)
        {
            if (allKids[i].GetComponent<Renderer>())
                renderers.Add(allKids[i].GetComponent<Renderer>());
        }
        return renderers.ToArray();
    }

    public static Vector3 GetEditorCenter(this Transform me)
    {
        Renderer[] renderers = GetRenderers(me);
        Vector3 center = Vector3.zero;
        for (int i = 0; i < renderers.Length; i++)
        {
            center += renderers[i].bounds.center;
        }
        center = center / renderers.Length;
        if (renderers.Length == 0)
            return Vector3.zero;
        return center;
    }

    public static bool IsInFrontOfMe(this Transform me, GameObject other)
    {
        if (Camera.main == null)
        {
            Debug.LogError("I'm null and will always return false");
            return false;
        }
        return me.IsInFrontOfMe(other.transform);
    }

    public static bool IsInFrontOfMe(this Transform me, Transform other)
    {
        if (Camera.main == null)
        {
            Debug.LogError("I'm null and will always return false");
            return false;
        }
        Vector3 directionToOther = me.DirectionTo(other);
        float dot = Vector3.Dot(me.forward, directionToOther);
        if (dot >= 0)
            return true;
        else
            return false;
    }

    public static Vector3 OffsetFromMouse(this Transform me)
    {
        if (Camera.main == null)
        {
            Debug.LogError("Camera is Null. Make sure you have a Main Camera in the scene or that you have assinged a camera properly");
            return Vector3.zero;
        }
        return me.OffsetFromMouse(Camera.main);
    }

    public static Vector3 OffsetFromMouse(this Transform me, Camera targetCamera)
    {
        if (targetCamera == null)
        {
            return me.OffsetFromMouse();
        }

        Vector3 startPos = me.position;
        me.MoveToMouse(targetCamera);
        Vector3 finalPos = me.position;
        me.position = startPos;
        return startPos - finalPos;

        //        Vector3 meInScreenSpace = targetCamera.WorldToScreenPoint(me.position);
        //
        ////        Vector3 heading = me.position - targetCamera.transform.position;
        ////        float depth = Vector3.Dot(heading, targetCamera.transform.forward);
        //        Vector3 mouseScreenPos = Input.mousePosition;
        //        Vector3 offsetInScreenSpace = meInScreenSpace - mouseScreenPos;
        ////        mouseScreenPos.z = depth;
        //        offsetInScreenSpace.z = me.DistanceTo(targetCamera.transform);
        //        Vector3 offsetInWorldSpace = targetCamera.ScreenToWorldPoint(offsetInScreenSpace);
        //        return me.position - offsetInWorldSpace;
    }

    /// <summary>
    /// Does any marked layers in the mask match the transform's mask?
    /// </summary>
    /// <param name="me"></param>
    /// <param name="mask"></param>
    /// <returns></returns>
    public static bool CompareLayerMask(this Transform me, LayerMask mask)
    {
        if ((1 << me.gameObject.layer & mask.value) != 0)
        {
            return true;
        }
        return false;
    }

    /// <summary>
    /// Returns a Vector3 with a vector2 value and transform.position.z as z
    /// </summary>
    /// <param name="me"></param>
    /// <param name="v2"></param>
    /// <returns></returns>
    public static Vector3 Vector2As3(this Transform me, Vector2 v2)
    {
        return new Vector3(v2.x, v2.y, me.position.z);
    }

    /// <summary>
    /// Sets a x and y value to transform.position retains old z position
    /// </summary>
    /// <param name="me"></param>
    /// <param name="x"></param>
    /// <param name="y"></param>
    public static void SetV2(this Transform me, float x, float y)
    {
        me.SetV2(new Vector2(x, y));
    }

    /// <summary>
    /// Sets a vector2 value to transform.position retains old z position
    /// </summary>
    /// <param name="me"></param>
    /// <param name="v2"></param>
    public static void SetV2(this Transform me, Vector2 v2)
    {
        if (me == null)
        {
            return;
        }
        me.position = new Vector3(v2.x, v2.y, me.position.z);
    }

    public static Vector3 PositionWithXAt(this Transform me, float x)
    {
        return new Vector3(x, me.position.y, me.position.z);
    }

    public static Vector3 PositionWithYAt(this Transform me, float y)
    {
        return new Vector3(me.position.x, y, me.position.z);
    }

    public static Vector3 PositionWithZAt(this Transform me, float z)
    {
        return new Vector3(me.position.x, me.position.y, z);
    }

    /// <summary>
    /// Sets Global Scale of an object
    /// </summary>
    /// <param name="me"></param>
    /// <param name="newScale"></param>
    public static void SetLossyScale(this Transform me, Vector3 newScale)
    {
        if (me == null)
        {
            return;
        }
        Transform parent = me.parent;
        me.parent = null;
        me.localScale = newScale;
        me.parent = parent;
    }

    /// <summary>
    /// Sets Global Scale of an object
    /// </summary>
    /// <param name="me"></param>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="z"></param>
    public static void SetLossyScale(this Transform me, float x, float y, float z)
    {
        if (me == null)
        {
            return;
        }
        me.SetLossyScale(new Vector3(x, y, z));
    }

    /// <summary>
    /// Sets the X position.
    /// </summary>
    /// <param name="me">Me.</param>
    /// <param name="x">The x coordinate.</param>
    public static void SetXPosition(this Transform me, float x)
    {
        if (me == null)
        {
            return;
        }
        me.position = new Vector3(x, me.position.y, me.position.z);
    }

    /// <summary>
    /// Sets the Y position.
    /// </summary>
    /// <param name="me">Me.</param>
    /// <param name="y">The y coordinate.</param>
    public static void SetYPosition(this Transform me, float y)
    {
        if (me == null)
        {
            return;
        }
        me.position = new Vector3(me.position.x, y, me.position.z);
    }

    /// <summary>
    /// Sets the Z position.
    /// </summary>
    /// <param name="me">Me.</param>
    /// <param name="z">The z coordinate.</param>
    public static void SetZPosition(this Transform me, float z)
    {
        if (me == null)
        {
            return;
        }
        me.position = new Vector3(me.position.x, me.position.y, z);
    }

    /// <summary>
    /// Sets the local X position.
    /// </summary>
    /// <param name="me">Me.</param>
    /// <param name="x">The x coordinate.</param>
    public static void SetLocalXPosition(this Transform me, float x)
    {
        if (me == null)
        {
            return;
        }
        me.localPosition = new Vector3(x, me.localPosition.y, me.localPosition.z);
    }

    /// <summary>
    /// Sets the local Y position.
    /// </summary>
    /// <param name="me">Me.</param>
    /// <param name="y">The y coordinate.</param>
    public static void SetLocalYPosition(this Transform me, float y)
    {
        if (me == null)
        {
            return;
        }
        me.localPosition = new Vector3(me.localPosition.x, y, me.localPosition.z);
    }

    /// <summary>
    /// Sets the local Z position.
    /// </summary>
    /// <param name="me">Me.</param>
    /// <param name="z">The z coordinate.</param>
    public static void SetLocalZPosition(this Transform me, float z)
    {
        if (me == null)
        {
            return;
        }
        me.localPosition = new Vector3(me.localPosition.x, me.localPosition.y, z);
    }

    /// <summary>
    /// Sets the X rotation.
    /// </summary>
    /// <param name="me">Me.</param>
    /// <param name="x">The x coordinate.</param>
    public static void SetXRotation(this Transform me, float x)
    {
        if (me == null)
        {
            return;
        }
        me.rotation = Quaternion.Euler(x, me.rotation.eulerAngles.y, me.rotation.eulerAngles.z);
    }

    /// <summary>
    /// Sets the Y rotation.
    /// </summary>
    /// <param name="me">Me.</param>
    /// <param name="y">The y coordinate.</param>
    public static void SetYRotation(this Transform me, float y)
    {
        if (me == null)
        {
            return;
        }
        me.rotation = Quaternion.Euler(me.rotation.eulerAngles.x, y, me.rotation.eulerAngles.z);
    }

    /// <summary>
    /// Sets the Z rotation.
    /// </summary>
    /// <param name="me">Me.</param>
    /// <param name="z">The z coordinate.</param>
    public static void SetZRotation(this Transform me, float z)
    {
        if (me == null)
        {
            return;
        }
        me.rotation = Quaternion.Euler(me.rotation.eulerAngles.x, me.rotation.eulerAngles.y, z);
    }

    /// <summary>
    /// Sets the X local scale.
    /// </summary>
    /// <param name="me">Me.</param>
    /// <param name="x">The x coordinate.</param>
    public static void SetXLocalScale(this Transform me, float x)
    {
        if (me == null)
        {
            return;
        }
        me.localScale = new Vector3(x, me.localScale.y, me.localScale.z);
    }

    /// <summary>
    /// Sets the local scale for all three axis.
    /// </summary>
    /// <param name="me">Me.</param>
    /// <param name="x">The x coordinate.</param>
    public static void SetLocalScale(this Transform me, float all)
    {
        if (me == null)
        {
            return;
        }
        me.localScale = new Vector3(all, all, all);
    }

    /// <summary>
    /// Sets the Y local scale.
    /// </summary>
    /// <param name="me">Me.</param>
    /// <param name="y">The y coordinate.</param>
    public static void SetYLocalScale(this Transform me, float y)
    {
        if (me == null)
        {
            return;
        }
        me.localScale = new Vector3(me.localScale.x, y, me.localScale.z);
    }

    /// <summary>
    /// Sets the Z local scale.
    /// </summary>
    /// <param name="me">Me.</param>
    /// <param name="z">The z coordinate.</param>
    public static void SetZLocalScale(this Transform me, float z)
    {
        if (me == null)
        {
            return;
        }
        me.localScale = new Vector3(me.localScale.x, me.localScale.y, z);
    }

    /// <summary>
    /// Sets the lossy scale.
    /// </summary>
    /// <param name="me">Me.</param>
    /// <param name="all">All coordinates.</param>
    public static void SetLossyScale(this Transform me, float all)
    {
        if (me == null)
        {
            return;
        }
        me.SetLossyScale(all, all, all);
    }

    /// <summary>
    /// Sets the X lossy scale.
    /// </summary>
    /// <param name="me">Me.</param>
    /// <param name="x">The x coordinate.</param>
    public static void SetXLossyScale(this Transform me, float x)
    {
        if (me == null)
        {
            return;
        }
        me.SetLossyScale(x, me.lossyScale.y, me.lossyScale.z);
    }

    /// <summary>
    /// Sets the Y lossy scale.
    /// </summary>
    /// <param name="me">Me.</param>
    /// <param name="y">The y coordinate.</param>
    public static void SetYLossyScale(this Transform me, float y)
    {
        if (me == null)
        {
            return;
        }
        me.SetLossyScale(me.lossyScale.x, y, me.lossyScale.z);
    }

    /// <summary>
    /// Sets the Z lossy scale.
    /// </summary>
    /// <param name="me">Me.</param>
    /// <param name="z">The z coordinate.</param>
    public static void SetZLossyScale(this Transform me, float z)
    {
        if (me == null)
        {
            return;
        }
        me.SetLossyScale(me.lossyScale.x, me.lossyScale.y, z);
    }

    /// <summary>
    /// Directions to target.
    /// </summary>
    /// <returns>The direction.</returns>
    /// <param name="me">Me.</param>
    /// <param name="to">To.</param>
    public static Vector3 DirectionTo(this Transform me, Transform target)
    {
        if (me == null)
        {
            return Vector3.zero;
        }
        return (Vector3.Normalize((target.position - me.position) / Vector3.Magnitude((target.position - me.position))));
    }

    /// <summary>
    /// Directions to targetPosition.
    /// </summary>
    /// <returns>The direction.</returns>
    /// <param name="me">Me.</param>
    /// <param name="targetPosition">Target position.</param>
    public static Vector3 DirectionTo(this Transform me, Vector3 targetPosition)
    {
        if (me == null)
        {
            return Vector3.zero;
        }
        return (Vector3.Normalize((targetPosition - me.position) / Vector3.Magnitude((targetPosition - me.position))));
    }

    public static Vector3 DirectionFrom(this Transform me, Transform _from)
    {
        if (me == null)
        {
            return Vector3.zero;
        }
        return (Vector3.Normalize((me.position - _from.position) / Vector3.Magnitude((me.position - _from.position))));
    }

    public static Vector3 DirectionFrom(this Transform me, Vector3 _from)
    {
        if (me == null)
        {
            return Vector3.zero;
        }
        return (Vector3.Normalize((me.position - _from) / Vector3.Magnitude((me.position - _from))));
    }

    /// <summary>
    /// Centers Gameobject on
    /// </summary>
    /// <param name="me"></param>
    public static void CenterOnChildren(this Transform me)
    {
        if (me == null)
        {
            //Debug.LogError("Cannot MoveTowards because Transform is null");
            return;
        }
        Vector3 averagePos = Vector3.zero;
        int count = 0;

        Transform[] kids = me.GetKids();

        for (int i = 0; i < kids.Length; i++)
        {
            averagePos += kids[i].position;
            kids[i].parent = null;
            count++;
        }
        if (count == 0)
            return;
        averagePos /= count;
        me.position = averagePos;
        for (int i = 0; i < kids.Length; i++)
        {
            kids[i].parent = me;
        }
    }

    public static bool CompareTags(this Transform me, string[] tags)
    {
        if (me == null)
        {
            return false;
        }
        foreach (string tag in tags)
        {
            if (me.CompareTag(tag))
                return true;
        }
        return false;
    }

    public static void SetActive(this Transform me, bool isActive)
    {
        if (me == null)
        {
            return;
        }
        me.gameObject.SetActive(isActive);
    }

    public static void SetKidsActive(this Transform me, bool isActive)
    {
        if (me == null)
        {
            return;
        }
        me.gameObject.GetKids().SetActive(isActive);
    }

    public static bool GetActiveInHiearchy(this Transform me)
    {
        if (me == null)
        {
            return false;
        }
        return me.gameObject.activeInHierarchy;
    }

    public static bool GetActiveSelf(this Transform me)
    {
        if (me == null)
        {
            return false;
        }
        return me.gameObject.activeSelf;
    }

    public static int GetLayer(this Transform me)
    {
        if (me == null)
        {
            return LayerMask.NameToLayer("Default");
        }
        return me.gameObject.layer;
    }

    public static void SetLayer(this Transform me, int newLayer)
    {
        if (me == null)
        {
            return;
        }
        me.gameObject.layer = newLayer;
    }

    public static bool LocalScaleTowards(this Transform me, Vector3 targetLocalScale, float speed)
    {
        if (me == null)
        {
            return false;
        }
        if (me.localScale == targetLocalScale || Vector3.Distance(me.localScale, targetLocalScale) < speed)
        {
            me.localScale = targetLocalScale;
            return false;
        }
        me.localScale = Vector3.MoveTowards(me.localScale, targetLocalScale, speed);
        return true;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="me"></param>
    /// <param name="targetPos"></param>
    /// <param name="travelTime"></param>
    public static Coroutine MoveTowardsPointOverTime(this Transform me, Vector3 targetPos, float travelTime)
    {
        return CoroutineManager.Instance.StartCoroutine(MoveTowardsPointRoutine(me, targetPos, travelTime));
    }

    /// <summary>
    /// Value between 0 and 1 for the curve
    /// </summary>
    /// <param name="me"></param>
    /// <param name="targetPos"></param>
    /// <param name="travelTime"></param>
    /// <param name="speedMod"></param>
    public static void MoveTowardsPointOverTime(this Transform me, Vector3 targetPos, float travelTime, AnimationCurve speedMod)
    {
        CoroutineManager.Instance.StartCoroutine(MoveTowardsPointRoutine(me, targetPos, travelTime, speedMod));
    }

    public static void MoveTowardsPointOverTime(this Transform me, Vector3 targetPos, float travelTime, AnimationCurve3 positionMod)
    {
        CoroutineManager.Instance.StartCoroutine(MoveTowardsPointRoutine(me, targetPos, travelTime, null, positionMod));
    }

    public static void MoveTowardsPointOverTime(this Transform me, Vector3 targetPos, float travelTime, AnimationCurve speedMod, AnimationCurve3 positionMod)
    {
        CoroutineManager.Instance.StartCoroutine(MoveTowardsPointRoutine(me, targetPos, travelTime, speedMod, positionMod));
    }

    public static IEnumerator MoveTowardsPointRoutine(Transform me, Vector3 targetPos, float travelTime, AnimationCurve speedMod, AnimationCurve3 positionMod)
    {
        Vector3 startPos = me.position;
        float currentTime = 0;
        float percent = 0;

        if (speedMod == null)
        {
            speedMod = new AnimationCurve(new Keyframe(0, 0, 0, 0), new Keyframe(1, 1, 0, 0));
        }

        while (percent != 1f)
        {
            percent = currentTime / travelTime;
            percent = percent.Clamp01();
            me.position = Vector3.Lerp(startPos, targetPos, speedMod.Evaluate(percent));
            me.position += positionMod.Evaluate(percent);
            currentTime += Time.deltaTime;
            yield return null;
        }

        yield return null;
    }

    public static IEnumerator MoveTowardsPointRoutine(Transform me, Vector3 targetPos, float travelTime)
    {
        Vector3 startPos = me.position;
        float currentTime = 0;
        while (currentTime != travelTime)
        {
            me.position = Vector3.Lerp(startPos, targetPos, currentTime / travelTime);
            currentTime += Time.deltaTime;
            currentTime = currentTime.Clamp(0, travelTime);
            Debug.Log("Still Jumping to Player position SHake");
            yield return null;
        }

        yield return null;
    }

    public static IEnumerator MoveTowardsPointRoutine(Transform me, Vector3 targetPos, float travelTime, AnimationCurve curve)
    {
        Vector3 startPos = me.position;
        float currentTime = 0;
        while (me.position != targetPos)
        {
            me.position = Vector3.Lerp(startPos, targetPos, curve.Evaluate(currentTime / travelTime));
            currentTime += Time.deltaTime;
            yield return null;
        }

        yield return null;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="me"></param>
    /// <param name="targetPos"></param>
    /// <param name="travelTime"></param>
    public static void MoveTowardsPointOverTime(this Transform me, Transform targetPos, float travelTime)
    {
        CoroutineManager.Instance.StartCoroutine(MoveTowardsPointRoutine(me, targetPos, travelTime));
    }

    /// <summary>
    /// Value between 0 and 1 for the curve
    /// </summary>
    /// <param name="me"></param>
    /// <param name="targetPos"></param>
    /// <param name="travelTime"></param>
    /// <param name="speedMod"></param>
    public static void MoveTowardsPointOverTime(this Transform me, Transform targetPos, float travelTime, AnimationCurve speedMod)
    {
        CoroutineManager.Instance.StartCoroutine(MoveTowardsPointRoutine(me, targetPos, travelTime, speedMod));
    }

    //public static void MoveTowardsPointOverTime(this Transform me, Transform targetPos, Transition transition)
    //{
    //    CoroutineManager.Instance.StartCoroutine(MoveTowardsPointRoutine(me, targetPos, transition.time.max, transition.curve));
    //}

    public static void MoveTowardsPointOverTime(this Transform me, Transform targetPos, float travelTime, AnimationCurve3 positionMod)
    {
        CoroutineManager.Instance.StartCoroutine(MoveTowardsPointRoutine(me, targetPos, travelTime, null, positionMod));
    }

    public static void MoveTowardsPointOverTime(this Transform me, Transform targetPos, float travelTime, AnimationCurve speedMod, AnimationCurve3 positionMod)
    {
        CoroutineManager.Instance.StartCoroutine(MoveTowardsPointRoutine(me, targetPos, travelTime, speedMod, positionMod));
    }

    public static void MoveTowardsPointOverTime(this Transform me, Transform startPos, Transform targetPos, float travelTime, AnimationCurve speedMod, AnimationCurve3 positionMod)
    {
        CoroutineManager.Instance.StartCoroutine(MoveTowardsPointRoutine(me, startPos, targetPos, travelTime, speedMod, positionMod));
    }

    public static IEnumerator MoveTowardsPointRoutine(Transform me, Transform targetPos, float travelTime, AnimationCurve speedMod, AnimationCurve3 positionMod)
    {
        Vector3 startPos = me.position;
        float currentTime = 0;
        float percent = 0;

        if (speedMod == null)
        {
            speedMod = new AnimationCurve(new Keyframe(0, 0, 0, 0), new Keyframe(1, 1, 0, 0));
        }

        while (percent != 1f)
        {
            percent = currentTime / travelTime;
            percent = percent.Clamp01();
            me.position = Vector3.Lerp(startPos, targetPos.position, speedMod.Evaluate(percent));
            me.position += positionMod.Evaluate(percent);
            currentTime += Time.deltaTime;
            yield return null;
        }

        yield return null;
    }


    public static IEnumerator MoveTowardsPointRoutine(Transform me, Transform startPos, Transform targetPos, float travelTime, AnimationCurve speedMod, AnimationCurve3 positionMod)
    {
        float currentTime = 0;
        float percent = 0;

        if (speedMod == null)
        {
            speedMod = new AnimationCurve(new Keyframe(0, 0, 0, 0), new Keyframe(1, 1, 0, 0));
        }

        while (percent != 1f)
        {
            percent = currentTime / travelTime;
            percent = percent.Clamp01();
            me.position = Vector3.Lerp(startPos.position, targetPos.position, speedMod.Evaluate(percent));
            me.position += positionMod.Evaluate(percent);
            currentTime += Time.deltaTime;
            yield return null;
        }

        yield return null;
    }



    public static IEnumerator MoveTowardsPointRoutine(Transform me, Transform targetPos, float travelTime)
    {
        Vector3 startPos = me.position;
        float currentTime = 0;
        while (me.position != targetPos.position)
        {
            me.position = Vector3.Lerp(startPos, targetPos.position, Mathf.Clamp01(currentTime / travelTime));
            currentTime += Time.deltaTime;
            yield return null;
        }

        yield return null;
    }

    public static IEnumerator MoveTowardsPointRoutine(Transform me, Transform targetPos, float travelTime, AnimationCurve curve)
    {
        Vector3 startPos = me.position;
        float currentTime = 0;
        while (me.position != targetPos.position)
        {
            me.position = Vector3.Lerp(startPos, targetPos.position, curve.Evaluate(Mathf.Clamp01(currentTime / travelTime)));
            currentTime += Time.deltaTime;
            yield return null;
        }

        yield return null;
    }

    /// <summary>
    /// Note: All Scales must be same value to work
    /// </summary>
    /// <param name="me"></param>
    /// <param name="curve"></param>
    /// <param name="space"></param>
    /// <param name="isRelative"></param>
    public static void Scale(this Transform me, AnimationCurve curve, Space space, bool isRelative)
    {
        CoroutineManager.Instance.StartCoroutine(ScaleRoutine(me, curve, space, isRelative));
    }

    /// <summary>
    /// Note: All Scales must be same value to work
    /// </summary>
    /// <param name="me"></param>
    /// <param name="curve"></param>
    public static void Scale(this Transform me, AnimationCurve curve)
    {
        me.Scale(curve, Space.Self, false);
    }

    public static IEnumerator ScaleRoutine(Transform me, AnimationCurve curve, Space space, bool isRelative)
    {
        float currentTime = 0;
        Vector3 relative = space == Space.World ? me.lossyScale : me.localScale;
        relative = isRelative ? relative : Vector3.zero;
        while (currentTime < curve.GetCurveTimeLength())
        {
            currentTime += Time.deltaTime;
            currentTime = currentTime.Min(curve.GetCurveTimeLength());
            if (space == Space.World)
            {
                me.SetLossyScale(relative.x + curve.Evaluate(currentTime));
            }
            else
            {
                me.SetLocalScale(relative.x + curve.Evaluate(currentTime));
            }

            yield return null;
        }

        yield return null;
    }



    /// <summary>
    /// 
    /// </summary>
    /// <param name="me"></param>
    /// <param name="targetPos"></param>
    /// <param name="travelTime"></param>
    public static void RotateTowardsOverTime(this Transform me, Quaternion targetRotation, float travelTime)
    {
        CoroutineManager.Instance.StartCoroutine(RotateTowardsRoutine(me, targetRotation, travelTime));
    }

    /// <summary>
    /// Value between 0 and 1 for the curve
    /// </summary>
    /// <param name="me"></param>
    /// <param name="targetPos"></param>
    /// <param name="travelTime"></param>
    /// <param name="curve"></param>
    public static void RotateTowardsOverTime(this Transform me, Quaternion targetRotation, float travelTime, AnimationCurve curve)
    {
        CoroutineManager.Instance.StartCoroutine(RotateTowardsRoutine(me, targetRotation, travelTime, curve));
    }

    public static IEnumerator RotateTowardsRoutine(Transform me, Quaternion targetRotation, float travelTime)
    {
        Quaternion startRot = me.rotation;
        float currentTime = 0;
        while (currentTime != travelTime)
        {
            me.rotation = Quaternion.Lerp(startRot, targetRotation, currentTime / travelTime);
            currentTime += Time.deltaTime;
            currentTime = currentTime.Min(currentTime, travelTime);
            yield return null;
        }

        yield return null;
    }

    public static IEnumerator RotateTowardsRoutine(Transform me, Quaternion targetRotation, float travelTime, AnimationCurve curve)
    {
        Quaternion startRot = me.rotation;
        float currentTime = 0;
        while (currentTime != travelTime)
        {
            me.rotation = Quaternion.Lerp(startRot, targetRotation, curve.Evaluate(currentTime / travelTime));
            currentTime += Time.deltaTime;
            currentTime = currentTime.Min(currentTime, travelTime);
            yield return null;
        }

        yield return null;
    }


    /// <summary>
    /// Movetowards method. returns true while moving towards target
    /// </summary>
    /// <param name="me"></param>
    /// <param name="targetPos"></param>
    /// <param name="speed"></param>
    /// <returns>true while moving towards target</returns>
    public static bool MoveTowards(this Transform me, Vector3 targetPos, float speed)
    {
        if (me == null)
        {
            return false;
        }
        if (me.position == targetPos || me.DistanceTo(targetPos) < speed)
        {
            me.position = targetPos;
            return false;
        }
        me.position = Vector3.MoveTowards(me.position, targetPos, speed);
        return true;
    }

    /// <summary>
    /// Movetowards method. returns true while moving towards target
    /// </summary>
    /// <param name="me"></param>
    /// <param name="targetPos"></param>
    /// <param name="speed"></param>
    /// <param name="axis"></param>
    /// <returns>true while moving towards target</returns>
    public static bool MoveTowards(this Transform me, Vector3 targetPos, float speed, Axis axis)
    {
        switch (axis)
        {
            case Axis.X:
                targetPos = new Vector3(targetPos.x, me.position.y, me.position.z);
                break;
            case Axis.Y:
                targetPos = new Vector3(me.position.x, targetPos.y, me.position.z);
                break;
            case Axis.Z:
                targetPos = new Vector3(me.position.x, me.position.y, targetPos.z);
                break;
        }

        return me.MoveTowards(targetPos, speed);
    }

    /// <summary>
    /// Movetowards method. returns true while moving towards target
    /// </summary>
    /// <param name="me"></param>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="z"></param>
    /// <param name="speed"></param>
    /// <returns>true while moving towards target</returns>
    public static bool MoveTowards(this Transform me, float x, float y, float z, float speed)
    {
        return me.MoveTowards(new Vector3(x, y, z), speed);
    }

    /// <summary>
    /// Movetowards method. returns true while moving towards target
    /// </summary>
    /// <param name="me"></param>
    /// <param name="targetPos"></param>
    /// <param name="speed"></param>
    /// <returns>true while moving towards target</returns>
    public static bool MoveTowards(this Transform me, Transform targetPos, float speed)
    {
        return me.MoveTowards(targetPos.position, speed);
    }

    /// <summary>
    /// Movetowards method. returns true while moving towards target
    /// </summary>
    /// <param name="me"></param>
    /// <param name="targetPos"></param>
    /// <param name="speed"></param>
    /// <param name="axis"></param>
    /// <returns>true while moving towards target</returns>
    public static bool MoveTowards(this Transform me, Transform targetPos, float speed, Axis axis)
    {
        return me.MoveTowards(targetPos.position, speed, axis);
    }

    /// <summary>
    /// Movetowards method. returns true while moving towards target
    /// </summary>
    /// <param name="me"></param>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="z"></param>
    /// <param name="speed"></param>
    /// <param name="axis"></param>
    /// <returns>true while moving towards target</returns>
    public static bool MoveTowards(this Transform me, float x, float y, float z, float speed, Axis axis)
    {
        return me.MoveTowards(new Vector3(x, y, z), speed, axis);
    }

    public static bool MoveTowardsX(this Transform me, float x, float speed)
    {
        return me.MoveTowards(new Vector3(x, me.position.y, me.position.z), speed, Axis.X);
    }

    public static bool MoveTowardsX(this Transform me, Vector3 targetPos, float speed, Axis axis)
    {
        return me.MoveTowards(targetPos, speed, Axis.X);
    }

    public static bool MoveTowardsX(this Transform me, Transform targetPos, float speed, Axis axis)
    {
        return me.MoveTowards(targetPos.position, speed, Axis.X);
    }

    public static bool MoveTowardsY(this Transform me, float y, float speed)
    {
        return me.MoveTowards(new Vector3(me.position.x, y, me.position.z), speed, Axis.X);
    }

    public static bool MoveTowardsY(this Transform me, Vector3 targetPos, float speed, Axis axis)
    {
        return me.MoveTowards(targetPos, speed, Axis.Y);
    }

    public static bool MoveTowardsY(this Transform me, Transform targetPos, float speed, Axis axis)
    {
        return me.MoveTowards(targetPos.position, speed, Axis.Y);
    }

    public static bool MoveTowardsZ(this Transform me, float z, float speed)
    {
        return me.MoveTowards(new Vector3(me.position.x, me.position.y, z), speed, Axis.X);
    }

    public static bool MoveTowardsZ(this Transform me, Vector3 targetPos, float speed, Axis axis)
    {
        return me.MoveTowards(targetPos, speed, Axis.Z);
    }

    public static bool MoveTowardsZ(this Transform me, Transform targetPos, float speed, Axis axis)
    {
        return me.MoveTowards(targetPos.position, speed, Axis.Z);
    }

    public static bool LerpTowards(this Transform me, float x, float y, float z, float speed)
    {
        return me.LerpTowards(new Vector3(x, y, z), speed);
    }

    public static bool LerpTowards(this Transform me, Transform targetPos, float speed)
    {
        return me.LerpTowards(targetPos.position, speed, .01f);
    }

    public static bool LerpTowards(this Transform me, Vector3 targetPos, float speed)
    {
        return me.LerpTowards(targetPos, speed, .01f);
    }

    public static bool LerpTowards(this Transform me, Transform targetPos, float speed, float closeEnough)
    {
        return me.LerpTowards(targetPos.position, speed, closeEnough);
    }

    public static bool LerpTowards(this Transform me, Vector3 targetPos, float speed, float closeEnough)
    {
        if (me == null)
        {
            Debug.LogWarning("Nothing to Lerp Towards with");
            return false;
        }
        if (me.DistanceTo(targetPos) < closeEnough)
        {
            me.position = targetPos;
            return false;
        }
        else
        {
            me.position = Vector3.Lerp(me.position, targetPos, speed);
        }
        return true;
    }

    public static bool RotateTowards(this Transform me, Quaternion targetRot, float speed)
    {
        me.rotation = Quaternion.RotateTowards(me.rotation, targetRot, speed);
        return me.rotation == targetRot;
    }

    public static bool RotateTowards(this Transform me, Quaternion targetRot, float speed, Axis axis)
    {
        switch (axis)
        {
            case Axis.X:
                targetRot = Quaternion.Euler(new Vector3(me.position.x, targetRot.eulerAngles.y, targetRot.eulerAngles.z));
                break;
            case Axis.Y:
                targetRot = Quaternion.Euler(new Vector3(targetRot.eulerAngles.x, me.position.y, targetRot.eulerAngles.z));
                break;
            case Axis.Z:
                targetRot = Quaternion.Euler(new Vector3(targetRot.eulerAngles.x, targetRot.eulerAngles.y, me.position.z));
                break;
        }
        return me.RotateTowards(targetRot, speed);
    }

    public static bool RotateTowardsX(this Transform me, Quaternion targetRot, float speed)
    {
        return me.RotateTowards(targetRot, speed, Axis.X);
    }

    public static bool RotateTowardsY(this Transform me, Quaternion targetRot, float speed)
    {
        return me.RotateTowards(targetRot, speed, Axis.Y);
    }

    public static bool RotateTowardsZ(this Transform me, Quaternion targetRot, float speed)
    {
        return me.RotateTowards(targetRot, speed, Axis.Z);
    }

    public static bool SmoothLookAt(this Transform me, Vector3 targetPos, float speed)
    {
        if (targetPos - me.position != Vector3.zero)
        {
            Quaternion rotation = Quaternion.LookRotation(targetPos - me.position);
            me.rotation = Quaternion.Slerp(me.rotation, rotation, speed);
            if (me.rotation == rotation)
                return true;
        }
        return false;
    }

    public static bool SmoothLookAt(this Transform me, Vector3 targetPos, float speed, Axis axis)
    {
        switch (axis)
        {
            case Axis.X:
                targetPos = new Vector3(me.position.x, targetPos.y, targetPos.z);
                break;
            case Axis.Y:
                targetPos = new Vector3(targetPos.x, me.position.y, targetPos.z);
                break;
            case Axis.Z:
                targetPos = new Vector3(targetPos.x, targetPos.y, me.position.z);
                break;
        }
        return me.SmoothLookAt(targetPos, speed);
    }

    public static bool SmoothLookAtX(this Transform me, Vector3 targetPos, float speed)
    {
        return me.SmoothLookAt(targetPos, speed, Axis.X);
    }

    public static bool SmoothLookAtY(this Transform me, Vector3 targetPos, float speed)
    {
        return me.SmoothLookAt(targetPos, speed, Axis.Y);
    }

    public static bool SmoothLookAtZ(this Transform me, Vector3 targetPos, float speed)
    {
        return me.SmoothLookAt(targetPos, speed, Axis.Z);
    }

    public static float DistanceTo(this Transform me, Vector3 targetPos)
    {
        if (me == null)
        {
            return -1;
        }
        return Vector3.Distance(me.position, targetPos);
    }

    public static Vector3 GetClosestPosition(this Transform me, params Vector3[] possiblePositions)
    {
        if (me == null)
        {
            return Vector3.zero;
        }
        int finalPosition = 0;
        for (int i = 1; i < possiblePositions.Length; i++)
        {
            if (me.DistanceTo(possiblePositions[i]) < me.DistanceTo(possiblePositions[finalPosition]))
            {
                finalPosition = i;
            }
        }
        return possiblePositions[finalPosition];
    }

    public static Vector3 GetClosestPosition(this Transform me, params Transform[] possiblePositions)
    {
        if (me == null)
        {
            return Vector3.zero;
        }
        int finalPosition = 0;
        for (int i = 1; i < possiblePositions.Length; i++)
        {
            if (me.DistanceTo(possiblePositions[i].position) < me.DistanceTo(possiblePositions[finalPosition].position))
            {
                finalPosition = i;
            }
        }
        return possiblePositions[finalPosition].position;
    }

    public static float DistanceTo(this Transform me, Transform targetPos)
    {
        if (me == null || targetPos == null)
        {
            return -1;
        }
        return Vector3.Distance(me.position, targetPos.position);
    }

    public static void SmoothLookAt(this Transform me, Transform targetPos, float speed)
    {
        if (me == null)
        {
            return;
        }
        if (targetPos.position - me.position != Vector3.zero)
        {
            Quaternion rotation = Quaternion.LookRotation(targetPos.position - me.position);
            me.rotation = Quaternion.Slerp(me.rotation, rotation, speed);
        }
    }

    public static void LookTowards(this Transform me, Vector3 targetPos, float speed)
    {
        if (me == null)
        {
            return;
        }
        if (targetPos - me.position != Vector3.zero)
        {
            Quaternion rotation = Quaternion.LookRotation(targetPos - me.position);
            me.rotation = Quaternion.RotateTowards(me.rotation, rotation, speed);
        }
    }

    public static void LookTowards(this Transform me, Transform targetPos, float speed)
    {
        if (me == null)
        {
            return;
        }
        if (targetPos.position - me.position != Vector3.zero)
        {
            Quaternion rotation = Quaternion.LookRotation(targetPos.position - me.position);
            me.rotation = Quaternion.RotateTowards(me.rotation, rotation, speed);
        }
    }

    public static Transform[] GetKids(this Transform me)
    {
        List<Transform> kids = new List<Transform>();
        foreach (Transform kid in me)
        {
            kids.Add(kid);
        }
        return kids.ToArray();
    }

    public static Transform[] GetKids(this Transform me, bool isRecursive)
    {
        if (!isRecursive)
            return me.GetKids();
        List<Transform> kids = new List<Transform>();
        foreach (Transform kid in me)
        {
            kids.Add(kid);
        }
        return GetKidsRecursive(me);
    }

    static Transform[] GetKidsRecursive(Transform kid)
    {
        Transform[] newKids = kid.GetKids();
        List<Transform> AllKids = new List<Transform>();
        AllKids.AddRange(newKids);
        for (int i = 0; i < newKids.Length; i++)
        {
            AllKids.AddRange(GetKidsRecursive(newKids[i]));
        }
        return AllKids.ToArray();
    }

    public static void SetChildrenActive(this Transform me, bool active)
    {
        SetChildrenActiveRecursive(me, active, false);
    }

    public static void SetChildrenActive(this Transform me, bool active, bool isRecursive)
    {
        SetChildrenActiveRecursive(me, active, isRecursive);
    }

    public static void SetChildrenActiveRecursive(Transform parent, bool activity, bool isRecursive)
    {
        foreach (Transform kid in parent)
        {
            kid.SetActive(activity);
            if (isRecursive)
            {
                SetChildrenActiveRecursive(kid, activity, isRecursive);
            }
        }
    }

    #endregion

}
