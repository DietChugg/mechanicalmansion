using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using StarterKit;

public static class AnimationX
{
	public static AnimationClip[] GetClips(this Animation me)
	{
		List<AnimationClip> clips = new List<AnimationClip>();
		foreach (AnimationState animationState in me ) 
		{
			clips.Add(animationState.clip);
		}
		return clips.ToArray();
	}

	public static void AddClip(this Animation me, AnimationClip clip)
	{
		me.AddClip(clip,clip.name);
	}

	/// <summary>
	/// DOES NOT REMOVE NULL OR EMPTY CLIPS!!!
	/// </summary>
	/// <param name="me">Me.</param>
	public static void ClearClips(this Animation me)
	{
        AnimationClip[] clips = me.GetClips();
        for (int i = 0; i < clips.Length; i++)
        {
            me.RemoveClip(clips[i]);
        }

        //List<string> clipNames = new List<string>();
        //foreach (AnimationState v in me)
        //    clipNames.Add(v.clip.name);

        //foreach (string s in clipNames)
        //{
        //    try
        //    {
        //        me.RemoveClip(s);
        //    }
        //    catch (System.Exception e)
        //    {
        //        Debug.LogWarning("Failed to remove animation : " + e.ToString());
        //    }
        //}
	}


	public static bool HasClip(this Animation me, string name)
	{
		AnimationClip[] clips = me.GetClips();
		for (int i = 0; i < clips.Length; i++) 
		{
			if(clips[i].name == name)
				return true;
		}
		return false;
	}

	public static bool HasClip(this Animation me, AnimationClip clip)
	{
		return me.GetClips().ToList().Contains(clip);
	}
}
