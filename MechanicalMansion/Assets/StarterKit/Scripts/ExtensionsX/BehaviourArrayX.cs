using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using StarterKit;

public static class BehaviourListX 
{

	public static void SetEnabled(this Behaviour[] me, bool enabled)
	{
		if(me == null || me.Length == 0)
		{
			Debug.LogError("I'm Nonexistant");
			return;
		}
		
		for (int i = 0; i < me.Length; i++) 
		{
			me[i].enabled = enabled;
		}
	}

	public static void InvertEnabled(this Behaviour[] me)
	{
		if(me == null || me.Length == 0)
		{
			Debug.LogError("I'm Nonexistant");
			return;
		}
		
		for (int i = 0; i < me.Length; i++) 
		{
			me[i].enabled = !me[i].enabled;
		}
	}
}
