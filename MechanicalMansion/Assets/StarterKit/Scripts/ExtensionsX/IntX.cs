using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class IntX 
{
    /// <summary>
    /// sets the int to the closest of two values
    /// </summary>
    /// <param name="me"></param>
    /// <param name="one"></param>
    /// <param name="two"></param>
	public static void SetToNearest(this int me, int one, int two)
	{
		if(Mathf.Abs(me - one) < Mathf.Abs(me - two))
			me = one;
		me = two;
	}

    /// <summary>
    /// Don't Use... Doesn't work as intended
    /// </summary>
    /// <param name="me"></param>
    /// <returns></returns>
    public static int NextPowTwoValue(this int me)
    {
        for (int i = 1; i < int.MaxValue; i++)
        {
            int raisedValue = (int)Mathf.Pow(2, i);
            if (me <= raisedValue)
                return raisedValue;
        }
        return -1;
    }
	
    /// <summary>
    /// sets the int to the nearest of multiple values
    /// </summary>
    /// <param name="me"></param>
    /// <param name="testValues"></param>
	public static void SetToNearest(this int me, params int[] testValues)
	{
		int clostest = testValues[0];
		for(int i = 0; i < testValues.Length; i++)
		{
			if(Mathf.Abs(me - testValues[i]) < Mathf.Abs(me - clostest))
			{
				clostest = testValues[i];
			}
		}
		me = clostest;
	}

    /// <summary>
    /// returns true when int is between min and max inclusively (int can be min or max to returns true)
    /// </summary>
    /// <param name="me"></param>
    /// <param name="min"></param>
    /// <param name="max"></param>
    /// <returns></returns>
	public static bool IsBetween(this int me, int min, int max)
	{
		if(min <= me && me <= max)
			return true;
		return false;
	}

    /// <summary>
    /// returns true when int is between min and max. 
    /// </summary>
    /// <param name="me"></param>
    /// <param name="min"></param>
    /// <param name="max"></param>
    /// <param name="isInclusive">when inclusive is true, the int can be min to returns true and when false it can't be</param>
    /// <returns></returns>
    public static bool IsBetween(this int me, int min, int max, bool isInclusive)
    {
        if (min < me && me < max)
            return true;
        if (min == me && me == max && isInclusive)
            return true;
        return false;
    }

    /// <summary>
    /// Return the max value between me and all values passed
    /// </summary>
    /// <param name="me"></param>
    /// <param name="values"></param>
    /// <returns></returns>
    public static int Max(this int me, params int[] values)
    {
        List<int> checkValues = new List<int>();
        checkValues.Add(me);
        checkValues.AddRange(values);
        return Mathf.Max(checkValues.ToArray());
    }
    
    /// <summary>
    /// Return the min value between me and all values passed
    /// </summary>
    /// <param name="me"></param>
    /// <param name="values"></param>
    /// <returns></returns>
    public static int Min(this int me, params int[] values)
    {
        List<int> checkValues = new List<int>();
        checkValues.Add(me);
        checkValues.AddRange(values);
        return Mathf.Min(checkValues.ToArray());
    }

    /// <summary>
    /// Returns the distance between me and the other number
    /// </summary>
    /// <param name="me"></param>
    /// <param name="other"></param>
    /// <returns></returns>
    public static int DistanceTo(this int me, int other)
    {
        int maxVal = me.Abs().Max(other.Abs());
        int minVal = me.Abs().Min(other.Abs());
        bool isSameSign = (me.Sign() == me.Sign());

        if(isSameSign)
            return maxVal - minVal;
        else
            return me.Abs() + other.Abs();
    }

    /// <summary>
    /// Returns the absolute value of a number
    /// </summary>
    /// <param name="me"></param>
    /// <returns></returns>
	public static int Abs(this int me)
	{
		return Mathf.Abs(me);
	}

    /// <summary>
    /// returns -1f if the number is negative and returns 1 if it is 0 or greater
    /// </summary>
    /// <param name="me"></param>
    /// <returns></returns>
    public static float Sign(this int me)
    {
        return Mathf.Sign(me);
    }

    /// <summary>
    /// returns the smaller of the int and the high value
    /// </summary>
    /// <param name="me"></param>
    /// <param name="high"></param>
    /// <returns></returns>
    public static int HighCut(this int me, int high)
    {
        if(me < high)
            return high;
        return me;
    }
    
    /// <summary>
    /// returns the greater of the int and the lowcut value 
    /// </summary>
    /// <param name="me"></param>
    /// <param name="low"></param>
    /// <returns></returns>
    public static int LowCut(this int me, int low)
    {
        if(me < low)
            return low;
        return me;
    }

    /// <summary>
    /// Clamps value between min and max and returns value.
    /// </summary>
    /// <param name="me"></param>
    /// <param name="min"></param>
    /// <param name="max"></param>
    /// <returns></returns>
	public static int Clamp(this int me, int min, int max)
	{
		return Mathf.Clamp(me,min,max);
	}

    /// <summary>
    /// Selects at random a number between 0 and the int
    /// </summary>
    /// <param name="me"></param>
    /// <returns></returns>
	public static int Randomize(this int me)
	{
		Random.seed++;
        if (me > 0)
        {
            return Random.Range(0, me);
        }
        if (me < 0)
        {
            return Random.Range(me, 0);
        }
        return 0;
	}

    /// <summary>
    /// returns true if the number is even
    /// </summary>
    /// <param name="me"></param>
    /// <returns></returns>
    public static bool isEven(this int me)
    {
        return me % 2 == 0;
    }

    /// <summary>
    /// returns true if the number is odd
    /// </summary>
    /// <param name="me"></param>
    /// <returns></returns>
    public static bool isOdd(this int me)
    {
        return me % 2 == 1;
    }
}
