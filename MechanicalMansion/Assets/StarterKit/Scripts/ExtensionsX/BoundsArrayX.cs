using UnityEngine;
using System.Collections;
using StarterKit;

public static class BoundsArrayX 
{

	public static float GetTotalWidth(this Bounds[] me)
	{
        float width = 0f;
        for (int i = 0; i < me.Length; i++)
        {
            width += me[i].size.x;
        }
        return width;
	}

    public static float GetTotalHeight(this Bounds[] me)
    {
        float height = 0f;
        for (int i = 0; i < me.Length; i++)
        {
            height += me[i].size.x;
        }
        return height;
    }

    public static float GetTotalDepth(this Bounds[] me)
    {
        float depth = 0f;
        for (int i = 0; i < me.Length; i++)
        {
            depth += me[i].size.z;
        }
        return depth;
    }

    public static Vector3 GetTotalSize(this Bounds[] me)
    {
        return new Vector3(me.GetTotalWidth(),me.GetTotalHeight(),me.GetTotalDepth());
    }
}
