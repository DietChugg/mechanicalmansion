using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace StarterKit
{
    public enum LoadLevelOptions
    { 
        LoadLevel,
        LoadLevelAdditive,
        LoadLevelAsync,
        LoadLevelAdditiveAsync
    }

    public static class ApplicationX
    {
        public static SafeAction<LoadingLevelData> OnLevelLoading;
        public static SafeAction<LoadingLevelData> OnLevelLoaded;
        public static List<LoadingLevelData> loadedLevelHistory = new List<LoadingLevelData>();

        static ApplicationX()
        {
            loadedLevelHistory.Add(new LoadingLevelData(Application.loadedLevelName));
        }

        public static void LoadPreviousLevel()
        {
            if (ApplicationX.loadedLevelHistory.Count > 1)
            {
                LoadingLevelData lld = ApplicationX.loadedLevelHistory[ApplicationX.loadedLevelHistory.Count - 2];
                if (lld.useName)
                {
                    ApplicationX.LoadLevel(lld.name);
                }
                else
                {
                    ApplicationX.LoadLevel(lld.index);
                }
            }
            else
            {
                Debug.LogWarning("Not Enough Loaded Level History Exists! Don't call unless you have LLD history");
            }
        }

        public static LoadingLevelData GetLastLevelLoaded()
        {
            if (ApplicationX.loadedLevelHistory.Count > 1)
            {
                return ApplicationX.loadedLevelHistory[ApplicationX.loadedLevelHistory.Count - 2];
            }
            else
            {
                Debug.LogWarning("Not Enough Loaded Level History Exists! Don't call unless you have LLD history");
            }
            return null;
        }

        public static void ReloadLevel()
        {
            ApplicationX.LoadLevel(Application.loadedLevel);
        }

        public static void WaitToReloadLevel(float seconds)
        {
            WaitToLoadLevel(Application.loadedLevelName, seconds);
        }

        public static void WaitToLoadLevel(string name, float seconds)
        {
            CoroutineManager.Instance.Init();
            CoroutineManager.Instance.StartCoroutine(WaitToLoadLevelRoutine(name, seconds));
        }

        public static void WaitToLoadLevel(int index, float seconds)
        {
            CoroutineManager.Instance.Init();
            CoroutineManager.Instance.StartCoroutine(WaitToLoadLevelRoutine(index, seconds));
        }

        public static void LoadLevel(string name)
        {
            CoroutineManager.Instance.Init();
            CoroutineManager.Instance.StartCoroutine(LoadLevelRoutine(name));
        }

        public static void LoadLevel(int index)
        {
            CoroutineManager.Instance.Init();
            CoroutineManager.Instance.StartCoroutine(LoadLevelRoutine(index));
        }

        public static void LoadLevelAdditive(string name)
        {
            CoroutineManager.Instance.Init();
            CoroutineManager.Instance.StartCoroutine(LoadLevelAdditiveRoutine(name));
        }

        public static void LoadLevelAdditive(int index)
        {
            CoroutineManager.Instance.Init();
            CoroutineManager.Instance.StartCoroutine(LoadLevelAdditiveRoutine(index));
        }

        public static void LoadLevelAsync(string name)
        {
            CoroutineManager.Instance.Init();
            CoroutineManager.Instance.StartCoroutine(LoadLevelAsyncRoutine(name));
        }

        public static void LoadLevelAsync(int index)
        {
            CoroutineManager.Instance.Init();
            CoroutineManager.Instance.StartCoroutine(LoadLevelAsyncRoutine(index));
        }

        public static void LoadLevelAdditiveAsync(string name)
        {
            CoroutineManager.Instance.Init();
            CoroutineManager.Instance.StartCoroutine(LoadLevelAdditiveAsyncRoutine(name));
        }

        public static void LoadLevelAdditiveAsync(int index)
        {
            CoroutineManager.Instance.Init();
            CoroutineManager.Instance.StartCoroutine(LoadLevelAdditiveAsyncRoutine(index));
        }

        static IEnumerator WaitToLoadLevelRoutine(string name, float seconds)
        {
            yield return new WaitForSeconds(seconds);
            LoadingLevelData lld = new LoadingLevelData(name);
            OnLevelLoading.Call(lld);
            loadedLevelHistory.Add(lld);
            Application.LoadLevel(name);
            OnLevelLoaded.Call(lld);
            yield return null;
        }

        static IEnumerator WaitToLoadLevelRoutine(int index, float seconds)
        {
            yield return new WaitForSeconds(seconds);
            LoadingLevelData lld = new LoadingLevelData(index);
            OnLevelLoading.Call(lld);
            loadedLevelHistory.Add(lld);
            Application.LoadLevel(index);
            OnLevelLoaded.Call(lld);
            yield return null;
        }

        static IEnumerator LoadLevelRoutine(string name)
        {
            LoadingLevelData lld = new LoadingLevelData(name);
            OnLevelLoading.Call(lld);
            loadedLevelHistory.Add(lld);
            Application.LoadLevel(name);
            OnLevelLoaded.Call(lld);
            yield return null;
        }

        static IEnumerator LoadLevelRoutine(int index)
        {
            LoadingLevelData lld = new LoadingLevelData(index);
            OnLevelLoading.Call(lld);
            loadedLevelHistory.Add(lld);
            Application.LoadLevel(index);
            OnLevelLoaded.Call(lld);
            yield return null;
        }

        static IEnumerator LoadLevelAdditiveRoutine(string name)
        {
            LoadingLevelData lld = new LoadingLevelData(name, true, false);

            OnLevelLoading.Call(lld);
            loadedLevelHistory.Add(lld);
            Application.LoadLevelAdditive(name);
            OnLevelLoaded.Call(lld);
            yield return null;
        }

        static IEnumerator LoadLevelAdditiveRoutine(int index)
        {
            LoadingLevelData lld = new LoadingLevelData(index, true, false);
            OnLevelLoading.Call(lld);
            loadedLevelHistory.Add(lld);
            Application.LoadLevelAdditive(index);
            OnLevelLoaded.Call(lld);

            yield return null;
        }

        static IEnumerator LoadLevelAsyncRoutine(string name)
        {
            LoadingLevelData lld = new LoadingLevelData(name, false, true);
            loadedLevelHistory.Add(lld);
            AsyncOperation asyncOperation = Application.LoadLevelAsync(name);
            lld.asyncOperation = asyncOperation;
            OnLevelLoading.Call(lld);
            yield return asyncOperation;
            OnLevelLoaded.Call(lld);
        }

        static IEnumerator LoadLevelAsyncRoutine(int index)
        {
            LoadingLevelData lld = new LoadingLevelData(index, false, true);
            loadedLevelHistory.Add(lld);
            AsyncOperation asyncOperation = Application.LoadLevelAsync(index);
            lld.asyncOperation = asyncOperation;
            OnLevelLoading.Call(lld);
            yield return asyncOperation;
            OnLevelLoaded.Call(lld);
        }

        static IEnumerator LoadLevelAdditiveAsyncRoutine(string name)
        {
            LoadingLevelData lld = new LoadingLevelData(name, true, true);
            loadedLevelHistory.Add(lld);
            AsyncOperation asyncOperation = Application.LoadLevelAdditiveAsync(name);
            lld.asyncOperation = asyncOperation;
            OnLevelLoading.Call(lld);
            yield return asyncOperation;
            OnLevelLoaded.Call(lld);
        }

        static IEnumerator LoadLevelAdditiveAsyncRoutine(int index)
        {
            LoadingLevelData lld = new LoadingLevelData(index, true, true);
            loadedLevelHistory.Add(lld);
            AsyncOperation asyncOperation = Application.LoadLevelAdditiveAsync(index);
            lld.asyncOperation = asyncOperation;
            OnLevelLoading.Call(lld);
            yield return asyncOperation;
            OnLevelLoaded.Call(lld);
        }
    }

    [System.Serializable]
    public class LoadingLevelData
    {
        public string name;
        public int index = -1;
        public bool isAdditive;
        public bool isAsync;
        public bool useName;
        public AsyncOperation asyncOperation;

        public LoadingLevelData(string _name)
            : this(_name, -1, false, false, true)
        {
        }

        public LoadingLevelData(int _index)
            : this("", _index, false, false, false)
        {
        }

        public LoadingLevelData(string _name, bool _isAdditive, bool _isAsync)
            : this(_name, -1, false, false, true)
        {
        }

        public LoadingLevelData(int _index, bool _isAdditive, bool _isAsync)
            : this("", _index, false, false, false)
        {
        }

        private LoadingLevelData(string _name, int _index, bool _isAdditive, bool _isAsync, bool _useName)
        {
            name = _name;
            index = _index;
            isAdditive = _isAdditive;
            isAsync = _isAsync;
            useName = _useName;
        }

        public void LoadLevel()
        {
            if (isAsync)
            {
                if (isAdditive)
                {
                    if (useName)
                        ApplicationX.LoadLevelAdditiveAsync(name);
                    else
                        ApplicationX.LoadLevelAdditiveAsync(index);

                }
                else
                {
                    if (useName)
                        ApplicationX.LoadLevelAsync(name);
                    else
                        ApplicationX.LoadLevelAsync(index);
                }
            }
            else
            {
                if (isAdditive)
                {
                    if (useName)
                        ApplicationX.LoadLevelAdditive(name);
                    else
                        ApplicationX.LoadLevelAdditive(index);
                }
                else
                {
                    if (useName)
                        ApplicationX.LoadLevel(name);
                    else
                        ApplicationX.LoadLevel(index);
                }
            }
        }
    }
}