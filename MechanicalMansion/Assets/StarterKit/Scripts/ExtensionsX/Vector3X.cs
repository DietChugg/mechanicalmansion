﻿using UnityEngine;
using System.Collections;
using UnityEngine.Serialization;

namespace StarterKit
{
	public static class Vector3X
	{
		#region Constants
		#endregion

		#region StaticMethods

		public static float RadAtPoint(Vector3 p2, Vector3 p1, Vector3 p3)
		{
			float a = Vector3.Distance(p1, p2);
			float b = Vector3.Distance(p2, p3);
			float c = Vector3.Distance(p3, p1);
			
			float newAngleCalc = Mathf.Acos((Mathf.Pow(a, 2) + Mathf.Pow(b, 2) - Mathf.Pow(c, 2))/(2 * a * b));
			return newAngleCalc;
		}

		public static Vector3 LerpByDistance(Vector3 A, Vector3 B, float x)
		{
			Vector3 P = x * Vector3.Normalize(B - A) + A;
			return P;
		}

	//	public static Vector3 LerpBetween(Vector3 A, Vector3 B, float x)
	//	{
	//		Vector3 P = x * Vector3.Normalize(B - A) + A;
	//		return P;
	//	}

		static public Vector3 RandomOne()
		{
			Random.seed ++;
			float x = Random.value;
			Random.seed ++;
			float y = Random.value;
			Random.seed ++;
			float z = Random.value;
			Random.seed ++;
			return new Vector3(x,y,z);
		}

        static public Vector3 MoveTowards(this Vector3 me, Vector3 target, float maxDistanceDelta)
        {
            return Vector3.MoveTowards(me, target, maxDistanceDelta);
        }

		static public Vector3 GetDirection(Vector3 from, Vector3 to)
		{
			return (Vector3.Normalize((to-from)/Vector3.Magnitude((to-from))));
		}
		
		static public Vector3 RandomPointBetweenVectors(Vector3 from, Vector3 to)
		{
			return new Vector3( Random.Range(from.x,to.x),Random.Range(from.y,to.y),Random.Range(from.z,to.z));	
		}

		static public Vector3 RandomPointInSphere(Vector3 center, float radius)
		{
			return center + Random.insideUnitSphere*radius;	
		}

		public static Vector3 MidPoint(Vector3 pointOne, Vector3 pointTwo)
		{
			return new Vector3((pointOne.x+pointTwo.x)/2, (pointOne.y+pointTwo.y)/2, (pointOne.z+pointTwo.z)/2);
		}

        public static Vector3 MidPoint(params Vector3[] points)
        {
            float[] xValues = points.GetXValues();
            float[] yValues = points.GetYValues();
            float[] zValues = points.GetZValues();
            return (new Vector3(xValues.Medium(), yValues.Medium(), zValues.Medium()));
        }

        [System.Obsolete("Where Am I Used... I Need a better name. Fix me!")]
		public static Vector3 CalcPointLocallyBelowTransform(Transform transform, float distanceOut)
		{
			return new Vector3(transform.position.x + Mathf.Cos((transform.rotation.eulerAngles.z+90)* Mathf.Deg2Rad)  * distanceOut, transform.position.y + Mathf.Sin((transform.rotation.eulerAngles.z+90)* Mathf.Deg2Rad) * distanceOut, transform.position.z);
		}

		public static Vector3 Round(Vector3 v3)
		{
			return new Vector3((float)System.Convert.ToInt32(Mathf.Round(v3.x)),(float)System.Convert.ToInt32(Mathf.Round(v3.y)),(float)System.Convert.ToInt32(Mathf.Round(v3.z)));
		}

		public static Vector2 ToVector2(this Vector3 v3)
		{
			return new Vector2(v3.x,v3.y);
		}

		public static bool IsNaN(this Vector3 v3)
		{
			if(float.IsNaN(v3.x))
				return true;
			if(float.IsNaN(v3.y))
				return true;
			if(float.IsNaN(v3.z))
				return true;
			return false;
		}

		public static bool IsInfinity(this Vector3 v3)
		{
			if(Mathf.Infinity == v3.x || Mathf.NegativeInfinity == v3.x)
				return true;
			if(Mathf.Infinity == v3.y || Mathf.NegativeInfinity == v3.y)
				return true;
			if(Mathf.Infinity == v3.z || Mathf.NegativeInfinity == v3.z)
				return true;
			return false;
		}


		public static Vector3 Size(params Vector3[] points)
		{
			float[] xValues = points.GetXValues();
			float[] yValues = points.GetXValues();
			float[] zValues = points.GetXValues();
			return (new Vector3(xValues.Range(),yValues.Range(),zValues.Range()));
		}

		public static float Min(Axis axis, params Vector3[] points)
		{
			return points.GetAxisValues(axis).Min();
		}

		public static float Max(Axis axis, params Vector3[] points)
		{
			return points.GetAxisValues(axis).Max();
		}

		
		#endregion

		#region ExtensionMethods
		public static Vector3 GetDirectionTo(this Vector3 me, Vector3 to)
		{
			return (Vector3.Normalize((to-me)/Vector3.Magnitude((to-me))));
		}

		public static float Distance(this Vector3 me, Vector3 to)
		{
			return Vector3.Distance(me, to);
		}

	    public static Vector3 SetX(this Vector3 me, float x)
	    {
	        return new Vector3(x,me.y,me.z);
	    }

	    public static Vector3 SetY(this Vector3 me, float y)
	    {
	        return new Vector3(me.x,y,me.z);
	    }

	    public static Vector3 SetZ(this Vector3 me, float z)
	    {
            me[2] = z;
	        return new Vector3(me.x,me.y,z);
	    }
		#endregion


	}
}