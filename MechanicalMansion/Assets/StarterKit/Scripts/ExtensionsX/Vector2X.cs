using UnityEngine;
using System.Collections;

public static class Vector2X
{
    public static Vector2 ScreenPosition(this Vector2 me)
    {
        return new Vector2(me.x * Screen.width, me.y * Screen.height);
    }

    public static Vector2 PointOnLine(Vector2 point1, Vector2 point2, float distance)
    {
        float a1 = point1.x;
        float a2 = point1.y;

        float b1 = point2.x;
        float b2 = point2.y;

        float xC;
        float yC;

        float cDenom = Mathf.Sqrt(Mathf.Pow(a1 - b1, 2) + Mathf.Pow(a2 - b2, 2));

        if (cDenom == 0)
            cDenom = Mathf.Epsilon;

        xC = a1 + (distance * (b1 - a1)) / cDenom;
        yC = a2 + (distance * (b2 - a2)) / cDenom;

        return new Vector2(xC, yC);
    }

    public static Vector2 MidPoint(Vector2 p1, Vector2 p2)
    {
        return new Vector2((p1.x + p2.x) / 2, (p1.y + p2.y) / 2);
    }

    public static Vector3 ToVector3(this Vector2 me)
    {
        return new Vector3(me.x, me.y, 0);
    }

    public static Vector3 ToVector3(this Vector2 me, float z)
    {
        return new Vector3(me.x, me.y, z);
    }

    public static Vector2 Slope(Vector2 v0, Vector2 v1)
    {
        float rise = v1.x - v0.x;
        float run = v1.y - v0.y;
        return new Vector2(rise, run);
    }

    public static Vector2 Perpendicular(Vector2 slope)
    {
        return new Vector2(-slope.y, slope.x);
    }
}
