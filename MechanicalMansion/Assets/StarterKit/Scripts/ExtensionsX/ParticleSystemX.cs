using UnityEngine;
using System.Collections;

public static class ParticleSystemX 
{
	#region Constants
	#endregion
	
	#region StaticMethods
	#endregion
	
	#region ExtensionMethods
	public static void ClearStop(this ParticleSystem ps)
	{
		if(ps == null)
		{
			//Debug.LogError("Cannot ClearStop because ParticleSystem is null");
			return;
		}
		ps.Clear();
		ps.Stop();
	}
	#endregion
}
