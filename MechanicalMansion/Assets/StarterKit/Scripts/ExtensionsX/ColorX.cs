using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public static class ColorX
{
    public static Color orange = new Color32(232,96,12,255);
    public static Color gold = new Color32(238,201,0,255);
    public static Color samon = new Color32(198,113,113,255);
    public static Color flesh = new Color32(255,160,122,255);
    public static Color goldenRod = new Color32(255,193,37,255);
    public static Color darkOliveGreen = new Color32(85,197,47,255);
    public static Color sapGreen = new Color32(48,128,20,255);
    public static Color mintSpringGreen = new Color32(0,250,154,255);
    public static Color dodgerBlue = new Color32(24,116,205,255);
    public static Color midnightBlue = new Color32(25,25,112,255);
    public static Color indigo = new Color32(75,0,130,255);
    public static Color indianRed = new Color32(176,23,31,255);
    public static Color pink = new Color32(255,182,193,255);
    public static Color purple = new Color32(128,0,128,255);
    public static Color mediumSlateBlue = new Color32(123,104,238,255);
    public static Color cadetBlue = new Color32(142,229,238,255);
    public static Color chocolate = new Color32(205,105,30,255);
    public static Color darkenedRed = new Color32(139,0,0,255);
    public static Color samGreen = new Color32(0,196,0,255);

    public static Color SetR(this Color me, float value)
    {
        return new Color(value,me.g,me.b,me.a);
    }
    
    public static Color SetG(this Color me, float value)
    {
        return new Color(me.r,value,me.b,me.a);
    }
    
    public static Color SetB(this Color me, float value)
    {
        return new Color(me.r,me.g,value,me.a);
    }
    
    public static Color SetA(this Color me, float value)
    {
        return new Color(me.r,me.g,me.b,value);
    }
}
