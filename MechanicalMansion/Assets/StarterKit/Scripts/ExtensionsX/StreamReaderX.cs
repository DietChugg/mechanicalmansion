using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public static class StreamReaderX
{

	public static string[] GetTextLines (string path) 
    {
        List<string> lines = new List<string>();
        StreamReader reader = new StreamReader(path);
        while (!reader.EndOfStream)
            lines.Add(reader.ReadLine());
        reader.Close();
        return lines.ToArray();
	}

    public static void SetTextLines(string path, string[] lines)
    {
        StreamWriter writer = new StreamWriter(path);
        for (int i = 0; i < lines.Length; i++)
        {
            writer.WriteLine(lines[i]);
        }
        writer.Close();
    }

    public static string GetText(string path)
    {
        string text = "";
        StreamReader reader = new StreamReader(path);
        while (!reader.EndOfStream)
            text = reader.ReadToEnd();
        reader.Close();
        return text;
    }

    public static void SetText(string path, string text)
    {
        StreamWriter writer = new StreamWriter(path);
        writer.Write(text);
        writer.Close();
    }
	
}
