using UnityEngine;
using System.Collections;

public static class RectX
{
	#region Constants
	#endregion
	
	#region StaticMethods
	public static Rect ScreenPercentRect(Rect toConvert)
	{
		return new Rect(toConvert.x*Screen.width,
		                toConvert.y*Screen.height,
		                toConvert.width*Screen.width,
		                toConvert.height*Screen.height);
	}
	#endregion
	
	#region ExtensionMethods
	public static Rect AsScreenPercentRect(this Rect me)
	{
		return new Rect(me.x*Screen.width,
		                me.y*Screen.height,
		                me.width*Screen.width,
		                me.height*Screen.height);
	}
	#endregion
}
