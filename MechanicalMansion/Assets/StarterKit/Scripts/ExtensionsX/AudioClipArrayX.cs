﻿using UnityEngine;
using System.Collections;

public static class AudioClipArrayX
{
    public static void PlayClipsAtPoint(this AudioClip[] me, Vector3 vector3, float volume, float pitch, float delay)
    {
        AudioSourceX.PlayClipsAtPoint(me,vector3,volume,pitch,delay);
    }
}
