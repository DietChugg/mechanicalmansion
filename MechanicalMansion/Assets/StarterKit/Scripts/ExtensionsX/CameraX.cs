using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class CameraX
{

	public static Vector3[] WorldToViewportPoints(this Camera me, Vector3[] points)
	{
		List<Vector3> convertedPoints = new List<Vector3>();
		for (int i = 0; i < points.Length; i++) 
		{
			convertedPoints.Add(me.WorldToViewportPoint(points[i]));
		}
		return convertedPoints.ToArray();
	}

	public static Vector3[] WorldToScreenPoints(this Camera me, Vector3[] points)
	{
		List<Vector3> convertedPoints = new List<Vector3>();
		for (int i = 0; i < points.Length; i++) 
		{
			convertedPoints.Add(me.WorldToScreenPoint(points[i]));
		}
		return convertedPoints.ToArray();
	}

	public static Vector3[] ScreenToWorldPoints(this Camera me, Vector3[] points)
	{
		List<Vector3> convertedPoints = new List<Vector3>();
		for (int i = 0; i < points.Length; i++) 
		{
			convertedPoints.Add(me.ScreenToWorldPoint(points[i]));
		}
		return convertedPoints.ToArray();
	}

	public static Vector3[] ScreenToViewportPoints(this Camera me, Vector3[] points)
	{
		List<Vector3> convertedPoints = new List<Vector3>();
		for (int i = 0; i < points.Length; i++) 
		{
			convertedPoints.Add(me.ScreenToViewportPoint(points[i]));
		}
		return convertedPoints.ToArray();
	}

	public static Vector3[] ViewportToScreenPoints(this Camera me, Vector3[] points)
	{
		List<Vector3> convertedPoints = new List<Vector3>();
		for (int i = 0; i < points.Length; i++) 
		{
			convertedPoints.Add(me.ViewportToScreenPoint(points[i]));
		}
		return convertedPoints.ToArray();
	}

	public static Vector3[] ViewportToWorldPoints(this Camera me, Vector3[] points)
	{
		List<Vector3> convertedPoints = new List<Vector3>();
		for (int i = 0; i < points.Length; i++) 
		{
			convertedPoints.Add(me.ViewportToWorldPoint(points[i]));
		}
		return convertedPoints.ToArray();
	}
}
