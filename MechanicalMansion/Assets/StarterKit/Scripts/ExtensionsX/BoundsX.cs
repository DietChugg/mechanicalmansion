using UnityEngine;
using System.Collections;
using StarterKit;

public static class BoundsX 
{

	public static Bounds Bounds(params Vector3[] points)
	{
		Bounds bounds = new Bounds(Vector3X.MidPoint (points),Vector3X.Size (points));
		return bounds;
	}

    public static float Left(this Bounds me)
    {
        return me.center.x - me.extents.x;
    }

    public static float Right(this Bounds me)
    {
        return me.center.x + me.extents.x;
    }

    public static float Top(this Bounds me)
    {
        return me.center.y + me.extents.y;
    }

    public static float Bottom(this Bounds me)
    {
        return me.center.y - me.extents.y;
    }
}
