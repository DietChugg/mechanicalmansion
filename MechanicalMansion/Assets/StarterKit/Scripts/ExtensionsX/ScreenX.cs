using UnityEngine;
using System.Collections;

public static class ScreenX 
{

	public static float ScreenSize()
	{
		return Mathf.Sqrt(Mathf.Pow(Screen.width,2) + Mathf.Pow(Screen.height,2));
	}

	public static float WidthOfVisibleWorld(float distanceFromCamera)
	{
		return WidthOfVisibleWorld(Camera.main, distanceFromCamera);
	}

	public static float WidthOfVisibleWorld(Camera camera, float distanceFromCamera)
	{
		Vector3 left = camera.ScreenToWorldPoint(new Vector3(0,0,distanceFromCamera));
		Vector3 right = camera.ScreenToWorldPoint(new Vector3(Screen.width,0,distanceFromCamera));
//		Debug.DrawLine(left, right,Color.green);
		return Vector3.Distance(left, right);
	}

	public static float HeightOfVisibleWorld(float distanceFromCamera)
	{
		return HeightOfVisibleWorld(Camera.main, distanceFromCamera);
	}
	
	public static float HeightOfVisibleWorld(Camera camera, float distanceFromCamera)
	{
		Vector3 bottom = camera.ScreenToWorldPoint(new Vector3(0,0,distanceFromCamera));
		Vector3 top = camera.ScreenToWorldPoint(new Vector3(0,Screen.height,distanceFromCamera));
//		Debug.DrawLine(bottom, top,Color.red);
		return Vector3.Distance(bottom, top);
	}
}
