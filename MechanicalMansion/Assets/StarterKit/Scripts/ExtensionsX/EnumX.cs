using UnityEngine;
using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
 
 
public static class EnumX
{
	#region Constants
	#endregion
	
	#region StaticMethods
	#endregion
	
	#region ExtensionMethods
	public static bool TryParse<T>(this Enum theEnum, string valueToParse, out T returnValue)
	{
		returnValue = default(T);    
		if (Enum.IsDefined(typeof(T), valueToParse))
		{
			TypeConverter converter = TypeDescriptor.GetConverter(typeof(T));
			returnValue = (T)converter.ConvertFromString(valueToParse);
			return true;
		}
		return false;
	}

    public static T Parse<T>(this Enum theEnum, string valueToParse)
    {
        T returnValue = default(T);    
        if (Enum.IsDefined(typeof(T), valueToParse))
        {
            TypeConverter converter = TypeDescriptor.GetConverter(typeof(T));
            returnValue = (T)converter.ConvertFromString(valueToParse);
        }
        return returnValue;
    }

	public static string GetEnumDescription(this Enum value)
	{
		FieldInfo fi = value.GetType().GetField(value.ToString());
		
		DescriptionAttribute[] attributes = 
			(DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
		
		if (attributes != null && attributes.Length > 0)
			return attributes[0].Description;
		else
			return value.ToString();
	}

    public static IEnumerable<T> EnumToList<T>()
    {
        Type enumType = typeof(T);
        
        // Can't use generic type constraints on value types,
        // so have to do check like this
        if (enumType.BaseType != typeof(Enum))
            throw new ArgumentException("T must be of type System.Enum");
        
        Array enumValArray = Enum.GetValues(enumType);
        List<T> enumValList = new List<T>(enumValArray.Length);
        
        foreach (int val in enumValArray)
        {
            enumValList.Add((T)Enum.Parse(enumType, val.ToString()));
        }
        
        return enumValList;
    }

    public static IEnumerable<T> GetValues<T>()
    {
        return Enum.GetValues(typeof(T)).Cast<T>();
    }

	#endregion
   
 
 
}
