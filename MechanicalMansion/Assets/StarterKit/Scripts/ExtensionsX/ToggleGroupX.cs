using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using StarterKit;

public static class ToggleGroupX
{
    public static Toggle[] AllActiveToggles(this ToggleGroup me)
    {
        IEnumerable<Toggle> activeToggles = me.ActiveToggles();
        List<Toggle> toggles = new List<Toggle>();

        foreach (Toggle tg in activeToggles)
        {
            toggles.Add(tg);
        }

        return toggles.ToArray();
    }

    public static Toggle ActiveToggle(this ToggleGroup me)
    {
        return me.AllActiveToggles()[0];
    }
}
