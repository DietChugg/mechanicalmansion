using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public static class TransformArrayX
{

    /// <summary>
    /// Call a method that takes a transform as a parameter on all transforms of this array.
    /// </summary>
    /// <param name="me"></param>
    /// <param name="action"></param>
    public static void ApplyMethod(this Transform[] me, Action<Transform> action)
    {
        for (int i = 0; i < me.Length; i++)
        {
            if (action != null)
            {
                action(me[i]);
            }
        }
    }

    /// <summary>
    /// Returns a gameObject array of the transform array
    /// </summary>
    /// <param name="me"></param>
    /// <returns></returns>
    public static GameObject[] ToGameObjectArray(this Transform[] me)
    {
        GameObject[] gos = new GameObject[me.Length];
        for (int i = 0; i < me.Length; i++)
        {
            gos[i] = me[i].gameObject;
        }
        return gos;
    }

	public static void SetPositions(this Transform[] me, Vector3 position)
	{
		me.SetPositions(position, Vector3.zero);
	}

	public static void SetPositions(this Transform[] me, Vector3 position, Vector3 offset)
	{
		for (int i = 0; i < me.Length; i++) 
		{
			me[i].position = position + (offset * i);
		}
	}

	public static Vector3[] GetPositions(this Transform[] me)
	{
		List<Vector3> positions = new List<Vector3>(); 
		for (int i = 0; i < me.Length; i++) 
		{
			positions.Add(me[i].position);
		}
		return positions.ToArray();
	}

    public static Vector3 GetCenterPosition(this Transform[] me)
    {
        Vector3 centerPosition = Vector3.zero;
        for (int i = 0; i < me.Length; i++)
        {
            centerPosition += me[i].position;
        }
        centerPosition /= me.Length;
        return centerPosition;
    }

    public static void SetActive(this Transform[] me, bool value)
    {
        for (int i = 0; i < me.Length; i++)
        {
            me[i].gameObject.SetActive(value);
        }
    }

    public static void SetParent(this Transform[] me, GameObject newParent, bool worldPositionStays = false)
    {
        for (int i = 0; i < me.Length; i++)
        {
            me[i].SetParent(newParent.transform, worldPositionStays);
        }
    }

    public static void SetParent(this Transform[] me, Transform newParent, bool worldPositionStays = false)
    {
        for (int i = 0; i < me.Length; i++)
        {
            me[i].SetParent(newParent, worldPositionStays);
        }
    }

    /// <summary>
    /// Toggle Active Added
    /// </summary>
    /// <param name="me"></param>
    public static void ToggleActive(this Transform[] me)
    {
        for (int i = 0; i < me.Length; i++)
        {
            me[i].gameObject.SetActive(!me[i].gameObject.activeSelf);
        }
    }
}
