using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public static class BoolX
{
    public static bool CoinToss()
    {
        UnityEngine.Random.seed++;
        return UnityEngine.Random.value >= .5f;
    }

    public static bool Random()
    {
        return BoolX.CoinToss();
    }
	
}
