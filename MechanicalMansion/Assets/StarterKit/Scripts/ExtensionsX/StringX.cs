using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class StringX
{
    /// <summary>
    /// Returns true if the string is null or ""
    /// </summary>
    /// <param name="me"></param>
    /// <returns></returns>
    public static bool IsNullOrEmpty(this string me)
    {
        return string.IsNullOrEmpty(me);
    }

    /// <summary>
    /// Returns true if the string is not null or ""
    /// </summary>
    /// <param name="me"></param>
    /// <returns></returns>
    public static bool IsNotNullOrEmpty(this string me)
    {
        return !string.IsNullOrEmpty(me);
    }

    /// <summary>
    /// Creates a string[] of string where each element in the array is each character of the string as a string
    /// </summary>
    /// <param name="me"></param>
    /// <returns></returns>
    public static string[] ToStringOfCharArray(this string me)
    {
        char[] charArray = me.ToCharArray();
        List<string> stringList = new List<string>();

        for (int i = 0; i < charArray.Length; i++)
        {
            stringList.Add(charArray[i].ToString());
        }
        return stringList.ToArray();
    }

    /// <summary>
    /// removes inset amounts from string ie. "0example01" with leftIn at 1 and with rightIn at 2 would result in "example"
    /// </summary>
    /// <param name="me"></param>
    /// <param name="inset"></param>
    /// <returns></returns>
    public static string Inset(this string me, int leftIn, int rightIn)
    {
        return me.Substring(leftIn, me.Length - rightIn - leftIn);
    }

    /// <summary>
    /// removes inset amount from string end ie. "example01" with inset at 2 would result in "example"
    /// </summary>
    /// <param name="me"></param>
    /// <param name="inset"></param>
    /// <returns></returns>
    public static string InsetFromEnd(this string me, int inset)
    {
        return me.Substring(0, me.Length - inset);
    }

    /// <summary>
    /// removes inset amount from string start ie. "01example" with inset at 2 would result in "example"
    /// </summary>
    /// <param name="me"></param>
    /// <param name="inset"></param>
    /// <returns></returns>
    public static string InsetFromStart(this string me, int inset)
    {
        return me.Substring(inset, me.Length - inset);
    }

    /// <summary>
    /// Gets a string starting at last index going to first index by a length ie. "Monkey".SubStringFromEnd(1,3) returns "nke"
    /// </summary>
    /// <param name="me"></param>
    /// <param name="index"></param>
    /// <param name="length"></param>
    /// <returns></returns>
	public static string SubStringFromEnd(this string me, int index, int length)
	{
		return(me.Substring(me.Length-index-length,length));
	}

    /// <summary>
    /// Gets a string starting at last index going to first index by a length ie. "Monkey".SubStringFromEnd(1,3) returns "Monke"
    /// </summary>
    /// <param name="me"></param>
    /// <param name="index"></param>
    /// <returns></returns>
	public static string SubStringFromEnd(this string me, int index)
	{
		return(me.SubStringFromEnd(index,me.Length-index));
	}

	public static string ToOther(this string me)
	{
		if(me == me.ToUpper())
			return me.ToLower();
		return me.ToUpper();
	}
 
    /// <summary>
    /// Returns true when all characters of the string are upppercase
    /// </summary>
    /// <param name="me"></param>
    /// <returns></returns>
	public static bool IsUpper(this string me)
	{
		return me == me.ToUpper();
	}

    /// <summary>
    /// Returns true when all characters of the string are lowercase
    /// </summary>
    /// <param name="me"></param>
    /// <returns></returns>
    public static bool IsLower(this string me)
    {
        return me == me.ToLower();
    }

    /// <summary>
    /// returns true when characters are not all uppercase and not all lowercase
    /// </summary>
    /// <param name="me"></param>
    /// <returns></returns>
    public static bool isMixed(this string me)
    {
        return (!me.IsUpper() && !me.IsLower());
    }

    /// <summary>
    /// Returns true if the string can be parced into an int
    /// </summary>
    /// <param name="me"></param>
    /// <returns></returns>
	public static bool IsInt(this string me)
	{
		int result = 0;
		if(int.TryParse(me,out result))
			return true;
		return false;
	}

    /// <summary>
    /// Parses string for a int value
    /// </summary>
    /// <param name="me"></param>
    /// <returns></returns>
    public static int IntParse(this string me)
    {
        return int.Parse(me);
    }

    /// <summary>
    /// Parses string for a float value
    /// </summary>
    /// <param name="me"></param>
    /// <returns></returns>
    public static float FloatParse(this string me)
    {
        return float.Parse(me);
    }

    /// <summary>
    /// Returns true if the string can be parced into an float
    /// </summary>
    /// <param name="me"></param>
    /// <returns></returns>
    public static bool IsFloat(this string me)
    {
        int result = 0;
        if (int.TryParse(me, out result))
            return true;
        return false;
    }

    /// <summary>
    /// Bolds a string
    /// </summary>
    /// <param name="me"></param>
    /// <returns></returns>
	public static string RTBold(this string me)
	{
		return("<b>"+me+"</b>");
	}

    /// <summary>
    /// Italicizes a string
    /// </summary>
    /// <param name="me"></param>
    /// <returns></returns>
	public static string RTItalicize(this string me)
	{
		return("<i>"+me+"</i>");
	}

    /// <summary>
    /// Changes font size of string
    /// </summary>
    /// <param name="me"></param>
    /// <param name="size"></param>
    /// <returns></returns>
	public static string RTSize(this string me, int size)
	{
		return("<size="+size+">"+me+"</size>");
	}

	/// <summary>
	/// RT the specified me and color.
	/// Use Example "red" || DebugColor.RED || "#ff0000ff"
	/// </summary>
	/// <param name="me">Me.</param>
	/// <param name="color">Color.</param>
	public static string RTColor(this string me, string color)
	{
		return("<color="+color+">"+me+"</color>");
	}

	/// <summary>
	/// (same as cyan)
	/// </summary>
	/// <returns>The aqua.</returns>
	/// <param name="me">Me.</param>
	public static string RTAqua(this string me)
	{
		return(me.RTColor(DebugColor.AQUA));
	}

    /// <summary>
    /// colors string black
    /// </summary>
    /// <param name="me"></param>
    /// <returns></returns>
	public static string RTBlack(this string me)
	{
		return(me.RTColor(DebugColor.BLACK));
	}

    /// <summary>
    /// colors string blue
    /// </summary>
    /// <param name="me"></param>
    /// <returns></returns>
	public static string RTBlue(this string me)
	{
		return(me.RTColor(DebugColor.BLUE));
	}

    /// <summary>
    /// colors string brown
    /// </summary>
    /// <param name="me"></param>
    /// <returns></returns>
	public static string RTBrown(this string me)
	{
		return(me.RTColor(DebugColor.BROWN));
	}

	/// <summary>
	/// (same as aqua)
	/// </summary>
	/// <returns>The cyan.</returns>
	/// <param name="me">Me.</param>
	public static string RTCyan(this string me)
	{
		return(me.RTColor(DebugColor.CYAN));
	}

    /// <summary>
    /// colors string dark blue
    /// </summary>
    /// <param name="me"></param>
    /// <returns></returns>
	public static string RTDarkBlue(this string me)
	{
		return(me.RTColor(DebugColor.DARKBLUE));
	}

	/// <summary>
	/// (same as magenta)
	/// </summary>
	/// <returns>The fuchsia.</returns>
	/// <param name="me">Me.</param>
	public static string RTFuchsia(this string me)
	{
		return(me.RTColor(DebugColor.FUCHSIA));
	}

    /// <summary>
    /// colors string green
    /// </summary>
    /// <param name="me"></param>
    /// <returns></returns>
	public static string RTGreen(this string me)
	{
		return(me.RTColor(DebugColor.GREEN));
	}

    /// <summary>
    /// colors string grey
    /// </summary>
    /// <param name="me"></param>
    /// <returns></returns>
	public static string RTGrey(this string me)
	{
		return(me.RTColor(DebugColor.GREY));
	}

    /// <summary>
    /// colors string light blue
    /// </summary>
    /// <param name="me"></param>
    /// <returns></returns>
	public static string RTLightBlue(this string me)
	{
		return(me.RTColor(DebugColor.LIGHTBLUE));
	}

    /// <summary>
    /// colors string lime
    /// </summary>
    /// <param name="me"></param>
    /// <returns></returns>
	public static string RTLime(this string me)
	{
		return(me.RTColor(DebugColor.LIME));
	}

	/// <summary>
	/// (same as fuchsia)
	/// </summary>
	/// <returns>The magenta.</returns>
	/// <param name="me">Me.</param>
	public static string RTMagenta(this string me)
	{
		return(me.RTColor(DebugColor.MAGENTA));
	}

    /// <summary>
    /// colors string maroon
    /// </summary>
    /// <param name="me"></param>
    /// <returns></returns>
	public static string RTMaroon(this string me)
	{
		return(me.RTColor(DebugColor.MAROON));
	}

    /// <summary>
    /// colors string nany
    /// </summary>
    /// <param name="me"></param>
    /// <returns></returns>
	public static string RTNavy(this string me)
	{
		return(me.RTColor(DebugColor.NAVY));
	}

    /// <summary>
    /// colors string olive
    /// </summary>
    /// <param name="me"></param>
    /// <returns></returns>
	public static string RTOlive(this string me)
	{
		return(me.RTColor(DebugColor.OLIVE));
	}

    /// <summary>
    /// colors string orange
    /// </summary>
    /// <param name="me"></param>
    /// <returns></returns>
	public static string RTOrange(this string me)
	{
		return(me.RTColor(DebugColor.ORANGE));
	}

    /// <summary>
    /// colors string purple
    /// </summary>
    /// <param name="me"></param>
    /// <returns></returns>
	public static string RTPurple(this string me)
	{
		return(me.RTColor(DebugColor.PURPLE));
	}

    /// <summary>
    /// colors string red
    /// </summary>
    /// <param name="me"></param>
    /// <returns></returns>
	public static string RTRed(this string me)
	{
		return(me.RTColor(DebugColor.RED));
	}

    /// <summary>
    /// colors string silver
    /// </summary>
    /// <param name="me"></param>
    /// <returns></returns>
	public static string RTSilver(this string me)
	{
		return(me.RTColor(DebugColor.SILVER));
	}

    /// <summary>
    /// colors string teal
    /// </summary>
    /// <param name="me"></param>
    /// <returns></returns>
	public static string RTTeal(this string me)
	{
		return(me.RTColor(DebugColor.TEAL));
	}

    /// <summary>
    /// colors string white
    /// </summary>
    /// <param name="me"></param>
    /// <returns></returns>
	public static string RTWhite(this string me)
	{
		return(me.RTColor(DebugColor.WHITE));
	}

    /// <summary>
    /// colors string yellow
    /// </summary>
    /// <param name="me"></param>
    /// <returns></returns>
	public static string RTYellow(this string me)
	{
		return(me.RTColor(DebugColor.YELLOW));
	}
}
