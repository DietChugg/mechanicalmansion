using UnityEngine;
using System.Collections;

public static class CircleCollider2DX
{
	#region Constants
	#endregion
	
	#region StaticMethods
	#endregion
	
	#region ExtensionMethods
	public static void SetEnabled(this CircleCollider2D me, bool enabled)
	{
		if(me.enabled != enabled)
			me.enabled = enabled;
	}
	#endregion


}
