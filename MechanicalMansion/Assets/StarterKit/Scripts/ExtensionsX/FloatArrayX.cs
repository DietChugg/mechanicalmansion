using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class FloatArrayX
{

	public static float Min(this float[] me)
	{
		if(me == null || me.Length == 0)
		{
			Debug.LogError("I'm Nonexistant");
			return 0;
		}

		float min = me[0];
		for (int i = 0; i < me.Length; i++) 
		{
			min = Mathf.Min(min,me[i]);
		}
		return min;
	}

	public static float Max(this float[] me)
	{
		if(me == null || me.Length == 0)
		{
			Debug.LogError("I'm Nonexistant");
			return 0;
		}
		
		float max = me[0];
		for (int i = 0; i < me.Length; i++) 
        {
			max = Mathf.Max(max,me[i]);
        }
		return max;
    }

	public static float Medium(this float[] me)
	{
		return (me.Max () + me.Min ())/2;
	}

	public static float Range(this float[] me)
	{
		return (me.Max () - me.Min ());
	}

	public static float Mean(this float[] me)
	{
		float sum = me.Sum();
		return sum/me.Length;
	}

	public static float Sum(this float[] me)
	{
		float sum = 0;
		for (int i = 0; i < me.Length; i++) 
		{
			sum += me[i];
		}
		return sum;
	}
}
