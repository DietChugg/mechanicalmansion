using UnityEngine;
using System.Collections;

public static class Collider2DX
{
    #region Constants
    #endregion

    #region StaticMethods
    #endregion

    #region ExtensionMethods
    /// <summary>
    /// Calculates the Top Right Corner of the Collider in World Space
    /// </summary>
    /// <param name="box"></param>
    /// <returns></returns>
    public static Vector2 TopRightCorner(this BoxCollider2D box)
    {
        return new Vector2((box.transform.position.x) + ((box.size.x + box.offset.x * 2f) * box.transform.lossyScale.x) / 2f,
                (box.transform.position.y) + ((box.size.y + box.offset.y * 2f) * box.transform.lossyScale.y) / 2f);
    }
    /// <summary>
    /// Calculates the Bottom Right Corner of the Collider in World Space
    /// </summary>
    /// <param name="box"></param>
    /// <returns></returns>
    public static Vector2 BottomRightCorner(this BoxCollider2D box)
    {
        return new Vector2((box.transform.position.x) + ((box.size.x + box.offset.x * 2f) * box.transform.lossyScale.x) / 2f,
                (box.transform.position.y) - ((box.size.y - +box.offset.y * 2f) * box.transform.lossyScale.y) / 2f);
    }

    /// <summary>
    /// Calculates the Top Left Corner of the Collider in World Space
    /// </summary>
    /// <param name="box"></param>
    /// <returns></returns>
    public static Vector2 TopLeftCorner(this BoxCollider2D box)
    {
        return new Vector2((box.transform.position.x) - ((box.size.x - box.offset.x * 2f) * box.transform.lossyScale.x) / 2f,
                (box.transform.position.y) + ((box.size.y + box.offset.y * 2f) * box.transform.lossyScale.y) / 2f);
    }

    /// <summary>
    /// Calculates the Bottom Left Corner of the Collider in World Space
    /// </summary>
    /// <param name="box"></param>
    /// <returns></returns>
    public static Vector2 BottomLeftCorner(this BoxCollider2D box)
    {
        return new Vector2((box.transform.position.x) - ((box.size.x - box.offset.x * 2f) * box.transform.lossyScale.x) / 2f,
                (box.transform.position.y) - ((box.size.y - box.offset.y * 2f) * box.transform.lossyScale.y) / 2f);
    }

    /// <summary>
    /// Calculates the x position vertically of the Right Side of the collider in World Space
    /// </summary>
    /// <param name="box"></param>
    /// <returns></returns>
    public static float RightSide(this BoxCollider2D box)
    {
        return (box.transform.position.x) + ((box.size.x + box.offset.x * 2f) * box.transform.lossyScale.x) / 2f;
    }

    /// <summary>
    /// Calculates the x position vertically of the LEft Side of the collider in World Space
    /// </summary>
    /// <param name="box"></param>
    /// <returns></returns>
    public static float LeftSide(this BoxCollider2D box)
    {
        return (box.transform.position.x) - ((box.size.x - box.offset.x * 2f) * box.transform.lossyScale.x) / 2f;
    }

    /// <summary>
    /// Calculates the y position horizontally of the Top Side of the collider in World Space
    /// </summary>
    /// <param name="box"></param>
    /// <returns></returns>
    public static float TopSide(this BoxCollider2D box)
    {
        return (box.transform.position.y) + ((box.size.y + box.offset.y * 2f) * box.transform.lossyScale.y) / 2f;
    }

    /// <summary>
    /// Calculates the y position horizontally of the Bottom Side of the collider in World Space
    /// </summary>
    /// <param name="box"></param>
    /// <returns></returns>
    public static float BottomSide(this BoxCollider2D box)
    {
        return (box.transform.position.y) - ((box.size.y - +box.offset.y * 2f) * box.transform.lossyScale.y) / 2f;
    }


    #endregion


}
