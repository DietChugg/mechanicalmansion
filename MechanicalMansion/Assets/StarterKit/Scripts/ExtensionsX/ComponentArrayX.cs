using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public static class ComponentArrayX 
{
	public static T[] GetComponents<T>(params Component[] components) where T: Component
	{
		List<T> foundComponents = new List<T>();
		for (int i = 0; i < components.Length; i++) 
		{
			T component = components[i].GetComponent<T>();
			if(component != null)
				foundComponents.Add(component);
		}
		return foundComponents.ToArray();
	}
}
