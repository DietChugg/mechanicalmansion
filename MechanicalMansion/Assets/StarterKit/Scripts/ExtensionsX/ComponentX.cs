using UnityEngine;
using System.Collections;
using System;
using System.Reflection;

public static class ComponentX 
{
    static public T ObtainComponent<T> (this Component child) where T: Component 
    {
        T result = child.GetComponent<T>();
        if (result == null) {
            result = child.gameObject.AddComponent<T>();
        }
        return result;
    }

	public static T GetCopyOf<T>(this Component comp, T other) where T : Component
	{
		Type type = comp.GetType();
		if (type != other.GetType()) return null; // type mis-match
		BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Default | BindingFlags.DeclaredOnly;
		PropertyInfo[] pinfos = type.GetProperties(flags);
		foreach (var pinfo in pinfos) {
			if (pinfo.CanWrite) {
				try {
					pinfo.SetValue(comp, pinfo.GetValue(other, null), null);
				}
				catch { }
			}
		}
		FieldInfo[] finfos = type.GetFields(flags);
		foreach (var finfo in finfos) {
			finfo.SetValue(comp, finfo.GetValue(other));
		}
		return comp as T;
	}

	public static Component CopyComponentTo(this GameObject original, Component component, GameObject destination)
	{
		System.Type type = component.GetType();
		Component copy = destination.GetComponent(type);
		if(copy == null)
		{
			copy = destination.AddComponent(type);
		}

		BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Default | BindingFlags.DeclaredOnly;
		PropertyInfo[] pinfos = type.GetProperties(flags);

		Debug.Log("pinfos: " + pinfos.Length);

		foreach (var pinfo in pinfos) {
			if (pinfo.CanWrite) {
				try {
					pinfo.SetValue(destination, pinfo.GetValue(type, null), null);
				}
				catch { }
			}
		}


		FieldInfo[] fields = type.GetFields(flags); 
		Debug.Log(type.ToString() + " fields " + fields.Length);
		foreach (FieldInfo field in fields)
		{
			field.SetValue(copy, field.GetValue(component));
		}
		return copy;
	}

//	public static Component[] CopyComponentsTo(this GameObject original, Component component, GameObject destination)
//	{
//		System.Type type = original.GetType();
//		Component copy = destination.AddComponent(type);
//		System.Reflection.FieldInfo[] fields = type.GetFields(); 
//		foreach (System.Reflection.FieldInfo field in fields)
//		{
//			field.SetValue(copy, field.GetValue(component));
//		}
//		return copy;
//	}


}
