using UnityEngine;
using UnityEngine.UI;
//using System;
using System.Collections;
using System.Collections.Generic;
using StarterKit;

public static class ObjectX 
{
    public static void Destroy(this Object o, float time)
    {
        CoroutineManager.Instance.StartCoroutine(DestoryAfterTime(o, time));
    }

    public static void Destroy(this Object o)
    {
        CoroutineManager.Instance.StartCoroutine(DestoryMe(o));
    }

    public static IEnumerator DestoryAfterTime(Object o, float time)
    {
        yield return new WaitForSeconds(time);
        Object.Destroy(o);
    }

    public static IEnumerator DestoryMe(Object o)
    {
        Object.Destroy(o);
        yield return null;
    }
}
