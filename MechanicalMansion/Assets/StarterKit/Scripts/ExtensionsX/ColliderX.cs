using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using StarterKit;

public static class ColliderX
{
    // Have not found a good solution for this problem yet...
    public static bool Contains(this Collider me, Collider collider)
    {
        bool isColliderInsideCollider = false;
        Vector3 targetBoundsClosePoint = collider.ClosestPointOnBounds(me.GetComponent<Collider>().bounds.center);
        Vector3 directionToSelf = targetBoundsClosePoint.GetDirectionTo(collider.bounds.center);
        RaycastHit[] hits;
        hits = Physics.RaycastAll(targetBoundsClosePoint,directionToSelf);
        for (int i = 0; i < hits.Length; i++)
        {
            if(hits[i].transform == collider.transform)
            {
                isColliderInsideCollider = me.GetComponent<Collider>().Contains(hits[i].point);
            }
        }
        return isColliderInsideCollider;
    }


    public static bool Contains(this Collider me, Vector3 point)
    {
        Vector3 currentPoint;
        Vector3 startPos = new Vector3(0,100,0); // This is defined to be some arbitrary point far away from the collider.
        int bumpCount = 0;
        while(me.bounds.Contains(startPos) && bumpCount < 100)
        {
            startPos += new Vector3(0,100,0); 
            Debug.Log("Raise The Bar");
            bumpCount++;
        }
        if(bumpCount >= 100)
        {
            Debug.LogWarning("BumpCount TOo High...");
            return false;
        }
        
        Vector3 direction = (point-startPos).normalized; // This is the direction from start to goal.
        
        int itterations = 0; // If we know how many times the raycast has hit faces on its way to the target and back, we can tell through logic whether or not it is inside.
        int checkCount = 0;
        currentPoint = startPos;
        
        while(currentPoint != point && checkCount < Physics.solverIterationCount) // Try to reach the point starting from the far off point.  This will pass through faces to reach its objective.
        {
            RaycastHit hit;
            if( Physics.Linecast(currentPoint, point, out hit)) // Progressively move the point forward, stopping everytime we see a new plane in the way.
            {
                if(hit.transform == me.transform)
                    itterations ++;
                currentPoint = hit.point + (direction/100.0f); // Move the Point to hit.point and push it forward just a touch to move it through the skin of the mesh (if you don't push it, it will read that same point indefinately).
            }
            else
            {
                currentPoint = point; // If there is no obstruction to our goal, then we can reach it in one step.
            }
            if(checkCount > Physics.solverIterationCount)
                currentPoint = point;
            checkCount ++;
        }
        while(currentPoint != startPos && checkCount < Physics.solverIterationCount) // Try to return to where we came from, this will make sure we see all the back faces too.
        {
            RaycastHit hit;
            if( Physics.Linecast(currentPoint, startPos, out hit))
            {
                if(hit.transform == me.transform)
                    itterations ++;
                currentPoint = hit.point + (-direction/100.0f);
            }
            else
            {
                currentPoint = startPos;
            }
            if(checkCount > Physics.solverIterationCount)
                currentPoint = startPos;
            checkCount ++;
        }

//        if(itterations > 1)
//            Debug.Log("itterations is " + itterations);
//        if(checkCount > 1)
//            Debug.Log("checkCount is " + checkCount);

        if(checkCount > Physics.solverIterationCount)
            Debug.LogWarning("itterations count too high > " + Physics.solverIterationCount + " Results will be skewed... Change Physics.solverIterationCount to get more accurate results");
        return itterations % 2 == 1;
    }
}
