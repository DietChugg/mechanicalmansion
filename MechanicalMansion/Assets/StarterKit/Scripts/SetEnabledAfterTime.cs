﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using StarterKit;

public class SetEnabledAfterTime : LifetimeBehaviour 
{
    public float time;
    [Tooltip("Toggles the state of the objects")]
    public bool isToggle;
    public bool setEnabled;
    public bool disablesAfterUse = false;
    public List<UnityEngine.Object> objects = new List<UnityEngine.Object>();

	public IEnumerator WaitAndActivate()
	{
        yield return new WaitForSeconds(time);
        for (int i = 0; i < objects.Count; i++)
        {
            GameObject go = objects[i] as GameObject;
            Behaviour behaviour = objects[i] as Behaviour;
            if (go != null)
            {
                if (isToggle)
                {
                    go.SetActive(!go.activeSelf);
                }
                else
                {
                    go.SetActive(setEnabled);
                }
            }
            else if (behaviour != null)
            {
                if (isToggle)
                {
                    behaviour.enabled = !behaviour.enabled;
                }
                else
                {
                    behaviour.enabled = setEnabled;
                }
            }
        }
        if (disablesAfterUse)
        {
            enabled = false;    
        }
	}

    [ContextMenu("Begin")]
    public override void Begin()
    {
        StartCoroutine(WaitAndActivate());
    }

}