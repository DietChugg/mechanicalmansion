using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Bool3
{
    public bool x;
    public bool y;
    public bool z;

	public Bool3()
	{}

	public Bool3(bool x,bool y,bool z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}

    public bool IsAllMarkedAs(bool value)
    {
        return x == value && y == value && z == value;
    }
}
