﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using StarterKit;

[Serializable]
public class AnimationCurve3
{
    public AnimationCurve x = new AnimationCurve(new Keyframe(0, 0, 0, 0), new Keyframe(.5f, 1, 0, 0), new Keyframe(1, 0, 0, 0));
    public AnimationCurve y = new AnimationCurve(new Keyframe(0, 0, 0, 0), new Keyframe(.5f, 1, 0, 0), new Keyframe(1, 0, 0, 0));
    public AnimationCurve z = new AnimationCurve(new Keyframe(0, 0, 0, 0), new Keyframe(1, 0, 0, 0));
    public Vector3 multiplier;

    /// <summary>
    /// Returns a Vector3 based off the x,y, and z's animation curves of the time passed in their respective curves
    /// </summary>
    /// <param name="time"></param>
    /// <returns></returns>
    public Vector3 Evaluate(float time)
    {
        return new Vector3(x.Evaluate(time) * multiplier.x, 
            y.Evaluate(time) * multiplier.y, 
            z.Evaluate(time) * multiplier.z);
    }

    public AnimationCurve3 InverseCurveTimes()
    {
        AnimationCurve3 inverse = new AnimationCurve3();
        inverse.x = x.GetInverseTimeOfCurve();
        inverse.y = y.GetInverseTimeOfCurve();
        inverse.z = z.GetInverseTimeOfCurve();
        inverse.multiplier = multiplier;
        return inverse;
    }
}
