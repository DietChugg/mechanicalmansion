using UnityEngine;
using System.Collections;

public class EnableGameObjectOnAnimationComplete : MonoBehaviour 
{
	float timePassed;
	public float offset;
	public GameObject toActivate;
    public float deactivateOffset = 0f;
	public GameObject toDeactivate;
    public bool disableSelf = true;

	void OnEnable () 
	{
		timePassed = 0f;
	}
	
	void Update () 
	{
		timePassed += Time.deltaTime;

		if(timePassed > GetComponent<Animation>()[GetComponent<Animation>().clip.name].length+offset)
		{
			if(toActivate)
				toActivate.SetActive(true);
            if (disableSelf)
			    enabled = false;
//			gameObject.SetActive(false);
		}

        if (timePassed > GetComponent<Animation>()[GetComponent<Animation>().clip.name].length+deactivateOffset)
        {
           if(toDeactivate)
				toDeactivate.SetActive(false);
        }
	}
}
