using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AnimationByMatSwap : MonoBehaviour 
{
	public List<Material> materials = new List<Material>();
	public int index = 0;
	public FloatClamp refreshRate = new FloatClamp();

	void Update () 
	{
		refreshRate -= Time.deltaTime;
		if(refreshRate.IsAtMin())
		{
			refreshRate.Maximize();
			index = (index+1)%materials.Count;
			GetComponent<Renderer>().material = materials[index];
		}
	}
}
