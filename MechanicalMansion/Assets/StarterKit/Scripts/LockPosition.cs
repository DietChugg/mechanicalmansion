using UnityEngine;
using System.Collections;
using StarterKit;

public class LockPosition : MonoBehaviourPlus 
{
	public Vector2 yRange;
	
	void Update () 
	{
		transform.SetYPosition(Mathf.Clamp(transform.position.y,yRange.x, yRange.y));
	}
}
