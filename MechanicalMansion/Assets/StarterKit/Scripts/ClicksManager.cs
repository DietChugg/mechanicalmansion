using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace StarterKit
{
    public class ClicksManager : MonoBehaviour
    {
        public bool clicks2D = true;
        public bool clicks3D = true;
        public bool clicksAll = true;

        public static SafeAction<GameObject> OnClickDown;
        public static SafeAction<GameObject> OnClickUp;
        public static SafeAction<GameObject> OnClickDown2D;
        public static SafeAction<GameObject> OnClickUp2D;

        private Camera clickCamera;

        void OnDrawGizmos()
        {

        }

        void OnEnable()
        {
            if (GetComponent<Camera>() != null)
                clickCamera = GetComponent<Camera>();
            else
                clickCamera = Camera.main;
        }

        void Update()
        {
            if (clickCamera == null)
            {
                OnEnable();
                return;
            }

            if (Input.GetMouseButtonDown(0))
            {
                EmulateClickDown(Input.mousePosition);
            }

            if (Input.GetMouseButtonUp(0))
            {
                EmulateClickUp(Input.mousePosition);
            }
        }

        #region Public Methods
        /// <summary>
        /// Simulates a click down at the screen point
        /// </summary>
        /// <param name="screenPoint">Screen point to simulate click</param>
        public void EmulateClickDown(Vector3 screenPoint)
        {
            if (clicks3D)
                RaycastMethod(OnClickDown, screenPoint);
            if (clicks2D)
                RaycastMethod2D(OnClickDown2D, screenPoint);
        }

        /// <summary>
        /// Simulates a click up at the screen point
        /// </summary>
        /// <param name="screenPoint">Screen point to simulate click</param>
        public void EmulateClickUp(Vector3 screenPoint)
        {
            if (clicks3D)
                RaycastMethod(OnClickUp, screenPoint);
            if (clicks2D)
                RaycastMethod2D(OnClickUp2D, screenPoint);
        }
        #endregion

        void RaycastMethod2D(SafeAction<GameObject> action, Vector3 screenPoint)
        {
            Ray mouseRay = clickCamera.ScreenPointToRay(Input.mousePosition);
            if (!clicksAll)
            {
                RaycastHit2D hit = Physics2D.GetRayIntersection(mouseRay, Mathf.Infinity, clickCamera.cullingMask);
                if (hit.transform != null && hit.transform != null && hit.transform.gameObject != null)
                {
                    CallAction(action, hit.transform.gameObject);
                }
                return;
            }

            List<RaycastHit2D> hits = Physics2D.GetRayIntersectionAll(mouseRay, Mathf.Infinity, clickCamera.cullingMask).ToList();
            hits.Sort(delegate(RaycastHit2D rc1, RaycastHit2D rc2)
            {
                return Vector3.Distance(this.transform.position, rc1.transform.position).CompareTo
                            ((Vector3.Distance(this.transform.position, rc2.transform.position)));
            });

            foreach (RaycastHit2D hit in hits)
            {
                if (hit.transform)
                {
                    if (CallAction(action, hit.transform.gameObject))
                        break;
                }
            }
        }

        void RaycastMethod(SafeAction<GameObject> action, Vector3 screenPoint)
        {
            Ray mouseRay = clickCamera.ScreenPointToRay(Input.mousePosition);

            if (!clicksAll)
            {
                RaycastHit hit;
                if (Physics.Raycast(mouseRay, out hit, Mathf.Infinity, clickCamera.cullingMask))
                {
                    CallAction(action, hit.transform.gameObject);
                }
                return;
            }

            List<RaycastHit> hits = Physics.RaycastAll(mouseRay, Mathf.Infinity, clickCamera.cullingMask).ToList();
            hits.Sort(delegate(RaycastHit rc1, RaycastHit rc2)
            {
                return Vector3.Distance(this.transform.position, rc1.transform.position).CompareTo
                            ((Vector3.Distance(this.transform.position, rc2.transform.position)));
            });

            foreach (RaycastHit hit in hits)
            {
                if (hit.transform)
                {
                    if (CallAction(action, hit.transform.gameObject))
                        break;
                }
            }
        }

        bool CallAction(SafeAction<GameObject> action, GameObject go)
        {
            BlockClicks blockClicks = go.GetComponent<BlockClicks>();

            if (blockClicks != null)
            {
                if (!blockClicks.ignoreSelf)
                    action.Call(go);

                return blockClicks.blockClicks;
            }
            else
                action.Call(go);

            return false;
        }
    }


}