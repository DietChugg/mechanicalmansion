using UnityEngine;
using System.Collections;
using StarterKit;

public class AddRandomForce : MonoBehaviour 
{
	public Vector3 min, max;

	void OnEnable()
	{
		GetComponent<Rigidbody>().AddForce(new Vector3(RandomX.Range(min.x, max.x),
		                               RandomX.Range(min.y, max.y),
		                               RandomX.Range(min.z, max.z)));
	}
}
