using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using StarterKit;

public class PauseButton : MonoBehaviourPlus
{
    public Sprite pauseSprite;
    public Sprite playSprite;

    public new void OnEnable()
    {
        (this as MonoBehaviourPlus).OnEnable();
    }

    public new void OnDisable()
    {
        (this as MonoBehaviourPlus).OnDisable();
    }

    public void OnClicked()
    {
        PauseController.Instance.IsPaused = !PauseController.Instance.IsPaused;
        GetComponent<Image>().sprite = PauseController.Instance.IsPaused ? playSprite : pauseSprite;
    }

    //public void OnApplicationPause()
    //{
    //    image.enabled = false;
    //}

    public override void OnPause(bool isPaused)
    {
        if (!isPaused)
        {
            image.enabled = true;
        }
        else
        {
            image.enabled = false;
        }
    }
}
