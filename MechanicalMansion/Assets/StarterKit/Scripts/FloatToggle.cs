using UnityEngine;
using System.Collections;

[System.Serializable]
public class FloatToggle 
{
	public float value;
	public bool enabled;

	public static implicit operator float(FloatToggle c)
	{
		return c.value;
	}

	public static implicit operator bool(FloatToggle c)
	{
		return c.enabled;
	}
}
