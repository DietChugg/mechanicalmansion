using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Camera))]
public class Wireframe : MonoBehaviour 
{
	public bool displayWireframe;
	public Color background = new Color32(45,45,45,0);

	private Camera requiredCamera;
	private Color initBackgroundColor;

	void OnEnable()
	{
		requiredCamera = GetComponent<Camera>();
		initBackgroundColor = requiredCamera.backgroundColor;
	}

	void OnPreRender() 
	{
		SetWireFrame(true);
	}
	void OnPostRender() 
	{
		SetWireFrame(false);
	}

	void SetWireFrame(bool value)
	{
		if(displayWireframe)
		{
			GL.wireframe = value;
			requiredCamera.backgroundColor = background;
		}
		else
		{
			requiredCamera.backgroundColor = initBackgroundColor;
		}
	}


}
