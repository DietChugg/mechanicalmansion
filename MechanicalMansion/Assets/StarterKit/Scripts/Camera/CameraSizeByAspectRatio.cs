using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CameraSizeByAspectRatio : MonoBehaviour 
{
	public List<CameraOrthographicSize> cameraSizes = new List<CameraOrthographicSize>();
	void OnEnable () 
	{
		foreach(CameraOrthographicSize camSize in cameraSizes)
			if(Mathf.Approximately(GetComponent<Camera>().aspect,camSize.aspect))
				GetComponent<Camera>().orthographicSize = camSize.orthographicSize;
	}
}

[System.Serializable]
public class CameraOrthographicSize
{
	public float aspect;
	public float orthographicSize;
}
