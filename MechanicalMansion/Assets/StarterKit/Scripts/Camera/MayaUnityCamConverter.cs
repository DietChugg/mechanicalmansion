using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Camera))]
public class MayaUnityCamConverter : MonoBehaviour 
{
	[Header("Maya Camera FilmGate Vertical (mm)")]
	public float viewHeight;
	[Header("Maya Camera Focal Length")]
	public float focalLength;

	[ContextMenu("Convert")]
	public void Convert () 
	{
		float fov = Mathf.Rad2Deg * 2.0f * Mathf.Atan(viewHeight / (2.0f * focalLength));
		GetComponent<Camera>().fieldOfView = fov;
	}
}
