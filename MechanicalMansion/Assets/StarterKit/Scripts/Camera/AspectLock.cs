using UnityEngine;
using System.Collections;

public class AspectLock : MonoBehaviour
{
    public float aspect;
    void OnEnable()
    {
        GetComponent<Camera>().aspect = aspect;
    }
    
    void Update()
    {
    
    }
}
