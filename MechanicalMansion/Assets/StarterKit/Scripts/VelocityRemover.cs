using UnityEngine;
using System.Collections;
using StarterKit;

public class VelocityRemover : MonoBehaviourPlus 
{
	public float dampenSpeed;

	void FixedUpdate () 
	{
		if(GetComponent<Rigidbody>().velocity.magnitude > 0)
		{
			GetComponent<Rigidbody>().velocity = GetComponent<Rigidbody>().velocity.normalized * (GetComponent<Rigidbody>().velocity.magnitude - (dampenSpeed * Time.deltaTime));
		}
	}
}
