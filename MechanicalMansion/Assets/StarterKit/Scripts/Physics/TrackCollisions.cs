using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TrackCollisions : MonoBehaviour 
{
	public List<Transform> collisions;
    //public FloatClamp timer;

    //public void Update()
    //{
    //    timer.Adjust(-Time.deltaTime);
    //    if (timer.IsAtMin())
    //    {
    //        timer.Maximize();
    //        collisions.Clear();
    //    }
    //}
	
	public void OnTriggerEnter(Collider other)
	{
        if (!collisions.Contains(other.transform))
            collisions.Add(other.transform);
	}
	
	public void OnTriggerStay(Collider other)
	{
        if (!collisions.Contains(other.transform))
            collisions.Add(other.transform);
	}
	
	public void OnTriggerExit(Collider other)
	{
        if (collisions.Contains(other.transform))
            collisions.Remove(other.transform);
	}

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (!collisions.Contains(other.transform))
            collisions.Add(other.transform);
    }

    public void OnTriggerStay2D(Collider2D other)
    {
        if (!collisions.Contains(other.transform))
            collisions.Add(other.transform);
    }

    public void OnTriggerExit2D(Collider2D other)
    {
        if (collisions.Contains(other.transform))
            collisions.Remove(other.transform);
    }
}
