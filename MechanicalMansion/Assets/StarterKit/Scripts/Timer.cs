﻿using UnityEngine;
using System.Collections;
using StarterKit;

public class Timer : IPlayer
{
    private FloatClamp timer;
    public FloatRange waitTime;


    public Timer()
    {
        //Timer(new FloatClamp(0, 1f), new FloatRange(1f, 1f));
    }

    public Timer(FloatClamp floatClamp, FloatRange floatRange)
    {
    }

    void UpdateTimer()
    {
        if(OnCompleted != null)
            OnCompleted.Invoke(this,null);
    }

    bool isPlaying = false;
    public bool IsPlaying
    {
        get
        {
            return isPlaying;
        }
        set
        {
            isPlaying = value;
        }
    }

    public bool Loops
    {
        get
        {
            throw new System.NotImplementedException();
        }
        set
        {
            throw new System.NotImplementedException();
        }
    }

    public event System.EventHandler OnCompleted;

    public void Play()
    {
        throw new System.NotImplementedException();
    }

    public void Stop()
    {
        isPlaying = false;
        timer.Maximize();
        throw new System.NotImplementedException();
    }

    public void Pause()
    {
        throw new System.NotImplementedException();
    }

    public void Resume()
    {
        throw new System.NotImplementedException();
    }

    public void Reset()
    {
        throw new System.NotImplementedException();
    }

}
