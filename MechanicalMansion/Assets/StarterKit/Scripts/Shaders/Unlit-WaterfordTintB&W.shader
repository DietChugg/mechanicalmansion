// Unlit shader. Simplest possible textured shader.
// - no lighting
// - no lightmap support
// - no per-material color

Shader "Unlit/WaterfordTintB&W" {
Properties {
	_MainTex ("Base (RGB)", 2D) = "white" {}
	_Contrast ("Base Contrast", Range(0,5)) = 1
	_Brightness ("Base Brightness", Range(-1,1)) = 0
	_Color ("Main Color", Color) = (1,1,1,1)
	_Highlight ("Highlight", Color) = (1,1,1,1)
	_Shadow ("Shadow", Color) = (0,0,0,1)
}

SubShader {
	Tags { "RenderType"="Opaque" }
	LOD 100
	
	Pass {  
		CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata_t {
				float4 vertex : POSITION;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f {
				float4 vertex : SV_POSITION;
				half2 texcoord : TEXCOORD0;
				UNITY_FOG_COORDS(1)
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			float4 _Color;
			float _Contrast;
			float _Brightness;
			float4 _Highlight;
			float4 _Shadow;
			
			v2f vert (appdata_t v)
			{
				v2f o;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.texcoord = TRANSFORM_TEX(v.texcoord, _MainTex);
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.texcoord);
				UNITY_APPLY_FOG(i.fogCoord, col);
				UNITY_OPAQUE_ALPHA(col.a);
				
				float modCol = _Contrast*(col.r - 0.5) + 0.5 + _Brightness;
				
				fixed4 highlight = lerp(_Color, _Highlight, (modCol.r - 0.5)*2 );
				fixed4 shadow = lerp(_Shadow, _Color, modCol.r*2 );
				fixed4 final = lerp(shadow, highlight, col.r);
					
				return final;
			}
		ENDCG
	}
}

}
