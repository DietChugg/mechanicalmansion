﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using StarterKit;

public class DontDestroyOnLoad : MonoBehaviourPlus 
{
    public static int staticId = 0;
    public string nameID;
    public int instanceId;

    new void OnEnable()
    {
        if (this == null)
            return;
        base.OnEnable();
        DontDestroyOnLoad(this.transform.gameObject);
        DontDestroyOnLoad[] dontDestroyTheseOnLoad = FindObjectsOfType<DontDestroyOnLoad>();
        instanceId = staticId;
        staticId++;

        Debug.Log("My Instance ID: " + instanceId);

        DontDestroyOnLoad[] selectedDontDestroyOnLoad = GetAllOfNameId(nameID, dontDestroyTheseOnLoad);
        Debug.Log("DontDestroyOnLoad Count: " + selectedDontDestroyOnLoad.Length);

        int lowestActiveID = LowestActiveID(selectedDontDestroyOnLoad);

        for (int i = 0; i < selectedDontDestroyOnLoad.Length; i++)
        {
            if (selectedDontDestroyOnLoad[i].instanceId != lowestActiveID)
            {
                Debug.Log("Destroying ID Number: " + i);
                Destroy(selectedDontDestroyOnLoad[i].gameObject);
            }
            else
            {
                Debug.Log("Preserving ID Number: " + i);
            }
        }
        dontDestroyTheseOnLoad = FindObjectsOfType<DontDestroyOnLoad>();
        selectedDontDestroyOnLoad = GetAllOfNameId(nameID, dontDestroyTheseOnLoad);
        Debug.Log("DontDestroyOnLoad Remaining: " + selectedDontDestroyOnLoad.Length);
    }

    public DontDestroyOnLoad[] GetAllOfNameId(string nameID, DontDestroyOnLoad[] dontDestroyOnLoadsArray)
    {
        List<DontDestroyOnLoad> dontDestroyOnLoads = new List<DontDestroyOnLoad>();
        for (int i = 0; i < dontDestroyOnLoadsArray.Length; i++)
        {
            if (dontDestroyOnLoadsArray[i].nameID == nameID)
            {
                dontDestroyOnLoads.Add(dontDestroyOnLoadsArray[i]);
            }
        }
        return dontDestroyOnLoads.ToArray();
    }

    new void OnDisable()
    {
        base.OnDisable();
    }

    public int LowestActiveID(DontDestroyOnLoad[] dontDestroyTheseOnLoad)
    {
        int lowestActiveID = dontDestroyTheseOnLoad[0].instanceId;
        for (int i = 0; i < dontDestroyTheseOnLoad.Length; i++)
        {
            if (dontDestroyTheseOnLoad[i].instanceId < lowestActiveID)
                lowestActiveID = dontDestroyTheseOnLoad[i].instanceId;
        }
        return lowestActiveID;
    }
}
