using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections;

namespace StarterKit
{
    public class IntSelector : MonoBehaviour
    {
        public int currentValue;
        public int maxValue;
        public int minValue;
        public Button incrementButton;
        public Button decrementButton;
        public Text intDisplay;

        public UnityIntEvent OnValueChange;
       
        void OnEnable()
        {
            incrementButton.onClick.AddListener(Increment);
            decrementButton.onClick.AddListener(Decrement);
            Refresh();
        }

        void OnDisable()
        {
            incrementButton.onClick.RemoveAllListeners();
            decrementButton.onClick.RemoveAllListeners();
        }

        void Increment()
        {
            currentValue++;
            Refresh();
        }

        void Decrement()
        {
            currentValue--;
            Refresh();
        }

        public void Refresh()
        {
            UpdateIntDisplay();
            UpdateButtonStates();
            OnValueChange.Invoke(currentValue);
        }

        void UpdateIntDisplay()
        {
            intDisplay.text = currentValue.ToString();
        }

        void UpdateButtonStates()
        {
            incrementButton.interactable = (currentValue < maxValue);
            decrementButton.interactable = (currentValue > minValue);
        }
    }
}
