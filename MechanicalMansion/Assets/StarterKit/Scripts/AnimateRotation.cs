using UnityEngine;
using System.Collections;

public class AnimateRotation : MonoBehaviour 
{
	public AnimationCurve zCurve;
	public float multiplier;
	public bool active = true;
	public float offset;
	public float speed = 1f;

	IEnumerator Start () 
	{
		float time = 0f;
		while(active)
		{
			time += Time.deltaTime * speed;
			transform.rotation = Quaternion.Euler(transform.rotation.x,
			                                      transform.rotation.y,
			                                      zCurve.Evaluate(time) * multiplier + offset);
			yield return null;
		}
		yield return null;
	}
}
