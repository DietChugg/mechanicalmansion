using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using StarterKit;

public class SnappingAssetDropper : MonoBehaviour 
{
	public bool eatEvents;
	public bool deleteAllSpots;
	public float sensitivity;
	public bool loops;
	public int index;
	public List<AssetGroup> assetGroups;
	public Transform previewAsset;
	public List<Spot> spots = new List<Spot>();
	public Vector3 camPosition;
	public Vector3 rotationOffset;
	public GameObject 	snap;

	[ContextMenu("ReSnapPoints")]
	void ReSnap()
	{
		foreach(Spot spot in spots)
		{
			spot.transform.position = Vector3X.Round(spot.transform.position);
		}
	}


	void Start () 
	{
	
	}
	
	void Update () 
	{
	
	}

	public AssetGroup AssetGroupAtIndex()
	{
		return assetGroups[index];
	}

	public Transform TransformAtIndex()
	{
		return assetGroups[index].AtIndex();
	}

}

[System.Serializable]
public class Spot
{
	public Spot(string _name, Transform _transform, Vector2 _coordinates)
	{
		name = _name;
		transform = _transform;
		coordinates = _coordinates;
	}
	public string name;
	public Transform transform;
	public Vector2 coordinates;
}

[System.Serializable]
public class AssetGroup
{
	public string name;
	public int index;
	public List<Transform> prefabs = new List<Transform>();

	public Transform AtIndex()
	{
		return prefabs[index];
	}
}
