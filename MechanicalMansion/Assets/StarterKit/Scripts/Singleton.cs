using UnityEngine;
using System.Collections;
using StarterKit;

public class Singleton<T> : MonoBehaviourPlus where T : MonoBehaviourPlus
{
	protected static T instance;
    static GameObject singletonFolder;
	bool init;

	public static T Instance
	{
		get
		{
			if(instance == null)
			{
				instance = (T) FindObjectOfType(typeof(T));
                singletonFolder = GameObject.Find("_Singletons");
				if(singletonFolder == null)
                {
                    singletonFolder = new GameObject("_Singletons");
                    DontDestroyOnLoad(singletonFolder);
                }
				if (instance == null)
				{
					GameObject newObj = new GameObject();
					System.Type myType = typeof(T);
					newObj.name = myType.FullName;
					instance = (T)(newObj.AddComponent<T>());
                    newObj.transform.parent = singletonFolder.transform;
					DontDestroyOnLoad(newObj);
				}
			}
			return instance;
		}
	}

    //[RuntimeInitializeOnLoadMethod]
    //static void AutoLoad()
    //{
    //    Debug.Log("Why am I be dumb?");
    //    if (Instance != null)
    //    { 
        
    //    }
    //}

    
	public void Init()
	{
        if (!init)
        {
            init = true;
            FirstInit();
        }
	}

	virtual public void FirstInit()
	{
	}
}
