using UnityEngine;
using System.Collections;

public class PlayOneShotArray : MonoBehaviour
{
    public bool randomOrder;
    public bool loop;
    public int startIndex;
    public float gap;
    public AudioClip[] clips;
    public float volume = 1f;
    public bool playAtMainCam;
    public bool playOnEnable = true;
    public float enableDelay;

    void OnEnable()
    {
        if (playOnEnable)
            StartCoroutine(WaitToPlay(enableDelay));
    }

    public void Play()
    {
        StartCoroutine(WaitToPlay(0));
    }

    public void PlayIndex(int index)
    {
        startIndex = index;
        StartCoroutine(WaitToPlay(0));
    }

    IEnumerator WaitToPlay(float waitTime)
    {
        startIndex %= clips.Length;
        AudioClip clip = randomOrder ? clips[Random.Range(0, clips.Length)] : clips[startIndex];
        if(!randomOrder)
            startIndex++;

        yield return new WaitForSeconds(waitTime);

        if (playAtMainCam)
        {
            if (Camera.main != null)
                AudioSource.PlayClipAtPoint(clip, Camera.main.transform.position, volume);
        }
        else
        {
            AudioSource.PlayClipAtPoint(clip, transform.position, volume);
        }

        if (loop)
            StartCoroutine(WaitToPlay(clip.length + gap));
    }
}