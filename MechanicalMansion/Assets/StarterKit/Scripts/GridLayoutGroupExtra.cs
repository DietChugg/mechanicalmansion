using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[ExecuteInEditMode]
[RequireComponent(typeof(GridLayoutGroup))]
public class GridLayoutGroupExtra : MonoBehaviour 
{
	public bool bestFit;
	GridLayoutGroup gridLayoutGroup;
	RectTransform rectTransform;
	public float minWidth = 60f;
	public float minHeight = 30f;
	public float maxWidth = 120f;
	public float maxHeight = 60f;

	public Vector2 cellRatio;

	void OnEnable () 
	{
		gridLayoutGroup = GetComponent<GridLayoutGroup>();
		rectTransform = GetComponent<RectTransform>();
	}

	void Update () 
	{
		if(!bestFit)
			return;

		float area = rectTransform.rect.height * rectTransform.rect.width;
		int cellCount = transform.childCount;
		if(cellCount == 0)
			return;
		float areaPerCell = area/(float)cellCount;
		Vector2 cellSize = cellRatio * areaPerCell;
		gridLayoutGroup.cellSize = new Vector2(Mathf.Min(Mathf.Max(cellSize.x,minWidth),maxWidth),
		                                       Mathf.Min(Mathf.Max(cellSize.y,minHeight),maxHeight));
	}
}
