using UnityEngine;
using System.Collections;
using StarterKit;

public class Rotater : MonoBehaviourPlus 
{
	public Vector3 direction;
	public FloatClamp speed;
	public float acc;
    public Space space;

    new void OnEnable()
    {
    	(this as MonoBehaviourPlus).OnEnable();
    }

	new void OnDisable()
    {
    	(this as MonoBehaviourPlus).OnDisable();
    }

	void Update () 
	{
		Profiler.BeginSample("Rotate:Update");
		transform.Rotate(direction * speed.current * Time.deltaTime,space);
		Profiler.EndSample();
		speed.Adjust(acc * Time.deltaTime);
	}
}
