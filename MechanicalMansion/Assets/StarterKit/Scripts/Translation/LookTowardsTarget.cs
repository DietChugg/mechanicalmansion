using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using StarterKit;

public class LookTowardsTarget : MonoBehaviourPlus
{
    public Bool3 selectionAxis;
    public float speed;
    public Transform target;
    public bool autoAssignMainCamera;
    public bool snapTo;

    new void OnEnable()
    {
        base.OnEnable();
        if (target == null && autoAssignMainCamera)
        {
            target = Camera.main.transform;
        }
    }
    
    void Update() 
    {
        if (snapTo)
        {
            transform.LookAt(target);
            return;
        }

        if(selectionAxis.IsAllMarkedAs(true))
        {
            transform.SmoothLookAt(target.position,speed * Time.deltaTime);
            return;
        }
        if(selectionAxis.x)
        {
            transform.SmoothLookAt(target.position,speed * Time.deltaTime,Axis.X);
        }
        if(selectionAxis.y)
        {
            transform.SmoothLookAt(target.position,speed * Time.deltaTime,Axis.Y);
        }
        if(selectionAxis.z)
        {
            transform.SmoothLookAt(target.position,speed * Time.deltaTime,Axis.Z);
        }
    }
}
