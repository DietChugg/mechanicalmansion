using UnityEngine;
using System.Collections;

public class RotationConstraint : MonoBehaviour 
{
	public enum TargetRotationChannels{X,Y,Z};

	public bool constrainXRot;
	public TargetRotationChannels targetRotationChannelX;

	public bool constrainYRot;
	public TargetRotationChannels targetRotationChannelY;

	public bool constrainZRot;
	public TargetRotationChannels targetRotationChannelZ;

	public GameObject rotationTarget;

	public Vector3 rotationOffset;

	[Range (-2,2)]
	public float constraintValue;

	void Update () 
	{
		Vector3 newTargetRotation = new Vector3(rotationTarget.transform.rotation.eulerAngles.x + rotationOffset.x,
		                                        rotationTarget.transform.rotation.eulerAngles.y + rotationOffset.y,
		                                        rotationTarget.transform.rotation.eulerAngles.z + rotationOffset.z) * constraintValue;

		newTargetRotation = rotationTarget.transform.rotation.eulerAngles * constraintValue;

		if(constrainXRot)
		{
			switch(targetRotationChannelX)
			{
			case TargetRotationChannels.X:
				this.transform.SetXRotation(newTargetRotation.x);
				break;
			case TargetRotationChannels.Y:
				this.transform.SetXRotation(newTargetRotation.y);
				break;
			case TargetRotationChannels.Z:
				this.transform.SetXRotation(newTargetRotation.z);
				break;
			}
		}

		if(constrainYRot)
		{
			switch(targetRotationChannelY)
			{
			case TargetRotationChannels.X:
				this.transform.SetYRotation(newTargetRotation.x);
				break;
			case TargetRotationChannels.Y:
				this.transform.SetYRotation(newTargetRotation.y);
				break;
			case TargetRotationChannels.Z:
				this.transform.SetYRotation(newTargetRotation.z);
				break;
			}
		}

		if(constrainZRot)
		{
			switch(targetRotationChannelZ)
			{
			case TargetRotationChannels.X:
				this.transform.SetZRotation(newTargetRotation.x);
				break;
			case TargetRotationChannels.Y:
				this.transform.SetZRotation(newTargetRotation.y);
				break;
			case TargetRotationChannels.Z:
				this.transform.SetZRotation(newTargetRotation.z);
				break;
			}
		}
	}

}
