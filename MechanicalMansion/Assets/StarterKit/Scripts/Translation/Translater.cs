using UnityEngine;
using System.Collections;

public class Translater : MonoBehaviour 
{
	public Vector3 direction;
	public FloatClamp speed;
	public float acc;
    public Space space;
    public bool useRenderer;
    public Renderer renderer;

    void OnBecameVisible()
    {
        Debug.Log("OnBecameVisible: " + gameObject.name);
    }

	void Update () 
	{
        if (useRenderer && renderer != null && !renderer.isVisible)
            return;

		transform.Translate(direction * speed.current * Time.deltaTime, space);
		speed.Adjust(acc * Time.deltaTime);
	}
}
