using UnityEngine;
using System.Collections;

public class Scaler : MonoBehaviour 
{
	public Vector3 direction;
	public FloatClamp speed;
	public float acc;
	
	void Update () 
	{
		transform.localScale += (direction * speed.current);
		speed.Adjust(acc * Time.deltaTime);
	}
}
