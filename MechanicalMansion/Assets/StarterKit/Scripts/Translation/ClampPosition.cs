using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using StarterKit;

public class ClampPosition : MonoBehaviourPlus
{
    public UnityLifetime timing = UnityLifetime.Update;
    public Space space;
#if UNITY_EDITOR
    [Comment("Space is World Space Only Currently","More Soon Though")]
    public int comment0;
#endif
    public bool clampXMin;
    public float minClampX;
    public bool clampXMax;
    public float maxClampX;
    public bool clampYMin;
    public float minClampY;
    public bool clampYMax;
    public float maxClampY;
    public bool clampZMin;
    public float minClampZ;
    public bool clampZMax;
    public float maxClampZ;

	void Awake()
	{
        if (timing == UnityLifetime.Awake)
        {
            UpdatePositions();
        }
	}

    void Start()
    {
        if (timing == UnityLifetime.Start)
        {
            UpdatePositions();
        }
    }

	new void OnEnable() 
	{
		(this as MonoBehaviourPlus).OnEnable();
        if (timing == UnityLifetime.OnEnable)
        {
            UpdatePositions();
        }
	}
	
	new void OnDisable() 
	{
		(this as MonoBehaviourPlus).OnDisable();
        if (timing == UnityLifetime.OnDisable)
        {
            UpdatePositions();
        }
	}
	
	void Update() 
	{
        if (timing == UnityLifetime.Update)
        {
            UpdatePositions();
        }
	}

    void LateUpdate() 
    {
        if (timing == UnityLifetime.LateUpdate)
        {
            UpdatePositions();
        }
    }

    void FixedUpdate() 
    {
        if (timing == UnityLifetime.FixedUpdate)
        {
            UpdatePositions();
        }
    }

    void UpdatePositions()
    {
        if(position.x < minClampX && clampXMin)
            positionX = minClampX;
        if(position.x > maxClampX && clampXMax)
            positionX = maxClampX;
        if(position.y < minClampY && clampYMin)
            positionY = minClampY;
        if(position.y > maxClampY && clampYMax)
            positionY = maxClampY;
        if(position.z < minClampZ && clampZMin)
            positionZ = minClampZ;
        if(position.z > maxClampZ && clampZMax)
            positionZ = maxClampZ;
    }
	
	new void OnPause(bool isPaused)
	{
	
	}
}
