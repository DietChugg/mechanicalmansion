#if UNITY_5
using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

namespace StarterKit
{
	public class AudioExtra : MonoBehaviourPlus
	{
		public AudioMixer audioMixer;
		[HideInInspector]
		public bool isMusic;

		public new void OnEnable()
		{
			AudioSource audio = GetComponent<AudioSource>();
			Debug.Log(audio.outputAudioMixerGroup.name);
			audio.outputAudioMixerGroup.audioMixer.FindMatchingGroups("SFX");
	//		if(audio.outputAudioMixerGroup.audioMixer.FindMatchingGroups(.name+"Volume";
			audio.volume = isMusic ? Settings.currentOptions.musicVolume : Settings.currentOptions.sfxVolume;
		}
	}
}
#endif
