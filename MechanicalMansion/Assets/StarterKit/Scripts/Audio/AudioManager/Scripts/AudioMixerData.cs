#if UNITY_5
using UnityEngine;
using UnityEngine.Audio;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Audio mixer data.
/// To use All your AudioMixers must contain two audio groups - SFX group and Music group.
/// You must expose their volume settings as Exposed Parameters and they muse be named "MusicVolume"
/// and "SFXVolume".
/// </summary>
public class AudioMixerData : SingletonScriptableObject<AudioMixerData> 
{
	public float musicVolume = 1f;
	public float sfxVolume = 1f;
	public List<AudioMixer> audioMixers = new List<AudioMixer>();


	public void SaveAndUpdateAudioVolumeLevels()
	{
//		Debug.Log(music.ToString() + " And "+sfx);
		musicVolume = Settings.currentOptions.musicVolumePercent;
		sfxVolume = Settings.currentOptions.sfxVolumePercent;
		UpdateAudioVolumeLevels();
	}

	public void UpdateAudioVolumeLevels()
	{
		for (int i = 0; i < audioMixers.Count; i++) 
		{
			audioMixers[i].SetFloat("MusicVolume", Settings.currentOptions.musicVolume);
			audioMixers[i].SetFloat("SFXVolume", Settings.currentOptions.sfxVolume);
		}
	}

}
#endif
