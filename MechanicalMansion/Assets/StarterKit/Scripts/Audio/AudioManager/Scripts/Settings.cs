using UnityEngine;
using System.Collections;

public static class Settings 
{
	public static Options currentOptions = new Options();
	public static Options prevOptions = new Options();

	static Settings()
	{
		SetOptions(prevOptions);
		SetOptions(currentOptions);
		//AudioMixerData.Instance.SaveAndUpdateAudioVolumeLevels();
    }

	public static void SetOptions(Options options)
	{
		if(options == null)
			return;
		options.currentResolution = Screen.currentResolution;
		options.fullscreen = Screen.fullScreen;
		options.antiAliasing = QualitySettings.antiAliasing;
		options.vSyncCount = QualitySettings.vSyncCount;
		options.masterTextureLimit = QualitySettings.masterTextureLimit;
		//options.musicVolumePercent = AudioMixerData.Instance.musicVolume;
		//ptions.sfxVolumePercent = AudioMixerData.Instance.sfxVolume;
	}

	public static void Apply()
	{
		Screen.SetResolution(currentOptions.currentResolution.width,currentOptions.currentResolution.height,currentOptions.fullscreen);
		QualitySettings.antiAliasing = currentOptions.antiAliasing;
		QualitySettings.vSyncCount = currentOptions.vSyncCount;
		QualitySettings.masterTextureLimit = currentOptions.masterTextureLimit;
		//AudioMixerData.Instance.SaveAndUpdateAudioVolumeLevels();
		SetOptions(prevOptions);
	}
}

public class Options
{
	public Resolution currentResolution;
	public bool fullscreen;
	public int antiAliasing;
	public int vSyncCount;
	public int masterTextureLimit;
	static float _musicVolume = 1f;
	public float musicVolume
	{
		get
		{
			return _musicVolume;
		}
		
		set
		{
			_musicVolume = Mathf.Clamp(value,-80,20);
			_musicVolumePercent = (_sfxVolume + 80)/100;
		}
	}

	static float _musicVolumePercent = 1f;
	public float musicVolumePercent
	{
		get
		{
			return _musicVolumePercent;
		}
		
		set
		{
			_musicVolumePercent = Mathf.Clamp01(value);
			_musicVolume = (_musicVolumePercent * 100)-80;
		}
	}
	
	static float _sfxVolume = 1f;
	public float sfxVolume
	{
		get
		{
			return _sfxVolume;
		}
		
		set
		{
			_sfxVolume = Mathf.Clamp(value,-80,20);
			_sfxVolumePercent = (_sfxVolume + 80)/100;
		}
	}

	static float _sfxVolumePercent = 1f;
	public float sfxVolumePercent
	{
		get
		{
			return _sfxVolumePercent;
		}
		
		set
		{
			_sfxVolumePercent = Mathf.Clamp01(value);
			_sfxVolume = (_sfxVolumePercent * 100)-80;
		}
	}

	public override bool Equals(System.Object other)
	{
		Options otherOptions = (other as Options);
		if(this.currentResolution.width != otherOptions.currentResolution.width ||
		   this.currentResolution.height != otherOptions.currentResolution.height ||
		   this.fullscreen != otherOptions.fullscreen ||
		   this.antiAliasing != otherOptions.antiAliasing ||
		   this.vSyncCount != otherOptions.vSyncCount ||
		   this.masterTextureLimit != otherOptions.masterTextureLimit ||
		   this.musicVolume != otherOptions.musicVolume ||
		   this.sfxVolume != otherOptions.sfxVolume)
			return false;
		return true;
    }

    public override int GetHashCode()
    {
        return base.GetHashCode();
    }
    
}
