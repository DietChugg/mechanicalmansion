using UnityEngine;
using System.Collections;

public class AudioSettings : Singleton<AudioSettings> 
{
	public float soundVolume = 0.5f;
	public float ambientVolume = 0.5f;
	public float musicVolume = 0.5f;
}
