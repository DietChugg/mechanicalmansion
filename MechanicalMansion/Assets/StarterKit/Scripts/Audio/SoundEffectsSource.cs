//© Waterford Institute 2014
#if UNITY_5
using UnityEngine;
using UnityEngine.Audio;
using System.Collections;
using System.Collections.Generic;

public class SoundEffectsSource : SingletonScriptableObject<SoundEffectsSource>
{
	public AudioMixer audioMixer;
    public SDictionaryStringGameObject sources = new SDictionaryStringGameObject();
}
#endif
