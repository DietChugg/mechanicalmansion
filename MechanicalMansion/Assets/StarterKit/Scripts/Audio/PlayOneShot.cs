using UnityEngine;
using System.Collections;

public class PlayOneShot : MonoBehaviour
{
    public AudioClip clip;
    public float volume = 1f;
    public bool playAtMainCam;
    public float waitToPlay;
    public bool playOnEnable = true;

    void OnEnable()
    {
        if (playOnEnable)
            StartCoroutine(WaitToPlay(waitToPlay));
    }

    public void Play()
    {
        StartCoroutine(WaitToPlay(0));
    }

    IEnumerator WaitToPlay(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);

        if (playAtMainCam)
        {
            if (Camera.main != null)
                AudioSource.PlayClipAtPoint(clip, Camera.main.transform.position, volume);
        }
        else
        {
            AudioSource.PlayClipAtPoint(clip, transform.position, volume);
        }
    }
}