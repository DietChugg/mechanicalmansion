using UnityEngine;
using System.Collections;

public class ParticleAudio : MonoBehaviour 
{
	public Vector2 volumeRange = new Vector2(1f,1f);
	public AudioClip onBirth;
	public AudioClip onDeath;

	private int numberOfParticles;

	void OnEnable()
	{
		numberOfParticles = 0;
	}

	void Update () 
	{
		if(!onBirth && !onDeath)
			return;

		int count = GetComponent<ParticleSystem>().particleCount;

		if(count < numberOfParticles)
		{
			if(onDeath)
				AudioSource.PlayClipAtPoint(onDeath,transform.position, Random.Range(volumeRange.x, volumeRange.y));
		}
		else if(count > numberOfParticles)
		{
			if(onBirth)
				AudioSource.PlayClipAtPoint(onBirth,transform.position, Random.Range(volumeRange.x, volumeRange.y));
		}

		numberOfParticles = count;
	}
}
