//© Waterford Institute 2014

using UnityEngine;
using System;
using System.Collections;

public class SoundEffectsManager : Singleton<SoundEffectsManager>
{
	public static Action<GameObject, GameObject> SoundEffectFinished;

	public GameObject PlaySoundEffect(GameObject soundEffect, GameObject sender, bool attachToMainCamera)
	{
		GameObject soundEffectInstance = PoolManager.Request(soundEffect);

		StartCoroutine(WaitForAudioToFinish(soundEffectInstance, sender));

		return soundEffectInstance;
	}

	public GameObject PlaySoundEffect(GameObject soundEffect, GameObject sender)
	{	
		GameObject soundEffectInstance = PoolManager.Request(soundEffect);
		
		StartCoroutine(WaitForAudioToFinish(soundEffectInstance, sender));
		
		return soundEffectInstance;
	}

	IEnumerator WaitForAudioToFinish(GameObject soundEffectInstance, GameObject sender, bool enableCollider2D)
	{
		soundEffectInstance.GetComponent<AudioSource>().Play();
		while(soundEffectInstance.GetComponent<AudioSource>().isPlaying)
		{
			yield return null;
		}
		soundEffectInstance.gameObject.SetActive(false);

		if(enableCollider2D)
			sender.GetComponent<Collider2D>().enabled = true;

		if(SoundEffectFinished != null)
			SoundEffectFinished(soundEffectInstance, sender);
	}

	IEnumerator WaitForAudioToFinish(GameObject soundEffectInstance, GameObject sender)
	{
		soundEffectInstance.GetComponent<AudioSource>().Play();
		while(soundEffectInstance.GetComponent<AudioSource>().isPlaying)
		{
			yield return null;
		}
		soundEffectInstance.gameObject.SetActive(false);
		if(SoundEffectFinished != null)
			SoundEffectFinished(soundEffectInstance, sender);
	}


}
