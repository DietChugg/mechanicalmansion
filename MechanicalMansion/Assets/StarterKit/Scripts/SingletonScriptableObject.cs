using UnityEngine;
using System.Collections;
using System.ComponentModel;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class SingletonScriptableObject<T> : ScriptableObject where T : ScriptableObject
{
	protected static T instance;
	public static T Instance
	{
        get
        {
            if(instance == null)
            {
                System.Type myType = typeof(T);
				UnityEngine.Object o = Resources.Load(myType.FullName,typeof(T));
                if(o != null)
                {
                    instance = (T)o;
                }
                else
                {
                    Debug.Log("Creating ScritpableObject Asset: " + myType.ToString().RTGreen());
                    instance = CreateAsset();
                }
            }
            return instance;
        }
	}

    public virtual void Init()
    {

    }


    public static T CreateAsset()
    {
        
        instance = CreateInstance<T>();//new InputPlus();
        #if UNITY_EDITOR
		System.Type myType = typeof(T);
//		MonoScript script = MonoScript.((myType as ScriptableObject));
//		Debug.Log(AssetDatabase.GetAssetPath(script));
         AssetDatabase.CreateAsset(instance, "Assets/"+myType.FullName+".asset");
		MonoScript script = MonoScript.FromScriptableObject(instance as ScriptableObject);


		//Build Proper Path


		string scriptPath = AssetDatabase.GetAssetPath(script);
		string resourcesFolderPath = scriptPath.Substring(0,scriptPath.Length - ("/" + myType.FullName + ".cs").Length);
//		Debug.Log(resourcesFolderPath);

#if UNITY_5
		if(!AssetDatabase.IsValidFolder(resourcesFolderPath + "/Resources"))
		{
			AssetDatabase.CreateFolder(resourcesFolderPath ,"Resources");
		}
#else
		if(!System.IO.Directory.Exists(resourcesFolderPath + "/Resources"))
		{
			AssetDatabase.CreateFolder(resourcesFolderPath ,"Resources");
		}

#endif
		AssetDatabase.MoveAsset("Assets/" + myType.FullName + ".asset",
		                        resourcesFolderPath + "/Resources/" + myType.FullName + ".asset");
        AssetDatabase.SaveAssets();
		AssetDatabase.Refresh();
        #endif
//        Debug.Log("There is a Scriptable Object in your Resources folder called "+ myType.FullName);
        return instance;
    }



	public static T CreateAssetWithName(string name)
	{
		
		instance = CreateInstance<T>();//new InputPlus();
		#if UNITY_EDITOR
		System.Type myType = typeof(T);
		//		MonoScript script = MonoScript.((myType as ScriptableObject));
		//		Debug.Log(AssetDatabase.GetAssetPath(script));
		AssetDatabase.CreateAsset(instance, "Assets/"+name+".asset");
		MonoScript script = MonoScript.FromScriptableObject(instance as ScriptableObject);
		
		
		//Build Proper Path
		
		
		string scriptPath = AssetDatabase.GetAssetPath(script);
		string resourcesFolderPath = scriptPath.Substring(0,scriptPath.Length - ("/" + myType.FullName + ".cs").Length);
		//		Debug.Log(resourcesFolderPath);
		
		#if UNITY_5
		if(!AssetDatabase.IsValidFolder(resourcesFolderPath + "/Resources"))
		{
			AssetDatabase.CreateFolder(resourcesFolderPath ,"Resources");
		}
		#else
		if(!System.IO.Directory.Exists(resourcesFolderPath + "/Resources"))
		{
			AssetDatabase.CreateFolder(resourcesFolderPath ,"Resources");
		}
		
		#endif
		AssetDatabase.MoveAsset("Assets/" + name + ".asset",
		                        resourcesFolderPath + "/Resources/" + name + ".asset");
		AssetDatabase.SaveAssets();
		AssetDatabase.Refresh();
		#endif
		//        Debug.Log("There is a Scriptable Object in your Resources folder called "+ myType.FullName);
		return instance;
	}
}


