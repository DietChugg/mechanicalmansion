
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System.Collections;

/**************
 * HOW TO USE *
 **************/
//Place this code where you declare the variables in your class:
//
//#if UNITY_EDITOR
//[Comment("This is the description","And this is the tooltip")]
//public string dummyString;
//#endif


public class CommentAttribute : PropertyAttribute 
{
	public readonly string tooltip;
	public readonly string comment;
	
	public CommentAttribute( string comment, string tooltip ) 
	{
		this.tooltip = tooltip;
		this.comment = comment;
	}
}
#if UNITY_EDITOR

[CustomPropertyDrawer(typeof(CommentAttribute))]
public class CommentDrawer : PropertyDrawer 
{
	const int textHeight = 20;
	
	CommentAttribute commentAttribute { get { return (CommentAttribute)attribute; } }
	
	public override float GetPropertyHeight(SerializedProperty prop, GUIContent label) 
	{
		return textHeight;
	}
	
	public override void OnGUI(Rect position, SerializedProperty prop, GUIContent label) 
	{
		EditorGUI.LabelField(position,new GUIContent(commentAttribute.comment,commentAttribute.tooltip));
	}
}

#endif
