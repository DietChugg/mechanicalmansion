using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Dropper : MonoBehaviour 
{
	public bool dropmodeActive;
	public float sensitivity = 30f;
	public bool loops = false;
	public int index;
	public List<Transform> prefabs;
	public Transform toDrop;
	public Vector3 camPosition;
	public float moveSpeed;
	public bool snapToGrid;
	public Vector3 rotationOffset;
	
	void Start () 
	{
	
	}
	
	void Update () 
	{
	
	}
	
}
