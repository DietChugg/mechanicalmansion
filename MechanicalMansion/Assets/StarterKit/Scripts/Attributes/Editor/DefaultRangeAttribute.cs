﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using StarterKit;

public class DefaultRangeAttribute : PropertyAttribute
{
    public readonly float minFloat = -Mathf.Infinity;
    public readonly float maxFloat = Mathf.Infinity;
    public readonly bool useBoth = false;
    public readonly bool isMin = false;

    public DefaultRangeAttribute(float minFloat, float maxFloat)
    {
        this.minFloat = minFloat;
        this.maxFloat = maxFloat;
        this.useBoth = true;
    }

    public DefaultRangeAttribute(bool isMin, float value)
    {
        if (isMin)
            this.minFloat = value;
        else
            this.maxFloat = value;
        this.useBoth = false;
        this.isMin = isMin;
    }
}

[CustomPropertyDrawer(typeof(DefaultRangeAttribute))]
public class DefaultRangeDrawer : PropertyDrawer
{
    private DefaultRangeAttribute _defaultRangeAttribute = null;
    private DefaultRangeAttribute defaultRangeAttribute
    {
        get
        {
            if (_defaultRangeAttribute == null)
            {
                _defaultRangeAttribute = (DefaultRangeAttribute)attribute;
            }
            return _defaultRangeAttribute;
        }
    }


    public override float GetPropertyHeight(SerializedProperty property,
                                            GUIContent label)
    {
        return EditorGUI.GetPropertyHeight(property, label, true);
    }

    public override void OnGUI(Rect position,
                               SerializedProperty property,
                               GUIContent label)
    {
        //GUI.enabled = false;
        EditorGUI.PropertyField(position, property, label, true);
        if (property.propertyType == SerializedPropertyType.Float)
        {
            property.floatValue = property.floatValue.Clamp(defaultRangeAttribute.minFloat, defaultRangeAttribute.maxFloat);
        }
        if (property.propertyType == SerializedPropertyType.Integer)
        {
            if (defaultRangeAttribute.useBoth)
            {
                int maxValue = (int)defaultRangeAttribute.maxFloat;
                int minValue = (int)defaultRangeAttribute.minFloat;

                property.intValue = (int)property.intValue.Clamp(minValue, maxValue);
            }
            else 
            {
                if (defaultRangeAttribute.isMin)
                {
                    if (property.intValue < (int)defaultRangeAttribute.minFloat)
                        property.intValue = (int)defaultRangeAttribute.minFloat;
                }
                else
                {
                    if (property.intValue > (int)defaultRangeAttribute.maxFloat)
                        property.intValue = (int)defaultRangeAttribute.maxFloat;
                }
            }
        }
        
        //GUI.enabled = true;
    }
}