﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using StarterKit;

public class ToggleButtonAttribute : PropertyAttribute
{

}


[CustomPropertyDrawer(typeof(ToggleButtonAttribute))]
public class ToggleDrawer : PropertyDrawer
{
    float curPercent;
    float targetPercent;


    private ToggleButtonAttribute _toggleAttribute = null;
    private ToggleButtonAttribute toggleAttribute
    {
        get
        {
            if (_toggleAttribute == null)
            {
                _toggleAttribute = (ToggleButtonAttribute)attribute;
            }
            return _toggleAttribute;
        }
    }

    public override float GetPropertyHeight(SerializedProperty property,
                                            GUIContent label)
    {
        return EditorGUI.GetPropertyHeight(property, label, true) + 12f;
    }

    public override void OnGUI(Rect position,
                               SerializedProperty property,
                               GUIContent label)
    {
        if (property.propertyType == SerializedPropertyType.Boolean)
        {
            //if (GUI.Button(position, property.name + " = " + property.boolValue))
            //    property.boolValue = !property.boolValue;


            GUIStyle onStyle = (GUIStyle)"flow node 0 on";//(GUIStyle)"ProgressBarBack";
            GUIStyle offStyle = (GUIStyle)"ProgressBarBack";
            GUIStyle selected = property.boolValue ? onStyle : offStyle;

            GUIContent guiContent = new GUIContent();
            guiContent.text = property.name;

            bool wasPressed =  GUI.Button(position, "", (GUIStyle)"ShurikenEffectBg");

            if (!property.boolValue)
            {
                targetPercent = 0f;
                curPercent = targetPercent;
                guiContent.image = (Texture)EditorGUIUtility.FindTexture("toggle");
                if (GUI.Button(new Rect(position.x + curPercent * position.width, position.y, position.width * .5f, position.height), guiContent) || wasPressed)
                    property.boolValue = !property.boolValue;
                
            }
            else
            {
                targetPercent = .5f;
                curPercent = targetPercent;
                guiContent.image = (Texture)EditorGUIUtility.FindTexture("toggle on");
                if (GUI.Button(new Rect(position.x + curPercent * position.width, position.y, position.width * .5f, position.height), guiContent, selected) || wasPressed)
                    property.boolValue = !property.boolValue;
                //property.boolValue = GUI.Toggle(new Rect(position.x, position.y, position.width * .5f, position.height), property.boolValue,guiContent);
                
            }
            curPercent = curPercent.MoveTowards(targetPercent, .2f);
        }
        else
        {
            GUI.Label(position, "Invalid Target. Attribute should only be on Bool");

        }
    }
}