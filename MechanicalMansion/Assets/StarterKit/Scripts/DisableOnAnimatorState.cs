using UnityEngine;
using System.Collections;
using StarterKit;

public class DisableOnAnimatorState : MonoBehaviourPlus 
{
	public string layerDotStateName = "LayerName.StateName";
	public int layerNumber = 0;
	public bool destroyParent;
	
	void Update () 
	{
		AnimatorStateInfo asi = animator.GetCurrentAnimatorStateInfo(layerNumber);
//		Debug.Log(asi.nameHash);
//		Debug.Log(Animator.StringToHash(layerDotStateName).ToString().RTOlive());
		if(asi.nameHash == Animator.StringToHash(layerDotStateName))
		{
			if(destroyParent)
				Destroy(transform.parent.gameObject);
			else
				Destroy(gameObject);
		}
	}
}
