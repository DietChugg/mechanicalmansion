using UnityEngine;
using System.Collections;

namespace StarterKit
{
public class RandomAnimationSpeed : MonoBehaviour 
{
	public Vector2 range;
	public new Animation animation;

	void OnEnable () 
	{
		if(animation == null)
			animation = GetComponent<Animation>();

		SetRandom(range);
	}
	
	public void SetRandom (Vector2 range) 
	{
		animation[animation.clip.name].speed = Random.Range(range.x, range.y);
	}

	public void SetSpecific(float speed) 
	{
		animation[animation.clip.name].speed = speed;
	}
}
}
