﻿using UnityEngine;
using System.Collections;

public class DownloadPrefab : MonoBehaviour
{
    public string prefabName;
    public string packageName;
    public bool autoCreatePrefab = false;

	void Start () 
    {
        AssetBundleManager.Instance.LoadPrefab(packageName, prefabName, Blank);
	}

    void Blank(GameObject blank)
    { 
    
    }

	void OnDownloadComplete (GameObject prefab) 
    {
        if (autoCreatePrefab)
            GameObject.Instantiate(prefab);
	}
}