﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using StarterKit;
using System.IO;

public static class AssetBundleBuilder 
{
	[MenuItem("Assets/Create/AssetBundles",false,11)]
    public static void CreateAssetBundles()
    {
        BuildBundle("Standalone", BuildTarget.StandaloneWindows);
        BuildBundle("iOS", BuildTarget.iOS);
        BuildBundle("Android", BuildTarget.Android);
        BuildBundle("WebGL", BuildTarget.WebGL);
	}

    public static void CreateIOSBundles()
    {
        BuildBundle("iOS", BuildTarget.iOS);
    }

    public static void CreateAndroidBundles()
    {
        BuildBundle("Android", BuildTarget.Android);
    }

    public static void CreateWebGLBundles()
    {
        BuildBundle("WebGL", BuildTarget.WebGL);
    }

    public static void CreateStandaloneBundles()
    {
        BuildBundle("Standalone", BuildTarget.StandaloneWindows);
    }

    public static void BuildBundle(string platformName, BuildTarget buildTarget)
    {
        string outputPath = "AssetBundles";
        string fullOutputPath = Application.dataPath.InsetFromEnd(7) + "/AssetBundles";
        Debug.Log(fullOutputPath);
        if (!Directory.Exists(fullOutputPath))
            Directory.CreateDirectory(fullOutputPath);

        if (!Directory.Exists(fullOutputPath + "/" + platformName))
            Directory.CreateDirectory(fullOutputPath + "/" + platformName);

        BuildPipeline.BuildAssetBundles(outputPath + "/" + platformName, BuildAssetBundleOptions.None, buildTarget);
    }


}
