﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(AssetBundleManager))]
[CanEditMultipleObjects]
public class AssetBundleManager_Editor : Editor 
{
    SerializedProperty standalone, ios, android, webGL, assetBundlePlatforms, index;
    AssetBundleManager manager;

    public void OnEnable()
    {
        standalone = serializedObject.FindProperty("standalone");
        ios = serializedObject.FindProperty("ios");
        android = serializedObject.FindProperty("android");
        webGL = serializedObject.FindProperty("webGL");
        assetBundlePlatforms = serializedObject.FindProperty("assetBundlePlatforms");
        index = serializedObject.FindProperty("index");

        BuildDefaultSettings();

        BuildTarget buildTarget = EditorUserBuildSettings.activeBuildTarget;
        switch (buildTarget)
        {
            case BuildTarget.Android:
             break;
            case BuildTarget.SamsungTV:
             break;
            case BuildTarget.StandaloneGLESEmu:
             break;
            case BuildTarget.StandaloneLinux:
             break;
            case BuildTarget.StandaloneLinux64:
             break;
            case BuildTarget.StandaloneLinuxUniversal:
             break;
            case BuildTarget.StandaloneOSXIntel:
             break;
            case BuildTarget.StandaloneOSXIntel64:
             break;
            case BuildTarget.StandaloneOSXUniversal:
             break;
            case BuildTarget.StandaloneWindows:
             break;
            case BuildTarget.StandaloneWindows64:
             break;
            case BuildTarget.WebGL:
             break;
            case BuildTarget.iOS:
             break;
        }
    }

    public void BuildDefaultSettings()
    {
        manager = (AssetBundleManager)target;
        if (manager.assetBundlePlatforms.Count == 0)
        {
            manager.assetBundlePlatforms.Add(new AssetBundleManager.AssetBundlePlatformData() { name = "Standalone" });
            manager.assetBundlePlatforms.Add(new AssetBundleManager.AssetBundlePlatformData() { name = "IOS" });
            manager.assetBundlePlatforms.Add(new AssetBundleManager.AssetBundlePlatformData() { name = "Android" });
            manager.assetBundlePlatforms.Add(new AssetBundleManager.AssetBundlePlatformData() { name = "WebGL" });
        }
    }

    public override void OnInspectorGUI()
    {
        
        
        BuildDefaultSettings();
        GUIStyle style = (GUIStyle)"toolbarbutton";
        float labelWidth = (EditorGUIUtility.currentViewWidth - 20f)/4;
        EditorGUI.BeginChangeCheck();
        {
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Toggle(standalone.boolValue, "Standalone", style) 
                && !standalone.boolValue)
            {
                standalone.boolValue = true;
                ios.boolValue = false;
                android.boolValue = false;
                webGL.boolValue = false;
                index.intValue = 0;
            }
            if (GUILayout.Toggle(ios.boolValue, "IOS", style) 
                && !ios.boolValue)
            {
                standalone.boolValue = false;
                ios.boolValue = true;
                android.boolValue = false;
                webGL.boolValue = false;
                index.intValue = 1;
            }
            if (GUILayout.Toggle(android.boolValue, "Android", style)
                && !android.boolValue)
            {
                standalone.boolValue = false;
                ios.boolValue = false;
                android.boolValue = true;
                webGL.boolValue = false;
                index.intValue = 2;
            }
            if (GUILayout.Toggle(webGL.boolValue, "WEBGL", style)
                && !webGL.boolValue)
            {
                standalone.boolValue = false;
                ios.boolValue = false;
                android.boolValue = false;
                webGL.boolValue = true;
                index.intValue = 3;
            }
            EditorGUILayout.EndHorizontal();

            

            //SerializedProperty name = assetBundlePlatforms.GetArrayElementAtIndex(index.intValue).FindPropertyRelative("name");

            //name.stringValue = EditorGUILayout.TextField("Manifest Name", name.stringValue);
            //EditorGUILayout.TextField("Manifest Path", "Test");

        EditorGUILayout.PropertyField(assetBundlePlatforms.GetArrayElementAtIndex(index.intValue), true);
        
        GUILayout.Space(20f);

        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        if (standalone.boolValue && GUILayout.Button("Build Standalone Asset Bundles",GUILayout.Width(230),GUILayout.Height(50)))
            AssetBundleBuilder.CreateStandaloneBundles();
        if (ios.boolValue && GUILayout.Button("Build iOS Asset Bundles",GUILayout.Width(230),GUILayout.Height(50)))
            AssetBundleBuilder.CreateIOSBundles();
        if (android.boolValue && GUILayout.Button("Build Android Asset Bundles",GUILayout.Width(230),GUILayout.Height(50)))
            AssetBundleBuilder.CreateAndroidBundles();
        if (webGL.boolValue && GUILayout.Button("Build WebGL Asset Bundles",GUILayout.Width(230),GUILayout.Height(50)))
            AssetBundleBuilder.CreateWebGLBundles();
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();
        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        if (GUILayout.Button("Build All Asset Bundles", GUILayout.Width(230), GUILayout.Height(25)))
            AssetBundleBuilder.CreateAssetBundles();
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();
        
        }
        //{
        //    for (int i = 0; i < assetBundlePlatforms.CountInProperty(); i++)
        //    {
        //        assetBundlePlatforms.GetArrayElementAtIndex(i);
        //    }
        //}

        if (EditorGUI.EndChangeCheck())
        {
            serializedObject.ApplyModifiedProperties();
        }

        
        

        //DrawDefaultInspector();
    }
}
