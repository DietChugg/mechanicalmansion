using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System;
using System.Collections;
using System.Collections.Generic;
using StarterKit;
using UnityEngine.EventSystems;

public class AssetBundleManager : SingletonPrefab<AssetBundleManager> 
{
    public bool standalone = true;
    public bool ios = false;
    public bool android = false;
    public bool webGL = false;
    public int index = 0;

    [Serializable]
    public class AssetBundleData
    {
        public string name;
        public string path;
    }

    [Serializable]
    public class AssetBundlePlatformData
    {
        //[HideInInspector]
        public string name;
        public string manifestPath;
        //[HideInInspector]
        public List<RuntimePlatform> platforms = new List<RuntimePlatform>();
        public List<AssetBundleData> assetBundleDatas = new List<AssetBundleData>();
    }

    /// <summary>
    /// TODO: Make This a List For Target Platforms...
    /// </summary>
    public List<AssetBundlePlatformData> assetBundlePlatforms = new List<AssetBundlePlatformData>();
    AssetBundleManifest manifest;
    

#if UNITY_EDITOR
	[MenuItem ("Edit/Project Settings/AssetBundles")]
	static void DrawHierarchyDataMenuItem () 
	{
		AssetBundleManager.Instance.SetUpPrefab();
		Selection.activeObject = (UnityEngine.Object)AssetBundleManager.Instance;
	}
#endif

    
    public void LoadSceneFromAssetBundle(string assetBundleName, string sceneName)
    {
        CoroutineManager.Instance.StartCoroutine(LoadSceneFromAssetBundleRoutine(assetBundleName, sceneName));
    }

    public AssetBundlePlatformData GetCurrentPlatform()
    {
        for (int i = 0; i < assetBundlePlatforms.Count; i++)
        {
            for (int j = 0; j < assetBundlePlatforms[i].platforms.Count; j++)
            {
                if (Application.platform == assetBundlePlatforms[i].platforms[j])
                    return assetBundlePlatforms[i];    
            }
            
        }
        return null;
    }

    public AssetBundleData FindAssetBundleData(string name)
    {
        AssetBundlePlatformData assetBundlePlatformData = GetCurrentPlatform();
        AssetBundleData[] assetBundleDatas = assetBundlePlatformData.assetBundleDatas.ToArray();
        for (int i = 0; i < assetBundleDatas.Length; i++)
        {
            if (assetBundleDatas[i].name.ToUpper() == name.ToUpper())
                return assetBundleDatas[i];
        }
        return null;
    }

    public string GetPath(string assetBundleName, bool fullPath)
    {
        AssetBundleData assetBundleData = FindAssetBundleData(assetBundleName);
        if (fullPath)
            return assetBundleData.path;
        return assetBundleData.path.InsetFromEnd(assetBundleName.Length);
    }

    public void LoadPrefab(string assetBundleName, string prefabName, Action<GameObject> OnDownloadComplete)
    {
        CoroutineManager.Instance.StartCoroutine(LoadPrefabRoutine(assetBundleName, prefabName, OnDownloadComplete));
    }

    IEnumerator LoadPrefabRoutine(string assetBundleName, string prefabName, Action<GameObject> OnDownloadComplete)
    {
        if (manifest == null)
            yield return CoroutineManager.Instance.StartCoroutine(ManifestSetup());
        Debug.Log("LoadPrefabRoutine for Prefab load complete");

        string[] dependentAssetBundles = manifest.GetAllDependencies(assetBundleName);
        Debug.Log(" Downloading Required Asset Bundles");
        AssetBundle[] assetBundles = new AssetBundle[dependentAssetBundles.Length];
        for (int i = 0; i < dependentAssetBundles.Length; i++)
        {
            Hash128 hash = manifest.GetAssetBundleHash(dependentAssetBundles[i]);
            AssetBundleData assetBundleData = FindAssetBundleData(dependentAssetBundles[i]);
            WWW www = WWW.LoadFromCacheOrDownload(assetBundleData.path, hash);
            yield return www;
            assetBundles[i] = www.assetBundle;
        }
        string assetBundleFolderPath = GetPath(assetBundleName, false);
        Debug.Log("prefab WWW Searching" + assetBundleFolderPath + "GAP" +assetBundleName);
        WWW wwwPrefab = WWW.LoadFromCacheOrDownload(assetBundleFolderPath + assetBundleName, manifest.GetAssetBundleHash(assetBundleName));
        if (wwwPrefab == null)
            Debug.Log("Not found My Poor Prefab");
        yield return wwwPrefab;
        AssetBundle prefabBundle = wwwPrefab.assetBundle;

        string[] assetNames = prefabBundle.GetAllAssetNames();

        Debug.Log(prefabName);
        GameObject fab = null;
        for (int i = 0; i < assetNames.Length; i++)
        {
            Debug.Log(assetNames[i].RTOlive());

            string[] split = assetNames[i].Split(new char[] { '/' });
            if (split.GetLast().InsetFromEnd(7).ToUpper() == prefabName.ToUpper())
            {
                Debug.Log("Found a Fab...");
                fab = prefabBundle.LoadAsset<GameObject>(assetNames[i].ToLower());
            }
        }
        OnDownloadComplete(fab);
        //ExecuteEvents.Execute<IAssetBundlePrefab>(gameObject,null,null);
    }

    /// <summary>
    /// Not Currently Used... Cause it don't work
    /// </summary>
    /// <returns></returns>
    Dictionary<string, string> GetResponseHeaders()
    {
        Dictionary<string, string> responseHeaders = new Dictionary<string, string>();
        responseHeaders.Add("Access-Control-Allow-Credentials", "true");
        responseHeaders.Add("Access-Control-Allow-Headers", "Accept, X-Access-Token, X-Application-Name, X-Request-Sent-Time");
        responseHeaders.Add("Access-Control-Allow-Methods", "GET, POST, OPTIONS");
        responseHeaders.Add("Access-Control-Allow-Origin", "*");
        return responseHeaders;
    }

    IEnumerator ManifestSetup()
    {
        Debug.Log("Manifest Setup");
        AssetBundlePlatformData currentPlatform = GetCurrentPlatform();
        Debug.Log("Grabbing manifest At: " + currentPlatform.manifestPath);
        Debug.Log("Manifest Name: " + currentPlatform.name);
        //Debug.Log("Manifest Escape URL: " + WWW.UnEscapeURL(currentPlatform.manifestPath));
        WWW wwwManifest = new WWW(currentPlatform.manifestPath);

        if (wwwManifest == null)
            Debug.Log("Failed to Start Download Properly");

        //wwwManifest.responseHeaders = GetResponseHeaders();
        yield return wwwManifest;
        Debug.Log("Yielding Complete");

        if (wwwManifest == null)
            Debug.Log("wwwManifest does not exist");

        if (wwwManifest.error.IsNotNullOrEmpty())
        {
            Debug.LogError("Log: " + wwwManifest.error);
        }
        else
        {
            Debug.Log("Log: " + "No Error Found");
        }
        AssetBundle manifestBundle = wwwManifest.assetBundle;
        if (manifestBundle == null)
        {
            Debug.LogError("Log: " + "No manifestBundle found");
        }
        else
        {
            Debug.Log("Log: " + "Manifest Found");
        }
        Debug.Log("Loading AssetBundleManifest");
        manifest = manifestBundle.LoadAsset("AssetBundleManifest") as AssetBundleManifest;
        Debug.Log("Unload manifestBundle");
        manifestBundle.Unload(false);
        Debug.Log("Unload completed");

    }

    //void DependanciesSetup(string assetBundleName)
    //{
    //    string[] dependentAssetBundles = manifest.GetAllDependencies(assetBundleName);
    //    AssetBundle[] assetBundles = new AssetBundle[dependentAssetBundles.Length];
    //    for (int i = 0; i < dependentAssetBundles.Length; i++)
    //    {
    //        Hash128 hash = manifest.GetAssetBundleHash(dependentAssetBundles[i]);
    //        AssetBundleData assetBundleData = FindAssetBundleData(dependentAssetBundles[i]);
    //        WWW www = WWW.LoadFromCacheOrDownload(assetBundleData.path, hash);
    //        yield return www;
    //        assetBundles[i] = www.assetBundle;
    //    }
    //}

    IEnumerator LoadSceneFromAssetBundleRoutine(string assetBundleName, string sceneName)
    {
        if (manifest == null)
            yield return CoroutineManager.Instance.StartCoroutine(ManifestSetup());
        Debug.Log("ManifestSetup yield Complete");
        if (manifest == null)
            Debug.Log("Manifest never found...");

        string[] dependentAssetBundles = manifest.GetAllDependencies(assetBundleName);
        AssetBundle[] assetBundles = new AssetBundle[dependentAssetBundles.Length];
        for (int i = 0; i < dependentAssetBundles.Length; i++)
        {
            Debug.Log("dependentAssetBundles at " + i + ": " + dependentAssetBundles[i]);
            Hash128 hash = manifest.GetAssetBundleHash(dependentAssetBundles[i]);
            AssetBundleData assetBundleData = FindAssetBundleData(dependentAssetBundles[i]);
            WWW www = WWW.LoadFromCacheOrDownload(assetBundleData.path, hash);
            yield return www;
            assetBundles[i] = www.assetBundle;
        }

        string assetBundleFolderPath = GetPath(assetBundleName, false);

        WWW wwwScene = WWW.LoadFromCacheOrDownload(assetBundleFolderPath + assetBundleName, manifest.GetAssetBundleHash(assetBundleName));
        yield return wwwScene;
        AssetBundle sceneBundle = wwwScene.assetBundle;

        string[] scenes = sceneBundle.GetAllScenePaths();
        for (int i = 0; i < scenes.Length; i++)
        {
            string[] split = scenes[i].Split(new char[]{'/'});
            if (split.GetLast().InsetFromEnd(6) == sceneName)
            {
                Debug.Log("Wait A Second...");
                yield return new WaitForSeconds(5f);
                Application.LoadLevel(sceneName);
            }
        }

        Debug.Log("Loading Scene From Asset Bundle Complete: " + assetBundleName);

        //for (int i = 0; i < assetBundles.Length; i++)
        //{
        //    assetBundles[i].Unload(false);
        //}
        //sceneBundle.Unload(false);
    }
        
}
