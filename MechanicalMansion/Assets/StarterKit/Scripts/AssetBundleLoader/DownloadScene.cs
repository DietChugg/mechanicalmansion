﻿using UnityEngine;
using System.Collections;
using StarterKit;

public class DownloadScene : MonoBehaviour 
{
    public string sceneToLoad = "PreciousTextureScene";
    public string assetBundleName = "precioustexturescene";
    //public string assetBundlePath = "";
    //public string manifestPath = "";

    void OnEnable()
    {
        AssetBundleManager.Instance.LoadSceneFromAssetBundle(assetBundleName, sceneToLoad);
    }
}
