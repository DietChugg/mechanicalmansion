using UnityEngine;
using System.Collections;

public class AnimateTranslation : MonoBehaviour 
{

	public AnimationCurve zCurve;
	public FloatRange multiplier;
	public bool active = true;
	public float offset;
	public FloatRange speed;
	public Space space;
	
	IEnumerator Start () 
	{
		float time = 0f;
		float randomSpeed = speed.GetRandom();
		float randomMultiplier = multiplier.GetRandom();
		while(active)
		{
			time += Time.deltaTime * randomSpeed;
			transform.Translate(0,(zCurve.Evaluate(time) * randomMultiplier + offset)*Time.deltaTime,0,space);
			yield return null;
		}
		yield return null;
	}
}
