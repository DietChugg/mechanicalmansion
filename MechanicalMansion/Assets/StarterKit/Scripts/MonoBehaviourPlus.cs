using UnityEngine;
using UnityEngine.UI;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System;
using System.Collections;
using System.Collections.Generic;
//using FullInspector;

namespace StarterKit
{
	public class MonoBehaviourPlus : MonoBehaviour 
	{
		[HideInInspector]
		Vector3 _position;
		[HideInInspector]
		public Vector3 position
		{
			get
			{
				_position = transform.position;
				return _position;
			}
			
			set
			{
				transform.position = value;
				_position = value;
			}
		}
		
		[HideInInspector]
		float _positionX;
		[HideInInspector]
		public float positionX
		{
			get
			{
				positionX = transform.position.x;
				return positionX;
			}
			
			set
			{
				transform.SetXPosition(value);
				positionX = value;
			}
		}
		
		[HideInInspector]
		float _positionY;
		[HideInInspector]
		public float positionY
		{
			get
			{
				positionY = transform.position.y;
				return positionY;
			}
			
			set
			{
				transform.SetYPosition(value);
				positionY = value;
			}
		}
		
		[HideInInspector]
		float _positionZ;
		[HideInInspector]
		public float positionZ
		{
			get
			{
				positionZ = transform.position.z;
				return positionZ;
			}
			
			set
			{
				transform.SetZPosition(value);
				positionZ = value;
			}
		}
		
		[HideInInspector]
		Vector3 _localPosition;
		[HideInInspector]
		public Vector3 localPosition
		{
			get
			{
				_localPosition = transform.localPosition;
				return _localPosition;
			}
			
			set
			{
				transform.localPosition = value;
				_localPosition = value;
			}
		}
		
		[HideInInspector]
		Quaternion _rotation;
		[HideInInspector]
		public Quaternion rotation
		{
			get
			{
				_rotation = transform.rotation;
				return _rotation;
			}
			
			set
			{
				_rotation = value;
				transform.rotation = value;
			}
		}
		
		[HideInInspector]
		Vector3 _localScale;
		[HideInInspector]
		public Vector3 localScale
		{
			get
			{
				_localScale = transform.localScale;
				return _localScale;
			}
			
			set
			{
				_localScale = value;
				transform.localScale = value;
			}
		}
		
		[HideInInspector]
		Vector3 _eulerAngles;
		[HideInInspector]
		public Vector3 eulerAngles
		{
			get
			{
				_eulerAngles = transform.eulerAngles;
				return _eulerAngles;
			}
			
			set
			{
				transform.eulerAngles = value;
				_eulerAngles = value;
			}
		}
		
		[HideInInspector]
		Vector3 _localEulerAngles;
		[HideInInspector]
		public Vector3 localEulerAngles
		{
			get
			{
				_localEulerAngles = transform.localEulerAngles;
				return _localEulerAngles;
			}
			
			set
			{
				transform.localEulerAngles = value;
				_localEulerAngles = value;
			}
		}
		
		
		[HideInInspector]
		TextMesh _textMesh;
		[HideInInspector]
		public TextMesh textMesh
		{
			get
			{
				if(!_textMesh)
					_textMesh = GetComponent<TextMesh>();
				return _textMesh;
			}
		}
		
		[HideInInspector]
		Animator _animator;
		[HideInInspector]
		public Animator animator
		{
			get
			{
				if(!_animator)
					_animator = GetComponent<Animator>();
				return _animator;
			}
		}
		
		[HideInInspector]
		TrailRenderer _trailRenderer;
		[HideInInspector]
		public TrailRenderer trailRenderer
		{
			get
			{
				if(!_trailRenderer)
					_trailRenderer = GetComponent<TrailRenderer>();
				return _trailRenderer;
			}
		}
		
		[HideInInspector]
		LineRenderer _lineRenderer;
		[HideInInspector]
		public LineRenderer lineRenderer
		{
			get
			{
				if(!_lineRenderer)
					_lineRenderer = GetComponent<LineRenderer>();
				return _lineRenderer;
			}
		}
		
		[HideInInspector]
		BoxCollider _boxCollider;
		[HideInInspector]
		public BoxCollider boxCollider
		{
			get
			{
				if(!_boxCollider)
					_boxCollider = GetComponent<BoxCollider>();
				return _boxCollider;
			}
		}
		
		[HideInInspector]
		SphereCollider _sphereCollider;
		[HideInInspector]
		public SphereCollider sphereCollider
		{
			get
			{
				if(!_sphereCollider)
					_sphereCollider = GetComponent<SphereCollider>();
				return _sphereCollider;
			}
		}
		
		[HideInInspector]
		CapsuleCollider _capsuleCollider;
		[HideInInspector]
		public CapsuleCollider capsuleCollider
		{
			get
			{
				if(!_capsuleCollider)
					_capsuleCollider = GetComponent<CapsuleCollider>();
				return _capsuleCollider;
			}
		}
		
		[HideInInspector]
		MeshCollider _meshCollider;
		[HideInInspector]
		public MeshCollider meshCollider
		{
			get
			{
				if(!_meshCollider)
					_meshCollider = GetComponent<MeshCollider>();
				return _meshCollider;
			}
		}
		
		[HideInInspector]
		WheelCollider _wheelCollider;
		[HideInInspector]
		public WheelCollider wheelCollider
		{
			get
			{
				if(!_wheelCollider)
					_wheelCollider = GetComponent<WheelCollider>();
				return _wheelCollider;
			}
		}
		
		[HideInInspector]
		TerrainCollider _terrainCollider;
		[HideInInspector]
		public TerrainCollider terrainCollider
		{
			get
			{
				if(!_terrainCollider)
					_terrainCollider = GetComponent<TerrainCollider>();
				return _terrainCollider;
			}
		}
		
	//	[HideInInspector]
	//	InteractiveCloth _interactiveCloth;
	//	[HideInInspector]
	//	public InteractiveCloth interactiveCloth
	//	{
	//		get
	//		{
	//			if(!_interactiveCloth)
	//				_interactiveCloth = GetComponent<InteractiveCloth>();
	//			return _interactiveCloth;
	//		}
	//	}
	//	
	//	[HideInInspector]
	//	ClothRenderer _clothRenderer;
	//	[HideInInspector]
	//	public ClothRenderer clothRenderer
	//	{
	//		get
	//		{
	//			if(!_clothRenderer)
	//				_clothRenderer = GetComponent<ClothRenderer>();
	//			return _clothRenderer;
	//		}
	//	}
		
		[HideInInspector]
		FixedJoint _fixedJoint;
		[HideInInspector]
		public FixedJoint fixedJoint
		{
			get
			{
				if(!_fixedJoint)
					_fixedJoint = GetComponent<FixedJoint>();
				return _fixedJoint;
			}
		}
		
		[HideInInspector]
		SpringJoint _springJoint;
		[HideInInspector]
		public SpringJoint springJoint
		{
			get
			{
				if(!_springJoint)
					_springJoint = GetComponent<SpringJoint>();
				return _springJoint;
			}
		}
		
		[HideInInspector]
		CharacterJoint _characterJoint;
		[HideInInspector]
		public CharacterJoint characterJoint
		{
			get
			{
				if(!_characterJoint)
					_characterJoint = GetComponent<CharacterJoint>();
				return _characterJoint;
			}
		}
		
		[HideInInspector]
		ConfigurableJoint _configurableJoint;
		[HideInInspector]
		public ConfigurableJoint configurableJoint
		{
			get
			{
				if(!_configurableJoint)
					_configurableJoint = GetComponent<ConfigurableJoint>();
				return _configurableJoint;
			}
		}
		
		[HideInInspector]
		CharacterController _characterController;
		[HideInInspector]
		public CharacterController characterController
		{
			get
			{
				if(!_characterController)
					_characterController = GetComponent<CharacterController>();
				return _characterController;
			}
		}

		[HideInInspector]
		Collider2D _collider2D;
		[HideInInspector]
		public new Collider2D collider2D
		{
			get
			{
				if(!_collider2D)
					_collider2D = GetComponent<Collider2D>();
				return _collider2D;
			}
		}

		[HideInInspector]
		CircleCollider2D _circleCollider2D;
		[HideInInspector]
		public CircleCollider2D circleCollider2D
		{
			get
			{
				if(!_circleCollider2D)
					_circleCollider2D = GetComponent<CircleCollider2D>();
				return _circleCollider2D;
			}
		}
		
		[HideInInspector]
		BoxCollider2D _boxCollider2D;
		[HideInInspector]
		public BoxCollider2D boxCollider2D
		{
			get
			{
				if(!_boxCollider2D)
					_boxCollider2D = GetComponent<BoxCollider2D>();
				return _boxCollider2D;
			}
		}
		
		[HideInInspector]
		EdgeCollider2D _edgeCollider2D;
		[HideInInspector]
		public EdgeCollider2D edgeCollider2D
		{
			get
			{
				if(!_edgeCollider2D)
					_edgeCollider2D = GetComponent<EdgeCollider2D>();
				return _edgeCollider2D;
			}
		}
		
		[HideInInspector]
		PolygonCollider2D _polygonCollider2D;
		[HideInInspector]
		public PolygonCollider2D polygonCollider2D
		{
			get
			{
				if(!_polygonCollider2D)
					_polygonCollider2D = GetComponent<PolygonCollider2D>();
				return _polygonCollider2D;
			}
		}
		
		[HideInInspector]
		SpringJoint2D _springJoint2D;
		[HideInInspector]
		public SpringJoint2D springJoint2D
		{
			get
			{
				if(!_springJoint2D)
					_springJoint2D = GetComponent<SpringJoint2D>();
				return _springJoint2D;
			}
		}
		
		[HideInInspector]
		DistanceJoint2D _distanceJoint2D;
		[HideInInspector]
		public DistanceJoint2D distanceJoint2D
		{
			get
			{
				if(!_distanceJoint2D)
					_distanceJoint2D = GetComponent<DistanceJoint2D>();
				return _distanceJoint2D;
			}
		}
		
		[HideInInspector]
		HingeJoint2D _hingeJoint2D;
		[HideInInspector]
		public HingeJoint2D hingeJoint2D
		{
			get
			{
				if(!_hingeJoint2D)
					_hingeJoint2D = GetComponent<HingeJoint2D>();
				return _hingeJoint2D;
			}
		}
		
		[HideInInspector]
		SliderJoint2D _sliderJoint2D;
		[HideInInspector]
		public SliderJoint2D sliderJoint2D
		{
			get
			{
				if(!_sliderJoint2D)
					_sliderJoint2D = GetComponent<SliderJoint2D>();
				return _sliderJoint2D;
			}
		}
		
		[HideInInspector]
		WheelJoint2D _wheelJoint2D;
		[HideInInspector]
		public WheelJoint2D wheelJoint2D
		{
			get
			{
				if(!_wheelJoint2D)
					_wheelJoint2D = GetComponent<WheelJoint2D>();
				return _wheelJoint2D;
			}
		}
		
		[HideInInspector]
		Projector _projector;
		[HideInInspector]
		public Projector projector
		{
			get
			{
				if(!_projector)
					_projector = GetComponent<Projector>();
				return _projector;
			}
		}
		
		[HideInInspector]
		LensFlare _lensFlare;
		[HideInInspector]
		public LensFlare lensFlare
		{
			get
			{
				if(!_lensFlare)
					_lensFlare = GetComponent<LensFlare>();
				return _lensFlare;
			}
		}
		
		[HideInInspector]
		MeshRenderer _meshRenderer;
		[HideInInspector]
		public MeshRenderer meshRenderer
		{
			get
			{
				if(!_meshRenderer)
					_meshRenderer = GetComponent<MeshRenderer>();
				return _meshRenderer;
			}
		}
		
		[HideInInspector]
		MeshFilter _meshFilter;
		[HideInInspector]
		public MeshFilter meshFilter
		{
			get
			{
				if(!_meshFilter)
					_meshFilter = GetComponent<MeshFilter>();
				return _meshFilter;
			}
		}
		
		[HideInInspector]
		SkinnedMeshRenderer _skinnedMeshRenderer;
		[HideInInspector]
		public SkinnedMeshRenderer skinnedMeshRenderer
		{
			get
			{
				if(!_skinnedMeshRenderer)
					_skinnedMeshRenderer = GetComponent<SkinnedMeshRenderer>();
				return _skinnedMeshRenderer;
			}
		}
		
		[HideInInspector]
		SpriteRenderer _spriteRenderer;
		[HideInInspector]
		public SpriteRenderer spriteRenderer
		{
			get
			{
				if(!_spriteRenderer)
					_spriteRenderer = GetComponent<SpriteRenderer>();
				return _spriteRenderer;
			}
		}
		
		[HideInInspector]
		Skybox _skybox;
		[HideInInspector]
		public Skybox skybox
		{
			get
			{
				if(!_skybox)
					_skybox = GetComponent<Skybox>();
				return _skybox;
			}
		}
		
		[HideInInspector]
		NavMeshAgent _navMeshAgent;
		[HideInInspector]
		public NavMeshAgent navMeshAgent
		{
			get
			{
				if(!_navMeshAgent)
					_navMeshAgent = GetComponent<NavMeshAgent>();
				return _navMeshAgent;
			}
		}
		
		[HideInInspector]
		OffMeshLink _offMeshLink;
		[HideInInspector]
		public OffMeshLink offMeshLink
		{
			get
			{
				if(!_offMeshLink)
					_offMeshLink = GetComponent<OffMeshLink>();
				return _offMeshLink;
			}
		}
		
		[HideInInspector]
		NavMeshObstacle _navMeshObstacle;
		[HideInInspector]
		public NavMeshObstacle navMeshObstacle
		{
			get
			{
				if(!_navMeshObstacle)
					_navMeshObstacle = GetComponent<NavMeshObstacle>();
				return _navMeshObstacle;
			}
		}
		
		[HideInInspector]
		AudioListener _audioListener;
		[HideInInspector]
		public AudioListener audioListener
		{
			get
			{
				if(!_audioListener)
					_audioListener = GetComponent<AudioListener>();
				return _audioListener;
			}
		}

		[HideInInspector]
		AudioSource _audioSource;
		[HideInInspector]
		public AudioSource audioSource
		{
			get
			{
				if(!_audioSource)
					_audioSource = GetComponent<AudioSource>();
				return _audioSource;
			}
		}
		
		[HideInInspector]
		AudioReverbZone _audioReverbZone;
		[HideInInspector]
		public AudioReverbZone audioReverbZone
		{
			get
			{
				if(!_audioReverbZone)
					_audioReverbZone = GetComponent<AudioReverbZone>();
				return _audioReverbZone;
			}
		}
		
		[HideInInspector]
		AudioLowPassFilter _audioLowPassFilter;
		[HideInInspector]
		public AudioLowPassFilter audioLowPassFilter
		{
			get
			{
				if(!_audioLowPassFilter)
					_audioLowPassFilter = GetComponent<AudioLowPassFilter>();
				return _audioLowPassFilter;
			}
		}
		
		[HideInInspector]
		AudioHighPassFilter _audioHighPassFilter;
		[HideInInspector]
		public AudioHighPassFilter audioHighPassFilter
		{
			get
			{
				if(!_audioHighPassFilter)
					_audioHighPassFilter = GetComponent<AudioHighPassFilter>();
				return _audioHighPassFilter;
			}
		}
		
		[HideInInspector]
		AudioEchoFilter _audioEchoFilter;
		[HideInInspector]
		public AudioEchoFilter audioEchoFilter
		{
			get
			{
				if(!_audioEchoFilter)
					_audioEchoFilter = GetComponent<AudioEchoFilter>();
				return _audioEchoFilter;
			}
		}
		
		[HideInInspector]
		AudioDistortionFilter _audioDistortionFilter;
		[HideInInspector]
		public AudioDistortionFilter audioDistortionFilter
		{
			get
			{
				if(!_audioDistortionFilter)
					_audioDistortionFilter = GetComponent<AudioDistortionFilter>();
				return _audioDistortionFilter;
			}
		}
		
		[HideInInspector]
		AudioReverbFilter _audioReverbFilter;
		[HideInInspector]
		public AudioReverbFilter audioReverbFilter
		{
			get
			{
				if(!_audioReverbFilter)
					_audioReverbFilter = GetComponent<AudioReverbFilter>();
				return _audioReverbFilter;
			}
		}
		
		[HideInInspector]
		AudioChorusFilter _audioChorusFilter;
		[HideInInspector]
		public AudioChorusFilter audioChorusFilter
		{
			get
			{
				if(!_audioChorusFilter)
					_audioChorusFilter = GetComponent<AudioChorusFilter>();
				return _audioChorusFilter;
			}
		}
		
		[HideInInspector]
		Transform _Transform;
		[HideInInspector]
		public new Transform transform
		{
			get
			{
				if(!_Transform)
					_Transform = GetComponent<Transform>();
				return _Transform;
			}
		}
		
		[HideInInspector]
		GameObject _GameObject;
		[HideInInspector]
		public new GameObject gameObject
		{
			get
			{
				if(!_GameObject)
					_GameObject = (this as MonoBehaviour).gameObject;
				return _GameObject;
			}
		}

        [HideInInspector]
        Image _Image;
        [HideInInspector]
        public Image image
        {
            get
            {
                if (!_Image)
                    _Image = GetComponent<Image>();
                return _Image;
            }
        }

        [HideInInspector]
        Button _Button;
        [HideInInspector]
        public Button button
        {
            get
            {
                if (!_Button)
                    _Button = GetComponent<Button>();
                return _Button;
            }
        }

        [HideInInspector]
        Canvas _Canvas;
        [HideInInspector]
        public Canvas canvas
        {
            get
            {
                if (!_Canvas)
                    _Canvas = GetComponent<Canvas>();
                return _Canvas;
            }
        }

        [HideInInspector]
        Text _Text;
        [HideInInspector]
        public Text text
        {
            get
            {
                if (!_Text)
                    _Text = GetComponent<Text>();
                return _Text;
            }
        }


		//These two don't like to work... Don't know why
		
		//    [HideInInspector]
		//    WindZone _windZone;
		//    public WindZone windZone
		//    {
		//        get
		//        {
		//            if(!_windZone)
		//                _windZone = GetComponent<WindZone>();
		//            return _windZone;
		//        }
		//    }
		//
		//    [HideInInspector]
		//    Halo _halo;
		//    public Halo halo
		//    {
		//        get
		//        {
		//            if(!_halo)
		//                _halo = GetComponent<Halo>();
		//            return _halo;
		//        }
		//    }

		public MonoBehaviourPlus()
		{
//			Debug.Log("MonoBehaviourPlus Created");
			#if UNITY_EDITOR
			EditorApplication.update += EditorUpdate;
			#endif 
		}

		~MonoBehaviourPlus()
		{
//			Debug.Log("MonoBehaviourPlus Destroyed");
			#if UNITY_EDITOR
			EditorApplication.update -= EditorUpdate;
			#endif 
		}

		public void OnEnable()
		{
//			Debug.Log("OneEnable");
			ClicksManager.OnClickDown += OnClickDown;
			ClicksManager.OnClickUp += OnClickUp;
			ClicksManager.OnClickDown2D += OnClickDown2D;
			ClicksManager.OnClickUp2D += OnClickUp2D;
			PauseController.OnPause += OnPause;
			ApplicationX.OnLevelLoaded += OnLevelLoaded;
			ApplicationX.OnLevelLoading += OnLevelLoading;
		}
		
		public void OnDisable()
		{
			ClicksManager.OnClickDown -= OnClickDown;
			ClicksManager.OnClickUp -= OnClickUp;
			ClicksManager.OnClickDown2D -= OnClickDown2D;
			ClicksManager.OnClickUp2D -= OnClickUp2D;
			PauseController.OnPause -= OnPause;
			ApplicationX.OnLevelLoaded -= OnLevelLoaded;
			ApplicationX.OnLevelLoading -= OnLevelLoading;
			//        UnRegisterAllEvents();
		}

#if UNITY_EDITOR
		public virtual void EditorUpdate()
		{
			if(this == null)
				EditorApplication.update -= EditorUpdate;
		}
#endif
		public virtual void OnLevelLoaded(LoadingLevelData lld)
		{
			
		}
		
		public virtual void OnLevelLoading(LoadingLevelData lld)
		{
			
		}
		
		public virtual void OnClickDown(GameObject g)
		{
			
		}
		
		public virtual void OnClickUp(GameObject g)
		{
			
		}
		
		public virtual void OnClickDown2D(GameObject g)
		{
			
		}
		
		public virtual void OnClickUp2D(GameObject g)
		{
			
		}
		
		public virtual void OnPause(bool isPaused)
		{
			//COULD BE A ROUTE TO GO... Keep thinging about this one.
			
			//        if (isPaused)
			//        {
			//            if(collider2D)
			//                pauseColliderState = collider2D.enabled;
			//            else if(collider)
			//                pauseColliderState = collider.enabled;
			//        }
			//        else
			//        {
			//            if(collider2D)
			//                collider2D.enabled = pauseColliderState;
			//            else if(collider)
			//                collider.enabled = pauseColliderState;
			//        }
		}
		
		public void StartSafeCoroutine(IEnumerator routine)
		{
			if(gameObject.activeInHierarchy)
				StartCoroutine(routine);
		}
		
		public void StartSafeCoroutine(string methodName)
		{
			if(gameObject.activeInHierarchy)
				StartCoroutine(methodName);
		}
		
		public void StartSafeCoroutine(string methodName, object value)
		{
			if(gameObject.activeInHierarchy)
				StartCoroutine(methodName,value);
		}
		
		public void Invoke(float time, Action action)
		{
			StartCoroutine(Waiting(time,action));
		}

		IEnumerator Waiting(float time, Action action)
		{
			yield return new WaitForSeconds(time);
			action();
		}

		/// <summary>
		/// Example Usage:
		/// Invoke<float>(1.5f,
		///delegate(float volume){AudioSource.PlayClipAtPoint(goodJobSounds.GetRandom(), Camera.main.transform.position,volume);},.5f);
		/// </summary>
		/// <param name="time">Time.</param>
		/// <param name="action">Action.</param>
		/// <param name="param">Parameter.</param>
		/// <typeparam name="T1">The 1st type parameter.</typeparam>
		public void Invoke<T1>(float time, Action<T1> action, T1 param)
		{
			StartCoroutine(Waiting(time,action,param));
		}
		
		IEnumerator Waiting<T1>(float time, Action<T1> action, T1 param)
		{
			yield return new WaitForSeconds(time);
			action(param);
		}

		public void Invoke<T1,T2>(float time, Action<T1,T2> action, T1 param, T2 param2)
		{
			StartCoroutine(Waiting(time,action,param,param2));
		}
		
		IEnumerator Waiting<T1,T2>(float time, Action<T1,T2> action, T1 param, T2 param2)
		{
			yield return new WaitForSeconds(time);
			action(param,param2);
		}

		public void Invoke<T1,T2,T3>(float time, Action<T1,T2,T3> action, T1 param, T2 param2, T3 param3)
		{
			StartCoroutine(Waiting(time,action,param,param2,param3));
		}
		
		IEnumerator Waiting<T1,T2,T3>(float time, Action<T1,T2,T3> action, T1 param, T2 param2, T3 param3)
		{
			yield return new WaitForSeconds(time);
			action(param,param2,param3);
		}

		
		
		
		
		
		
		
		
		
		
		
		//    public List<EventInfo> events = new List<EventInfo>();
		//
		//    public void RegisterEvent(Action regEvent, Action listener)
		//    {
		//        events.Add(new EventInfo(regEvent,listener));
		//        
		//    }
		//
		//    public void UnRegisterEvent(Action regEvent, Action listener)
		//    {
		//        Debug.Log("events.Count " + events.Count);
		//        for (int i = 0; i < events.Count; i++)
		//        {
		//            Debug.Log(events[i].regEvent.Equals(regEvent));
		//            Debug.Log(events[i].listener.Equals(listener));
		//            if(events[i].regEvent.Equals(regEvent) &&
		//               events[i].listener.Equals(listener))
		//            {
		//                Debug.Log("Found Matching Event To Unregister");
		//                events[i].UnRegister();
		//                events.RemoveAt(i);
		//                i--;
		//            }
		//        }
		//    }
		//
		//    public void UnRegisterAllEvents()
		//    {
		//        for (int i = 0; i < events.Count; i++)
		//        {
		//            events[i].UnRegister();
		//            events.RemoveAt(i);
		//        }
		//    }
		
		
		
	}

	public class EventInfo
	{
		public Action regEvent;
		public Action listener;
		public EventInfo(Action regEvent, Action listener)
		{
			this.regEvent = regEvent;
			this.listener = listener;
			Register();
		}
		public void Register()
		{
			Debug.Log("Registered".RTPurple());
			regEvent += listener;
		}
		public void UnRegister()
		{
			Debug.Log("UnRegistered".RTLightBlue());
			regEvent -= listener;
		}
	}
}
