using UnityEngine;
using System.Collections;

public class PlayAnimationAtSpeed : MonoBehaviour 
{
	public float speed = 1f;

	void OnEnable () 
	{
		for (int i = 0; i < GetComponent<Animation>().GetClips().Length; i++) 
		{
			GetComponent<Animation>()[GetComponent<Animation>().GetClips()[i].name].speed = speed;
		}
	}
}
