using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using StarterKit;

public class ResumeButton : MonoBehaviourPlus 
{

	public new void OnEnable () 
	{
		(this as MonoBehaviourPlus).OnEnable();
	}

	public new void OnDisable () 
	{
		(this as MonoBehaviourPlus).OnDisable();
	}
	
	public void OnClicked () 
	{
        PauseController.Instance.IsPaused = false;
	}
}
