﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using StarterKit;

public class LifetimeBehaviour : MonoBehaviourPlus 
{
    public UnityLifetime activateTiming = UnityLifetime.None;
    public float delay = 0f;

    public void Awake()
    {
        if (activateTiming == UnityLifetime.Awake)
        {
            StartCoroutine(WaitToBegin());
        }
    }

	public void Start()
	{
        if (activateTiming == UnityLifetime.Start)
        {
            StartCoroutine(WaitToBegin());
        }
	}

	public new void OnEnable () 
	{
		(this as MonoBehaviourPlus).OnEnable();
        if (activateTiming == UnityLifetime.OnEnable)
        {
            StartCoroutine(WaitToBegin());
        }
	}

	public new void OnDisable () 
	{
		(this as MonoBehaviourPlus).OnDisable();
        if (activateTiming == UnityLifetime.OnDisable)
        {
            StartCoroutine(WaitToBegin());
        }
	}

    public void Update()
    {
        if (activateTiming == UnityLifetime.Update)
        {
            StartCoroutine(WaitToBegin());
        }
    }

    public IEnumerator WaitToBegin()
    {
        if (delay != 0f)
            yield return new WaitForSeconds(delay);
        Begin();
        yield return null;
    }
	
	public virtual void Begin () 
	{
		
	}
}
