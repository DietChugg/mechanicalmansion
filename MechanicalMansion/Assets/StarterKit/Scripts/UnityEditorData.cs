using UnityEngine;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections;
public class UnityEditorData : SingletonScriptableObject<UnityEditorData> 
{
    public PlayerSettingsData playerSettingsData;
#if UNITY_EDITOR
	[MenuItem ("ScriptableObject/UnityEditorData")]
	static void DrawHierarchyDataMenuItem () 
	{
		Selection.activeObject = (UnityEngine.Object)UnityEditorData.Instance;
	}
#endif


}

[Serializable]
public class PlayerSettingsData
{
    public string bundleVersion;
}
