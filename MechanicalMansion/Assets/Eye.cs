﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using StarterKit;
using StarterKit.LifeSystem;

public class Eye : MonoBehaviourPlus 
{
    public MainPlayerTracker playerTracker;
    public Eye alternateEye;
    Health health;
    public SpriteRenderer glow;
    CircleCollider2D circleCollider;
    public static SafeAction OnEyeDestroyed;
    public bool isLeftEye;
    public Transform eyeBone;
    public AnimationCurve fadeAwayCurve;
    
	public IEnumerator Start()
	{
		yield return null;
	}

	public new void OnEnable () 
	{
		(this as MonoBehaviourPlus).OnEnable();
        health = GetComponent<Health>();
        circleCollider = GetComponent<CircleCollider2D>();
        glow = transform.Find("Light").GetComponent<SpriteRenderer>();
        health.OnTakeDamage += OnTakeDamage;
        health.OnDeath += OnDeath;
        playerTracker.OnPlayerEnter += OnPlayerEnter;
        playerTracker.OnPlayerExit += OnPlayerExit;
	}

	public new void OnDisable () 
	{
		(this as MonoBehaviourPlus).OnDisable();
        health.OnTakeDamage -= OnTakeDamage;
        health.OnDeath -= OnDeath;

        playerTracker.OnPlayerEnter -= OnPlayerEnter;
        playerTracker.OnPlayerExit -= OnPlayerExit;
	}

    public void OnTakeDamage(int damageDealt, DamageInflictor inflictor) 
	{
        //Debug.Log("Taking Damage... ".RTYellow() + (float)health.health.current / (float)health.health.max);
        glow.color = Color.Lerp(Color.red, Color.white, (float)health.health.current / (float)health.health.max);
	}

    public void OnDeath()
    {
        glow.gameObject.SetActive(false);
        eyeBone.SetLocalScale(0);
        circleCollider.enabled = false;
        //StartCoroutine(ShinkAwayEye());
        OnEyeDestroyed.Call();
    }

    //public IEnumerator ShinkAwayEye()
    //{
    //    float time = .3f, currentTime = 0f;
    //    while (time != currentTime)
    //    {
    //        currentTime += Time.deltaTime;
    //        currentTime = currentTime.Clamp(0, time);
    //        eyeBone.SetLocalScale(fadeAwayCurve.Evaluate(currentTime/time));
    //        yield return null;
    //    }
    //    yield return null;
    //}

    public void OnPlayerEnter()
    {
        if (!health.health.IsAtMin())
            circleCollider.enabled = true;
        else
        {
            //Debug.Log("Alternate".RTNavy());
            alternateEye.circleCollider.enabled = true;
        }
            
        //glow.enabled = true;
    }

    public void OnPlayerExit()
    {
        if (!health.health.IsAtMin())
            circleCollider.enabled = false;
        else
        {
            //Debug.Log("Alternate".RTNavy());
            alternateEye.circleCollider.enabled = true;
        }
        //glow.enabled = false;
    }
}
