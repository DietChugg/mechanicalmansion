﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using StarterKit;

public class SpiritEnergy : MonoBehaviourPlus 
{
    public GameObject wisp;
    public GameObject spawnPoint;
    public static SafeAction OnSpiritEnergyDepleted;
    public FloatClamp lifeTimer = new FloatClamp();
    public bool touchingSpiritVaccum;
    public bool depleted;
    public int spiritsCollected;


	public IEnumerator Start()
	{
		yield return null;
	}

	public new void OnEnable () 
	{
		(this as MonoBehaviourPlus).OnEnable();
	}

	public new void OnDisable () 
	{
		(this as MonoBehaviourPlus).OnDisable();
	}

    public void Update()
    {
        if (touchingSpiritVaccum && !depleted)
        {
            lifeTimer -= Time.deltaTime;

            if (lifeTimer.current <  lifeTimer.max - (spiritsCollected * 2.25f))
            {
                spiritsCollected++;
                Debug.Log("Spirit Created".RTCyan());
                GameObject instance = wisp.Instantiate(spawnPoint.transform.position, spawnPoint.transform.rotation);
                instance.name = "FAT STUPID UNITY";
                instance.GetComponent<Wisp>().target = GameObject.FindGameObjectWithTag("Player").transform.GetComponent<PlayerMotor>().vaccum.transform;
                instance.GetComponent<Wisp>().GoToTarget();
            }
            
            if (lifeTimer.IsAtMin())
            {
                OnSpiritEnergyDepleted.Call();
                lifeTimer.Maximize();
                depleted = true;
            }
        }
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.transform.GetComponent<Vaccum>())
        {
            touchingSpiritVaccum = true;
        }
    }

    public void OnTriggerStay2D(Collider2D other)
    {
        if (other.transform.GetComponent<Vaccum>())
        {
            touchingSpiritVaccum = true;
        }
    }

    public void OnTriggerExit2D(Collider2D other)
    {
        if (other.transform.GetComponent<Vaccum>())
        {
            touchingSpiritVaccum = false;
        }
    }
}
