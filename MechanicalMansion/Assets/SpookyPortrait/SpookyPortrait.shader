﻿Shader "MechanicalMansion/SpookyPortrait" 
{ 
Properties 
{
	_Fog("Fog", Color) = (1,1,1,1)
	_Tint("Tint", Color) = (1,1,1,1)
	_Mask ("Mask", 2D) = "black" {}
	_Normal ("Normal", 2D) = "white" {}
	_Spooky ("Spooky", 2D) = "white" {}
}

SubShader {
	Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
		//Cull Off
		Lighting Off
		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha
		
	Pass {  

		CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			struct appdata_t {
				float4 vertex : POSITION;
				float2 texcoord : TEXCOORD0;
				float4 color : COLOR;
			};

			struct v2f {
				float4 vertex : SV_POSITION;
				half2 texcoord : TEXCOORD0;
				float4 projPos : TEXCOORD1;
				half4 position_in_screen_space : TEXCOORD2;
				float4 color : COLOR;
			};

			float4 _Fog;
			float4 _Tint;
			sampler2D _Mask;
			sampler2D _Normal;
			sampler2D _Spooky;
			
			v2f vert (appdata_t v)
			{
				v2f o;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.texcoord = v.texcoord;
				o.projPos = ComputeScreenPos(o.vertex);
				o.color = v.color;

				half4 clipSpace =  mul(UNITY_MATRIX_MVP, v.vertex);
				clipSpace.xy /= clipSpace.w;
				clipSpace.xy = 0.5*(clipSpace.xy+1.0);// * _ScreenParams.xy;
				o.position_in_screen_space = clipSpace;

				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				half4 mask = tex2D(_Mask, i.position_in_screen_space);
				half4 normal = tex2D(_Normal, i.texcoord);
				half4 spooky = tex2D(_Spooky, i.texcoord);

				float depth = 19.2/UNITY_PROJ_COORD(i.projPos).z;
				if(depth > 1)
					depth = 1;
				if(depth < 0)
					depth = 1;
					
				half4 finalTexture = lerp(normal,spooky,mask.r);

				half4 fog = lerp(_Fog,finalTexture * _Tint ,depth);
				finalTexture = half4(fog.rgb,finalTexture.a * _Tint.a);

				return finalTexture;
			}
		ENDCG
	}
}
	//CustomEditor "PlatformFloatingShaderGUI"
}
