﻿using UnityEngine;
using System.Collections;
using StarterKit;

public class Shaker : MonoBehaviour
{
    // Transform of the camera to shake. Grabs the gameObject's transform
    // if null.

    // How long the object should shake for.
    public float shake = 0f;

    // Amplitude of the shake. A larger value shakes the camera harder.
    public float shakeAmount = 0.7f;
    public float decreaseFactor = 1.0f;
    public float timeToHitEachPoint = .03f;
    public bool lockY;
        

    Vector3 originalPos;

    void OnEnable()
    {
        originalPos = transform.position;
        StopAllCoroutines();
        StartCoroutine(Shake());
    }

    void OnDisable()
    {
        transform.position = originalPos;
    }

    IEnumerator Shake()
    {
        yield return null;
        float startShake = shake;
        Debug.Log("Start Position: " + originalPos);
        Coroutine tracker = null;
        while (shake > 0)
        {
            yield return null;
            Vector3 targetPos = originalPos + Random.insideUnitSphere * shakeAmount;

            tracker = transform.MoveTowardsPointOverTime(targetPos, timeToHitEachPoint);
            float time = timeToHitEachPoint;
            while (time > 0)
            {
                time -= Time.deltaTime;
                if (lockY)
                    transform.SetYPosition(originalPos.y);
                yield return null;
            }
            if (lockY)
                transform.SetYPosition(originalPos.y);
            shake -= timeToHitEachPoint;//Time.deltaTime * decreaseFactor;
        }
        if (tracker != null)
            yield return tracker;
        Debug.Log("Restoring Start Position: " + originalPos);
        transform.position = originalPos;
        shake = startShake;
        enabled = false;
        StopAllCoroutines();
    }
}