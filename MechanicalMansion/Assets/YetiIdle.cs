﻿using UnityEngine;
using System.Collections;
using StarterKit;

public class YetiIdle : StateMachineBehaviour 
{
    public FloatClamp timer;
    public static  bool swipe = true;
	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        timer.Maximize();
        if (animator.GetComponent<YetiBoss>().eyesDestoyed == 1)
        {
            animator.speed = 1.3f;
        }
        else
        {
            animator.speed = .8f;
        }
    }

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        timer -= Time.deltaTime;
        if (timer.IsAtMin())
        {
            timer.Maximize();
            if (animator.GetBool("Swipe") == false)
            {
                animator.SetInteger("SlamIndex",RandomX.Range(1,6));
            }
            else
	        {
                YetiBoss yetiBoss = animator.GetComponent<YetiBoss>();
                bool isLeftEyeTracking = yetiBoss.leftEyeTrigger.GetComponent<MainPlayerTracker>().isPlayerInTrigger;
                bool isRightEyeTracking = yetiBoss.rightEyeTrigger.GetComponent<MainPlayerTracker>().isPlayerInTrigger;


                if (!isLeftEyeTracking && !isRightEyeTracking)
                {
                    //Debug.Log("CHOSEN RANDOMLY".RTPurple());
                    if (RandomX.value > .5f)
                    {
                        animator.SetTrigger("SwipeRight");
                    }
                    else
                    {
                        animator.SetTrigger("SwipeLeft");
                    } 
                }
                else if (isRightEyeTracking)
                {
                    //Debug.Log("CHOSEN SwipeLeft".RTPurple());
                    animator.SetTrigger("SwipeLeft");
                }
                else if (isLeftEyeTracking)
                {
                    //Debug.Log("CHOSEN SwipeRight".RTPurple());

                    animator.SetTrigger("SwipeRight");
                }
	        }
            animator.SetBool("Swipe", !animator.GetBool("Swipe"));
            animator.SetTrigger("ExitIdle");
        }
    }

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	//override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
