﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using StarterKit;
using Prime31;

public class Zoom : LifetimeBehaviour 
{
    public Transform targetPositon;
    public Vector3 offset;
    public float zoomToTime;
    public AnimationCurve curve;

	public IEnumerator Start()
	{
		yield return null;
	}

	public new void OnEnable () 
	{
		(this as MonoBehaviourPlus).OnEnable();
	}

	public new void OnDisable () 
	{
		(this as MonoBehaviourPlus).OnDisable();
	}

    public void Begin()
    {
        StartCoroutine(ZoomTowardsTarget());
    }
	
	public IEnumerator ZoomTowardsTarget () 
	{
        CueFocusRing[] cues = UnityEngine.Object.FindObjectsOfType<CueFocusRing>();
        for (int i = 0; i < cues.Length; i++)
        {
            //Destroy(cues[i].gameObject);
        }
        float time = 0;
        Vector3 startPos = transform.position;
        while (time != zoomToTime)
        {
            time += Time.deltaTime;
            time = time.Clamp(0, zoomToTime);
            Vector3 prevPos = transform.position;
            transform.position = Vector3.Lerp(startPos, targetPositon.position + offset, curve.Evaluate(time / zoomToTime));
            transform.SetXPosition(prevPos.x);
            transform.SetYPosition(prevPos.y);
            yield return null;
        }
	}
}
