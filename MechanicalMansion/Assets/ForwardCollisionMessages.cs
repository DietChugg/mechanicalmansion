﻿using UnityEngine;
using System.Collections;
using StarterKit;
using StarterKit.LifeSystem;

public class ForwardCollisionMessages : MonoBehaviour 
{
    public Health health;
    public CharacterController2D cc2d;
    public WayPointTracker wayPointTracker;
    public MovingPlatformCharacter movingPlatformCharacter;
    public bool isPlatform;


    public void OnTriggerEnter2D(Collider2D other)
    {
        if (isPlatform)
        {
            transform.parent.GetComponent<MovingPlatform>().Apply(other);
            return;
        }
        cc2d.OnTriggerEnter2D(other);
        //health.OnTriggerEnter2D(other);
        wayPointTracker.OnTriggerEnter2D(other);
        //movingPlatformCharacter.OnTriggerEnter2D(other);
    }

    public void OnTriggerStay2D(Collider2D other)
    {
        if (isPlatform)
        {
            transform.parent.GetComponent<MovingPlatform>().Apply(other);
            return;
        }
        cc2d.OnTriggerStay2D(other);
        health.OnTriggerStay2D(other);
        wayPointTracker.OnTriggerStay2D(other);
        //movingPlatformCharacter.OnTriggerStay2D(other);
        
    }

    public void OnTriggerExit2D(Collider2D other)
    {
        if (isPlatform)
        {
            transform.parent.GetComponent<MovingPlatform>().Apply(other);
            return;
        }
        cc2d.OnTriggerExit2D(other);
        health.OnTriggerExit2D(other);
        wayPointTracker.OnTriggerExit2D(other);
        cc2d.OnTriggerExit2D(other);
        
    }
	
}
