﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Camera))]
public class AmbientLight : MonoBehaviour 
{
    public AnimationCurve _colorChangeCurve;

    private Camera _camera;
    private Color _initColor;
    private bool _colorChanging = false;

    private static AmbientLight _ambientLight;
    public static AmbientLight Instance()
    {
        if (!_ambientLight)
        {
            _ambientLight = FindObjectOfType(typeof(AmbientLight)) as AmbientLight;
            if (!_ambientLight)
                Debug.LogError("There needs to be one active AmbientLight script on a GameObject in your scene.");
        }

        return _ambientLight;
    }

	void OnEnable () 
    {
        _camera = GetComponent<Camera>();
        _initColor = _camera.backgroundColor;
	}

	public void SetColor (Color color, float time) 
    {
        SetColor(color, time, false);
	}

    public void SetColor(Color color, float time, bool setBack)
    {
        if (!_colorChanging)
            StartCoroutine(Transition(color, time, setBack));
    }

    IEnumerator Transition(Color color, float time, bool setBack)
    {
        _colorChanging = true;
        float currentTime = 0;
        while (currentTime < time)
        {
            float percent = currentTime / time;
            float changeCurveEval = _colorChangeCurve.Evaluate(percent);
            _camera.backgroundColor = Color.Lerp(_initColor, color, changeCurveEval);

            currentTime += Time.deltaTime;
            yield return null;
        }

        Color setBackColor = _initColor;
        _camera.backgroundColor = _initColor = color;

        _colorChanging = false;

        if (setBack)
        {
            SetColor(setBackColor, time);
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
            SetColor(Color.red, 0.25f, true);

    }
}
