﻿using UnityEngine;
using System.Collections;

public class GhostTrigger : MonoBehaviour 
{
    public GameObject[] ghosts;

	void OnTriggerEnter2D (Collider2D col) 
    {
        if (col.name == "PlayerCol")
        {
            foreach (GameObject ghost in ghosts)
            {
                ghost.SetActive(true);
            }
        }
	}
}
