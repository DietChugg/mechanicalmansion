﻿using UnityEngine;
using System.Collections;

public class DropHeartOnVacuum : MonoBehaviour 
{
    public GameObject heart;

	void OnTriggerEnter2D (Collider2D other) 
    {
        if (other.GetComponent<Vaccum>() != null && other.GetComponent<Vaccum>().active && Random.Range(0,100) > 80)
            heart.Instantiate(transform.position, transform.rotation);
	}
}
