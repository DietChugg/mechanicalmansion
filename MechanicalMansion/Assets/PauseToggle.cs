﻿using UnityEngine;
using System.Collections;
using Rewired;
using StarterKit;

public class PauseToggle : MonoBehaviourPlus
{
    Player player;
    public GameObject pauseMenu;
    public GameObject backdrop;

	void OnEnable () 
    {
        player = ReInput.players.GetPlayer(0);
	}
	
	void Update () 
    {
        if (player == null)
        {
            OnEnable();
            return;
        }

        if(player.GetButtonDown("Pause"))
        {
            Debug.Log("Pause Pressed");
            TogglePause();
        }
	}

    public void TogglePause()
    {
        PauseController.Instance.IsPaused = false;
        PauseController.Instance.IsPaused = true;
        pauseMenu.SetActive(PauseController.Instance.IsPaused);
        backdrop.SetActive(PauseController.Instance.IsPaused);
    }
}
