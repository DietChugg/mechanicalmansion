﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using StarterKit;

public class LockRotations : MonoBehaviourPlus 
{
	public IEnumerator Start()
	{
		yield return null;
	}

	public new void OnEnable () 
	{
		(this as MonoBehaviourPlus).OnEnable();
	}

	public new void OnDisable () 
	{
		(this as MonoBehaviourPlus).OnDisable();
	}
	
	public void LateUpdate () 
	{
        transform.rotation = Quaternion.Euler(0, 0, transform.rotation.eulerAngles.z);
        transform.SetLocalScale(1.3f);
	}
}
