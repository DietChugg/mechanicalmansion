﻿using UnityEngine;
using System.Collections;

public class SetSortOrder : MonoBehaviour 
{

    public int _sortingOrder;
    public string _sortingLayer;

	void OnEnable () 
    {
        GetComponent<Renderer>().sortingOrder = _sortingOrder;
        GetComponent<Renderer>().sortingLayerName = _sortingLayer;
	}
}
