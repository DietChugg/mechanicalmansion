﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using StarterKit;
using StarterKit.LifeSystem;

public class MainCharacterController : MonoBehaviourPlus 
{
    private static MainCharacterController instance;

    public static MainCharacterController Instance
    {
        get
        {
            if (!instance)
            {
                instance = FindObjectOfType(typeof(MainCharacterController)) as MainCharacterController;
                if (!instance)
                    Debug.LogError("There needs to be one active MainCharacterController script on a GameObject in your scene.");
            }

            return instance;
        }
    }


    Health health;
    public Animator animator;
	public IEnumerator Start()
	{
		yield return null;
	}

	public new void OnEnable () 
	{
		(this as MonoBehaviourPlus).OnEnable();
        health = GetComponent<Health>();
        health.OnDeath += OnDeath;
        health.OnTakeDamage += OnTakeDamage;
	}

    public new void OnDisable()
    {
        (this as MonoBehaviourPlus).OnDisable();
        health.OnDeath -= OnDeath;
        health.OnTakeDamage -= OnTakeDamage;

    }

    public void Update()
    {
        transform.SetZPosition(0);
    }

    private void OnDeath()
    {
        GetComponent<PlayerMotor>().controllingAutoMove = true;
        GetComponent<PlayerMotor>()._velocity = Vector3.zero;
        GetComponent<PlayerMotor>().moveSpeed = -Vector3.up * 3f;
        GetComponent<PlayerMotor>()._controller.velocity = Vector3.zero;
        animator.SetTrigger("Death");
    }

    public void OnTakeDamage(int damageDealt, DamageInflictor damageInflictor) 
	{
        animator.SetTrigger("KnockBack");
	}
}
