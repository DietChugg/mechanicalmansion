﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using StarterKit;

public class Wisp : MonoBehaviourPlus 
{
    public Transform target;
    public float time;
    public AnimationCurve curve;

	public IEnumerator Start()
	{
		yield return null;
	}

	public new void OnEnable () 
	{
		(this as MonoBehaviourPlus).OnEnable();
	}

	public new void OnDisable () 
	{
		(this as MonoBehaviourPlus).OnDisable();
	}
	
	public void OnComplete () 
	{
        Stats.Instance().Spirits++;
        gameObject.SetActive(false);
	}

    public void GoToTarget()
    {
        StartCoroutine(GoToTargetRoutine());
    }

    public IEnumerator GoToTargetRoutine()
    {
        //GetComponent<AudioSource>().Play();
        //Debug.Log("Going To Target");
        float currentTime = 0;
        Vector3 startPos = transform.position;
        while (currentTime != time)
        {
            currentTime += Time.deltaTime;
            currentTime = currentTime.Clamp(0, time);
            transform.position = Vector3.Lerp(startPos, target.position, curve.Evaluate(currentTime/time));
            yield return null;
        }
        yield return null;
        OnComplete ();
        //Debug.Log("Done");
        //GetComponent<AudioSource>().Stop();
    }
}
