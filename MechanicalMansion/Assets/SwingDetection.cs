﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using StarterKit;

public class SwingDetection : MonoBehaviourPlus
{
    public LayerMask mask;
    public string[] tags;
    public SafeAction<GameObject> OnSwingHit;

    public IEnumerator Start()
    {
        yield return null;
    }

    public new void OnEnable()
    {
        (this as MonoBehaviourPlus).OnEnable();
    }

    public new void OnDisable()
    {
        (this as MonoBehaviourPlus).OnDisable();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        //Debug.Log(other.transform.name);
        if (other.transform.CompareLayerMask(mask) || other.transform.CompareTags(tags))
        {
          //  Debug.Log(other.transform.name.RTGreen());
            OnSwingHit.Call(other.gameObject);
        }
           
    }


    void OnTriggerStay2D(Collider2D other)
    {
        //Debug.Log(other.transform.name);
        if (other.transform.CompareLayerMask(mask) || other.transform.CompareTags(tags))
        {
            //Debug.Log(other.transform.name.RTGreen());
            OnSwingHit.Call(other.gameObject);
        }

    }
}
