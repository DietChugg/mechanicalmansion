﻿using UnityEngine;
using System.Collections;

public class EnemyMaterialController : MonoBehaviour 
{
    public Clown clown;

	void OnEnable () 
    {
        clown.OnRaiseToLife += Brighten;
        if (clown.rise)
            Darken();
	}

    void Disable()
    {
        clown.OnRaiseToLife -= Brighten;
    }

    void Brighten() 
    {
        GetComponent<Renderer>().material.SetFloat("_LightEffect", .25f);
	}

    void Darken()
    {
        GetComponent<Renderer>().material.SetFloat("_LightEffect", 0);
    }
}
