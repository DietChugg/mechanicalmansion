﻿using UnityEngine;
using System.Collections;
using StarterKit;
using StarterKit.LifeSystem;


public class YetiDeath : StateMachineBehaviour 
{
    public FloatClamp waitToShowLevelComplete;
	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        YetiBoss yetiBoss = animator.GetComponent<YetiBoss>();
        yetiBoss.headSlam.GetComponent<DamageInflictor>().canDamage = false;
        yetiBoss.headSlam.SetActive(false);
        yetiBoss.leftHor.GetComponent<DamageInflictor>().canDamage = false;
        yetiBoss.rightHor.GetComponent<DamageInflictor>().canDamage = false;
        yetiBoss.leftVer.GetComponent<DamageInflictor>().canDamage = false;
        yetiBoss.rightVer.GetComponent<DamageInflictor>().canDamage = false;
        yetiBoss.leftHor.SetActive(false);
        yetiBoss.rightHor.SetActive(false);
        yetiBoss.leftVer.SetActive(false);
        yetiBoss.rightVer.SetActive(false);
        yetiBoss.leftShock.Clear();
        yetiBoss.leftShock.Stop();
        yetiBoss.rightShock.Clear();
        yetiBoss.leftShock.Stop();
        waitToShowLevelComplete.Maximize();
        yetiBoss.BigRoar();
    }

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) 
    {
	    waitToShowLevelComplete -= Time.deltaTime;
        if (waitToShowLevelComplete.IsAtMin())
        {
            animator.GetComponent<YetiBoss>().LevelComplete();
        }
	}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	//override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
