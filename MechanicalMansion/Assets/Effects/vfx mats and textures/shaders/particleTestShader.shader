﻿Shader "Custom/particleTestShader" 
{
	Properties 
	{
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_Color ("Color", Color) = (1,1,1,1)
		_SecondTex ("The other fucking one", 2D) = "white" {}
		_SecondColor ("Second Color", Color) = (1,1,1,1)
		_SecondTexSpeed("Second Tex X Offset", Range(0,2)) = 0
		
	}
	SubShader 
	{
		Tags { "RenderType"="Transparent" }
		Pass
		{
			Blend SrcAlpha OneMinusSrcAlpha
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			
			uniform float4 _Color;
			uniform float4 _SecondColor;
			uniform sampler2D _MainTex;
			uniform sampler2D _SecondTex;
			uniform float _SecondTexSpeed;
			
			struct vertexInput
			{
				half4 vertex : POSITION;
				half4 texcoord : TEXCOORD0;
			};
			
			struct vertexOutput
			{
				half4 pos : SV_POSITION;
				half2 uv : TEXCOORD0;
				half2 uv2 : TEXCOORD1;
			};

			vertexOutput vert (vertexInput input)
			{
				vertexOutput output;
				
				output.pos = mul(UNITY_MATRIX_MVP, input.vertex);
				output.uv.y = input.texcoord.y + _Time.z;
				output.uv.x = input.texcoord.x;
				
				output.uv2.x = input.texcoord.x;
				output.uv2.y = input.texcoord.y + (_Time.y * _SecondTexSpeed);
				
				return output;
			} 
			
			float4 frag (vertexOutput input):COLOR
			{
				float4 mainTex = tex2D(_MainTex, input.uv);
				float4 secondTex = tex2D(_SecondTex, input.uv2);
				
				float4 mainTexAlpha = lerp(float4(mainTex.rgb,0), mainTex, mainTex.r) * _Color;
				float4 secondTexAlpha = lerp(float4(secondTex.rgb,0), secondTex, secondTex.r) * _SecondColor;
				
				if(_SecondTexSpeed < 1.9)
					return (mainTexAlpha + secondTexAlpha);
				else
					return mainTexAlpha;
			}
			
			ENDCG
		}
	} 
	
	FallBack "Diffuse"
}
