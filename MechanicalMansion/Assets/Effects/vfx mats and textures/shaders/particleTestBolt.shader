﻿Shader "Custom/particleTestBolt" 
{
	Properties 
	{
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_MainTexSpeedX("Second Tex X Offset", Range(0,2)) = 0
		_MainTexSpeedY("Second Tex Y Offset", Range(0,2)) = 0
		_Color ("Color", Color) = (1,1,1,1)
		_SecondTex ("The Other One", 2D) = "white" {}
		_SecondColor ("Second Color", Color) = (1,1,1,1)
		_SecondTexSpeedX("Second Tex X Offset", Range(0,2)) = 0
		_SecondTexSpeedY("Second Tex Y Offset", Range(0,2)) = 0
		_ThirdTex ("The Other Other One", 2D) = "white" {}
		_ThirdColor ("Third Color", Color) = (1,1,1,1)
		_ThirdTexSpeedX("Third Tex X Offset", Range(0,2)) = 0
		_ThirdTexSpeedY("Third Tex Y Offset", Range(0,0)) = 0

		
		
	}
	SubShader 
	{
		Tags { "RenderType"="Transparent" }
		Pass
		{
			Blend SrcAlpha OneMinusSrcAlpha
			Cull Off 
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			
			uniform float4 _Color;
			uniform sampler2D _MainTex;
			uniform float _MainTexSpeedY;
			uniform float _MainTexSpeedX;
			
			uniform float4 _SecondColor;
			uniform sampler2D _SecondTex;
			uniform float _SecondTexSpeedY;
			uniform float _SecondTexSpeedX;
			
			uniform float4 _ThirdColor;
			uniform sampler2D _ThirdTex;
			uniform float _ThirdTexSpeedY;
			uniform float _ThirdTexSpeedX;
			
			struct vertexInput
			{
				half4 vertex : POSITION;
				half4 texcoord : TEXCOORD0;
			};
			
			struct vertexOutput
			{
				half4 pos : SV_POSITION;
				half2 uv : TEXCOORD0;
				half2 uv2 : TEXCOORD1;
				half2 uv3 : TEXCOORD2;
			};

			vertexOutput vert (vertexInput input)
			{
				vertexOutput output;
				
				//Change uv's over time
				output.pos = mul(UNITY_MATRIX_MVP, input.vertex);
				//1st texture UV over time
				output.uv.y = input.texcoord.y + (_Time.x * _MainTexSpeedX);
				output.uv.x = input.texcoord.x + (_Time.y * _MainTexSpeedY);
				//2nd texture UV over time
				output.uv2.x = input.texcoord.x + (_Time.x * _SecondTexSpeedX);
				output.uv2.y = input.texcoord.y + (_Time.y * _SecondTexSpeedY);
				
				output.uv3.x = input.texcoord.x + (_Time.x * _ThirdTexSpeedX);
				output.uv3.y = input.texcoord.y + (_Time.y * _ThirdTexSpeedY);
				
				return output;
			} 
			
			float4 frag (vertexOutput input):COLOR
			{
				float4 mainTex = tex2D(_MainTex, input.uv);
				float4 secondTex = tex2D(_SecondTex, input.uv2);
				float4 thirdTex = tex2D(_ThirdTex, input.uv3);
				
				float4 mainTexAlpha = lerp(float4(mainTex.rgb,0), mainTex, mainTex.r) * _Color;
				float4 secondTexAlpha = lerp(float4(secondTex.rgb,0), secondTex, secondTex.r) * _SecondColor;
				float4 thirdTexAlpha = lerp(float4(thirdTex.rgb,0), thirdTex, thirdTex.r) * _ThirdColor;
				
				if(_SecondTexSpeedY < 1.9)
					return (mainTexAlpha + secondTexAlpha + thirdTexAlpha);
				else
					return mainTexAlpha;
			}
			
			ENDCG
		}
	} 
	
	FallBack "Diffuse"
}
