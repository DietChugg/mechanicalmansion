﻿Shader "Custom/particleTexture3Add" 
{
	Properties 
	{
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_MainTexSpeedX("Second Tex X Offset", Float) = 0
		_MainTexSpeedY("Second Tex Y Offset", Float) = 0
		_TintColor ("Color", Color) = (1,1,1,1)
		_MainTexBright("Main Tex Brightness", Float) = 1
		_MainTexXTile("Main Tex X TileNumber", Float) = 1
		_MainTexYTile("Main Tex Y TileNumber", Float) = 1
		[Toggle]_MainTextBrightPulseSwitch("Main Tex Brightness Pulse Switch", Float) = 0
		_MainTextBrightPulse("Main Tex Brightness Pulse", Float) = 1

		_SecondTex ("The Other One", 2D) = "white" {}
		_SecondColor ("Second Color", Color) = (1,1,1,1)
		_SecondTexSpeedX("Second Tex X Offset", Float) = 0
		_SecondTexSpeedY("Second Tex Y Offset", Float) = 0
		_SecondTexBright("Second Tex Brightness", Float) = 1
		_SecondTexXTile("Second Tex X TileNumber", Float) = 1
		_SecondTexYTile("Second Tex Y TileNumber", Float) = 1
		[Toggle]_SecondTextBrightPulseSwitch("Second Tex Brightness Pulse Switch", Float) = 0
		_SecondTextBrightPulse("Second Tex Brightness Pulse", Float) = 1
		
		_ThirdTex ("The Other Other One", 2D) = "white" {}
		_ThirdColor ("Third Color", Color) = (1,1,1,1)
		_ThirdTexSpeedX("Third Tex X Offset", Float) = 0
		_ThirdTexSpeedY("Third Tex Y Offset", Float) = 0
		_ThirdTexBright("Third Tex Brightness", Float) = 1
		_ThirdTexXTile("Third Tex X TileNumber", Float) = 1
		_ThirdTexYTile("Third Tex Y TileNumber", Float) = 1
		[Toggle]_ThirdTextBrightPulseSwitch("Second Tex Brightness Pulse Switch", Float) = 0
		_ThirdTextBrightPulse("Third Tex Brightness Pulse", Float) = 1

		
		
	}
	SubShader 
	{
		Tags { "RenderType"="Transparent" "Queue"="Transparent"}
		Pass
		{
			
			//*Additive blend mode*
			//Blend OneMinusDstColor One
			//Blend SrcAlpha SrcAlpha
			Blend SrcAlpha One
			AlphaTest Greater .01
			//Blend SrcAlpha OneMinusSrcAlpha

			//*Alpha blend mode*
			//Blend SrcAlpha OneMinusSrcAlpha
			Cull Off 
			
			ZWrite Off
			
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			#pragma multi_compile_particles
			
			uniform float4 _TintColor;
			uniform sampler2D _MainTex;
			uniform float _MainTexSpeedY;
			uniform float _MainTexSpeedX;
			uniform float _MainTexBright;
			uniform float _MainTextBrightPulse;
			uniform float _MainTextBrightPulseSwitch;
			uniform float _MainTexXTile;
			uniform float _MainTexYTile;
			
			uniform float4 _SecondColor;
			uniform sampler2D _SecondTex;
			uniform float _SecondTexSpeedY;
			uniform float _SecondTexSpeedX;
			uniform float _SecondTexBright;
			uniform float _SecondTexXTile;
			uniform float _SecondTexYTile;
			uniform float _SecondTextBrightPulse;
			uniform float _SecondTextBrightPulseSwitch;
			
			uniform float4 _ThirdColor;
			uniform sampler2D _ThirdTex;
			uniform float _ThirdTexSpeedY;
			uniform float _ThirdTexSpeedX;
			uniform float _ThirdTexBright;
			uniform float _ThirdTexXTile;
			uniform float _ThirdTexYTile;
			uniform float _ThirdTextBrightPulse;
			uniform float _ThirdTextBrightPulseSwitch;

//			uniform float4 _MainTex_ST;
//			uniform float4 _SecondTex_ST;
			
			struct vertexInput
			{
				half4 vertex : POSITION;
				float4 color : COLOR;
				half4 texcoord : TEXCOORD0;
			};
			
			struct vertexOutput
			{
				half4 pos : SV_POSITION;
				half2 uv : TEXCOORD0;
				half2 uv2 : TEXCOORD1;
				half2 uv3 : TEXCOORD2;
				float4 color : COLOR;


			};

			vertexOutput vert (vertexInput input)
			{
				vertexOutput output;
				
				//Change uv's over time
				output.pos = mul(UNITY_MATRIX_MVP, input.vertex);
				
//				//1st texture UV over time
				output.uv.x = input.texcoord.x * _MainTexXTile + (_Time.x * _MainTexSpeedX);
				output.uv.y = input.texcoord.y * _MainTexYTile + (_Time.y * _MainTexSpeedY);
//				//2nd texture UV over time, squash and stretch
				output.uv2.x = input.texcoord.x * _SecondTexXTile + (_Time.x * _SecondTexSpeedX) ;
				output.uv2.y = input.texcoord.y * _SecondTexYTile + (_Time.y * _SecondTexSpeedY);
				//3rd texture UV over time, squash and stretch
				output.uv3.x = input.texcoord.x * _ThirdTexXTile + (_Time.x * _ThirdTexSpeedX) ;
				output.uv3.y = input.texcoord.y * _ThirdTexYTile + (_Time.y * _ThirdTexSpeedY);
				output.color = input.color;
//				output.uv = TRANSFORM_TEX(input.texcoord, _MainTex);
//				output.uv2 = TRANSFORM_TEX(input.texcoord, _SecondTex);
				
				return output;
			} 
			
			
			float4 frag (vertexOutput input):COLOR
			{
				float mainTexPulse = (sin(_Time.w*_MainTextBrightPulse)+1)/2;
				if(!_MainTextBrightPulseSwitch)
					mainTexPulse = 1;
				
				float secondTexPulse = (sin(_Time.w*_SecondTextBrightPulse)+1)/2;
				if(!_SecondTextBrightPulseSwitch)
					secondTexPulse = 1;
					
				float thirdTexPulse = (sin(_Time.w*_ThirdTextBrightPulse)+1)/2;
				if(!_ThirdTextBrightPulseSwitch)
					thirdTexPulse = 1;
				
				float4 mainTex = tex2D(_MainTex, input.uv);
				float4 mainTexMod = mainTex * _MainTexBright   *   mainTexPulse;
				
				float4 secondTex = tex2D(_SecondTex, input.uv2);
				float4 secondTexMod = secondTex * _SecondTexBright * secondTexPulse;
				
				float4 thirdTex = tex2D(_ThirdTex, input.uv2);
				float4 thirdTexMod = thirdTex * _ThirdTexBright * thirdTexPulse;
				
				float4 mainTexAlpha   = lerp(float4(mainTexMod.rgb,0), mainTexMod, mainTex.r) * _TintColor;
				float4 secondTexAlpha = lerp(float4(secondTexMod.rgb,0), secondTexMod, secondTex.r) * _SecondColor;
				float4 thirdTexAlpha = lerp(float4(thirdTexMod.rgb,0), thirdTexMod, thirdTex.r) * _ThirdColor;
				
				float4 combined = (((mainTexAlpha + secondTexAlpha + thirdTexAlpha) * input.color).rgb, input.color.a);
				float4 lerped = lerp(mainTexAlpha+thirdTexAlpha, secondTexAlpha+thirdTexAlpha, mainTex.r);
				
				return lerped;
				//return float4((lerped * input.color).rgb, input.color.a);
			}
			
			ENDCG
		}
	} 
	
	FallBack "Diffuse"
}
