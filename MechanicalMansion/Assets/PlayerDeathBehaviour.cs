﻿using UnityEngine;
using System.Collections;
using StarterKit;
using StarterKit.LifeSystem;
using Prime31;

public class PlayerDeathBehaviour : StateMachineBehaviour 
{
    public static SafeAction ZoomInOnPlayer;
    public float startGameOverUI;
    public FloatClamp waitTime; 
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetBool("HasDeathTriggered", true);
        animator.transform.parent.GetComponent<BoxCollider2D>().isTrigger = true;
        animator.transform.parent.GetComponent<PlayerMotor>().autoMove = true;
        animator.transform.parent.GetComponent<PlayerMotor>().controllingAutoMove = false;
        animator.transform.parent.GetComponent<PlayerMotor>().moveSpeed = new Vector3(0,0,0);
        animator.transform.parent.GetComponent<ShootingAimer>().enabled = false;
        animator.transform.parent.GetComponent<Health>().enabled = false;
        //Camera.main.GetComponent<CameraKit2D>().enabled = false;
        Camera.main.GetComponent<CameraKit2D>().enablePlatformSnap = false;
        Camera.main.GetComponent<CameraWindow>().width = 0f;
        Camera.main.GetComponent<CameraWindow>().height = 0f;
        //Camera.main.GetComponent<DualForwardFocus>().enabled = false;
        //Camera.main.GetComponent<PositionLocking>().enabled = true;
        Camera.main.GetComponent<Zoom>().Begin();
        ZoomInOnPlayer.Call();
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        waitTime -= Time.deltaTime;
        if (waitTime.IsAtMin())
        {
            waitTime.Maximize();
            ApplicationX.ReloadLevel();
        }
    }

    // OnStateExit
}
