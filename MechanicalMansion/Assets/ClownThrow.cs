﻿using UnityEngine;
using System.Collections;
using StarterKit;

public class ClownThrow : StateMachineBehaviour 
{
    public GameObject bowlingPinPrefab;
    //public GameObject instance;
    public FloatClamp waitToAppear;
    public FloatClamp waitToActivate;
    bool thrownOnce;
    bool activateOnce;
    public Vector3 positionOffset;
    public Vector3 eulerAngleOffset;
	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        thrownOnce = true;
        activateOnce = true;
        waitToAppear.Maximize();
        waitToActivate.Maximize();

        Transform throwReference = animator.transform.parent.GetComponent<Clown>().throwPosition.transform;
        Enemy enemy = animator.gameObject.transform.parent.GetComponent<Enemy>();
        if (enemy != null && enemy.renderer.isVisible)
            CoroutineManager.Instance.StartCoroutine(WaitToAppearRoutine(throwReference, animator));
    }

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //waitToAppear -= Time.deltaTime;
        //waitToActivate -= Time.deltaTime;
        //if (waitToAppear.IsAtMin() && thrownOnce)
        //{
        //    thrownOnce = false;
        //    Transform throwReference = animator.transform.parent.GetComponent<Clown>().throwPosition.transform;
        //    CoroutineManager.Instance.StartCoroutine(WaitToAppearRoutine(throwReference));
        //}
        //if (waitToActivate.IsAtMin() && activateOnce)
        //{
        //    instance.GetComponent<MoveTowards>().enabled = true;
        //    instance.transform.SetParent(null);
        //    activateOnce = false;
        //}
    }

    public IEnumerator WaitToAppearRoutine(Transform throwReference, Animator animator)
    {
        yield return new WaitForSeconds(waitToAppear.max);
        if(throwReference != null)
        {
            GameObject instance = bowlingPinPrefab.Instantiate(throwReference.position = positionOffset, Quaternion.Euler(eulerAngleOffset + throwReference.rotation.eulerAngles));
            instance.transform.SetParent(throwReference);
            CoroutineManager.Instance.StartCoroutine(WaitToActivateRoutine(instance, animator));
        }
    }

    public IEnumerator WaitToActivateRoutine(GameObject instance, Animator animator)
    {
        yield return new WaitForSeconds(waitToActivate.max);
        if (instance != null)
        {
            instance.GetComponent<Translater>().direction.x = animator.transform.parent.GetComponent<EnemyMotor>().isFacingRight?1f:-1f;
            instance.GetComponent<Translater>().enabled = true;
            instance.GetComponent<LockRotations>().enabled = true;
            instance.transform.Find("Rotation").GetComponent<Rotater>().enabled = true;
            
            instance.transform.SetParent(null); 
        }
    }

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	//override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
