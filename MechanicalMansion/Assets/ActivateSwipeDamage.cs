﻿using UnityEngine;
using System.Collections;
using StarterKit;
using StarterKit.LifeSystem;

public class ActivateSwipeDamage : StateMachineBehaviour {
    public bool enter = true;
    public bool enterOn = true;
    public bool exit = true;
    public bool exitOn = false;
	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (!enter)
            return;
        if(enterOn)
        {
            On(animator);
        }
        else
        {
            Off(animator);
        }
    }

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	//override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (!exit)
            return;
        if(exitOn)
        {
            On(animator);
        }
        else
        {
            Off(animator);
        }
    }

    public void On(Animator animator)
    {
        YetiBoss yetiBoss = animator.GetComponent<YetiBoss>();
        yetiBoss.leftVer.GetComponent<DamageInflictor>().canDamage = true;
        yetiBoss.rightVer.GetComponent<DamageInflictor>().canDamage = true;
        yetiBoss.leftVer.GetComponent<AudioSource>().Play();
        yetiBoss.rightVer.GetComponent<AudioSource>().Play();
        yetiBoss.leftShock.Play();
        yetiBoss.rightShock.Play();
    }

    public void Off(Animator animator)
    {
        YetiBoss yetiBoss = animator.GetComponent<YetiBoss>();
        yetiBoss.leftVer.GetComponent<DamageInflictor>().canDamage = false;
        yetiBoss.rightVer.GetComponent<DamageInflictor>().canDamage = false;
        yetiBoss.leftVer.GetComponent<AudioSource>().Stop();
        yetiBoss.rightVer.GetComponent<AudioSource>().Stop();
        yetiBoss.leftShock.Clear();
        yetiBoss.rightShock.Clear();
        yetiBoss.leftShock.Stop();
        yetiBoss.rightShock.Stop();
    }
	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
