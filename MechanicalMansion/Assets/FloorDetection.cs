﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using StarterKit;

public class FloorDetection : MonoBehaviourPlus
{
    public LayerMask mask;
    List<GameObject> floors = new List<GameObject>();
    public SafeAction OnNoFloorsLeft;
    public new void OnEnable()
    {
        (this as MonoBehaviourPlus).OnEnable();
    }

    public new void OnDisable()
    {
        (this as MonoBehaviourPlus).OnDisable();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.transform.CompareLayerMask(mask))
            floors.AddIfDoesNotContain(other.gameObject);
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (floors.Contains(other.gameObject))
        {
            floors.Remove(other.gameObject);
            if (floors.Count == 0)
                OnNoFloorsLeft.Call();
        }
            
    }
}
