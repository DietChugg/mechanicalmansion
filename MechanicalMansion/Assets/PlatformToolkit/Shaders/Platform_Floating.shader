﻿Shader "PlatformToolkit/Floating" 
{ 
Properties 
{
	_Fog("Fog", Color) = (1,1,1,1)
	_CamDist ("Camera Distance", Float) = 15
	_Tint("Tint", Color) = (1,1,1,1)
	_LightEffect ("LightEffect", Range(0,1)) = 1
	_Lighting ("Lighting", 2D) = "black" {}
	_TopEdge ("TopEdge", 2D) = "white" {}
	_TopLeftCornerOut ("TopLeftCornerOut", 2D) = "white" {}
	_TopRightCornerOut ("TopRightCornerOut", 2D) = "white" {}
}

SubShader {
	Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
		//Cull Off
		Lighting Off
		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha
		
	Pass {  

		CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			struct appdata_t {
				float4 vertex : POSITION;
				float2 texcoord : TEXCOORD0;
				float4 color : COLOR;
			};

			struct v2f {
				float4 vertex : SV_POSITION;
				half2 texcoord : TEXCOORD0;
				float4 projPos : TEXCOORD1;
				half4 position_in_screen_space : TEXCOORD2;
				float4 color : COLOR;
			};

			float4 _Fog;
			float _CamDist;
			float4 _Tint;
			float _LightEffect;
			sampler2D _Lighting;
			sampler2D _TopEdge;
			sampler2D _TopLeftCornerOut;
			sampler2D _TopRightCornerOut;
			
			v2f vert (appdata_t v)
			{
				v2f o;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.texcoord = v.texcoord;
				o.projPos = ComputeScreenPos(o.vertex);
				o.color = v.color;

				half4 clipSpace =  mul(UNITY_MATRIX_MVP, v.vertex);
				clipSpace.xy /= clipSpace.w;
				clipSpace.xy = 0.5*(clipSpace.xy+1.0);// * _ScreenParams.xy;
				o.position_in_screen_space = clipSpace;

				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 topEdge = tex2D(_TopEdge, i.texcoord);
				half4 lighting = tex2D(_Lighting, i.position_in_screen_space)*2;
				fixed4 topLeftCornerOut = tex2D(_TopLeftCornerOut, i.texcoord);
				fixed4 topRightCornerOut = tex2D(_TopRightCornerOut, i.texcoord);
				
				float3 colorID = i.color.rgb * float3(1,2,4);
				int id = round(colorID.r + colorID.g + colorID.b);
				float section = round(i.color.a * 10);

				half depth = (_CamDist * _CamDist)/(UNITY_PROJ_COORD(i.projPos).z * UNITY_PROJ_COORD(i.projPos).z);
				if(depth > 1)
					depth = 1;
				if(depth < 0)
					depth = 1;
					
				float4 finalTexture;

				if(section == 1) //Corners
				{
					if(id == 0)
						finalTexture = topLeftCornerOut;
					else if(id == 4)
						finalTexture = topRightCornerOut;
				}
				else
					finalTexture = topEdge;


				half4 litFog = lerp(_Fog, finalTexture * (lighting*finalTexture + lighting) * _Tint ,depth);
				half4 fog = lerp(_Fog, finalTexture  * _Tint ,depth);
				half4 finalLit = lerp (litFog, fog, _LightEffect);
				finalTexture = float4(finalLit.rgb,finalTexture.a * _Tint.a);


				//half4 fog = lerp(_Fog,finalTexture * (lighting*finalTexture + lighting) * _Tint ,depth);
				//finalTexture = float4(fog.rgb,finalTexture.a * _Tint.a);

				return finalTexture;
			}
		ENDCG
	}
}
	//CustomEditor "PlatformFloatingShaderGUI"
}
