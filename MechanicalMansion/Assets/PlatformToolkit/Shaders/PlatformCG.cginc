
half4 Blur(sampler2D _Texture, half2 uv, float blurAmount)
{
	return tex2D(_Texture, uv);

	//I don't know, I'll get this working some day...
	half4 color = half4(0,0,0,0);

	color += tex2D(_Texture, uv + half2(-5, -5) * blurAmount) * 0.025;
	color += tex2D(_Texture, uv + half2(-4, -4) * blurAmount) * 0.05f;
	color += tex2D(_Texture, uv + half2(-3, -3) * blurAmount) * 0.09f;
	color += tex2D(_Texture, uv + half2(-2, -2) * blurAmount) * 0.12f;
	color += tex2D(_Texture, uv + half2(-1, -1) * blurAmount) * 0.15;
	color += tex2D(_Texture, uv) * 0.16;
	color += tex2D(_Texture, uv + half2(1, 1) * blurAmount) * 0.15;
	color += tex2D(_Texture, uv + half2(2, 2) * blurAmount) * 0.12f;
	color += tex2D(_Texture, uv + half2(3, 3) * blurAmount) * 0.09f;
	color += tex2D(_Texture, uv + half2(4, 4) * blurAmount) * 0.05f;
	color += tex2D(_Texture, uv + half2(5, 5) * blurAmount) * 0.025;

	return color;
}