﻿Shader "PlatformToolkit/Island" 
{ 
Properties 
{
	//[HideInInspector]
	_Fog("Fog", Color) = (1,1,1,1)
	_CamDist ("Camera Distance", Float) = 15
	_Tint("Tint", Color) = (1,1,1,1)

	_Lighting ("Lighting", 2D) = "black" {}
	_Edges ("Edges", 2D) = "white" {}

	_TopLeftCornerOut ("TopLeftCornerOut", 2D) = "white" {}
	_TopRightCornerOut ("TopRightCornerOut", 2D) = "white" {}
	_BottomLeftCornerOut ("BottomLeftCornerOut", 2D) = "white" {}
	_BottomRightCornerOut ("BottomRightCornerOut", 2D) = "white" {}

	_TopLeftCornerIn ("TopLeftCornerIn", 2D) = "white" {}
	_TopRightCornerIn ("TopRightCornerIn", 2D) = "white" {}
	_BottomLeftCornerIn ("BottomLeftCornerIn", 2D) = "white" {}
	_BottomRightCornerIn ("BottomRightCornerIn", 2D) = "white" {}

	_Fill ("Fill", 2D) = "white" {}
}

SubShader {
	Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
		//Cull Off
		Lighting Off
		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha
		
		Pass {  
		CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#include "UnityCG.cginc"
			#include "PlatformCG.cginc"

			struct appdata_t {
				half4 vertex : POSITION;
				half2 texcoord : TEXCOORD0;
				half4 color : COLOR;
			};

			struct v2f {
				half4 vertex : SV_POSITION;
				half2 texcoord : TEXCOORD0;
				half4 projPos : TEXCOORD1;
				half4 position_in_screen_space : TEXCOORD2;
				half4 color : COLOR;
			};

			sampler2D _Lighting;
			sampler2D _Fill;
			half4 _Fill_ST;
			half4 _Fog;
			half4 _Tint;
			float _CamDist;

			v2f vert (appdata_t v)
			{
				v2f o;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.texcoord = TRANSFORM_TEX(v.texcoord, _Fill);
				o.projPos = ComputeScreenPos(o.vertex);

				half4 clipSpace =  mul(UNITY_MATRIX_MVP, v.vertex);
				clipSpace.xy /= clipSpace.w;
				clipSpace.xy = 0.5*(clipSpace.xy+1.0);// * _ScreenParams.xy;
				o.position_in_screen_space = clipSpace;

				o.color = v.color;
				return o;
			}

			half4 frag (v2f i) : SV_Target
			{
				half4 lighting = tex2D(_Lighting, i.position_in_screen_space)*2;
				half4 fill = tex2D(_Fill, i.texcoord);
				half section = round(i.color.a * 10);
				half depth = (_CamDist * _CamDist)/(UNITY_PROJ_COORD(i.projPos).z * UNITY_PROJ_COORD(i.projPos).z);
				half blurDepth = 0.0005f/depth;
				if(depth > 1 || depth < 0)
				{
					depth = 1;
					//lighting = float4(0.5,0.5,0.5,1);
				}
				//depth = 19.2/UNITY_PROJ_COORD(i.projPos).z;
				
				half4 finalTexture = float4(0,0,0,0);
					
				if(section == 0)
				{
					finalTexture = Blur(_Fill, i.texcoord, blurDepth);
					half4 fog = lerp(_Fog,finalTexture * (lighting*finalTexture + lighting) * _Tint ,depth);
					finalTexture = float4(fog.rgb,fill.a * _Tint.a);
				}

				return finalTexture;
			}
			ENDCG
		}
	
	Pass {  
		CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			#include "PlatformCG.cginc"

			struct appdata_t {
				half4 vertex : POSITION;
				half2 texcoord : TEXCOORD0;
				half4 color : COLOR;
			};

			struct v2f {
				half4 vertex : SV_POSITION;
				half2 texcoord : TEXCOORD0;
				half4 projPos : TEXCOORD1;
				half4 position_in_screen_space : TEXCOORD2;
				half4 color : COLOR;
			};

			half4 _Fog;
			half4 _Tint;
			float _CamDist;

			sampler2D _Lighting;
			sampler2D _Edges;

			sampler2D _TopLeftCornerOut;
			sampler2D _TopRightCornerOut;
			sampler2D _BottomLeftCornerOut;
			sampler2D _BottomRightCornerOut;
			sampler2D _TopLeftCornerIn;
			sampler2D _TopRightCornerIn;
			sampler2D _BottomLeftCornerIn;
			sampler2D _BottomRightCornerIn;
			
			v2f vert (appdata_t v)
			{
				v2f o;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.texcoord = v.texcoord;
				o.projPos = ComputeScreenPos(o.vertex);

				half4 clipSpace =  mul(UNITY_MATRIX_MVP, v.vertex);
				clipSpace.xy /= clipSpace.w;
				clipSpace.xy = 0.5*(clipSpace.xy+1.0);// * _ScreenParams.xy;
				o.position_in_screen_space = clipSpace;

				o.color = v.color;
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{			
				half4 lighting = tex2D(_Lighting, i.position_in_screen_space) *2;

				half3 colorID = i.color.rgb * float3(1,2,4);
				int id = round(colorID.r + colorID.g + colorID.b);
				half section = round(i.color.a * 10);

				half depth = (_CamDist * _CamDist)/(UNITY_PROJ_COORD(i.projPos).z * UNITY_PROJ_COORD(i.projPos).z);
				//half depth = 19.2/UNITY_PROJ_COORD(i.projPos).z;
				half blurDepth = 0.0005f/depth;

				if(depth > 1 || depth < 0)
				{
					depth = 1;
				}
					
				half4 finalTexture = half4(0,0,0,0);

				if(section == 0) //Fill
				{
					return float4(0,0,0,0);
				}
				else if(section == 1) //Corners
				{
					if(id == 0)
						finalTexture = Blur(_TopLeftCornerOut, i.texcoord, blurDepth);
					else if(id == 4)
						finalTexture = Blur(_TopRightCornerOut, i.texcoord, blurDepth);
					else if(id == 2)
						finalTexture = Blur(_BottomLeftCornerOut, i.texcoord, blurDepth);
					else if(id == 6)
						finalTexture = Blur(_BottomRightCornerOut, i.texcoord, blurDepth);
					else if(id == 1)
						finalTexture = Blur(_TopLeftCornerIn, i.texcoord, blurDepth);
					else if(id == 5)
						finalTexture = Blur(_TopRightCornerIn, i.texcoord, blurDepth);
					else if(id == 3)
						finalTexture = Blur(_BottomLeftCornerIn, i.texcoord, blurDepth);
					else //if(id == 7)
						finalTexture = Blur(_BottomRightCornerIn, i.texcoord, blurDepth);
				}
				else
					finalTexture = Blur(_Edges, i.texcoord, blurDepth);

				half4 fog = lerp(_Fog,finalTexture * (lighting*finalTexture + lighting) * _Tint ,depth);
				finalTexture = float4(fog.rgb,finalTexture.a * _Tint.a);

				return finalTexture;
			}
		ENDCG
	}
}
}
