﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using StarterKit;

namespace PlatformToolkit
{
    public static class Edge
    {
        public static Mesh Mesh(List<Point> points, float width, Vector3 origin, EdgeType edgeType, bool isIsland)
        {
            Color vertexColor = Color.red;
            Vector2 uvVerticalBounds = Vector2.zero;
            switch(edgeType)
            {
                case EdgeType.Top:
                    uvVerticalBounds = new Vector2(0.75f, 1);
                    break;
                case EdgeType.Right:
                    uvVerticalBounds = new Vector2(0.5f, 0.75f);
                    break;
                case EdgeType.Bottom:
                    uvVerticalBounds = new Vector2(0.25f, 0);
                    break;
                case EdgeType.Left:
                    uvVerticalBounds = new Vector2(0.25f, 0.5f);
                    break;
                default:
                    break;
            }

            if(!isIsland)
                uvVerticalBounds = new Vector2(0, 1);

            Mesh mesh = new Mesh();
            List<Vector3> _verts = new List<Vector3>();
            List<Vector2> _uvs = new List<Vector2>();
            List<Vector3> _normals = new List<Vector3>();
            List<int> _tris = new List<int>();
            List<Color> _colors = new List<Color>();

            float totalLength = 0;
            List<float> lengths = new List<float>();
            for (int i = 0; i < points.Count - 1; i++)
            {
                totalLength += Vector2.Distance(points[i]._center, points[i + 1]._center) * (1/(width*2));
                lengths.Add(totalLength);
            }

            float uvMultiplier = (Mathf.Round(totalLength) / 4);
            if (uvMultiplier < 0.25f)
                uvMultiplier = 0.25f;

            _verts.Add(points[0]._bottomVert.ToVector3(origin.z));
            _verts.Add(Vector2X.MidPoint(points[0]._topVert, points[0]._bottomVert).ToVector3(origin.z));
            _verts.Add(points[0]._topVert.ToVector3(origin.z));

            _uvs.Add(new Vector2(0, uvVerticalBounds.x));
            _uvs.Add(new Vector2(0, (uvVerticalBounds.y + uvVerticalBounds.x)/2));
            _uvs.Add(new Vector2(0, uvVerticalBounds.y));

            _normals.Add(Vector3.forward);
            _normals.Add(Vector3.forward);
            _normals.Add(Vector3.forward);

            _colors.Add(vertexColor);
            _colors.Add(vertexColor);
            _colors.Add(vertexColor);

            for (int i = 0; i < points.Count-1; i++)
            {
                _verts.Add(points[i + 1]._bottomVert.ToVector3(origin.z));
                _verts.Add(Vector2X.MidPoint(points[i + 1]._topVert, points[i + 1]._bottomVert).ToVector3(origin.z));
                _verts.Add(points[i + 1]._topVert.ToVector3(origin.z));

                float uvSpace = (lengths[i] / totalLength) * uvMultiplier;
                _uvs.Add(new Vector2(uvSpace, uvVerticalBounds.x));
                _uvs.Add(new Vector2(uvSpace, (uvVerticalBounds.y + uvVerticalBounds.x)/2));
                _uvs.Add(new Vector2(uvSpace, uvVerticalBounds.y));

                _normals.Add(Vector3.forward);
                _normals.Add(Vector3.forward);
                _normals.Add(Vector3.forward);

                _colors.Add(vertexColor);
                _colors.Add(vertexColor);
                _colors.Add(vertexColor);

                int offset = i * 3;
                _tris.Add(offset + 0);
                _tris.Add(offset + 1);
                _tris.Add(offset + 4);

                _tris.Add(offset + 0);
                _tris.Add(offset + 4);
                _tris.Add(offset + 3);

                _tris.Add(offset + 1);
                _tris.Add(offset + 2);
                _tris.Add(offset + 5);

                _tris.Add(offset + 1);
                _tris.Add(offset + 5);
                _tris.Add(offset + 4);
            }

            mesh.vertices = _verts.ToArray();
            mesh.uv = _uvs.ToArray();
            mesh.normals = _normals.ToArray();
            mesh.colors = _colors.ToArray();
            mesh.triangles = _tris.ToArray();
            return mesh;
        }
    }

    public enum EdgeType
    {
        Top,
        Right,
        Bottom,
        Left
    }
}
