﻿using UnityEngine;
using System.Collections;
using StarterKit;

namespace PlatformToolkit
{
    public class Corner
    {

        public bool _setIn;
		public bool _setOut;
        public Vector2_2 _inEdge;
        public Vector2_2 _outEdge;
        public Vector2 _inEdgeCenter;
        public Vector2 _outEdgeCenter;
        public CornerType _cornerType;
        public EdgeType _outEdgeType;
        public Color _vertexColor;
        public Vector2 _cornerInOverride;		
		public Vector2 _cornerOutOverride;

		private Vector2_3 _centerPoint;
        private Vector2_2 _refPoints;
        
        private Vector2_3 _p0;
        private Vector2_3 _p1; 
        private Vector2_3 _p2;
        private float _pointAngle;
        //private bool _isEndCap;
        private EndCap _endCap;

        public Corner() { }
		public Corner(Vector2_3 p0, Vector2_3 p1, Vector2_3 p2, float pointAngle, float width, EndCap endCap, bool setIn, bool setOut, Vector2 cornerInOverride, Vector2 cornerOutOverride, CornerType prevCornerType, EdgeType prevEdgeType)
        {
            _centerPoint = p1;
            _refPoints = new Vector2_2(p1.v1, p2.v1);
            _p0 = p0;
            _p1 = p1;
            _p2 = p2;
            _setIn = setIn;
            _setOut = setOut;
            _pointAngle = pointAngle;
            _endCap = endCap;
            _cornerInOverride = cornerInOverride;
            _cornerOutOverride = cornerOutOverride;

            if (_endCap != EndCap.None)
            {
                if (_endCap == EndCap.First)
                {
                    _cornerType = CornerType.FIRSTCAP;
                    _vertexColor = new Color(0, 0, 0, 0.1f);
                }
                else
                {
                    _cornerType = CornerType.LASTCAP;
                    _vertexColor = new Color(0, 0, 1, 0.1f);
                }
            }
            else
                FindCornerType(prevCornerType, prevEdgeType);
        }

		void FindCornerType(CornerType prevCornerType, EdgeType prevEdgeType)
        {
            switch (prevEdgeType)
            {
                case EdgeType.Top:
                    if (prevCornerType == CornerType.BLI || prevCornerType == CornerType.TLO)
                    {
                        if (_refPoints.v1.y > _refPoints.v0.y)
                        {
                            _cornerType = CornerType.BRI;
                            _outEdgeType = EdgeType.Left;
                            _vertexColor = new Color(1, 1, 1, 0.1f);
                        }
                        else
                        {
                            _cornerType = CornerType.TRO;
                            _outEdgeType = EdgeType.Right;
                            _vertexColor = new Color(0, 0, 1, 0.1f);
                        }
                    }
                    else if (prevCornerType == CornerType.TRO || prevCornerType == CornerType.BRI)
                    {
                        if (_refPoints.v1.y > _refPoints.v0.y)
                        {
                            _cornerType = CornerType.BLI;
                            _outEdgeType = EdgeType.Right;
                            _vertexColor = new Color(1, 1, 0, 0.1f);
                        }
                        else
                        {
                            _cornerType = CornerType.TLO;
                            _outEdgeType = EdgeType.Left;
                            _vertexColor = new Color(0, 0, 0, 0.1f);
                        }
                    }
                    else
                        _cornerType = CornerType.TLO;//Debug.Log("This... This shouldn't happen.");
                    break;
                case EdgeType.Right:
                    if (prevCornerType == CornerType.TRO || prevCornerType == CornerType.TLI)
                    {
                        if (_refPoints.v1.x > _refPoints.v0.x)
                        {
                            _cornerType = CornerType.BLI;
                            _outEdgeType = EdgeType.Top;
                            _vertexColor = new Color(1, 1, 0, 0.1f);
                        }
                        else
                        {
                            _cornerType = CornerType.BRO;
                            _outEdgeType = EdgeType.Bottom;
                            _vertexColor = new Color(0, 1, 1, 0.1f);
                        }
                    }
                    else if (prevCornerType == CornerType.BLI || prevCornerType == CornerType.BRO)
                    {
                        if (_refPoints.v1.x > _refPoints.v0.x)
                        {
                            _cornerType = CornerType.TLI;
                            _outEdgeType = EdgeType.Bottom;
                            _vertexColor = new Color(1, 0, 0, 0.1f);
                        }
                        else
                        {
                            _cornerType = CornerType.TRO;
                            _outEdgeType = EdgeType.Top;
                            _vertexColor = new Color(0, 0, 1, 0.1f);
                        }
                    }
                    else
                        Debug.Log("This... This shouldn't happen.");
                    break;
                case EdgeType.Bottom:
                    if (prevCornerType == CornerType.TRI || prevCornerType == CornerType.BRO)
                    {
                        if (_refPoints.v1.y > _refPoints.v0.y)
                        {
                            _cornerType = CornerType.BLO;
                            _outEdgeType = EdgeType.Left;
                            _vertexColor = new Color(0, 1, 0, 0.1f);
                        }
                        else
                        {
                            _cornerType = CornerType.TLI;
                            _outEdgeType = EdgeType.Right;
                            _vertexColor = new Color(1, 0, 0, 0.1f);
                        }
                    }
                    else if (prevCornerType == CornerType.TLI || prevCornerType == CornerType.BLO)
                    {
                        if (_refPoints.v1.y > _refPoints.v0.y)
                        {
                            _cornerType = CornerType.BRO;
                            _outEdgeType = EdgeType.Right;
                            _vertexColor = new Color(0, 1, 1, 0.1f);
                        }
                        else
                        {
                            _cornerType = CornerType.TRI;
                            _outEdgeType = EdgeType.Left;
                            _vertexColor = new Color(1, 0, 1, 0.1f);
                        }
                    }
                    else
                        Debug.Log("This... This shouldn't happen.");
                    break;
                case EdgeType.Left:
                     if (prevCornerType == CornerType.BRI || prevCornerType == CornerType.BLO)
                    {
                        if (_refPoints.v1.x > _refPoints.v0.x)
                        {
                            _cornerType = CornerType.TLO;
                            _outEdgeType = EdgeType.Top;
                            _vertexColor = new Color(0, 0, 0, 0.1f);
                        }
                        else
                        {
                            _cornerType = CornerType.TRI;
                            _outEdgeType = EdgeType.Bottom;
                            _vertexColor = new Color(1, 0, 1, 0.1f);
                        }
                    }
                     else if (prevCornerType == CornerType.TRI || prevCornerType == CornerType.TLO)
                    {
                        if (_refPoints.v1.x > _refPoints.v0.x)
                        {
                            _cornerType = CornerType.BLO;
                            _outEdgeType = EdgeType.Bottom;
                            _vertexColor = new Color(0, 1, 0, 0.1f);
                        }
                        else
                        {
                            _cornerType = CornerType.BRI;
                            _outEdgeType = EdgeType.Top;
                            _vertexColor = new Color(1, 1, 1, 0.1f);
                        }
                    }
                    else
                        Debug.Log("This... This shouldn't happen.");
                    break;
            }
        }
        
        void SetPoints()
        {
            float adjustedWidth = (Vector2.Distance(_p1.v2, _p1.v0) * Mathf.Cos(_pointAngle / 2));
            if (_cornerType == CornerType.TLO || _cornerType == CornerType.TRO || _cornerType == CornerType.BLO || _cornerType == CornerType.BRO)
            {
                _inEdge.v0 = _p1.v0;
                if(_setIn)
                    _inEdge.v1 = Vector2X.PointOnLine(_p1.v2, _p0.v2, adjustedWidth);
                else
                    _inEdge.v1 = _cornerInOverride;
                _inEdgeCenter = Vector2X.MidPoint(_inEdge.v0, _inEdge.v1);

                _outEdge.v0 = _p1.v0;
                if(_setOut)
                    _outEdge.v1 = Vector2X.PointOnLine(_p1.v2, _p2.v2, adjustedWidth);
                else
                    _outEdge.v1 = _cornerOutOverride;
                _outEdgeCenter = Vector2X.MidPoint(_outEdge.v0, _outEdge.v1);
            }
            else if (_cornerType == CornerType.FIRSTCAP || _cornerType == CornerType.LASTCAP)
            {
                //switch (_endCap)
                //{
                //    case EndCap.First:
                        _inEdge.v0 = _p0.v0;
                        if (_setIn)
                            _inEdge.v1 = _p0.v2;
                        else
                            _inEdge.v1 = _cornerInOverride;
                        _inEdgeCenter = Vector2X.MidPoint(_inEdge.v0, _inEdge.v1);

                        _outEdge.v0 = _p2.v0;
                        if (_setOut)
                            _outEdge.v1 = _p2.v2;
                        else
                            _outEdge.v1 = _cornerOutOverride;
                        _outEdgeCenter = Vector2X.MidPoint(_outEdge.v0, _outEdge.v1);
                //        break;
                //    case EndCap.Last:
                //        _inEdge.v0 = _p1.v0;
                //        if (_setIn)
                //            _inEdge.v1 = Vector2X.PointOnLine(_p1.v2, _p0.v2, adjustedWidth);
                //        else
                //            _inEdge.v1 = _cornerInOverride;
                //        _inEdgeCenter = Vector2X.MidPoint(_inEdge.v0, _inEdge.v1);

                //        _outEdge.v0 = _p1.v0;
                //        if (_setOut)
                //            _outEdge.v1 = Vector2X.PointOnLine(_p1.v2, _p2.v2, adjustedWidth);
                //        else
                //            _outEdge.v1 = _cornerOutOverride;
                //        _outEdgeCenter = Vector2X.MidPoint(_outEdge.v0, _outEdge.v1);
                //        break;
                //}
            }
            else
            {
                if(_setIn)
                    _inEdge.v0 = Vector2X.PointOnLine(_p1.v0, _p0.v0, adjustedWidth);
                else
                    _inEdge.v0 = _cornerInOverride;
                _inEdge.v1 = _p1.v2;
                _inEdgeCenter = Vector2X.MidPoint(_inEdge.v0, _inEdge.v1);

                if(_setOut)
                    _outEdge.v0 = Vector2X.PointOnLine(_p1.v0, _p2.v0, adjustedWidth);
                else
                    _outEdge.v0 = _cornerOutOverride;
                _outEdge.v1 = _p1.v2;
                _outEdgeCenter = Vector2X.MidPoint(_outEdge.v0, _outEdge.v1);
            }
        }

        public Mesh Mesh()
        {
            SetPoints();
            Mesh mesh = new Mesh();

            Vector3[] verts = new Vector3[4];

            if (_cornerType == CornerType.TLO || _cornerType == CornerType.TRO || _cornerType == CornerType.BLO || _cornerType == CornerType.BRO)
            {
                verts[0] = _centerPoint.v0.ToVector3();
                verts[1] = _inEdge.v1.ToVector3();
                verts[2] = _outEdge.v1.ToVector3();
                verts[3] = _centerPoint.v2.ToVector3();
            }
            else if (_cornerType == CornerType.FIRSTCAP || _cornerType == CornerType.LASTCAP)
            {
                //switch (_endCap)
                //{
                //    case EndCap.First:
                        verts[0] = _inEdge.v0.ToVector3();
                        verts[1] = _inEdge.v1.ToVector3();
                        verts[2] = _outEdge.v0.ToVector3();
                        verts[3] = _outEdge.v1.ToVector3();
                        //break;
                //    case EndCap.Last:
                //        verts[0] = _centerPoint.v0.ToVector3();
                //        verts[1] = _inEdge.v1.ToVector3();
                //        verts[2] = _outEdge.v1.ToVector3();
                //        verts[3] = _centerPoint.v2.ToVector3();
                //        break;
                //}
            }
            else
            {
                verts[0] = _centerPoint.v0.ToVector3();
                verts[1] = _inEdge.v0.ToVector3();
                verts[2] = _outEdge.v0.ToVector3();
                verts[3] = _centerPoint.v2.ToVector3();
            }

            Vector3[] normals = new Vector3[4];
            Color[] colors = new Color[4];

            for (int i = 0; i < 4; i++)
            {
                normals[i] = Vector3.forward;
                colors[i] = _vertexColor;
            }

            Vector2[] uvs = new Vector2[4];

            if(_cornerType == CornerType.TLO || _cornerType == CornerType.BRI)
			{
                uvs[0] = new Vector2(1, 0);
                uvs[1] = new Vector2(0, 0);
				uvs[2] = new Vector2(1, 1);
                uvs[3] = new Vector2(0, 1);
			}
            else if(_cornerType == CornerType.TRO || _cornerType == CornerType.BLI)
            {
                uvs[0] = new Vector2(0, 0);
                uvs[1] = new Vector2(0, 1);
                uvs[2] = new Vector2(1, 0);
                uvs[3] = new Vector2(1, 1);
            }
            else if (_cornerType == CornerType.BLO || _cornerType == CornerType.TRI)
            {
                uvs[0] = new Vector2(1, 1);
                uvs[1] = new Vector2(1, 0);
                uvs[2] = new Vector2(0, 1);
                uvs[3] = new Vector2(0, 0);
            }
            else if (_cornerType == CornerType.BRO || _cornerType == CornerType.TLI)
            {
                uvs[0] = new Vector2(0, 1);
                uvs[1] = new Vector2(1, 1);
                uvs[2] = new Vector2(0, 0);
                uvs[3] = new Vector2(1, 0);
            }
            else if(_cornerType == CornerType.FIRSTCAP || _cornerType == CornerType.LASTCAP)
            {
                uvs[0] = new Vector2(0, 0);
                uvs[1] = new Vector2(0, 1);
                uvs[2] = new Vector2(1, 0);
                uvs[3] = new Vector2(1, 1);
            }

            int[] tris = 
			{
				0,1,3,
                0,3,2
			};

            mesh.vertices = verts;
            mesh.normals = normals;
            mesh.colors = colors;
            mesh.uv = uvs;
            mesh.triangles = tris;

            return mesh;
        }
    }

    public enum CornerType
    {
        TLO,
        TRO,
        BLO,
        BRO,
        TLI,
        TRI,
        BLI,
        BRI,
        FIRSTCAP,
        LASTCAP
    }

    public enum EndCap
    {
        None,
        First,
        Last
    }
}
