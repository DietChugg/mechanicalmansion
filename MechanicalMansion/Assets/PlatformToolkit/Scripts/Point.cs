using UnityEngine;
using System.Collections;

namespace PlatformToolkit
{
	[System.Serializable]
	public class Point
	{
        public Vector2 _widthOffset = Vector2.one;
		public bool _setTop;
		public bool _setBottom;
		public bool _setCornerIn = true;
		public bool _setCornerOut = true;
		public Vector2 _cornerInOverride = new Vector2(20000,20000);		
		public Vector2 _cornerOutOverride = new Vector2(20000,20000);
		
		public Vector2 _topVert = Vector2.zero;
		public Vector2 _center = Vector2.zero;
		public Vector2 _bottomVert = Vector2.zero;
        public bool _isCorner;
        public bool _flipCornerState;
        public float _angle;
		public Corner _corner;
        public EndCap _endCap;

		private float _width;
		private Vector2_3 _inPoint;
		private Vector2_3 _outPoint;

		public Vector2_3 SetPoints
		{
			get
			{
				return new Vector2_3(_bottomVert, _center, _topVert);
			}
		}

		private Vector2_3 _refPoints;

		//public void Set(Vector2_2 refPoints, float width, bool isFirst)
		public void Set(Vector2_3 p0, Vector2_3 p1, float width, bool isFirst)
		{
            _center = p0.v1;//refPoints.v0;
            _inPoint = p0;
            _outPoint = p1;
            _width = width;

            float adjustedWidth = width / (Mathf.Sin(_angle * 0.5f));

			Vector2 slope = isFirst ? Vector2X.Slope(p0.v1, p1.v1) : Vector2X.Slope(p1.v1, p0.v1);
            Vector2 perpSlope = Vector2X.Perpendicular(slope);

            if (_setBottom)
                _bottomVert = Vector2X.PointOnLine(_center, _center - perpSlope, adjustedWidth);
            if (_setTop)
                _topVert = Vector2X.PointOnLine(_center, _center + perpSlope, adjustedWidth);

            if (isFirst)
            {
                _outPoint.v1 = Vector2X.PointOnLine(p0.v1, p1.v1, width);
                _outPoint.v0 = Vector2X.PointOnLine(_outPoint.v1, _outPoint.v1 - perpSlope, adjustedWidth);
                _outPoint.v2 = Vector2X.PointOnLine(_outPoint.v1, _outPoint.v1 + perpSlope, adjustedWidth);

                _inPoint.v1 = Vector2X.PointOnLine(_outPoint.v1, p0.v1, width * 2);
                _inPoint.v0 = Vector2X.PointOnLine(_inPoint.v1, _inPoint.v1 - perpSlope, adjustedWidth);
                _inPoint.v2 = Vector2X.PointOnLine(_inPoint.v1, _inPoint.v1 + perpSlope, adjustedWidth);
            }
            else
            {
                _inPoint.v1 = Vector2X.PointOnLine(p0.v1, p1.v1, width);
                _inPoint.v0 = Vector2X.PointOnLine(_inPoint.v1, _inPoint.v1 - perpSlope, adjustedWidth);
                _inPoint.v2 = Vector2X.PointOnLine(_inPoint.v1, _inPoint.v1 + perpSlope, adjustedWidth);

                _outPoint.v1 = Vector2X.PointOnLine(_inPoint.v1, p0.v1, width * 2);
                _outPoint.v0 = Vector2X.PointOnLine(_outPoint.v1, _outPoint.v1 - perpSlope, adjustedWidth);
                _outPoint.v2 = Vector2X.PointOnLine(_outPoint.v1, _outPoint.v1 + perpSlope, adjustedWidth);
            }

            _endCap = isFirst ? EndCap.First : EndCap.Last;
		}

//		public void Set(Vector2_3 refPoints, float width)
		public void Set(Vector2_3 p0, Vector2_3 p1, Vector2_3 p2, float width)
		{
			this._refPoints = new Vector2_3(p0.v1, p1.v1, p2.v1);
			_center = p1.v1;//refPoints.v1;
			_inPoint = p0;
			_outPoint = p2;
			_width = width;
            GetAngle();

            float adjustedWidth = width / (Mathf.Sin(_angle * 0.5f));

			Vector2 perpSlopeIn = Vector2X.Perpendicular( Vector2X.Slope(p0.v1, _center));
			Vector2 perpSlopeOut = Vector2X.Perpendicular(Vector2X.Slope(_center, p2.v1));

			if(_setBottom)
				_bottomVert = AveragePoint(_center, perpSlopeIn, perpSlopeOut, adjustedWidth, false);
			if(_setTop)
				_topVert = AveragePoint(_center, perpSlopeIn, perpSlopeOut, adjustedWidth, true);
		}

		Vector2 AveragePoint(Vector2 center, Vector2 slopeIn, Vector2 slopeOut, float width, bool isTopVert)
		{
			if (!isTopVert) {slopeIn = -slopeIn; slopeOut = -slopeOut;}

			return Vector2X.PointOnLine(
				center, 
				Vector2X.MidPoint(Vector2X.PointOnLine(center, center + slopeIn, width), Vector2X.PointOnLine(center, center + slopeOut, width)), 
				width);
		}

        private void GetAngle()
        {
            float a = Vector2.Distance(_refPoints.v0, _refPoints.v1);
            float b = Vector2.Distance(_refPoints.v1, _refPoints.v2);
            float c = Vector2.Distance(_refPoints.v0, _refPoints.v2);

            _angle = Mathf.Acos((Mathf.Pow(c, 2) - Mathf.Pow(a, 2) - Mathf.Pow(b, 2)) / (-2 * a * b));

            if (_angle == float.NaN)
                _angle = Mathf.PI;
        }

		public bool IsCorner(float minAngle, CornerType prevCornerType, EdgeType prevEdgeType)
		{
            _isCorner = (minAngle * Mathf.Deg2Rad) > _angle;

            _corner = new Corner(_inPoint, SetPoints, _outPoint, _angle, _width, _endCap, _setCornerIn, _setCornerOut, _cornerInOverride, _cornerOutOverride, prevCornerType, prevEdgeType);

            //if(_cornerInOverride == Vector2.zero && _isCorner)
            //    Debug.Log("This shouldn't happen, type = " + _corner._cornerType.ToString());
				
            if (_angle == float.NaN || _angle == 0)
                _isCorner = false;

            if (_endCap != EndCap.None)
                _isCorner = true;

            _isCorner = !_flipCornerState ? _isCorner : !_isCorner;
            return _isCorner;
		} 
	}
}
