﻿using UnityEngine;

namespace PlatformToolkit
{
	[System.Serializable]
	public struct Vector2_2 
	{
		public Vector2 v0;
		public Vector2 v1;
		public static Vector2_2 zero = new Vector2_2(Vector2.zero, Vector2.zero);
		public static Vector2_2 one = new Vector2_2(Vector2.one, Vector2.one);

		public Vector2_2(Vector2 v0, Vector2 v1)
		{
			this.v0 = v0;
			this.v1 = v1;
		}
	}

	[System.Serializable]
	public struct Vector2_3 
	{
		public Vector2 v0;
		public Vector2 v1;
		public Vector2 v2;
		public static Vector2_3 zero = new Vector2_3(Vector2.zero, Vector2.zero, Vector2.zero);
		public static Vector2_3 one = new Vector2_3(Vector2.one, Vector2.one, Vector2.one);

		public Vector2_3(Vector2 v0, Vector2 v1, Vector2 v2)
		{
			this.v0 = v0;
			this.v1 = v1;
			this.v2 = v2;
		}
	}

	[System.Serializable]
	public struct Vector2_4
	{
		public Vector2_2 v0;
		public Vector2_2 v1;
		public static Vector2_4 zero = new Vector2_4(Vector2_2.zero, Vector2_2.zero);
		public static Vector2_4 one = new Vector2_4(Vector2_2.one, Vector2_2.one);

		public Vector2_4(Vector2_2 v0, Vector2_2 v1)
		{
			this.v0 = v0;
			this.v1 = v1;
		}

		public Vector2_4(Vector2 v0, Vector2 v1, Vector2 v2, Vector2 v3)
		{
			this.v0.v0 = v0;
			this.v0.v1 = v1;
			this.v1.v0 = v2;
			this.v1.v1 = v3;
		}
	}
}
