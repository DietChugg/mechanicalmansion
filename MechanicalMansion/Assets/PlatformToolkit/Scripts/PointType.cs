﻿namespace PlatformToolkit
{
		public enum PointType
		{
				Segment,
				CornerTLO,
				CornerTRO,
				CornerBLO,
				CornerBRO,
				CornerTLI,
				CornerTRI,
				CornerBLI,
				CornerBRI,
				Start,
				End
		}
}
