﻿using UnityEngine;
using System.Collections.Generic;

namespace PlatformToolkit
{
	public static class FillMesh
	{

		public static Mesh Mesh (List<Point> points, float width, Vector3 origin) 
		{
            List<Vector2> verts2D = new List<Vector2>();
            List<Vector3> verts = new List<Vector3>();
            List<Vector2> uvs = new List<Vector2>();
			List<Vector3> normals = new List<Vector3> ();
			List<Color> colors = new List<Color> ();
			

			foreach(Point point in points)
                verts2D.Add(point._center);

            Triangulator tr = new Triangulator(verts2D.ToArray());
			int[] indices = tr.Triangulate();

            for (int i = 0; i < verts2D.Count; i++)
            {
                verts.Add(new Vector3(verts2D[i].x, verts2D[i].y, origin.z));
				normals.Add(-Vector3.forward);
                uvs.Add(verts2D[i] * (1/(width*4)));
				colors.Add(new Color(float.NaN,0,0,0));
			}
			
			Mesh mesh = new Mesh();
            mesh.vertices = verts.ToArray();
			mesh.normals = normals.ToArray();
			mesh.triangles = indices;
            mesh.uv = uvs.ToArray();
			mesh.colors = colors.ToArray();
			return mesh;
		}
	}
}