using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
using System.Collections;
public class PlatformSettings : ScriptableObject
{
    public Color _fog = Color.gray;
    public bool _drawWireframe;
    public bool _drawCage = true;
    public float _snapDistance = 0.5f;

    //Default colors from https://color.adobe.com/oddend-color-theme-2181/
    public Color _controlsColor = new Color32(1, 176, 240, 255);
    public Color _controlsSelectedColor = new Color32(255, 53, 139, 255);
    public Color _controlsBorderColor = Color.black;
    public Color _controlsModifiedColor = new Color32(174, 238, 0, 255);

    private static PlatformSettings instance;
    public static PlatformSettings Instance
    {
        get
        {
            if (instance == null)
            {
                System.Type myType = typeof(PlatformSettings);
                instance = AssetDatabase.LoadAssetAtPath<PlatformSettings>("Assets/PlatformToolkit/PlatformSettings.asset");
                if (instance == null)
                {
                    instance = CreateAsset();
                }
            }
            return instance;
        }
    }

    public static PlatformSettings CreateAsset()
    {
        instance = CreateInstance<PlatformSettings>();
		System.Type myType = typeof(PlatformSettings);
        AssetDatabase.CreateAsset(instance, "Assets/"+myType.FullName+".asset");
		MonoScript script = MonoScript.FromScriptableObject(instance as ScriptableObject);

		string scriptPath = AssetDatabase.GetAssetPath(script);
		string folderPath = scriptPath.Substring(0,scriptPath.Length - ("/Scripts" + myType.FullName + ".cs").Length);

		AssetDatabase.MoveAsset("Assets/" + myType.FullName + ".asset",
		                        folderPath + myType.FullName + ".asset");
        AssetDatabase.SaveAssets();
		AssetDatabase.Refresh();
        return instance;
    }

    [MenuItem("PlatformToolkit/Settings")]
    static void DrawHierarchyDataMenuItem()
    {
        Selection.activeObject = (UnityEngine.Object)PlatformSettings.Instance;
    }
}
#endif
