using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using StarterKit;

namespace PlatformToolkit
{
	[ExecuteInEditMode]
	[RequireComponent(typeof(MeshFilter))]
	[RequireComponent(typeof(MeshRenderer))]
	[RequireComponent(typeof(PolygonCollider2D))]    
	public class Platform : MonoBehaviour
	{
		[HideInInspector]
		public bool buildMesh;
        [HideInInspector]
        public bool isClosed = true;
        [HideInInspector]
		public float width = 0.5f;
        [HideInInspector]
		public float cornerAngle = 130f;
		[HideInInspector]
		public List<Point> _points = new List<Point> ();

		private MeshFilter mf;
		private Mesh mesh;
        private PolygonCollider2D collider;

        private Point firstCornerInEdge;
        private Point lastCornerOutEdge;
        private EdgeType lastCornerEdgeType;

        //Editor Stuff
        public bool showManipulatorPoints;
        public bool useSnapping;
        public int sortOrder = 0;
        public string sortLayer = "Default";

        void Update()
        {
            //if (buildMesh)
            //    BuildMesh();
        }

		[ContextMenu("Build Mesh")]
		public void BuildMesh()
		{
            //Declare local variables
			Vector3 initPos = transform.position;            
            List<Mesh> meshes = new List<Mesh>();
            List<List<Point>> edges = new List<List<Point>>();
            List<EdgeType> edgeTypes = new List<EdgeType>();
            int segmentsIndex = 0;
            CornerType prevCornerType = CornerType.BRO;
            EdgeType prevEdgeType = EdgeType.Bottom;
            //Initialized variables
            edges.Add(new List<Point>());            
            edgeTypes.Add(EdgeType.Bottom);
            //End local variables
            
			Initialize();

			for(int i = 0; i < _points.Count; i++)
			{
                bool firstIndex = (i == 0);
                bool lastIndex = (i == _points.Count-1);
                int previousIndex = firstIndex ? _points.Count - 1 : i - 1;
                int nextIndex = lastIndex ? 0 : i + 1;
                    
                if ((!isClosed && firstIndex) || (!isClosed && lastIndex)) //If you are NOT closed and at the First or Last
                {
                    int pairIndex = firstIndex ? i + 1 : i - 1;
                    _points[i].Set(_points[i].SetPoints, _points[pairIndex].SetPoints, width, firstIndex);
                }
                else //All middle points
                    _points[i].Set(_points[previousIndex].SetPoints, _points[i].SetPoints, _points[nextIndex].SetPoints, width);

                if (    (isClosed && _points[i].IsCorner(cornerAngle, prevCornerType, prevEdgeType)) 
                    || (!isClosed && lastIndex && _points[i].IsCorner(cornerAngle, prevCornerType, prevEdgeType)) 
                    || (!isClosed && firstIndex && _points[i].IsCorner(cornerAngle, prevCornerType, prevEdgeType)) ) //If the point is a corner
                {
					Corner corner = _points[i]._corner;
                    meshes.Add(_points[i]._corner.Mesh());
                    Point inPoint = new Point { _topVert = corner._inEdge.v1, _bottomVert = corner._inEdge.v0, _center = corner._inEdgeCenter};
                    Point outPoint = new Point { _topVert = corner._outEdge.v1, _bottomVert = corner._outEdge.v0, _center = corner._outEdgeCenter };
                    prevCornerType = corner._cornerType;
                    prevEdgeType = corner._outEdgeType;
                    edgeTypes.Add(prevEdgeType);

                    //if(_points[previousIndex]._corner != null && _points[previousIndex]._isCorner)
                    //{
                    //    if(Vector2.Distance(_points[previousIndex]._center, _points[i]._center) <= width*3)
                    //    {
                    //        _points[previousIndex]._setCornerIn = false;
                    //        _points[previousIndex]._setCornerOut = false; 
                    //        _points[previousIndex]._setBottom = false; 
                    //        _points[i]._setCornerIn = false;
                    //        _points[i]._setCornerOut = false;  
                    //        _points[i]._setBottom = false;                           
                    //        Vector2 top = Vector2X.MidPoint(_points[previousIndex]._corner._outEdge.v1, _points[i]._corner._inEdge.v1);
                    //        Vector2 bottom = Vector2X.MidPoint(_points[previousIndex]._corner._outEdge.v0, _points[i]._corner._inEdge.v0);
                            
                    //        _points[previousIndex]._corner._outEdge.v1 = top;
                    //        _points[previousIndex]._corner._outEdge.v0 = bottom; 
                    //        _points[previousIndex]._corner._inEdge.v0 = bottom; 
                    //        _points[previousIndex]._bottomVert = bottom; 
                    //        _points[i]._corner._inEdge.v1 = top;
                    //        _points[i]._corner._inEdge.v0 = bottom; 
                    //        _points[i]._corner._inEdge.v0 = bottom;    
                    //        _points[i]._bottomVert = bottom;  
                                                    
                    //    }
                    //}
                    //  else
                    //  {
                        if (firstIndex)
                            firstCornerInEdge = inPoint;
                        else if (lastIndex)
                        {
                            lastCornerOutEdge = outPoint;
                            lastCornerEdgeType = corner._outEdgeType;
                        }
                        
                        edges[segmentsIndex].Add(inPoint);//Add in-edge of corner
                        
                        segmentsIndex++;
                        edges.Add(new List<Point>());
                        
                        edges[segmentsIndex].Add(outPoint);//Add out-edge of corner
                    //  }
                }
                else //If the point is NOT a corner
                    edges[segmentsIndex].Add(_points[i]);
			}

            if(isClosed)
            {
                segmentsIndex++;
                edges.Add(new List<Point>());
                edges[segmentsIndex].Add(lastCornerOutEdge);
                edges[segmentsIndex].Add(firstCornerInEdge);
                edgeTypes.Add(lastCornerEdgeType);
                meshes.Add(FillMesh.Mesh(_points, width, this.transform.position));
            }

            for (int i = 0; i < edges.Count; i++)
            {
                meshes.Add(Edge.Mesh(edges[i], width, this.transform.position, edgeTypes[i], isClosed));
            }

            mesh = CombineMesh(meshes);
			mf.mesh = mesh;

			transform.position = initPos;

            SetCollider();
		}

		Mesh CombineMesh (List<Mesh> meshes)
		{
			Mesh newMesh = new Mesh ();

			CombineInstance[] combine = new CombineInstance[meshes.Count];
            for (int i = 0; i < meshes.Count; i++)
            {
                combine [i].mesh = meshes[i];
			    combine [i].transform = transform.localToWorldMatrix;
            }

			newMesh.CombineMeshes (combine);
            newMesh.Optimize();
            newMesh.RecalculateBounds();
            newMesh.RecalculateNormals();
            newMesh.MarkDynamic();
			return newMesh;
		}

		void Initialize()
		{
			if (mf == null)
				mf = GetComponent<MeshFilter> ();
            if(collider == null)
                collider = GetComponent<PolygonCollider2D>();
			if (mesh == null)
				mesh = new Mesh ();

			mesh.Clear ();
			transform.position = Vector3.zero;
                        
            if(_points.Count == 0)
                SetDefaultPoints();
		}
        
        void SetDefaultPoints()
        {
                Point p0 = new Point { _center = new Vector2(-1, -1), _setTop = true, _setBottom = true };
                Point p1 = new Point { _center = new Vector2(-1, 1), _setTop = true, _setBottom = true };
                Point p2 = new Point { _center = new Vector2(1, 1), _setTop = true, _setBottom = true };
                Point p3 = new Point { _center = new Vector2(1, -1), _setTop = true, _setBottom = true };

				p0.Set(p3.SetPoints, p0.SetPoints, p1.SetPoints, width);
				p1.Set(p0.SetPoints, p1.SetPoints, p2.SetPoints, width);
				p2.Set(p1.SetPoints, p2.SetPoints, p3.SetPoints, width);
				p3.Set(p2.SetPoints, p3.SetPoints, p0.SetPoints, width);

                _points.Add(p0);
                _points.Add(p1);
                _points.Add(p2);
                _points.Add(p3);
        }
        
        void SetCollider()
        {
            int colliderPointLength = isClosed ? _points.Count : _points.Count * 2;
            Vector2[] pointCenters2D = new Vector2[colliderPointLength];
            for (int i = 0; i < _points.Count; i++)
                pointCenters2D[i] = Vector2X.PointOnLine(_points[i]._center, _points[i]._topVert, width/3);

            if(!isClosed)
            {
                for (int i = 0; i < _points.Count; i++)
                    pointCenters2D[_points.Count + i] = _points[_points.Count - 1 - i]._bottomVert;
            }
            collider.points = pointCenters2D;
        }
	}
}
