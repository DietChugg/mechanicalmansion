﻿//using System;
using UnityEngine;
using UnityEditor;

public class PlatformIslandShaderGUI : ShaderGUI
{
    MaterialProperty fog = null;

    MaterialProperty fill = null;
    MaterialProperty tint = null;

    MaterialProperty topEdge = null;
    MaterialProperty rightEdge = null;
    MaterialProperty bottomEdge = null;
    MaterialProperty leftEdge = null;

    MaterialProperty topEdgeShort = null;
    MaterialProperty rightEdgeShort = null;
    MaterialProperty bottomEdgeShort = null;
    MaterialProperty leftEdgeShort = null;

    MaterialProperty topLeftCornerOut = null;
    MaterialProperty topRightCornerOut = null;
    MaterialProperty bottomLeftCornerOut = null;
    MaterialProperty bottomRightCornerOut = null;

    MaterialProperty topLeftCornerIn = null;
    MaterialProperty topRightCornerIn = null;
    MaterialProperty bottomLeftCornerIn = null;
    MaterialProperty bottomRightCornerIn = null;

    MaterialProperty currentObjectPickerProp = null;
    Material material;

	public void FindProperties (MaterialProperty[] props)
	{
        fog = FindProperty("_Fog", props);
        fog.colorValue = PlatformSettings.Instance._fog;
        tint = FindProperty("_Tint", props);

        fill = FindProperty("_Fill", props);

        topEdge = FindProperty("_TopEdge", props);
        rightEdge = FindProperty("_RightEdge", props);
        bottomEdge = FindProperty("_BottomEdge", props);
        leftEdge = FindProperty("_LeftEdge", props);

        topEdgeShort = FindProperty("_TopEdgeShort", props);
        rightEdgeShort = FindProperty("_RightEdgeShort", props);
        bottomEdgeShort = FindProperty("_BottomEdgeShort", props);
        leftEdgeShort = FindProperty("_LeftEdgeShort", props);

        topLeftCornerOut = FindProperty("_TopLeftCornerOut", props);
        topRightCornerOut = FindProperty("_TopRightCornerOut", props);
        bottomLeftCornerOut = FindProperty("_BottomLeftCornerOut", props);
        bottomRightCornerOut = FindProperty("_BottomRightCornerOut", props);

        topLeftCornerIn = FindProperty("_TopLeftCornerIn", props);
        topRightCornerIn = FindProperty("_TopRightCornerIn", props);
        bottomLeftCornerIn = FindProperty("_BottomLeftCornerIn", props);
        bottomRightCornerIn = FindProperty("_BottomRightCornerIn", props);
	}

    public override void OnGUI(MaterialEditor materialEditor, MaterialProperty[] properties)
    {
        FindProperties(properties); // MaterialProperties can be animated so we do not cache them but fetch them every event to ensure animated values are updated correctly

        material = materialEditor.target as Material;

        Vector2 fillTiling = new Vector2(fill.textureScaleAndOffset.x, fill.textureScaleAndOffset.y);

        EditorGUI.BeginChangeCheck();
        {
            float horizontalDivisor = 4.5f;
            float verticalDivisor = 1.05f;

            materialEditor.ColorProperty(tint, "Tint");

            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            GUILayout.Label("Outside Corners", GUILayout.Width((EditorGUIUtility.currentViewWidth - 50) / (horizontalDivisor/2)));
            GUILayout.FlexibleSpace();
            GUILayout.Label("Inside Corners", GUILayout.Width((EditorGUIUtility.currentViewWidth - 50) / (horizontalDivisor / 2)));
            GUILayout.FlexibleSpace();
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            DrawTexture(topLeftCornerOut, "Top Left", horizontalDivisor, horizontalDivisor, false);
            DrawTexture(topRightCornerOut, "Top Right", horizontalDivisor, horizontalDivisor, false);
            GUILayout.FlexibleSpace();
            DrawTexture(topLeftCornerIn, "Top Left", horizontalDivisor, horizontalDivisor, false);
            DrawTexture(topRightCornerIn, "Top Right", horizontalDivisor, horizontalDivisor, false);
            GUILayout.FlexibleSpace();
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            DrawTexture(bottomLeftCornerOut, "Bottom Left", horizontalDivisor, horizontalDivisor, false);
            DrawTexture(bottomRightCornerOut, "Bottom Right", horizontalDivisor, horizontalDivisor, false);
            GUILayout.FlexibleSpace();
            DrawTexture(bottomLeftCornerIn, "Bottom Left", horizontalDivisor, horizontalDivisor, false);
            DrawTexture(bottomRightCornerIn, "Bottom Right", horizontalDivisor, horizontalDivisor, false);
            GUILayout.FlexibleSpace();
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            DrawTexture(topEdge, "Top", verticalDivisor, horizontalDivisor, true);
            GUILayout.FlexibleSpace();
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            DrawTexture(rightEdge, "Right", verticalDivisor, horizontalDivisor, true);
            GUILayout.FlexibleSpace();
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            DrawTexture(bottomEdge, "Bottom", verticalDivisor, horizontalDivisor, true);
            GUILayout.FlexibleSpace();
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            DrawTexture(leftEdge, "Left", verticalDivisor, horizontalDivisor, true);
            GUILayout.FlexibleSpace();
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            GUILayout.Label("ShortEdges", GUILayout.Width((EditorGUIUtility.currentViewWidth - 50) / (horizontalDivisor/4)));
            GUILayout.FlexibleSpace();
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            DrawTexture(topEdgeShort, "Top", horizontalDivisor, horizontalDivisor, false);
            GUILayout.Space(EditorGUIUtility.currentViewWidth/ 50);
            DrawTexture(rightEdgeShort, "Right", horizontalDivisor, horizontalDivisor, false);
            GUILayout.Space(EditorGUIUtility.currentViewWidth / 50);
            DrawTexture(bottomEdgeShort, "Bottom", horizontalDivisor, horizontalDivisor, false);
            GUILayout.Space(EditorGUIUtility.currentViewWidth / 50);
            DrawTexture(leftEdgeShort, "Left", horizontalDivisor, horizontalDivisor, false);
            GUILayout.FlexibleSpace();
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            DrawTexture(fill, "Fill", verticalDivisor, verticalDivisor, true);
            GUILayout.FlexibleSpace();
            EditorGUILayout.EndHorizontal();

            GUILayout.Label("Tiling");

            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            GUILayout.Label("Tiling");
            GUILayout.FlexibleSpace();
            GUILayout.Label("X");
            fillTiling.x = EditorGUILayout.FloatField(fill.textureScaleAndOffset.x, GUILayout.Width((EditorGUIUtility.currentViewWidth - 50) / (horizontalDivisor)));
            GUILayout.Label("Y");
            fillTiling.y = EditorGUILayout.FloatField(fill.textureScaleAndOffset.y, GUILayout.Width((EditorGUIUtility.currentViewWidth - 50) / (horizontalDivisor)));
            GUILayout.FlexibleSpace();
            EditorGUILayout.EndHorizontal();
        }

        if(EditorGUI.EndChangeCheck())
        {
            fill.textureScaleAndOffset = new Vector4(fillTiling.x, fillTiling.y, 0, 0);
        }

        if (Event.current.commandName == "ObjectSelectorUpdated")
        {
            if (currentObjectPickerProp != null)
            {
                currentObjectPickerProp.textureValue = EditorGUIUtility.GetObjectPickerObject() as Texture;
            }
        }

        if (Event.current.commandName == "ObjectSelectorClosed")
        {
            currentObjectPickerProp = null;
        }
    }

    void DrawTexture(MaterialProperty prop, string defaultText, float width, float height, bool label)
    {
        EditorGUILayout.BeginVertical();
        if(label)
            GUILayout.Label(defaultText);

        float windowWidth = (EditorGUIUtility.currentViewWidth - 50);
        Rect contentRect = GUILayoutUtility.GetRect( windowWidth / width, windowWidth / height );
        DropAreaGUI(contentRect, prop);

        bool button = prop.textureValue != null ? GUI.Button(contentRect, prop.textureValue) : GUI.Button(contentRect, defaultText);
        if (button)
        {
            EditorGUIUtility.PingObject(prop.textureValue);
            EditorGUIUtility.ShowObjectPicker<Texture>(prop.textureValue, false, material.name, 0);
            currentObjectPickerProp = prop;
        }
        EditorGUILayout.EndVertical();
    }

    public void DropAreaGUI(Rect drop_area, MaterialProperty prop)
    {
        Event evt = Event.current;

        switch (evt.type)
        {
            case EventType.DragUpdated:
            case EventType.DragPerform:
                if (!drop_area.Contains(evt.mousePosition))
                    return;

                DragAndDrop.visualMode = DragAndDropVisualMode.Copy;

                if (evt.type == EventType.DragPerform)
                {
                    DragAndDrop.AcceptDrag();

                    foreach (Object dragged_object in DragAndDrop.objectReferences)
                    {
                        if(dragged_object as Texture != null)
                            prop.textureValue = dragged_object as Texture;
                    }
                }
                break;
        }
    }
}