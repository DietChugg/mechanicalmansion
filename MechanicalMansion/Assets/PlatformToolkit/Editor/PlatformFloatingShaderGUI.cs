﻿//using System;
using UnityEngine;
using UnityEditor;

public class PlatformFloatingShaderGUI : ShaderGUI
{
    MaterialProperty fog = null;

    MaterialProperty fill = null;
    MaterialProperty tint = null;

    MaterialProperty topEdge = null;
    MaterialProperty topLeftCornerOut = null;
    MaterialProperty topRightCornerOut = null;

    MaterialProperty currentObjectPickerProp = null;
    Material material;

	public void FindProperties (MaterialProperty[] props)
	{
        fog = FindProperty("_Fog", props);
        fog.colorValue = PlatformSettings.Instance._fog;
        tint = FindProperty("_Tint", props);

        topEdge = FindProperty("_TopEdge", props);
        topLeftCornerOut = FindProperty("_TopLeftCornerOut", props);
        topRightCornerOut = FindProperty("_TopRightCornerOut", props);
	}

    public override void OnGUI(MaterialEditor materialEditor, MaterialProperty[] properties)
    {
        FindProperties(properties); // MaterialProperties can be animated so we do not cache them but fetch them every event to ensure animated values are updated correctly

        material = materialEditor.target as Material;

        EditorGUI.BeginChangeCheck();
        {
            float horizontalDivisor = 4.5f;
            float verticalDivisor = 1.05f;

            materialEditor.ColorProperty(tint, "Tint");

            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            GUILayout.Label("Left End", GUILayout.Width((EditorGUIUtility.currentViewWidth - 50) / (horizontalDivisor/2)));
            GUILayout.FlexibleSpace();
            GUILayout.Label("Right End", GUILayout.Width((EditorGUIUtility.currentViewWidth - 50) / (horizontalDivisor / 2)));
            GUILayout.FlexibleSpace();
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            DrawTexture(topLeftCornerOut, "Left End", horizontalDivisor * 0.5f, horizontalDivisor * 0.5f, false);
            GUILayout.FlexibleSpace();
            DrawTexture(topRightCornerOut, "Right End", horizontalDivisor * 0.5f, horizontalDivisor * 0.5f, false);
            GUILayout.FlexibleSpace();
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            DrawTexture(topEdge, "Top", verticalDivisor, horizontalDivisor, true);
            GUILayout.FlexibleSpace();
            EditorGUILayout.EndHorizontal();
        }

        if(EditorGUI.EndChangeCheck())
        {
        }

        if (Event.current.commandName == "ObjectSelectorUpdated")
        {
            if (currentObjectPickerProp != null)
            {
                currentObjectPickerProp.textureValue = EditorGUIUtility.GetObjectPickerObject() as Texture;
            }
        }

        if (Event.current.commandName == "ObjectSelectorClosed")
        {
            currentObjectPickerProp = null;
        }
    }

    void DrawTexture(MaterialProperty prop, string defaultText, float width, float height, bool label)
    {
        EditorGUILayout.BeginVertical();
        if(label)
            GUILayout.Label(defaultText);

        float windowWidth = (EditorGUIUtility.currentViewWidth - 50);
        Rect contentRect = GUILayoutUtility.GetRect( windowWidth / width, windowWidth / height );
        DropAreaGUI(contentRect, prop);

        bool button = prop.textureValue != null ? GUI.Button(contentRect, prop.textureValue) : GUI.Button(contentRect, defaultText);
        if (button)
        {
            EditorGUIUtility.PingObject(prop.textureValue);
            EditorGUIUtility.ShowObjectPicker<Texture>(prop.textureValue, false, material.name, 0);
            currentObjectPickerProp = prop;
        }
        EditorGUILayout.EndVertical();
    }

    public void DropAreaGUI(Rect drop_area, MaterialProperty prop)
    {
        Event evt = Event.current;

        switch (evt.type)
        {
            case EventType.DragUpdated:
            case EventType.DragPerform:
                if (!drop_area.Contains(evt.mousePosition))
                    return;

                DragAndDrop.visualMode = DragAndDropVisualMode.Copy;

                if (evt.type == EventType.DragPerform)
                {
                    DragAndDrop.AcceptDrag();

                    foreach (Object dragged_object in DragAndDrop.objectReferences)
                    {
                        if(dragged_object as Texture != null)
                            prop.textureValue = dragged_object as Texture;
                    }
                }
                break;
        }
    }
}