﻿using UnityEngine;
using UnityEditor;
using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using StarterKit;

namespace PlatformToolkit
{
	[CustomEditor(typeof(Platform))]
	public class PlatformEditor : Editor 
	{
		Platform platform;
        Texture2D lineTexture_Thick;
        Texture2D lineTexture_Medium;
        Texture2D lineTexture_Thin;
        Texture2D gizmo;
        Texture2D componentIcon;

		Vector2 transformOffset;
        SerializedProperty _useSnapping;
        SerializedProperty _showManipulatorPoints;

        int activePoint;

        void OnEnable()
        {
            platform = (Platform)target;

            platform.buildMesh = false;
            platform.showManipulatorPoints = false;
            platform.useSnapping = false;

            EditorUtility.SetSelectedWireframeHidden(platform.GetComponent<Renderer>(), !PlatformSettings.Instance._drawWireframe);
            //SceneView.currentDrawingSceneView.SetSceneViewShaderReplace(Shader.Find("PlatformToolkit/Island"), "Vertex color unlit");
            lineTexture_Thick = AssetDatabase.LoadAssetAtPath<Texture2D>("Assets/Gizmos/LineTexture_Thick.tif");
            lineTexture_Medium = AssetDatabase.LoadAssetAtPath<Texture2D>("Assets/Gizmos/LineTexture_Medium.tif");
            lineTexture_Thin = AssetDatabase.LoadAssetAtPath<Texture2D>("Assets/Gizmos/LineTexture_Thin.tif");
            gizmo = AssetDatabase.LoadAssetAtPath<Texture2D>("Assets/Gizmos/Platform Gizmo.tif");
            componentIcon = AssetDatabase.LoadAssetAtPath<Texture2D>("Assets/Gizmos/Platform Icon.tif");
            SetCustomGizmo();

            _useSnapping = serializedObject.FindProperty("useSnapping");
            _showManipulatorPoints = serializedObject.FindProperty("showManipulatorPoints");
        }

        void OnDisable()
        {
            //SceneView.currentDrawingSceneView.SetSceneViewShaderReplace(Shader.Find("PlatformToolkit/Island"), "Vertex color unlit");
        }

        void SetCustomGizmo()
        {
            Texture2D tex = gizmo;
            Type editorGUIUtilityType = typeof(EditorGUIUtility);
            BindingFlags bindingFlags = BindingFlags.InvokeMethod | BindingFlags.Static | BindingFlags.NonPublic;
            object[] args = new object[] { platform.gameObject, tex };
            editorGUIUtilityType.InvokeMember("SetIconForObject", bindingFlags, null, null, args);
        }

        public override void OnInspectorGUI()
        {
            if (!platform.buildMesh)
                EditorGUILayout.HelpBox("Press 'Tab' to enter Edit Mode", MessageType.Info);
            else
                EditorGUILayout.HelpBox("\n Press '1' to toggle \n Point Modifier controls. \n \n Press '3' to toggle \n Point Snapping.", MessageType.Info);
        }

		void OnSceneGUI()
        {
            if (Selection.objects.Length > 1)
                return;
            //EditorGUI.BeginChangeCheck();
            Event e = Event.current;
            switch (e.type)
            {
                case EventType.keyDown:
                    if (e.character == '\t')//e.keyCode == KeyCode.Tab   e.modifiers == EventModifiers.Shift
                    {
                        platform.buildMesh = !platform.buildMesh;
                        if (platform.buildMesh)
                        {
                            //UnityEditor.SceneView.currentDrawingSceneView.in2DMode = true;
                            
			                //SceneView.currentDrawingSceneView.AlignViewToObject(Camera.main.transform);
                            
                            //if(Camera.current != null)
                            //{
                            //    Camera.current.fieldOfView = 30;
                            //    Repaint();
                            //}
                            
                            Tools.current = Tool.Move;
                        }
                    }
                    break;
            }

            if (!platform.buildMesh)
                return;
            else
                platform.BuildMesh();

            transformOffset = new Vector2(platform.transform.position.x, platform.transform.position.y);
            List<Vector3> positions = new List<Vector3>();
            for (int i = 0; i < platform._points.Count; i++)
                positions.Add(platform._points[i]._center.ToVector3() + platform.transform.position);

            if (platform.isClosed && platform._points.Count >= 4)
                positions.Add(platform._points[0]._center.ToVector3() + platform.transform.position);

            Handles.color = PlatformSettings.Instance._controlsBorderColor;
            Handles.DrawAAPolyLine(lineTexture_Thick, positions.ToArray());

            float largeRadius = HandleUtility.GetHandleSize(platform.transform.position) * 0.25f;
            float largeRadiusBorder = largeRadius * 1.2f;
            float smallRadius = HandleUtility.GetHandleSize(platform.transform.position) * 0.125f;
            float smallRadiusBorder = smallRadius * 1.2f;

            for (int i = 0; i < platform._points.Count; i++)
            {
                bool firstIndex = (i == 0);
                bool lastIndex = (i == platform._points.Count - 1);
                int previousIndex = firstIndex ? platform._points.Count - 1 : i - 1;
                int nextIndex = lastIndex ? 0 : i + 1;

                if (platform.showManipulatorPoints)
                {
                    Handles.DrawAAPolyLine(lineTexture_Thin, platform._points[i]._topVert + transformOffset, platform._points[i]._center + transformOffset);
                    Handles.DrawAAPolyLine(lineTexture_Thin, platform._points[i]._bottomVert + transformOffset, platform._points[i]._center + transformOffset);
                    if (platform._points[i]._corner != null && platform._points[i]._isCorner)
                    {
                        CornerType _cornerType = platform._points[i]._corner._cornerType;
                        Vector2 inPoint = Vector2.zero;
                        Vector2 outPoint = Vector2.zero;
                        if (_cornerType == CornerType.TLO || _cornerType == CornerType.TRO || _cornerType == CornerType.BLO || _cornerType == CornerType.BRO)
                        {
                            inPoint = platform._points[i]._corner._inEdge.v1;
                            outPoint = platform._points[i]._corner._outEdge.v1;
                        }
                        else
                        {
                            inPoint = platform._points[i]._corner._inEdge.v0;
                            outPoint = platform._points[i]._corner._outEdge.v0;
                        }

                        Handles.DrawAAPolyLine(lineTexture_Thin, inPoint + transformOffset, platform._points[i]._center + transformOffset);
                        Handles.DrawAAPolyLine(lineTexture_Thin, outPoint + transformOffset, platform._points[i]._center + transformOffset);
                    }
                }

                if (PlatformSettings.Instance._drawCage && platform.showManipulatorPoints)
                {
                    if (platform._points[i]._corner != null && platform._points[i]._isCorner)
                    {
                        CornerType _cornerType = platform._points[i]._corner._cornerType;
                        Vector2 inPoint = Vector2.zero;
                        Vector2 outPoint = Vector2.zero;
                        Vector2 topBottom = Vector2.zero;
                        if (_cornerType == CornerType.TLO || _cornerType == CornerType.TRO || _cornerType == CornerType.BLO || _cornerType == CornerType.BRO)
                        {
                            inPoint = platform._points[i]._corner._inEdge.v1;
                            outPoint = platform._points[i]._corner._outEdge.v1;
                            topBottom = platform._points[i]._topVert;
                        }
                        else
                        {
                            inPoint = platform._points[i]._corner._inEdge.v0;
                            outPoint = platform._points[i]._corner._outEdge.v0;
                            topBottom = platform._points[i]._bottomVert;
                        }

                        Handles.DrawAAPolyLine(lineTexture_Thin, inPoint + transformOffset, topBottom + transformOffset);
                        Handles.DrawAAPolyLine(lineTexture_Thin, outPoint + transformOffset, topBottom + transformOffset);

                        if (platform.isClosed)
                        {
                            if (platform._points[nextIndex]._isCorner)
                            {
                                Handles.DrawAAPolyLine(lineTexture_Thin, platform._points[i]._corner._outEdge.v1 + transformOffset, platform._points[nextIndex]._corner._inEdge.v1 + transformOffset);
                                Handles.DrawAAPolyLine(lineTexture_Thin, platform._points[i]._corner._outEdge.v0 + transformOffset, platform._points[nextIndex]._corner._inEdge.v0 + transformOffset);
                            }
                            else
                            {
                                Handles.DrawAAPolyLine(lineTexture_Thin, platform._points[i]._corner._outEdge.v1 + transformOffset, platform._points[nextIndex]._topVert + transformOffset);
                                Handles.DrawAAPolyLine(lineTexture_Thin, platform._points[i]._corner._outEdge.v0 + transformOffset, platform._points[nextIndex]._bottomVert + transformOffset);
                            }
                        }
                    }
                    else if (platform._points[nextIndex]._corner != null && platform._points[nextIndex]._isCorner)
                    {
                        if (platform.isClosed)
                        {
                            Handles.DrawAAPolyLine(lineTexture_Thin, platform._points[i]._topVert + transformOffset, platform._points[nextIndex]._corner._inEdge.v1 + transformOffset);
                            Handles.DrawAAPolyLine(lineTexture_Thin, platform._points[i]._bottomVert + transformOffset, platform._points[nextIndex]._corner._inEdge.v0 + transformOffset);
                        }
                    }
                    else
                    {
                        if (platform.isClosed)
                        {
                            Handles.DrawAAPolyLine(lineTexture_Thin, platform._points[i]._topVert + transformOffset, platform._points[nextIndex]._topVert + transformOffset);
                            Handles.DrawAAPolyLine(lineTexture_Thin, platform._points[i]._bottomVert + transformOffset, platform._points[nextIndex]._bottomVert + transformOffset);
                        }
                    }
                }

                float snapValue = PlatformSettings.Instance._snapDistance;

                Vector2 offsetTop = new Vector2(platform._points[i]._center.x - platform._points[i]._topVert.x, platform._points[i]._center.y - platform._points[i]._topVert.y);
                Vector2 offsetBottom = new Vector2(platform._points[i]._center.x - platform._points[i]._bottomVert.x, platform._points[i]._center.y - platform._points[i]._bottomVert.y);
                Vector2 offsetIn = Vector2.zero;
                Vector2 offsetOut = Vector2.zero;
                if (platform._points[i]._corner != null)
                {
                    offsetIn = new Vector2(platform._points[i]._center.x - platform._points[i]._corner._inEdge.v1.x, platform._points[i]._center.y - platform._points[i]._corner._inEdge.v1.y);
                    offsetOut = new Vector2(platform._points[i]._center.x - platform._points[i]._corner._outEdge.v1.x, platform._points[i]._center.y - platform._points[i]._corner._outEdge.v1.y);
                }
                MyHandles.DragHandleResult dhResult;

                Vector3 dragPoint = MyHandles.DragHandle(platform._points[i]._center.ToVector3(0) + platform.transform.position, largeRadiusBorder, Handles.SphereCap, PlatformSettings.Instance._controlsSelectedColor, platform.useSnapping, snapValue, out dhResult) - platform.transform.position;
                platform._points[i]._center = dragPoint;
                platform._points[i]._topVert = dragPoint.ToVector2() - offsetTop;
                platform._points[i]._bottomVert = dragPoint.ToVector2() - offsetBottom;
                if (platform._points[i]._corner != null)
                {
                    platform._points[i]._corner._inEdge.v1 = dragPoint.ToVector2() - offsetIn;
                    platform._points[i]._corner._outEdge.v1 = dragPoint.ToVector2() - offsetOut;
                }

                switch (dhResult)
                {
                    case MyHandles.DragHandleResult.LMBDoubleClick:
                        activePoint = i;
                        platform._points[i]._flipCornerState = !platform._points[i]._flipCornerState;
                        break;
                    case MyHandles.DragHandleResult.RMBClick:
                        if (platform._points.Count <= 4 && platform.isClosed)
                            Debug.LogError("You cannot have less than 4 Control Points in a Closed Platform");
                        else
                            platform._points.RemoveAt(i);
                        break;
                }

                if (platform.showManipulatorPoints)
                {
                    platform._points[i]._topVert = MyHandles.DragHandle(platform._points[i]._topVert + transformOffset, smallRadiusBorder, Handles.SphereCap, PlatformSettings.Instance._controlsSelectedColor, false, snapValue, out dhResult) - platform.transform.position;
                    switch (dhResult)
                    {
                        case MyHandles.DragHandleResult.LMBDrag:
                            platform._points[i]._setTop = false;
                            break;
                        case MyHandles.DragHandleResult.RMBClick:
                            platform._points[i]._setTop = true;
                            break;
                    }

                    platform._points[i]._bottomVert = MyHandles.DragHandle(platform._points[i]._bottomVert + transformOffset, smallRadiusBorder, Handles.SphereCap, PlatformSettings.Instance._controlsSelectedColor, false, snapValue, out dhResult) - platform.transform.position;
                    switch (dhResult)
                    {
                        case MyHandles.DragHandleResult.LMBDrag:
                            platform._points[i]._setBottom = false;
                            break;
                        case MyHandles.DragHandleResult.RMBClick:
                            platform._points[i]._setBottom = true;
                            break;
                    }

                    if (platform._points[i]._corner != null && platform._points[i]._isCorner)
                    {
                        CornerType _cornerType = platform._points[i]._corner._cornerType;
                        Vector2 inPoint = Vector2.one;
                        Vector2 outPoint = Vector2.one;

                        if (_cornerType == CornerType.TLO || _cornerType == CornerType.TRO || _cornerType == CornerType.BLO || _cornerType == CornerType.BRO)
                        {
                            inPoint = platform._points[i]._corner._inEdge.v1;
                            outPoint = platform._points[i]._corner._outEdge.v1;
                        }
                        else
                        {
                            inPoint = platform._points[i]._corner._inEdge.v0;
                            outPoint = platform._points[i]._corner._outEdge.v0;
                        }

                        platform._points[i]._cornerInOverride = MyHandles.DragHandle(inPoint + transformOffset, smallRadiusBorder, Handles.SphereCap, PlatformSettings.Instance._controlsSelectedColor, false, snapValue, out dhResult) - platform.transform.position;
                        switch (dhResult)
                        {
                            case MyHandles.DragHandleResult.LMBDrag:
                                platform._points[i]._setCornerIn = false;
                                break;
                            case MyHandles.DragHandleResult.RMBClick:
                                platform._points[i]._setCornerIn = true;
                                break;
                        }

                        platform._points[i]._cornerOutOverride = MyHandles.DragHandle(outPoint + transformOffset, smallRadiusBorder, Handles.SphereCap, PlatformSettings.Instance._controlsSelectedColor, false, snapValue, out dhResult) - platform.transform.position;
                        switch (dhResult)
                        {
                            case MyHandles.DragHandleResult.LMBDrag:
                                platform._points[i]._setCornerOut = false;
                                break;
                            case MyHandles.DragHandleResult.RMBClick:
                                platform._points[i]._setCornerOut = true;
                                break;
                        }
                    }
                }

                if (platform.isClosed || (!platform.isClosed && !lastIndex))
                {
                    Vector2 startPoint = (platform.isClosed && platform._points[i]._corner != null && platform._points[i]._isCorner) ? Vector2X.MidPoint(platform._points[i]._corner._outEdge.v0, platform._points[i]._corner._outEdge.v1) : platform._points[i]._center;
                    Vector2 nextPoint = (platform.isClosed && platform._points[nextIndex]._corner != null && platform._points[nextIndex]._isCorner) ? Vector2X.MidPoint(platform._points[nextIndex]._corner._inEdge.v0, platform._points[nextIndex]._corner._inEdge.v1) : platform._points[nextIndex]._center;
                    Vector3 addPointPosition = Vector3X.MidPoint(startPoint.ToVector3() + platform.transform.position, nextPoint.ToVector3() + platform.transform.position);
                    MyHandles.DragHandle(addPointPosition, smallRadiusBorder, Handles.SphereCap, PlatformSettings.Instance._controlsSelectedColor, false, snapValue, out dhResult);
                    int addIndex = (nextIndex == 0) ? platform._points.Count : nextIndex;
                    switch (dhResult)
                    {
                        case MyHandles.DragHandleResult.LMBClick:
                            Point newPoint = new Point();
                            newPoint._setTop = newPoint._setBottom = true;

                            newPoint.Set(platform._points[addIndex - 1].SetPoints,
                                      new Vector2_3(Vector2.zero, addPointPosition - platform.transform.position, Vector2.zero),
                                      platform._points[nextIndex].SetPoints,
                                      platform.width);

                            platform._points.Insert(addIndex, newPoint);
                            break;
                    }
                }
            }

            Handles.color = PlatformSettings.Instance._controlsColor;
            Handles.DrawAAPolyLine(lineTexture_Medium, positions.ToArray());

            for (int i = 0; i < platform._points.Count; i++)
            {
                Handles.color = (platform._points[i]._flipCornerState) ? PlatformSettings.Instance._controlsModifiedColor : PlatformSettings.Instance._controlsColor;
                Handles.DrawSolidDisc(platform._points[i]._center.ToVector3() + platform.transform.position, Vector3.forward, largeRadius);

                if (platform.showManipulatorPoints)
                {
                    if (platform._points[i]._setTop)
                        Handles.color = PlatformSettings.Instance._controlsColor;
                    else
                        Handles.color = PlatformSettings.Instance._controlsModifiedColor;
                    Handles.DrawSolidDisc(platform._points[i]._topVert + transformOffset, Vector3.forward, smallRadius);

                    if (platform._points[i]._setBottom)
                        Handles.color = PlatformSettings.Instance._controlsColor;
                    else
                        Handles.color = PlatformSettings.Instance._controlsModifiedColor;
                    Handles.DrawSolidDisc(platform._points[i]._bottomVert + transformOffset, Vector3.forward, smallRadius);

                    if (platform._points[i]._corner != null)
                    {
                        CornerType _cornerType = platform._points[i]._corner._cornerType;
                        Vector2 inPoint;
                        Vector2 outPoint;
                        if (_cornerType == CornerType.TLO || _cornerType == CornerType.TRO || _cornerType == CornerType.BLO || _cornerType == CornerType.BRO)
                        {
                            inPoint = platform._points[i]._corner._inEdge.v1;
                            outPoint = platform._points[i]._corner._outEdge.v1;
                        }
                        else
                        {
                            inPoint = platform._points[i]._corner._inEdge.v0;
                            outPoint = platform._points[i]._corner._outEdge.v0;
                        }

                        if (platform._points[i]._setCornerIn)
                            Handles.color = PlatformSettings.Instance._controlsColor;
                        else
                            Handles.color = PlatformSettings.Instance._controlsModifiedColor;
                        Handles.DrawSolidDisc(inPoint + transformOffset, Vector3.forward, smallRadius);

                        if (platform._points[i]._setCornerOut)
                            Handles.color = PlatformSettings.Instance._controlsColor;
                        else
                            Handles.color = PlatformSettings.Instance._controlsModifiedColor;
                        Handles.DrawSolidDisc(outPoint + transformOffset, Vector3.forward, smallRadius);
                    }
                }
            }

            Handles.BeginGUI();
            bool useSnapping = platform.useSnapping;
            bool showManipulatorPoints = platform.showManipulatorPoints;
            bool isClosed = platform.isClosed;
            float width = platform.width;
            int sortOrder = platform.sortOrder;
            string sortLayer = platform.sortLayer;
            EditorGUI.BeginChangeCheck();
            {
                Rect logoArea = new Rect(0, 0, 37, 32);
                GUILayout.BeginArea(logoArea);
                GUILayout.BeginHorizontal();
                GUI.Box(new Rect(0, 0, 5, 18), "", (GUIStyle)"toolbar");
                GUILayout.Space(5);
                GUI.Box(new Rect(5, 0, 32, 32), "", (GUIStyle)"AnimationCurveEditorBackground");
                GUILayout.Box(componentIcon, GUILayout.Width(32), GUILayout.Height(32));
                GUILayout.EndHorizontal();
                GUILayout.EndArea();

                Rect toolbarArea = new Rect(37, 0, Screen.width - 23, 18);
                GUILayout.BeginArea(toolbarArea);
                GUILayout.BeginHorizontal();
                GUI.Box(new Rect(0, 0, Screen.width, 18), "", (GUIStyle)"toolbar");
                GUILayout.Space(5);
                useSnapping = GUILayout.Toggle(platform.useSnapping, "Snapping", (GUIStyle)"toolbarbutton", GUILayout.Width(70));
                showManipulatorPoints = GUILayout.Toggle(platform.showManipulatorPoints, "Manipulator Points", (GUIStyle)"toolbarbutton", GUILayout.Width(115));
                isClosed = GUILayout.Toggle(platform.isClosed, "Closed", (GUIStyle)"toolbarbutton", GUILayout.Width(70));

                GUILayout.Space(5);
                EditorGUIUtility.labelWidth = 40;
                EditorGUIUtility.fieldWidth = 50;
                width = EditorGUILayout.FloatField("Width", platform.width, GUILayout.Width(75));

                GUILayout.Space(5);
                EditorGUIUtility.labelWidth = 70;
                EditorGUIUtility.fieldWidth = 50;
                sortOrder = EditorGUILayout.IntField("Sort Order", platform.sortOrder, GUILayout.Width(105));

                GUILayout.Space(5);
                EditorGUIUtility.labelWidth = 70;
                EditorGUIUtility.fieldWidth = 50;
                sortLayer = EditorGUILayout.TextField("Sorting Layer", platform.sortLayer, GUILayout.Width(200));
                
                GUILayout.EndHorizontal();
                GUILayout.EndArea();
            }
            if (EditorGUI.EndChangeCheck())
            {
                platform.useSnapping = useSnapping;
                platform.showManipulatorPoints = showManipulatorPoints;
                platform.isClosed = isClosed;
                platform.width = width;
                platform.sortOrder = sortOrder;
                platform.sortLayer = sortLayer;
                platform.GetComponent<Renderer>().sortingOrder = platform.sortOrder;
                platform.GetComponent<Renderer>().sortingLayerName = platform.sortLayer;
            }
            Handles.EndGUI();
        }
	}
}
