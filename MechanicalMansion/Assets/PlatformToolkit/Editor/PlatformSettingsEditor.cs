﻿using UnityEngine;
using UnityEditor;
using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using StarterKit;

namespace PlatformToolkit
{
	[CustomEditor(typeof(PlatformSettings))]
	public class PlatformSettingsEditor : Editor 
	{
        SerializedProperty _fog;
        SerializedProperty _controlsColor;
        SerializedProperty _controlsSelectedColor;
        SerializedProperty _controlsBorderColor;
        SerializedProperty _controlsModifiedColor;

        SerializedProperty _drawWireFrame;
        SerializedProperty _drawCage;
        SerializedProperty _snapDistance;

		Texture2D _graphic_PlatformSettingsColor;
		Texture2D _graphic_PlatformSettingsFog;

        void OnEnable()
        {
            _fog = serializedObject.FindProperty("_fog");
            _controlsColor = serializedObject.FindProperty("_controlsColor");
            _controlsSelectedColor = serializedObject.FindProperty("_controlsSelectedColor");
            _controlsBorderColor = serializedObject.FindProperty("_controlsBorderColor");
            _controlsModifiedColor = serializedObject.FindProperty("_controlsModifiedColor");

            _drawWireFrame = serializedObject.FindProperty("_drawWireframe");
            _drawCage = serializedObject.FindProperty("_drawCage");
            _snapDistance = serializedObject.FindProperty("_snapDistance");

            SetFog("PlatformToolkit/Closed", _fog.colorValue);

			_graphic_PlatformSettingsColor = AssetDatabase.LoadAssetAtPath<Texture2D>("Assets/Gizmos/PlatformSettings_Color.tif");
			_graphic_PlatformSettingsFog = AssetDatabase.LoadAssetAtPath<Texture2D>("Assets/Gizmos/PlatformSettings_Fog.tif");
        }

        public override void OnInspectorGUI()
        {
            EditorGUI.BeginChangeCheck();

            GUILayout.Label("Platform Lighting Settings");
			GUILayout.BeginHorizontal();
			GUILayout.Box(_graphic_PlatformSettingsFog, GUILayout.Width(90), GUILayout.Height(67));
            _fog.colorValue = EditorGUILayout.ColorField("Fog", _fog.colorValue);
			GUILayout.EndHorizontal();

            GUILayout.Space(20);

            GUILayout.Label("Platform Color Settings");
            GUILayout.BeginHorizontal();
			GUILayout.Box(_graphic_PlatformSettingsColor, GUILayout.Width(90), GUILayout.Height(67));
            GUILayout.BeginVertical();
            _controlsColor.colorValue = EditorGUILayout.ColorField("Main", _controlsColor.colorValue);
            _controlsBorderColor.colorValue = EditorGUILayout.ColorField("Border", _controlsBorderColor.colorValue);
            _controlsSelectedColor.colorValue = EditorGUILayout.ColorField("Selected", _controlsSelectedColor.colorValue);
            _controlsModifiedColor.colorValue = EditorGUILayout.ColorField("Modified", _controlsModifiedColor.colorValue);
            GUILayout.EndVertical();
            GUILayout.EndHorizontal();
			
			GUILayout.Space(20);
			
			GUILayout.Label("Platform Draw Settings");
            _drawWireFrame.boolValue = EditorGUILayout.Toggle("Wireframe", _drawWireFrame.boolValue);
            _drawCage.boolValue = EditorGUILayout.Toggle("Cage", _drawCage.boolValue);
            _snapDistance.floatValue = EditorGUILayout.FloatField("Snap Distance", _snapDistance.floatValue);

            if (EditorGUI.EndChangeCheck())
            {
                serializedObject.ApplyModifiedProperties();
                SetFog("PlatformToolkit/Closed", _fog.colorValue);
            }
        }

        private void SetFog(string shaderName, Color newFogColor)
        {
            Material[] armat = (Material[])Resources.FindObjectsOfTypeAll<Material>();

            foreach (Material mat in armat)
            {
                if (mat.shader.name == shaderName)
                {
                    mat.SetVector("_Fog", (Vector4)newFogColor);
                }
            }
        }

	}
}
