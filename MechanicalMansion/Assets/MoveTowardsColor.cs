﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using StarterKit;

public class MoveTowardsColor : MonoBehaviourPlus 
{
    public float time = .75f;
    float currentTime = 0f;
	public IEnumerator Start()
	{
		yield return null;
	}

	public new void OnEnable () 
	{
		(this as MonoBehaviourPlus).OnEnable();
	}

	public new void OnDisable () 
	{
		(this as MonoBehaviourPlus).OnDisable();
	}

    public void Reset()
    {
        currentTime = 0f;
    }
        
	
	public void Update () 
	{
        currentTime += Time.deltaTime;
        currentTime = currentTime.Clamp(0, time);
        GetComponent<SpriteRenderer>().color = Color.Lerp(GetComponent<SpriteRenderer>().color, Color.white, currentTime/time);
	}
}
