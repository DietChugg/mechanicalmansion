﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using StarterKit;

public class WallDetection : MonoBehaviourPlus 
{
    public LayerMask mask;
    public string[] tags;
    public SafeAction<GameObject> OnWallHit;
	public IEnumerator Start()
	{
		yield return null;
	}

	public new void OnEnable () 
	{
		(this as MonoBehaviourPlus).OnEnable();
	}

	public new void OnDisable () 
	{
		(this as MonoBehaviourPlus).OnDisable();
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.transform.CompareLayerMask(mask) || other.transform.CompareTags(tags))
            OnWallHit.Call(other.gameObject);
    }
}
