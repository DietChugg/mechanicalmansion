﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using StarterKit;

public class LerpToRig : MonoBehaviourPlus 
{
    public Transform parent;
    public Vector2 offset;

    public Vector3 parentScreenSpace;
	
	public void FixedUpdate () 
	{
        transform.position = parent.transform.position + offset.ToVector3();
	}
}
