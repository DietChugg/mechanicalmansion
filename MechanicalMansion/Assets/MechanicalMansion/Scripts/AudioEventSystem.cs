﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class AudioEventSystem : MonoBehaviour 
{
    public AudioSource audioSource;
    public AnimationTimelineData animationTimelineData;
    public UnityEvent audioEvent;

    void Start()
    {
        for (int i = 0; i < animationTimelineData.keys.Count; i++)
        {
            StartCoroutine(RunEvents(animationTimelineData.keys[i]));
        }
	}
	
	// Update is called once per frame
	void Update () 
    {
	        
	}

    IEnumerator RunEvents(float timeStamp)
    {
        yield return new WaitForSeconds(timeStamp);
        audioEvent.Invoke();
    }
}
