﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class CharacterController2DEvents : MonoBehaviour 
{
    public CharacterController2DEvent onEnter = new CharacterController2DEvent();
    public CharacterController2DEvent onStay = new CharacterController2DEvent();
    public CharacterController2DEvent onExit = new CharacterController2DEvent();
}

[System.Serializable]
public class CharacterController2DEvent : UnityEvent<Character2DHit>
{
}
