﻿using UnityEngine;
using System.Collections;
using StarterKit;

namespace Chugg
{

public class CharacterController2D : MonoBehaviourPlus
{
	[Header("Input")]
	public string horizontalInput;
	public string jumpInput;
	[Header("Core")]
	public float speed;
	public float jumpStrengh;
	public float gravity;
	public FloatClamp verticalSpeed;
	public Vector2 movePosition;
	public LayerMask mask;
		
	public void Update () 
	{
		if(string.IsNullOrEmpty(horizontalInput) || string.IsNullOrEmpty(jumpInput))
			return;



		movePosition = (Vector2)transform.right * Input.GetAxisRaw(horizontalInput) * speed;
		if(Input.GetButtonDown(jumpInput))
			GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x,jumpStrengh);

//		RaycastHit2D hit = Physics2D.Raycast(transform.position+new Vector2(circleCollider2D.radius,0),transform.up * -1,Mathf.Infinity,mask);
//		if(hit != null)
//		{
//			Debug.Log(hit.transform.name);
//		}

//		transform.Translate(movePosition * Time.deltaTime);

	}

	public void FixedUpdate()
	{
//		rigidbody2D.velocity = (movePosition * Time.deltaTime);
		GetComponent<Rigidbody2D>().AddForce(movePosition * Time.deltaTime);
	}

	public void OnCollisionEnter2D(Collision2D other)
	{
		Debug.Log(other.transform.name);
		if(other.gameObject.CompareTag("floor"))
		{
			transform.rotation = Quaternion.Euler(0,0,0);
		}
		else if(other.gameObject.CompareTag("incline"))
		{
			transform.rotation = Quaternion.Euler(0,0,45f);
		}
	}
}
}
