﻿using UnityEngine;
using System.Collections;

public class DebugCollision : MonoBehaviour 
{
    public bool onTriggerEnter2D;
    public bool onTriggerStay2D;
    public bool onTriggerExit2D;
    public bool onCollisionEnter2D;
    public bool onCollisionStay2D;
    public bool onCollisionExit2D;

    public bool onTriggerEnter;
    public bool onTriggerStay;
    public bool onTriggerExit;
    public bool onCollisionEnter;
    public bool onCollisionStay;
    public bool onCollisionExit;




    void OnTriggerEnter2D(Collider2D other)
    {
        if (onTriggerEnter2D)
            Debug.Log(transform.name + " detected OnTriggerEnter2D with: " + other.transform.name);
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (onTriggerEnter2D)
            Debug.Log(transform.name + " detected OnTriggerStay2D with: " + other.transform.name);
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (onTriggerEnter2D)
            Debug.Log(transform.name + " detected OnTriggerExit2D with: " + other.transform.name);
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (onTriggerEnter2D)
            Debug.Log(transform.name + " detected OnCollisionEnter2D with: " + other.transform.name);
    }

    void OnCollisionStay2D(Collision2D other)
    {
        if (onTriggerEnter2D)
            Debug.Log(transform.name + " detected OnCollisionStay2D with: " + other.transform.name);
    }

    void OnCollisionExit2D(Collision2D other)
    {
        if (onTriggerEnter2D)
            Debug.Log(transform.name + " detected OnCollisionExit2D with: " + other.transform.name);
    }


    void OnTriggerEnter(Collider other)
    {
        if (onTriggerEnter)
            Debug.Log(transform.name + " detected OnTriggerEnter with: " + other.transform.name);
    }

    void OnTriggerStay(Collider other)
    {
        if (onTriggerEnter)
            Debug.Log(transform.name + " detected OnTriggerStay with: " + other.transform.name);
    }

    void OnTriggerExit(Collider other)
    {
        if (onTriggerEnter)
            Debug.Log(transform.name + " detected OnTriggerExit with: " + other.transform.name);
    }

    void OnCollisionEnter(Collision other)
    {
        if (onTriggerEnter)
            Debug.Log(transform.name + " detected OnCollisionEnter with: " + other.transform.name);
    }

    void OnCollisionStay(Collision other)
    {
        if (onTriggerEnter)
            Debug.Log(transform.name + " detected OnCollisionStay with: " + other.transform.name);
    }

    void OnCollisionExit(Collision other)
    {
        if (onTriggerEnter)
            Debug.Log(transform.name + " detected OnCollisionExit with: " + other.transform.name);
    }

}
