﻿using UnityEngine;
using System.Collections;
using StarterKit;
using SebLague;
using Prime31;

public class CameraShake : MonoBehaviour
{
    // Transform of the camera to shake. Grabs the gameObject's transform
    // if null.
    public Transform camTransform;

    // How long the object should shake for.
    public float shake = 0f;

    // Amplitude of the shake. A larger value shakes the camera harder.
    public float shakeAmount = 0.7f;
    public float decreaseFactor = 1.0f;
    public float timeToHitEachPoint = .03f;
    Coroutine storedRoutine;
    Vector3 originalPos;

    void OnEnable()
    {
        if (camTransform == null)
        {
            camTransform = GetComponent(typeof(Transform)) as Transform;
        }
        originalPos = camTransform.position;
        //if (storedRoutine != null)
        //{
        //    Debug.Log("Stop dat Dumb CoRoutine");
        //    StopCoroutine(storedRoutine);
        //}
            
        StopAllCoroutines();
        StartCoroutine(Shake());
    }

    IEnumerator Shake()
    {
        GetComponent<CameraKit2D>().enabled = false;
        GetComponent<CameraWindow>().enabled = false;
        GetComponent<DualForwardFocus>().enabled = false;
        float startShake = shake;
        while (shake > 0)
        {
            Vector3 targetPos = originalPos + Random.insideUnitSphere * shakeAmount;

            storedRoutine = camTransform.MoveTowardsPointOverTime(targetPos, timeToHitEachPoint);

            //while (! > 0)
            shake -= timeToHitEachPoint;//Time.deltaTime * decreaseFactor;
            yield return new WaitForSeconds(timeToHitEachPoint);
            //Debug.Log("SHAKE SHAKE SHAKE".RTOrange());
        }
        camTransform.localPosition = originalPos;
        shake = startShake;
        GetComponent<CameraKit2D>().enabled = true;
        GetComponent<CameraWindow>().enabled = true;
        GetComponent<DualForwardFocus>().enabled = true;
        enabled = false;
    }

    void OnDisable()
    {
        StopAllCoroutines();
        //if (storedRoutine != null)
        //{
        //    Debug.Log("Stop dat Dumb CoRoutine");
        //    StopCoroutine(storedRoutine);
        //}
    }
}