﻿using UnityEngine;
using System.Collections;

public class OnTriggerCheck : MonoBehaviour 
{
    public bool trigger;
    public bool collision;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void OnTriggerStay2D (Collider2D other) 
    {
        trigger = true;
	}

    void OnTriggerExit2D(Collider2D other)
    {
        trigger = false;
    }

    void OnCollisionStay2D(Collision2D other)
    {
        collision = true;
    }

    void OnTriggerExit2D(Collision2D other)
    {
        collision = false;
    }
}
