﻿using UnityEngine;
using System.Collections;

public class SetVelocity : MonoBehaviour 
{
	public Rigidbody rigidBody;
	public Rigidbody2D rigidBody2D;
	public Vector3 velocity;
	
	void OnEnable () 
	{
		Set(velocity);
	}

	public void Set (Vector3 value) 
	{
		if(GetComponent<Rigidbody>() != null)
		{
			GetComponent<Rigidbody>().velocity = value;
		}
		
		if(rigidBody2D != null)
		{
			rigidBody2D.velocity = value;
		}
	}
}
