﻿using UnityEngine;
using UnityEditor;
using System;
using System.Reflection;

public static class PublicAudioUtil 
{
	
	public static void PlayClip(AudioClip clip, float startTime, bool isLooping) {
		Assembly unityEditorAssembly = typeof(AudioImporter).Assembly;
		Type audioUtilClass = unityEditorAssembly.GetType("UnityEditor.AudioUtil");
		MethodInfo method = audioUtilClass.GetMethod(
			"PlayClip",
			BindingFlags.Static | BindingFlags.Public,
			null,
			new System.Type[] {
			typeof(AudioClip),typeof(Int32),typeof(Boolean)
		},
		null
		);

		Debug.Log(typeof(Int32).ToString());

		Int32 value = 20000;

		method.Invoke(
			null,
			new object[] {
			clip,value,isLooping
		}
		);

		SetClipAtTime(clip,startTime);

	} // PlayClip()


	public static void SetClipAtTime(AudioClip clip, float time) 
	{
		Assembly unityEditorAssembly = typeof(AudioImporter).Assembly;
		Type audioUtilClass = unityEditorAssembly.GetType("UnityEditor.AudioUtil");
		MethodInfo method = audioUtilClass.GetMethod(
			"SetClipSamplePosition",
			BindingFlags.Static | BindingFlags.Public,
			null,
			new System.Type[] {
			typeof(AudioClip),typeof(Int32)
		},
		null
		);

		float percent = time/clip.length;
		percent = percent.Clamp01();

		method.Invoke(
			null,
			new object[] {
			clip,(Int32)(percent * clip.samples)
		}
		);
	}

	public static float GetClipPosition(AudioClip clip)
	{
		Assembly unityEditorAssembly = typeof(AudioImporter).Assembly;
		Type audioUtilClass = unityEditorAssembly.GetType("UnityEditor.AudioUtil");
		MethodInfo method = audioUtilClass.GetMethod(
			"GetClipPosition",
			BindingFlags.Static | BindingFlags.Public,
			null,
			new System.Type[] {
			typeof(AudioClip)
		},
		null
		);
		
		return (float)method.Invoke(
			null,
			new object[] {
			clip
		}
		);
	}

	public static bool IsClipPlaying(AudioClip clip)
	{
		Assembly unityEditorAssembly = typeof(AudioImporter).Assembly;
		Type audioUtilClass = unityEditorAssembly.GetType("UnityEditor.AudioUtil");
		MethodInfo method = audioUtilClass.GetMethod(
			"IsClipPlaying",
			BindingFlags.Static | BindingFlags.Public,
			null,
			new System.Type[] {
			typeof(AudioClip)
		},
		null
		);
		
		return (bool)method.Invoke(
			null,
			new object[] {
			clip
		}
		);
	}


	public static void GetClipFuncitons(AudioClip clip)
	{
		Assembly unityEditorAssembly = typeof(AudioImporter).Assembly;
		Type audioUtilClass = unityEditorAssembly.GetType("UnityEditor.AudioUtil");

		for (int i = 0; i < unityEditorAssembly.GetTypes().Length; i++) {
			Debug.Log(unityEditorAssembly.GetTypes()[i]);
		}

		MethodInfo[] methodInfos = audioUtilClass.GetMethods();
		
		for (int i = 0; i < methodInfos.Length; i++) 
		{
			Debug.Log(methodInfos[i].Name.RTTeal());
			ParameterInfo[] parameters = methodInfos[i].GetParameters();
			for (int j = 0; j < parameters.Length; j++) 
			{
				Debug.Log(parameters[j].ToString().RTOlive());
			}
			
		}
	}

	public static void StopClip(AudioClip clip) 
	{
		Assembly unityEditorAssembly = typeof(AudioImporter).Assembly;
		Type audioUtilClass = unityEditorAssembly.GetType("UnityEditor.AudioUtil");

		MethodInfo method = audioUtilClass.GetMethod(
			"StopClip",
			BindingFlags.Static | BindingFlags.Public,
			null,
			new System.Type[] {
			typeof(AudioClip)
		},
		null
		);
		method.Invoke(
			null,
			new object[] {
			clip
		}
		);
	} // PlayClip()
	
} // class PublicAudioUtil