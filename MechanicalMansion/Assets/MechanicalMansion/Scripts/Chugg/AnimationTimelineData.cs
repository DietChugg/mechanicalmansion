﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AnimationTimelineData : ScriptableObject 
{
    public List<float> keys = new List<float>();
}
