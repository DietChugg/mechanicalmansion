﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
using StarterKit;

namespace WayPoints
{
    //https://color.adobe.com/Lime-Green-Theme-2-color-theme-6570838/edit/?copy=true&base=2&rule=Custom&selected=4&name=Copy%20of%20Lime%20Green%20Theme%202&mode=cmyk&rgbvalues=1,0.44999999999999996,0,0.15000000000000002,0.15000000000000002,0.15000000000000002,0.8,1,0,0,0.8,0.95,0.65,0,1&swatchOrder=0,1,2,3,4
    [RequireComponent(typeof(Rigidbody2D))]
    public class WayPointSystem : MonoBehaviour
    {
        [HideInInspector]
        public List<WayPoint> _wayPoints = new List<WayPoint>();
        [HideInInspector]
        public Color _controlColor = new Color32(255,115, 0, 255);
        [HideInInspector]
        public Color _selectedColor = new Color32(204, 255, 0, 255);

        [Range(0,1)]
        public float _positionOnPath;
        [HideInInspector]
        public Vector2 _currentPosition;
        public bool _canMove = true;
        public float _speed;
        public AnimationCurve _positionCurve;
        [Space(10)]
        public UnityEvent OnMovingOut;
        public UnityEvent OnMovingBack;

        private float timeTraveled = 0;
        private Rigidbody2D _rigidBody;
        private float previousPosition = -1;
        private bool movingOut = false;
        private bool movingBack = false;
        private bool stopped = false;
        private bool sendEvent = false;

        void OnEnable()
        {
            if (_rigidBody == null)
                _rigidBody = GetComponent<Rigidbody2D>();
        }

        void FixedUpdate()
        {
            if (_canMove)
            {
                FindCurrentPosition();
                _rigidBody.MovePosition(_currentPosition);
                _positionOnPath = _positionCurve.Evaluate(timeTraveled * _speed);
                timeTraveled += Time.deltaTime;
            
                if (_positionOnPath > previousPosition && !movingOut)
                {
                    movingOut = true;
                    movingBack = false;
                    sendEvent = true;
                }
                else if (_positionOnPath < previousPosition && !movingBack)
                {
                    movingOut = false;
                    movingBack = true;
                    sendEvent = true;
                }
                else if (_positionOnPath == previousPosition && !stopped)
                {
                    stopped = true;
                    movingOut = false;
                    movingBack = false;
                    sendEvent = true;
                }

                if (sendEvent)
                {
                    if (!movingOut)
                        OnMovingOut.Invoke();

                    if (!movingBack)
                        OnMovingBack.Invoke();

                    sendEvent = false;
                }

                previousPosition = _positionOnPath;
            }
        }

        void FindCurrentPosition()
        {
            float totalLength = 0;
            float[] positionLengths = new float[_wayPoints.Count - 1];
            for (int i = 0; i < _wayPoints.Count-1; i++)
            {
                totalLength += Vector3.Distance(_wayPoints[i]._position, _wayPoints[i + 1]._position);
                positionLengths[i] = totalLength;
            }

            float lengthPercentage = _positionOnPath * totalLength;
            int positionIndex = 0;
            for (int i = 0; i < positionLengths.Length; i++)
            {
                if (positionLengths[i] >= lengthPercentage)
                {
                    positionIndex = i;
                    break;
                }
            }

            float startingValue = (positionIndex == 0) ? 0 : positionLengths[positionIndex-1];

            float remainder = lengthPercentage - startingValue;

            _currentPosition = Vector2X.PointOnLine(_wayPoints[positionIndex]._position.ToVector2(), 
                _wayPoints[positionIndex + 1]._position.ToVector2(), 
                remainder);
        }

        public void BuildWayPoints()
        {
            if (_wayPoints.Count < 2)
            {
                _wayPoints.Clear();
                _wayPoints.Add(new WayPoint());
                _wayPoints[0].Set(transform.position, WaypointType.First);
                _wayPoints.Add(new WayPoint());
                _wayPoints[1].Set(transform.position + Vector3.right * 4, WaypointType.Last);
            }

            for (int i = 0; i < _wayPoints.Count; i++)
            {
                bool firstIndex = (i == 0);
                bool lastIndex = (i == _wayPoints.Count - 1);

                if (firstIndex)
                    _wayPoints[i]._wayPointType = WaypointType.First;
                else if (lastIndex)
                    _wayPoints[i]._wayPointType = WaypointType.Last;
                else
                    _wayPoints[i]._wayPointType = WaypointType.Middle;
            }

#if UNITY_Editor
            if(!UnityEditor.EditorApplication.isPlaying)
            FindCurrentPosition();
#endif
        }

        public void Reverse()
        {
            _wayPoints.Reverse();
            BuildWayPoints();
        }
    }
}
