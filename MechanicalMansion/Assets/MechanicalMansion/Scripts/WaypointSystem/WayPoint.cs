﻿using UnityEngine;
using System.Collections;

namespace WayPoints
{
    [System.Serializable]
    public class WayPoint
    {
        public Vector3 _position;
        public WaypointType _wayPointType;

        public void Set(Vector3 position, WaypointType waypointType)
        {
            this._position = position;
            this._wayPointType = waypointType;
        }

    }

    public enum WaypointType
    {
        First,
        Last,
        Middle
    }
}
