﻿using UnityEngine;
using UnityEditor;
using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using StarterKit;

namespace WayPoints
{
    [CustomEditor(typeof(WayPointSystem))]
    public class WayPointSystemEditor : Editor 
	{
        WayPointSystem wayPointSystem;
        Texture2D lineTexture_Thick;
        Texture2D lineTexture_Medium;
        Texture2D lineTexture_Thin;
        Texture2D gizmo;
        Texture2D componentIcon;

        //Vector2 transformOffset;
        //SerializedProperty _useSnapping;
        //SerializedProperty _showManipulatorPoints;

        //int activePoint;

        void OnEnable()
        {
            wayPointSystem = (WayPointSystem)target;

            //wayPointSystem.buildMesh = false;
            //wayPointSystem.showManipulatorPoints = false;
            //wayPointSystem.useSnapping = false;

            //EditorUtility.SetSelectedWireframeHidden(wayPointSystem.GetComponent<Renderer>(), !PlatformSettings.Instance._drawWireframe);
            lineTexture_Thick = AssetDatabase.LoadAssetAtPath<Texture2D>("Assets/Gizmos/LineTexture_Thick.tif");
            lineTexture_Medium = AssetDatabase.LoadAssetAtPath<Texture2D>("Assets/Gizmos/LineTexture_Medium.tif");
            lineTexture_Thin = AssetDatabase.LoadAssetAtPath<Texture2D>("Assets/Gizmos/LineTexture_Thin.tif");
            //gizmo = AssetDatabase.LoadAssetAtPath<Texture2D>("Assets/Gizmos/Platform Gizmo.tif");
            //componentIcon = AssetDatabase.LoadAssetAtPath<Texture2D>("Assets/Gizmos/Platform Icon.tif");
            //SetCustomGizmo();

            //_useSnapping = serializedObject.FindProperty("useSnapping");
            //_showManipulatorPoints = serializedObject.FindProperty("showManipulatorPoints");
        }

        //void OnDisable()
        //{
        //    wayPointSystem.buildMesh = false;
        //}

        //void SetCustomGizmo()
        //{
        //    Texture2D tex = gizmo;
        //    Type editorGUIUtilityType = typeof(EditorGUIUtility);
        //    BindingFlags bindingFlags = BindingFlags.InvokeMethod | BindingFlags.Static | BindingFlags.NonPublic;
        //    object[] args = new object[] { wayPointSystem.gameObject, tex };
        //    editorGUIUtilityType.InvokeMember("SetIconForObject", bindingFlags, null, null, args);
        //}

        public override void OnInspectorGUI()
        {
            GUILayout.Space(10);
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            if(GUILayout.Button("Reverse", GUILayout.Width(200), GUILayout.Height(25)))
                wayPointSystem.Reverse();
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
            GUILayout.Space(10);
            DrawDefaultInspector();
        }

		void OnSceneGUI()
        {
            if (Selection.objects.Length > 1)
                return;

            wayPointSystem.BuildWayPoints();
            Tools.current = Tool.None;

            List<Vector3> positions = new List<Vector3>();
            for (int i = 0; i < wayPointSystem._wayPoints.Count; i++)
                positions.Add(wayPointSystem._wayPoints[i]._position);

            Handles.color = Color.black;
            Handles.DrawAAPolyLine(lineTexture_Thick, positions.ToArray());

            float largeRadius = HandleUtility.GetHandleSize(wayPointSystem.transform.position) * 0.25f;
            float largeRadiusBorder = largeRadius * 1.2f;
            float smallRadius = HandleUtility.GetHandleSize(wayPointSystem.transform.position) * 0.125f;
            float smallRadiusBorder = smallRadius * 1.2f;

            for (int i = 0; i < wayPointSystem._wayPoints.Count; i++)
            {
                bool firstIndex = (i == 0);
                bool lastIndex = (i == wayPointSystem._wayPoints.Count - 1);
                int previousIndex = firstIndex ? wayPointSystem._wayPoints.Count - 1 : i - 1;
                int nextIndex = lastIndex ? 0 : i + 1;

                float snapValue = 1;
                MyHandles.DragHandleResult dhResult;

                Vector3 dragPoint = MyHandles.DragHandle(wayPointSystem._wayPoints[i]._position, largeRadiusBorder, Handles.SphereCap, wayPointSystem._selectedColor, false, snapValue, out dhResult);
                wayPointSystem._wayPoints[i]._position = dragPoint;
                switch (dhResult)
                {
                    case MyHandles.DragHandleResult.RMBClick:
                        if (wayPointSystem._wayPoints.Count <= 2)
                            Debug.LogError("You cannot have less than 2 Control Points");
                        else
                            wayPointSystem._wayPoints.RemoveAt(i);
                        break;
                }

                if (nextIndex != 0)
                {
                    Vector2 addPointPosition = Vector2X.MidPoint(wayPointSystem._wayPoints[i]._position, wayPointSystem._wayPoints[nextIndex]._position);
                    MyHandles.DragHandle(addPointPosition, smallRadiusBorder, Handles.SphereCap, wayPointSystem._selectedColor, false, snapValue, out dhResult);
                    switch (dhResult)
                    {
                        case MyHandles.DragHandleResult.LMBClick:
                            WayPoint newPoint = new WayPoint();
                            Vector3 setPos = new Vector3(addPointPosition.x,
                                addPointPosition.y,
                                wayPointSystem.transform.position.z);
                            newPoint.Set(setPos, WaypointType.Middle);
                            wayPointSystem._wayPoints.Insert(nextIndex, newPoint);
                            break;
                    }
                }
            }

            Handles.color = wayPointSystem._controlColor;
            Handles.DrawAAPolyLine(lineTexture_Medium, positions.ToArray());

            for (int i = 0; i < wayPointSystem._wayPoints.Count; i++)
            {
                Handles.color = wayPointSystem._wayPoints[i]._wayPointType == WaypointType.First ? Handles.color = new Color32(166, 0, 255, 255) : wayPointSystem._controlColor;
                Handles.DrawSolidDisc(wayPointSystem._wayPoints[i]._position, Vector3.forward, largeRadius);
            }

            Handles.color = new Color32(0, 204, 242, 255);
            Handles.DrawSolidDisc(wayPointSystem._currentPosition.ToVector3(), Vector3.forward, smallRadius);

            wayPointSystem.transform.position = wayPointSystem._currentPosition.ToVector3(wayPointSystem.transform.position.z);
        }
	}
}
