﻿using UnityEngine;
using System.Collections;


[RequireComponent(typeof(BoxCollider2D))]
public class CameraWayPoint : MonoBehaviour 
{
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("MainCamera"))
        {
            Debug.Log("Main Camera Detected");
            BoxCollider2D boxCollider2D = GetComponent<BoxCollider2D>();
            FloatRange floorAndCeling = new FloatRange(boxCollider2D.BottomLeftCorner().y,boxCollider2D.TopLeftCorner().y);
            FloatRange leftAndRight = new FloatRange(boxCollider2D.BottomLeftCorner().x, boxCollider2D.BottomRightCorner().x);
            other.GetComponent<MainCamera>().SmoothToWayPoint(floorAndCeling, leftAndRight);
        }
    }
}
