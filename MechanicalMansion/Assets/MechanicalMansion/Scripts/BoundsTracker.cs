﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using StarterKit;

public class BoundsTracker : MonoBehaviour 
{
    public LayerMask mask;
    public List<GameObject> gameObjectsInBound = new List<GameObject>();

    public void FixedUpdate()
    { 
    
    }

	void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("OnTriggerEnter2D: " + other.gameObject);
        if (!gameObjectsInBound.Contains(other.gameObject) && other.transform.CompareLayerMask(mask))
            gameObjectsInBound.Add(other.gameObject);
    }

    void OnTriggerExit2D(Collider2D other)
    {
        Debug.Log("OnTriggerExit2D: " + other.gameObject);
        if (gameObjectsInBound.Contains(other.gameObject))
            gameObjectsInBound.Remove(other.gameObject);
    }

    void OnCollisionEnter2D(Collision2D other) 
    {
        Debug.Log("OnCollisionEnter2D: " + other.gameObject);
        if (!gameObjectsInBound.Contains(other.gameObject) && other.transform.CompareLayerMask(mask))
            gameObjectsInBound.Add(other.gameObject);
    }

    void OnCollisionExit2D(Collision2D other)
    {
        Debug.Log("OnCollisionExit2D: " + other.gameObject);
        if (gameObjectsInBound.Contains(other.gameObject))
            gameObjectsInBound.Remove(other.gameObject);
    }
}
