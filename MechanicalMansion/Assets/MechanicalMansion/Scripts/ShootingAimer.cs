﻿using UnityEngine;
using System.Collections;
using StarterKit;
using Rewired;

public class ShootingAimer : MonoBehaviour 
{
    public float distanceOut;
    bool isForward = true;
    public bool createSphere;
    public Transform sphere;
    public bool createArrow;
    public GameObject arrow;
    public bool createCollisionBox;
    public GameObject collisionBox;
    public bool createBirthLight;
    public GameObject birthLight;
    public GameObject gunFront;
    public GameObject gunBack;
    public int playerId;
    Player player;
    public Vector2 debugDirection;
    public bool useMouse;
    public Vector3 rotateAfterwards;
    public Animator animator;
    public bool useAltAimSystem;
    PlayerMotor playerMotor;
    public AudioClip[] clips;

    void OnEnable()
    {
        player = ReInput.players.GetPlayer(playerId);
        playerMotor = GetComponent<PlayerMotor>();
    }

    void Update()
    {
        if (player == null)
            return;
        if (playerMotor == null)
            return;
        if (playerMotor.autoMove)
            return;

        Vector2 direction = new Vector2(player.GetAxis("Horizontal Aim"), player.GetAxis("Vertical Aim"));
        if (player.controllers.joystickCount == 0)
        {
            Vector3 mousePosition = Camera.main.ScreenToWorldPoint( new Vector3(player.controllers.Mouse.screenPosition.x,player.controllers.Mouse.screenPosition.y, Camera.main.transform.position.z.DistanceTo(transform.position.z)));
            Vector3 heading = mousePosition - transform.position;
            Debug.DrawLine(mousePosition, gunFront.transform.position, ColorX.purple);
            float distance = heading.magnitude;
            direction = heading / distance; // This is now the normalized direction.
        }

        debugDirection = direction;
        if (direction.x != 0)
        {
            isForward = direction.x > 0;
        }
        else if (direction == Vector2.zero)
        {
            if (GetComponent<PlayerMotor>() != null)
                direction.x = GetComponent<PlayerMotor>().isFacingRight ? -1f : 1f;
            if (GetComponent<SebLague.Player>() != null)
                direction.x = GetComponent<SebLague.Player>().IsFacingRight ? 1f : -1f;
        }
        sphere.position = transform.position.ToVector2() + direction.normalized * distanceOut;

        var dir = transform.position - sphere.position;
        float angle = Mathf.Atan2(dir.y, -dir.x) * Mathf.Rad2Deg;
        //sphere.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

        //Debug.Log("angle: " + angle);

        animator.SetFloat("GunDirection2", angle);

        //var gunDir = gunBack.transform.position - gunFront.transform.position;
        //float gunAngle = Mathf.Atan2(dir.y, -dir.x) * Mathf.Rad2Deg;

        if (player.GetButtonDown("Assault Rifle")
            || (player.controllers.joystickCount == 0 && player.controllers.Mouse.GetButtonDown(0)))
        {
            Quaternion startRotation = gunFront.transform.rotation;
            if (transform.localScale.x < 0)
            {
                Vector3 eulerAngles = startRotation.eulerAngles;
                startRotation = Quaternion.Euler(eulerAngles.x, eulerAngles.y * -1, eulerAngles.z);
            }




            if (createArrow)
            {
                
                GameObject newObj = (GameObject)GameObject.Instantiate(arrow, gunFront.transform.position, startRotation);
                newObj.transform.parent = gunFront.transform;
            }
                           //Move Me On The Z Instead
            //newObj.transform.rotation = Quaternion.Euler(0, 0, gunAngle);

            Quaternion startColliderRotation = QuaternionX.LookAtRotation2D(gunBack.transform, gunFront.transform);
            if (useAltAimSystem)
            {
                if (player.controllers.joystickCount == 0)
                {
                    Vector3 mousePosition = Camera.main.ScreenToWorldPoint(new Vector3(player.controllers.Mouse.screenPosition.x, player.controllers.Mouse.screenPosition.y, Camera.main.transform.position.z.DistanceTo(transform.position.z)));

                    startColliderRotation = QuaternionX.LookAtRotation2D(gunFront.transform.position, mousePosition);
                }
                else
                {
                    startColliderRotation = QuaternionX.LookAtRotation2D(transform.position, sphere.position);
                }
            }
            if (createCollisionBox)
            {
                AudioSource.PlayClipAtPoint(clips.ToList().GetRandom(), transform.position,.3f);
                GameObject collidingBox = (GameObject)GameObject.Instantiate(collisionBox, gunFront.transform.position, startColliderRotation);
            }
            if(createBirthLight)
            {
                GameObject birthLightInstance = (GameObject)GameObject.Instantiate(birthLight, gunFront.transform.position, startRotation);
                birthLightInstance.transform.parent = gunFront.transform;
            }
                

            //ParticleSystem particleSystem = GetComponent<ParticleSystem>();
            //ParticleSystem.Particle[] emittedParticles = new ParticleSystem.Particle[particleSystem.particleCount];
            //particleSystem.GetParticles(emittedParticles);
            //for (int i = 0; i < emittedParticles.Length; i++)
            //{
            //    emittedParticles[i].lifetime = 0f;
            //}
            //particleSystem.SetParticles(emittedParticles, emittedParticles.Length);
        }
    }
}
