﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class AudioWaveformToAnimationProperty : MonoBehaviour 
{
	public AudioClip audioClip;
	public AnimationClip animationClip;
	public string animationPropertyPath; //leave as null to directly affect parent
	public string animationProperty; //ex: "m_LocalPosition.y"

#if UNITY_EDITOR
	[ContextMenu("Set Audio Waveform To Animation")]
	public void Set () 
	{
		AnimationCurve animationCurve = new AnimationCurve();
		
		float[] samples = new float[ audioClip.samples * audioClip.channels];
		audioClip.GetData(samples, 0);

		Debug.Log("audioClip.samples: " + audioClip.samples);
		Debug.Log("audioClip.channels: " + audioClip.channels);
		Debug.Log("samples.Length: " + samples.Length);

		for(int i = 0; i < samples.Length; i += 147) // The 735 here "resamples" the 44100hz to 60hz so it fits nicely in the timeline
		{
			animationCurve.AddKey(new Keyframe( (i/(float)samples.Length)*audioClip.length, samples[i] ) );
		}

		AnimationUtility.SetEditorCurve(animationClip , EditorCurveBinding.FloatCurve(animationPropertyPath, typeof(Transform), animationProperty), animationCurve);
	}
#endif

	public void AudioEvent()
	{
		Debug.Log("AudioEvent: " + this.name);
	}
}