﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Turret : MonoBehaviour 
{
	public float time;
	bool isTiming = true;
	public Transform spawnPoint;
	[Header("The 'prefab' will be created for each EventTimeStamp \n to get the TimeStamps drag an AnimationClip with events in it onto 'animationClip' and press the 'Set TimeStamps' button")]
	[Space(25)]
	public List<StemData> stemData = new List<StemData>();


	void OnEnable()
	{
		GameManager.SongStart += SongStart;
	}

	void OnDisable()
	{
		GameManager.SongStart -= SongStart;
	}

	void SongStart()
	{
		foreach(StemData sd in stemData)
		{
			StartCoroutine(WaitToFire(sd, sd.eventTimeStamps[0]));
		}
		StartCoroutine(Timer());
	}

	IEnumerator Timer()
	{
		while(isTiming)
		{
			time += Time.deltaTime;
			yield return null;
		}
	}

	IEnumerator WaitToFire(StemData sd, float waitTime)
	{
		yield return new WaitForSeconds(waitTime);
		
		Fire(sd.prefab);

		sd.currentIndex++;

		if(sd.currentIndex < sd.eventTimeStamps.Count)
		{
			float newWait = sd.eventTimeStamps[sd.currentIndex] - sd.eventTimeStamps[sd.currentIndex-1];
			
			StartCoroutine(WaitToFire(sd, newWait));
		}
	}

	void Fire(GameObject g)
	{
		GameObject requestedFab = PoolManager.Request(g,spawnPoint.gameObject);
	}

	[ContextMenu("Set TimeStamps")]
	public void SetTimeStamps()
	{
        //foreach(StemData sd in stemData)
        //{
        //    sd.Set();
        //}
	}
}


