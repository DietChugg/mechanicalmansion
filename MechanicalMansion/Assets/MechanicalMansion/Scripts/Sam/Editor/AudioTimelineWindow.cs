#if UNITY_EDITOR
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

public class AudioTimelineWindow : EditorWindow
{
    //	string myString = "Hello World";
    //    bool groupEnabled;
    //    bool myBool = true;
    //    float myFloat = 1.23f;

    Texture texture;
    AudioClip audioClip;
    int width = 2048;
    int height = 512;
    Color backgroundColor = Color.gray;
    Gradient grad;
    FilterMode filterMode;
    float playAtTime;
    float audioClipCurrentPosition;
    bool isLooping;
    Vector2 scrollPos;
    float zoom;
    bool isSpaceDown;
    bool isLeftMouseClickDown;
    bool rebuild;
    int markerPressed = -1;
    float noiseGate = .5f;
    float audioGap = 1f;
    AnimationTimelineData animationTimelineData;
    bool enableShortcuts;
    Rect viewRect;
    Rect literalScreenPos;
    float[] samples;
    public List<float> markers = new List<float>();

    public float topBarHeight = 250f;


    [MenuItem("Window/Audio Timeline Window")]
    static void Init()
    {
        AudioTimelineWindow window = (AudioTimelineWindow)EditorWindow.GetWindow(typeof(AudioTimelineWindow));
        if (window)
        {

        }
    }

    void OnGUI()
    {
        UpdateInputs();
        int yPos = 0;
        AudioClip prev = audioClip;

        audioClip = EditorGUI.ObjectField(new Rect(0, yPos, topBarHeight, 20),
            "AudioClip", audioClip, typeof(AudioClip), false) as AudioClip;

        if (audioClip == null)
        {
            return;
        }

        //if (GUI.Button(new Rect(topBarHeight, yPos, 100, 20), "Get Functions"))
        //{
        //    PublicAudioUtil.GetClipFuncitons(audioClip);
        //}
        yPos += 20;


        if (prev != audioClip)
            rebuild = true;

        if (texture)
        {
            //GUILayout.BeginHorizontal();

            string playString = "Play (Ctrl -> Space)";
            if (playAtTime != 0)
                playString = "Resume (Ctrl -> Space)";

            if (!PublicAudioUtil.IsClipPlaying(audioClip))
            {
                if (GUI.Button(new Rect(0, yPos, 200, 20), playString))
                {
                    PublicAudioUtil.PlayClip(audioClip, playAtTime, isLooping);
                }
                yPos += 20;
                playAtTime = EditorGUI.FloatField(new Rect(0, yPos, 200, 20), "Play At: ", playAtTime);
                while (playAtTime < 0)
                    playAtTime += audioClip.length;
                playAtTime = playAtTime % audioClip.length;
                yPos += 20;
            }
            else
            {
                if (GUI.Button(new Rect(0, yPos, topBarHeight, 20), "Stop (Ctrl -> Space)"))
                {
                    playAtTime = audioClipCurrentPosition;
                    PublicAudioUtil.StopClip(audioClip);
                }
                yPos += 20;
            }
            isLooping = GUI.Toggle(new Rect(0, yPos, topBarHeight, 20), isLooping, "Loop");
            yPos += 20;

            enableShortcuts = GUI.Toggle(new Rect(0, yPos, topBarHeight, 20), enableShortcuts, "Enable keyboard Shortcurts");
            yPos += 20;



            //GUILayout.EndHorizontal();

            //GUILayout.BeginHorizontal();
            //			Debug.Log("Call");
            GUI.Label(new Rect(0, yPos, 100, 20), "Length: " + audioClip.length);
            yPos += 20;
            if (PublicAudioUtil.IsClipPlaying(audioClip))
            {
                GUI.Label(new Rect(0, yPos, 100, 20), "Current: " + audioClipCurrentPosition);
                yPos += 20;
            }

            GUI.Label(new Rect(0, yPos, 100, 20), "Width: " + width);
            yPos += 20;

            zoom = EditorGUI.FloatField(new Rect(0, yPos, topBarHeight, 20), "Zoom: ", zoom);
            LockZoom();
            yPos += 20;

            if (GUI.Button(new Rect(0, yPos, topBarHeight, 20), "Key Location (K)"))
            {
                AddMarkerPressed();
            }

            yPos += 20;
            if (GUI.Button(new Rect(0, yPos, topBarHeight, 20), "Delete Selected Marker (CTRL -> DEL)"))
            {
                DeletePressed();
            }

            yPos += 20;
            noiseGate = EditorGUI.FloatField(new Rect(0, yPos, topBarHeight, 20), "NoiseGate: ", noiseGate);
            yPos += 20;
            audioGap = EditorGUI.FloatField(new Rect(0, yPos, topBarHeight, 20), "Gap: ", audioGap);
            yPos += 20;
            if (GUI.Button(new Rect(0, yPos, topBarHeight, 20), "Apply Key Automation (A)"))
            {
                KeyAutomation();
            }
            yPos += 20;
            if (GUI.Button(new Rect(0, yPos, topBarHeight, 20), "Delete All"))
            {
                DeleteAllPressed();
            }
            

            literalScreenPos = new Rect(topBarHeight, 80, position.width - topBarHeight, position.height - 80);
            viewRect = new Rect(0, 0, Mathf.Max(width + zoom, literalScreenPos.width), height);
            //BEGIN SCROLL VIEW!!!
            scrollPos = GUI.BeginScrollView(literalScreenPos, scrollPos, viewRect);

            //Debug.Log("literalScreenPos: " + literalScreenPos);
            //Debug.Log("viewRect: " +viewRect);
            //Debug.Log("scrollPos: " +scrollPos);


            EditorGUI.DrawRect(new Rect(0, 0, width + zoom, 20), ColorX.gold);

            float divisions = 20 / audioClip.length;
            divisions = Mathf.Round(divisions);

            float percentOfScreenVisible = literalScreenPos.width / viewRect.width;

            //Debug.Log(percentOfScreenVisible);
            percentOfScreenVisible = Mathf.Clamp01(percentOfScreenVisible);

            //divisions = 5f;
            //for (float i = 0; i < audioClip.length; i += divisions)
            //{
            //    EditorGUI.LabelField(new Rect((i/audioClip.length) * (width + zoom) - 15, 0, 30, 20), i.ToString());
            //}

            float finalDivisions = audioClip.length * percentOfScreenVisible;
            finalDivisions /= 10f;
            for (float i = 0; i < audioClip.length; i += finalDivisions)
            {
                EditorGUI.LabelField(new Rect((i / audioClip.length) * (width + zoom) - 15, 0, 30, 20), i.ToString());
            }

            EditorGUI.DrawPreviewTexture(new Rect(0, 20, width + zoom, height), texture);//, new Material(new Shader()), ScaleMode.StretchToFill);


            float percent = audioClipCurrentPosition / audioClip.length;

            if (!PublicAudioUtil.IsClipPlaying(audioClip))
                percent = playAtTime / audioClip.length;



            for (int i = 0; i < animationTimelineData.keys.Count; i++)
            {
                float keyPercent = animationTimelineData.keys[i] / audioClip.length;
                Rect keyButton = new Rect((keyPercent * (width + zoom)) - 8, 5, 30f, 30f);
                if (keyButton.Contains(Event.current.mousePosition))
                {
                    if (Event.current.type == EventType.MouseDown)
                    {
                        markerPressed = i;
                    }

                    if (Event.current.type == EventType.MouseUp)
                    {
                        markerPressed = -1;
                    }
                }

                if (markerPressed == i && Event.current.type == EventType.MouseDrag)
                {
                    animationTimelineData.keys[i] = Mathf.Clamp(((float)(keyPercent * (width + zoom) + Event.current.delta.x) / (float)(width + zoom)) * audioClip.length,0,audioClip.length);
                    keyPercent = animationTimelineData.keys[i] / audioClip.length;
                    keyButton = new Rect((keyPercent * (width + zoom)) - 8, 5, 30f, 30f);
                    //buttonRect.y += Event.current.delta.y;
                }
                

                Handles.DrawBezier(new Vector2(keyPercent * (width + zoom), 20).ToVector3(),
                               new Vector2(keyPercent * (width + zoom), 532f).ToVector3(),
                               new Vector3(keyPercent * (width + zoom), 20, 0),
                               new Vector3(keyPercent * (width + zoom), 532f, 0),
                               Color.green, new Texture2D(1, 1),
                               1f);

                

                


                //-15 accounts for button width. if Button changes then this will be wrong
                if (GUI.Button(keyButton, "Stuff", (GUIStyle)"MeTransPlayhead"))
                {
                    playAtTime = animationTimelineData.keys[i];
                }
            }

            Handles.DrawBezier(new Vector2(percent * (width + zoom), 20).ToVector3(),
                               new Vector2(percent * (width + zoom), 532f).ToVector3(),
                               new Vector3(percent * (width + zoom), 20, 0),
                               new Vector3(percent * (width + zoom), 532f, 0),
                               Color.red, new Texture2D(1, 1),
                               1f);




            GUI.EndScrollView();


        }
        //		Drawing.DrawLine(new Vector2(10,512),new Vector2(10,0));



    }

    void UpdateInputs()
    {
        if (Event.current != null)
        {
            if (Event.current.isMouse)
            {
                Rect clickBounds = new Rect(topBarHeight, 80, 1200, height + 15);
                bool isInside = clickBounds.Contains(Event.current.mousePosition);
                if (Event.current.type == EventType.mouseDown && Event.current.button == 0 && isInside)
                {
                    //Debug.Log("Event.current.mousePosition.x: "+ Event.current.mousePosition.x);
                    //Debug.Log("scrollPos.x: " + scrollPos.x);
                    //Debug.Log("width: " + width);

                    isLeftMouseClickDown = true;

                    float percentOfClip = (Event.current.mousePosition.x - clickBounds.x + scrollPos.x) / (width + zoom);
                    //Debug.Log(percentOfClip.ToString().RTOlive());



                    if (PublicAudioUtil.IsClipPlaying(audioClip))
                    {
                        PublicAudioUtil.StopClip(audioClip);
                        PublicAudioUtil.PlayClip(audioClip, percentOfClip * audioClip.length, isLooping);
                        audioClipCurrentPosition = Event.current.mousePosition.x;
                    }
                    else
                    {
                        playAtTime = percentOfClip * audioClip.length;
                    }
                    Repaint();
                }
                else if (Event.current.type == EventType.mouseUp && Event.current.button == 0)
                {
                    isLeftMouseClickDown = false;
                }


                if (Event.current.type == EventType.mouseDrag)
                {
                    if (isLeftMouseClickDown)
                    {
                        //						Debug.Log(width+zoom);
                        //						Debug.Log(scrollPos.x.ToString().RTPurple());
                        float percentOfClip = (Event.current.mousePosition.x - clickBounds.x + scrollPos.x) / (width + zoom);
                        if (PublicAudioUtil.IsClipPlaying(audioClip))
                        {
                            PublicAudioUtil.StopClip(audioClip);

                            Debug.Log((Event.current.mousePosition.x + 25 + scrollPos.x - 1200));
                            Debug.Log(percentOfClip.ToString().RTOlive());

                            PublicAudioUtil.PlayClip(audioClip, percentOfClip * audioClip.length, isLooping);
                            audioClipCurrentPosition = Event.current.mousePosition.x;
                        }
                        else
                        {
                            playAtTime = percentOfClip * audioClip.length;
                        }
                        Repaint();
                    }
                }
            }
            else
            {
                if (Event.current.keyCode == KeyCode.Space && Event.current.control && Event.current.type == EventType.KeyDown && !isSpaceDown)
                {
                    isSpaceDown = true;
                    if (PublicAudioUtil.IsClipPlaying(audioClip))
                    {
                        playAtTime = audioClipCurrentPosition;
                        PublicAudioUtil.StopClip(audioClip);
                    }
                    else
                    {
                        PublicAudioUtil.PlayClip(audioClip, playAtTime, isLooping);
                    }
                }
                else if (Event.current.keyCode == KeyCode.Space && Event.current.type == EventType.KeyUp && isSpaceDown)
                {
                    isSpaceDown = false;
                }

                if (Event.current.keyCode == KeyCode.Minus)
                {
                    //Debug.Log("Zoom: ".RTOrange() + zoom);
                    zoom -= 100;
                    Repaint();
                }
                if (Event.current.keyCode == KeyCode.Equals)
                {
                    //Debug.Log("Zoom: ".RTOrange() + zoom);
                    zoom += 100;

                    Repaint();
                }
                if (Event.current.keyCode == KeyCode.Delete)
                {
                    //Debug.Log("Delete: ".RTRed() + playAtTime);
                    DeletePressed();

                    //if (markers.Contains(playAtTime))
                    //{
                    //    Debug.Log("Marker Found: ".RTRed() + playAtTime);
                    //    Debug.Log(markers.Count);
                    //    markers.Remove(audioClipCurrentPosition);
                    //    Debug.Log(markers.Count);
                    //    Repaint();
                    //}
                    //else
                    //{
                    //    Debug.Log("Nothing To Delete");
                    //}
                }
                if (Event.current.keyCode == KeyCode.K)
                {
                    AddMarkerPressed();
                    Repaint();
                }
                if (Event.current.keyCode == KeyCode.A)
                {
                    KeyAutomation();
                    Repaint();
                }

            }
            LockZoom();
        }


        if (Event.current.type == EventType.ScrollWheel)
        {
            zoom += (int)Event.current.delta.y * -topBarHeight;
            LockZoom();
            float percent = playAtTime / audioClip.length;
            viewRect = new Rect(0, 0, width + zoom, height);
            float percentOfScreenVisible = literalScreenPos.width / viewRect.width;

            scrollPos = new Vector2((viewRect.width * percent) - (percentOfScreenVisible * viewRect.width) / 2, 0);
            Repaint();
        }
    }

    private void AddMarkerPressed()
    {
        //Undo.RecordObject(animationTimelineData, "Add Key");
        animationTimelineData.keys.Add(playAtTime);
        animationTimelineData.keys.RemoveDuplicates<float>();
    }

    void DeleteAllPressed()
    {
        if (!EditorUtility.DisplayDialog("Delete All", "Are you sure?", "Yes", "No"))
        {
            return;
        }
        animationTimelineData.keys.Clear();
    }

    private void DeletePressed()
    {
        
        //Undo.RecordObject(animationTimelineData, "Delete Key");
        for (int i = 0; i < animationTimelineData.keys.Count; i++)
        {
            if (Mathf.Approximately(animationTimelineData.keys[i], playAtTime))
            {
                animationTimelineData.keys.RemoveAt(i);
                i--;
            }
        }
    }

    void LockZoom()
    {
        if (zoom < (-width / 1.4f))
            zoom = (-width / 1.4f);
        if (zoom > width * 20)
            zoom = width * 20;
    }

    void Awake()
    {
        //		Debug.Log("Called Awake");
    }

    void Start()
    {
        //		Debug.Log("Called Start");
    }

    void Update()
    {
        //		Debug.Log("Called Update");
        if ((audioClip != null && texture == null) || rebuild)
        {
            ConstructAnimationTimelineData();
            texture = AudioWaveform();
        }
            

        float lastWidth = width;


        if (audioClip != null)
        {
            float prevAudioClip = audioClipCurrentPosition;
            audioClipCurrentPosition = PublicAudioUtil.GetClipPosition(audioClip);
            if (prevAudioClip != audioClipCurrentPosition)
                Repaint();

        }



        //		Repaint();
    }

    public void ConstructAnimationTimelineData()
    {
        string path = AssetDatabase.GetAssetPath(audioClip);
        path = path.Substring(0,(path.Length-4-audioClip.name.Length));
        Debug.Log(path);
        animationTimelineData = (AnimationTimelineData)Resources.Load(audioClip.name + "_Timeline") as AnimationTimelineData;
        if (animationTimelineData == null)
        {
            animationTimelineData = (AnimationTimelineData)ScriptableObject.CreateInstance<AnimationTimelineData>();
            if (!AssetDatabase.IsValidFolder(path + "Resources"))
                AssetDatabase.CreateFolder(path, "Resources");
            AssetDatabase.CreateAsset(animationTimelineData, path + "Resources/" + audioClip.name + "_Timeline.asset");
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }
    }

    public Texture2D AudioWaveform()
    {
        rebuild = false;
        //		width = audioClip.samples/750;
        //		height = 256;
        int step = Mathf.CeilToInt((audioClip.samples * audioClip.channels) / width);
        samples = new float[audioClip.samples * audioClip.channels];
        audioClip.GetData(samples, 0);
        //animationTimelineData.audioData.Clear();
        //animationTimelineData.audioData.AddRange(samples);

        Texture2D img = new Texture2D(width, height, TextureFormat.RGBA32, false);

        Color[] xy = new Color[width * height];
        for (int x = 0; x < width * height; x++)
        {
            xy[x] = backgroundColor;
        }

        img.SetPixels(xy);

        int i = 0;
        while (i < width)
        {
            int barHeight = Mathf.CeilToInt(Mathf.Clamp(Mathf.Abs(samples[i * step]) * height, 0, height));
            int add = samples[i * step] > 0 ? 1 : -1;
            for (int j = 0; j < barHeight; j++)
            {
                img.SetPixel(i, Mathf.FloorToInt(height / 2) - (Mathf.FloorToInt(barHeight / 2) * add) + (j * add), Color.cyan);//grad.Evaluate( (float)j/(float)barHeight ) );
            }
            if (barHeight == 0)
                img.SetPixel(i, (height / 2), Color.black);
            ++i;
        }

        img.filterMode = filterMode;

        img.Apply();
        return img;
    }

    void KeyAutomation()
    {
        if (!EditorUtility.DisplayDialog("Apply Automation", "This will delete all your previous Key(s) Continue?", "Yes","No"))
        {
            return;
        }

        float lastKeyPlacedInClipTime = Mathf.NegativeInfinity;
        bool isQuietTime = false;
        float firstQuietTime = Mathf.NegativeInfinity;

        float[] samples = new float[audioClip.samples * audioClip.channels];
        audioClip.GetData(samples, 0);

        animationTimelineData.keys.Clear();

        float lastShownPercent = 0f;

        for (int i = 0; i < samples.Length; i+=100)
        {
            float percent = (float)(i) / (float)(samples.Length);
            float time = percent * audioClip.length;

            if (percent > lastShownPercent + .1f)
            {
                lastShownPercent = percent;
                EditorUtility.DisplayProgressBar("Processing", "Calculating Key Positions", percent);
            }
            EditorUtility.ClearProgressBar();

            if ((samples[i] + 1f) / 2f > noiseGate)
            {
                firstQuietTime = time;
                if (!isQuietTime)
                {
                    lastKeyPlacedInClipTime = time;
                    animationTimelineData.keys.AddIfDoesNotContain(time);
                    isQuietTime = true;
                }
            }
            else if ((samples[i] + 1f) / 2f <= noiseGate)
            {
                if (firstQuietTime.DistanceTo(time) > audioGap)
                {
                    isQuietTime = false;
                }
            }
        }


        //Build Automation Here
    }

}
#endif
