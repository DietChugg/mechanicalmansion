﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System;
using System.Collections;

[CustomEditor(typeof(Turret)), CanEditMultipleObjects]
public class TurretEditor : Editor 
{

	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();

		serializedObject.Update();

		EditorGUILayoutX.SuperSpace(2);
		MultiObjectEditor.Button(serializedObject, "Set TimeStamps", typeof(Turret), "SetTimeStamps", GUILayout.Height(40));

		serializedObject.ApplyModifiedProperties ();
	}

}
#endif

