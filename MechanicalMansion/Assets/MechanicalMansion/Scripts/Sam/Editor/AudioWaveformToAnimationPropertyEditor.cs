﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System;
using System.Collections;

[CustomEditor(typeof(AudioWaveformToAnimationProperty)), CanEditMultipleObjects]
public class AudioWaveformToAnimationPropertyEditor : Editor 
{

	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();

		serializedObject.Update();

		EditorGUILayoutX.SuperSpace(2);
		MultiObjectEditor.Button(serializedObject, "Set Audio Waveform To Animation", typeof(AudioWaveformToAnimationProperty), "Set", GUILayout.Height(40));

		serializedObject.ApplyModifiedProperties ();
	}

}
#endif

