﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

[System.Serializable]
public class StemData  
{
	public GameObject prefab;
	public AnimationClip animationClip;
	public List<float> eventTimeStamps = new List<float>();

	[HideInInspector]
	public int currentIndex;

#if UNITY_EDITOR
	public void Set()
	{
		eventTimeStamps.Clear();
		
		AnimationEvent[] animEvents = AnimationUtility.GetAnimationEvents(animationClip);
		
		for(int i = 0; i < animEvents.Length; i ++) 
		{
			eventTimeStamps.Add(animEvents[i].time);
		}
	}
#endif

	void OnEnable()
	{
		currentIndex = 0;
	}

}
