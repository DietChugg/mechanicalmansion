﻿using UnityEngine;
using System.Collections;

public class DisableAfterTime : MonoBehaviour 
{
	public FloatClamp timer;

	void OnEnable () 
	{
		timer.Maximize();
	}
	
	void Update () 
	{
		timer -= Time.deltaTime;
		if(timer.IsAtMin())
		{
			timer.Maximize();
			gameObject.SetActive(false);
		}
	}
}
