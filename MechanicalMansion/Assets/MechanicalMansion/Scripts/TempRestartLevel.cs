﻿using UnityEngine;
using System.Collections;
using StarterKit;

public class TempRestartLevel : MonoBehaviour 
{
    public float deathHeight;

	void Update () 
    {
        if (transform.position.y < deathHeight)
            ApplicationX.ReloadLevel();
	}
}
