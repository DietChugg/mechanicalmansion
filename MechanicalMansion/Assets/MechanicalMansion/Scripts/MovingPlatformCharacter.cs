﻿using UnityEngine;
using System.Collections;
using StarterKit;

public class MovingPlatformCharacter : MonoBehaviour 
{
    public SafeAction<Transform> OnEnteredMovingPlatform;
    public SafeAction<Transform> OnExitedMovingPlatform;
    public Transform platform;
    TransformData data;


    public void OnEnable()
    {
        GetComponent<CharacterController2D>().onEnter += OnEnter;
        GetComponent<CharacterController2D>().onExit += OnExit;
    }

    public void OnDisable()
    {
        GetComponent<CharacterController2D>().onEnter -= OnEnter;
        GetComponent<CharacterController2D>().onExit -= OnExit;
    }

    //void FixedUpdate()
    //{
    //    TransformData newData = new TransformData();
    //    newData.SaveFrom(platform);
    //    Vector3 offset = newData.position.Distance(data.position) * newData.position.GetDirectionTo(data.position);
    //    transform.position += offset;
    //    data = newData;
    //}

    public void OnEnter(Character2DHit other)
    {
        if (other.transform == null)
            return;
        if (other.transform.GetComponent<MovingPlatform>() != null)
        {
            //platform = other.transform;
            //data.SaveFrom(platform);
            transform.SetParent(other.transform);
            OnEnteredMovingPlatform.Call(other.transform);
        }

    }

    public void OnExit(Character2DHit other)
    {
        if (other.transform == null)
            return;
        if (other.transform.GetComponent<MovingPlatform>() != null)
        {
            transform.SetParent(null);
            OnExitedMovingPlatform.Call(other.transform);
        }
    }

    //public void OnTriggerEnter2D(Collider2D col)
    //{
    //    if (col.transform == null)
    //        return;
    //    if (col.transform.GetComponent<MovingPlatform>() != null)
    //    {
    //        Debug.Log("OnMoving Platform");
    //        transform.SetParent(col.transform);
    //        OnEnteredMovingPlatform.Call(col.transform);
    //    }
    //}


    //public void OnTriggerStay2D(Collider2D col)
    //{

    //}


    //public void OnTriggerExit2D(Collider2D col)
    //{
    //    if (col.transform == null)
    //        return;
    //    if (col.transform.GetComponent<MovingPlatform>() != null)
    //    {
    //        transform.SetParent(null);
    //        OnExitedMovingPlatform.Call(col.transform);
    //    }
    //}
         

}
