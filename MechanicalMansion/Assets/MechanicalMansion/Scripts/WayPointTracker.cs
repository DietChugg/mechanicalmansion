﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using StarterKit;

public class WayPointTracker : MonoBehaviour 
{
    public List<Transform> wayPoints = new List<Transform>();
    public SafeAction<Transform[]> OnWayPointsChanged;

    public void OnTriggerEnter2D(Collider2D other) 
    {
        if (!wayPoints.Contains(other.transform) && other.transform.GetComponent<CameraWayPoint>() != null)
        { 
            wayPoints.Add(other.transform);
            OnWayPointsChanged.Call(wayPoints.ToArray());
        }
            
	}

    public void OnTriggerStay2D(Collider2D other)
    {
    }

    public void OnTriggerExit2D(Collider2D other)
    {
        if (wayPoints.Contains(other.transform))
        {
            wayPoints.Remove(other.transform);
            OnWayPointsChanged.Call(wayPoints.ToArray());
        }
            
    }
}
