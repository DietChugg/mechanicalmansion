﻿using UnityEngine;
using System.Collections;

public class ZeroTransform : MonoBehaviour 
{

	public bool onEnable;
	public bool update;

	void OnEnable () 
	{
		if(onEnable)
			transform.localPosition = Vector3.zero;
	}
	
	void Update () 
	{
		if(update)
			transform.localPosition = Vector3.zero;
	}
}
