﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections;

public class AudioImage : MonoBehaviour 
{
	public Material mat;
	public AudioClip audioClip;
	public int width = 4096;
	public int height = 1;
	public Color backgroundColor;
	public Gradient grad;
	public FilterMode filterMode;

	[ContextMenu("Convert")]
	void Convert () 
	{
		mat.mainTexture = AudioWaveform();
	}
	
	public Texture2D AudioWaveform()
	{
		
		int step = Mathf.CeilToInt((audioClip.samples * audioClip.channels) / width);
		float[] samples = new float[audioClip.samples * audioClip.channels];
		audioClip.GetData(samples, 0);
		
		Texture2D img = new Texture2D(width, height, TextureFormat.RGBA32, false);
		
		Color[] xy = new Color[width * height];
		for (int x = 0; x < width * height; x++)
		{
			xy[x] = backgroundColor;
		}
		
		img.SetPixels(xy);
		
		int i = 0;
		while (i < width)
		{
			int barHeight = Mathf.CeilToInt(Mathf.Clamp(Mathf.Abs(samples[i * step]) * height, 0, height));
			int add = samples[i * step] > 0 ? 1 : -1;
			for (int j = 0; j < barHeight; j++)
			{
				img.SetPixel(i, Mathf.FloorToInt(height / 2) - (Mathf.FloorToInt(barHeight / 2) * add) + (j * add), grad.Evaluate( (float)j/(float)barHeight ) );
			}
			++i;
		}

		img.filterMode = filterMode;
		
		img.Apply();
		return img;
	}
}
