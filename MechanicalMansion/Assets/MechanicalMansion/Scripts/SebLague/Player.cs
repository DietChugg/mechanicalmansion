﻿using UnityEngine;
using System;
using System.Collections;
using Rewired;
using StarterKit;
using Prime31;
using StarterKit.LifeSystem;

namespace SebLague
{
    [RequireComponent(typeof(Controller2D))]
    public class Player : MonoBehaviour
    {

        public float maxJumpHeight = 4;
        public float minJumpHeight = 1;
        public float timeToJumpApex = .4f;
        float accelerationTimeAirborne = .2f;
        float accelerationTimeGrounded = .1f;
        float moveSpeed = 6;

        public Vector2 wallJumpClimb;
        public Vector2 wallJumpOff;
        public Vector2 wallLeap;

        public float wallSlideSpeedMax = 3;
        public float wallStickTime = .25f;
        float timeToWallUnstick;


        public float riseMultiplier = 1f;
        public float fallMultiplier = 1f;
        float gravity;
        float maxJumpVelocity;
        float minJumpVelocity;
        Vector3 velocity;
        float velocityXSmoothing;

        Controller2D controller;
        public int playerId = 0;
        Rewired.Player player;
        public CameraKit2D cameraKit2D;

        public AnimationCurve fallCurve;

        bool releasedJumpKey = false;
        Vector2 applyKickback;

        bool wallSliding;

        public bool IsFacingRight
        {
            get 
            {
                if (player == null)
                    return false;

                if (player.controllers.joystickCount == 0)
                {

                    Vector2 screenSpace = Camera.main.WorldToScreenPoint(transform.position).ToVector2();
                    return player.controllers.Mouse.screenPosition.x < screenSpace.x;
                }
                else
                {
                    if (player.GetAxisRaw("Horizontal Aim") == 0)
                    {

                    }
                    else if (player.GetAxis("Horizontal Aim") < 0)
                    {
                        return true;
                    }
                    else if (player.GetAxis("Horizontal Aim") > 0)
                    {
                        return false;
                    }
                }
                return false;
            }
        }

        void Start()
        {
            controller = GetComponent<Controller2D>();

            gravity = -(2 * maxJumpHeight) / Mathf.Pow(timeToJumpApex, 2);
            maxJumpVelocity = Mathf.Abs(gravity) * timeToJumpApex;
            minJumpVelocity = Mathf.Sqrt(2 * Mathf.Abs(gravity) * minJumpHeight);
            print("Gravity: " + gravity + "  Jump Velocity: " + maxJumpVelocity);
            player = ReInput.players.GetPlayer(playerId);
        }

        void OnEnable()
        {
            GetComponent<Health>().OnTakeDamage += OnTakeDamage;
        }

        void OnDisable()
        {
            GetComponent<Health>().OnTakeDamage -= OnTakeDamage;
        }

        public void OnTakeDamage(int damageDealt, DamageInflictor damageInflictor)
        {
            if (transform.position.x <= damageInflictor.transform.position.x)
            {
                applyKickback.x = -damageInflictor.kickback.x;
                applyKickback.y = damageInflictor.kickback.y;
            }
            else
            {
                applyKickback.x = damageInflictor.kickback.x;
                applyKickback.y = damageInflictor.kickback.y;
            }
        }

        void Update()
        {
            if (applyKickback != Vector2.zero)
            {
                controller.collisions.below = false;
                transform.Translate(0, .001f, 0);
                velocity = applyKickback;
                applyKickback = Vector2.zero;
            }

            Vector2 input = new Vector2(player.GetAxisRaw("Horizontal Movement"), player.GetAxisRaw("Vertical Movement"));
            int wallDirX = (controller.collisions.left) ? -1 : 1;

            float targetVelocityX = input.x * moveSpeed;
            velocity.x = Mathf.SmoothDamp(velocity.x, targetVelocityX, ref velocityXSmoothing, (controller.collisions.below) ? accelerationTimeGrounded : accelerationTimeAirborne);

            if (cameraKit2D != null)
                cameraKit2D.isPlayerGrounded = controller.IsGrounded;

            wallSliding = false;
            if ((controller.collisions.left || controller.collisions.right) && !controller.collisions.below && velocity.y < 0)
            {
                wallSliding = true;

                if (velocity.y < -wallSlideSpeedMax)
                {
                    velocity.y = -wallSlideSpeedMax;
                }

                if (timeToWallUnstick > 0)
                {
                    velocityXSmoothing = 0;
                    velocity.x = 0;

                    if (input.x != wallDirX && input.x != 0)
                    {
                        timeToWallUnstick -= Time.deltaTime;
                    }
                    else
                    {
                        timeToWallUnstick = wallStickTime;
                    }
                }
                else
                {
                    timeToWallUnstick = wallStickTime;
                }

            }

            if (player.GetButtonDown("Jump"))
            {
                if (wallSliding)
                {
                    if (wallDirX == input.x)
                    {
                        velocity.x = -wallDirX * wallJumpClimb.x;
                        velocity.y = wallJumpClimb.y;
                    }
                    else if (input.x == 0)
                    {
                        velocity.x = -wallDirX * wallJumpOff.x;
                        velocity.y = wallJumpOff.y;
                    }
                    else
                    {
                        velocity.x = -wallDirX * wallLeap.x;
                        velocity.y = wallLeap.y;
                    }
                    releasedJumpKey = false;
                }
                if (controller.collisions.below)
                {
                    velocity.y = maxJumpVelocity;
                    releasedJumpKey = false;
                }
            }
            if (player.GetButtonUp("Jump") && !releasedJumpKey)
            {
                releasedJumpKey = true;
                if (velocity.y > minJumpVelocity)
                {
                    velocity.y = minJumpVelocity;
                    velocity.y = 0;
                }
            }

            if (velocity.y > 0)
                velocity.y += gravity * Time.deltaTime * (riseMultiplier);
            else
            {
                velocity.y += gravity * Time.deltaTime * (fallMultiplier);
            }




            controller.Move(velocity * Time.deltaTime, input);

            if (controller.collisions.above || controller.collisions.below)
            {
                velocity.y = 0;
            }

        }


        //public IEnumerator


    } 


}
