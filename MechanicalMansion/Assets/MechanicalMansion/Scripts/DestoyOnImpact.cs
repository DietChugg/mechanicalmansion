﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using StarterKit;


public class DestoyOnImpact : MonoBehaviourPlus
{
    public LayerMask mask;
    public GameObject droppedFX;
    //public float delay = .01f;

    void OnCollisionEnter2D(Collision2D other) 
    {
        if (other.transform.CompareLayerMask(mask) || gameObject.name.Contains("Enemy"))
        {
            //Invoke(delay, delegate()
            //{
            if (droppedFX != null)
                droppedFX.Instantiate(transform.position, transform.rotation);
                Destroy(gameObject);
            //});
        }
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.transform.CompareLayerMask(mask) || gameObject.name.Contains("Enemy"))
        {
            //Invoke(delay, delegate()
            //{
            if (droppedFX != null)
                droppedFX.Instantiate(transform.position, transform.rotation);
                Destroy(gameObject);
            //});
        }
    }
}
