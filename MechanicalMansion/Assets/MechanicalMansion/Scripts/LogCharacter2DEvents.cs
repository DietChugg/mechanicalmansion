﻿using UnityEngine;
using System.Collections;

public class LogCharacter2DEvents : MonoBehaviour 
{
    public bool logEnter, logStay, logExit;

    public void OnEnter(Character2DHit hit) 
    {
        if (!logEnter)
            return;
        if (!hit.isTrigger)
            Debug.Log(transform.name + " OnEnter: ".RTGreen() + hit.transform.name);
        else
            Debug.Log(transform.name + " OnEnter: ".RTOrange() + hit.transform.name);
	}

    public void OnStay(Character2DHit hit) 
    {
        if (!logStay)
            return;
        if (!hit.isTrigger)
            Debug.Log(transform.name + " OnStay: ".RTGreen() + hit.transform.name);
        else
            Debug.Log(transform.name + " OnStay: ".RTOrange() + hit.transform.name);
    }

    public void OnExit(Character2DHit hit) 
    {
        if (!logExit)
            return;
        if (!hit.isTrigger)
            Debug.Log(transform.name + " OnExit: ".RTGreen() + hit.transform.name);
        else
            Debug.Log(transform.name + " OnExit: ".RTOrange() + hit.transform.name);
    }
}
