﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using StarterKit;
using WayPoints;

public class MovingPlatform : MonoBehaviourPlus
{
    //public bool debug;

    public bool isFalling = false;
    public float waitToDrop = 1.75f;
    public float maxVelocity = 5f;
    public void OnTriggerEnter2D(Collider2D hit)
    {
        Apply(hit);
    }

    public void OnCollisionEnter2D(Collision2D hit)
    {
        Apply(hit.collider);
    }

    public void Apply(Collider2D hit)
    {
        //if (debug)
        //    Debug.Log(hit.transform.name);

        if (hit.transform.name == "PlayerCol")
        {
            if (!isFalling)
            {
                if (this.GetComponent<WayPointSystem>())
                    this.GetComponent<WayPointSystem>()._canMove = true;
            }
            else
            {
                Invoke(waitToDrop, delegate() { GetComponent<Rigidbody2D>().isKinematic = false; });
                Invoke("DisableCollider", 1);
            }
        }
    }

    public void FixedUpdate()
    {
        if (!isFalling)
            return;
        GetComponent<Rigidbody2D>().velocity = GetComponent<Rigidbody2D>().velocity.normalized * maxVelocity;
    }

    void DisableCollider()
    {
        this.GetComponent<Collider2D>().enabled = false;
    }
}
