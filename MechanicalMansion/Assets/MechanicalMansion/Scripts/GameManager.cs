﻿using UnityEngine;
using System;
using System.Collections;

public class GameManager : MonoBehaviour 
{

	public static Action SongStart;
	public AudioSource audioSource;

	void OnEnable()
	{
		PoolManager.Instance.Init();
	}

	void Update () 
	{
		if(Input.GetKeyDown(KeyCode.Space))
		{
			if(SongStart != null)
				SongStart();

			audioSource.Play();
		}
	
	}
}
