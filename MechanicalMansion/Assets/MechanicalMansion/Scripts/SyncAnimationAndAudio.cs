﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using StarterKit;

public class SyncAnimationAndAudio : MonoBehaviourPlus
{
	public float normalAnimation;
	public float normalAudio;
	public List<Animation> animations = new List<Animation>();

	void OnEnable () 
	{
		audioSource.Play();

		foreach(Animation animation in animations)
			animation.Play(animation.clip.name);

		Debug.Log(audioSource.clip.length);
	}
	
	void LateUpdate () 
	{
		foreach(Animation animation in animations)
		{
			animation[animation.clip.name].normalizedTime = audioSource.time/audioSource.clip.length;
			normalAnimation = animation[animation.clip.name].normalizedTime * audioSource.clip.length;
		}

		normalAudio = (audioSource.time/audioSource.clip.length) * audioSource.clip.length;
	}
}
