﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using StarterKit;
using Rewired;
using StarterKit.LifeSystem;
using StarterKit;

[System.Serializable]
public class CameraBounds
{
    public Vector3 topRight;
    public Vector3 bottomRight;
    public Vector3 topLeft;
    public Vector3 bottomLeft;
    public float bottom { get { return bottomLeft.y; } }
    public float top { get { return topLeft.y; } }
    public float right { get { return topRight.x; } }
    public float left { get { return topLeft.x; } }
    public float height { get { return top.DistanceTo(bottom); } }
    public float width { get { return right.DistanceTo(left); } }

}

public class MainCamera : MonoBehaviour 
{
    public GameObject target;
    Player player;
    public int playerId;
    
    Rect whiteZone = new Rect(0,0,1,1);
    public Rect greenZone;
    public Rect redZone;
    Vector3 prevAvgPosition = Vector3.zero;
    public FloatRange floorAndCeling;
    public FloatRange leftAndRight;

    public FloatRange targetFloorAndCeling;
    public FloatRange targetLeftAndRight;

    public AnimationCurve redBoxCurve;
    public float toRedBoxTime;
    bool? isRedRightTarget;
    bool? isRedTopTarget;
    float currentLerpTime;
    
    public AnimationCurve wayPointCurve;
    public float toNewWayPointTime;
    

    bool lockYtop;
    bool lockYbottom;

    Vector3 currentVelocity = Vector3.zero;

    CameraBounds whiteBox;
    CameraBounds redBox;
    CameraBounds greenBox;
    CameraBounds blueBox;
    bool onMovingPlatform = false;
    Vector3 cameraStartPlatformPosition;
    float currentTime;
    public AnimationCurve platformTransionCurve;
    public float toPlatformTime;
    Transform platform;
    [Space(20f)]
    public float zPos = -19.3f;

	void OnEnable () 
    {
        player = ReInput.players.GetPlayer(playerId);
        prevAvgPosition = target.transform.position;
        target.GetComponent<MovingPlatformCharacter>().OnEnteredMovingPlatform += OnEnteredMovingPlatform;
        target.GetComponent<MovingPlatformCharacter>().OnExitedMovingPlatform += OnExitedMovingPlatform;
        target.GetComponent<Health>().OnTakeDamage += OnTakeDamage;
        //transform.SetXPosition(target.transform.position.x);
        //transform.SetYPosition(target.transform.position.y);

	}

    void OnDisable()
    {
        if (target != null)
        {
            target.GetComponent<MovingPlatformCharacter>().OnEnteredMovingPlatform -= OnEnteredMovingPlatform;
            target.GetComponent<MovingPlatformCharacter>().OnExitedMovingPlatform -= OnExitedMovingPlatform;
            target.GetComponent<Health>().OnTakeDamage -= OnTakeDamage;
        }
    }

    void OnTakeDamage(int damageDealt, DamageInflictor damageInflictor)
    {
        GetComponent<CameraShake>().enabled = true;
        //GetComponent<CameraWayPointLock>().enabled = false;
        enabled = false;
    }

    CameraBounds GetWorldCorners(Rect rect)
    {
        float distanceToCamera = target.transform.position.z.DistanceTo(transform.position.z);
        Vector3 bottomLeft = Camera.main.ScreenToWorldPoint(
            new Vector3(
                rect.min.x * Screen.width,
                rect.min.y * Screen.height,
                distanceToCamera));
        
        Vector3 topLeft = Camera.main.ScreenToWorldPoint(
            new Vector3(rect.min.x * Screen.width, 
                rect.max.y * Screen.height,
                distanceToCamera));

        Vector3 bottomRight = Camera.main.ScreenToWorldPoint(
            new Vector3(rect.max.x * Screen.width, 
                rect.min.y * Screen.height,
                distanceToCamera));

        Vector3 topRight = Camera.main.ScreenToWorldPoint(
            new Vector3(rect.max.x * Screen.width, 
                rect.max.y * Screen.height,
                distanceToCamera));

        return new CameraBounds() { bottomLeft = bottomLeft, bottomRight = bottomRight, topLeft = topLeft, topRight = topRight};
    }

    void DrawGizmoBox(CameraBounds box, Color color)
    {
        Gizmos.color = color;
        Gizmos.DrawLine(box.topLeft, box.topRight);
        Gizmos.DrawLine(box.bottomLeft, box.bottomRight);
        Gizmos.DrawLine(box.topLeft, box.bottomLeft);
        Gizmos.DrawLine(box.topRight, box.bottomRight);
    }

    CameraBounds GetBlueBox()
    { 
        return new CameraBounds()
        {
            bottomLeft = new Vector3(leftAndRight.min, floorAndCeling.min, target.transform.position.z),
            bottomRight = new Vector3(leftAndRight.max, floorAndCeling.min, target.transform.position.z),
            topRight = new Vector3(leftAndRight.max, floorAndCeling.max, target.transform.position.z),
            topLeft = new Vector3(leftAndRight.min, floorAndCeling.max, target.transform.position.z),
        };
    }

    void OnDrawGizmos()
    {
        if (target == null)
            return;
        CameraBounds whiteBox = GetWorldCorners(whiteZone);
        DrawGizmoBox(whiteBox, Color.white);

        CameraBounds redBox = GetWorldCorners(redZone);
        DrawGizmoBox(redBox, Color.red);

        CameraBounds greenBox = GetWorldCorners(greenZone);
        DrawGizmoBox(greenBox, Color.green); 

        CameraBounds blueBox = GetBlueBox();
        DrawGizmoBox(blueBox, Color.blue);

    }

    public void MovingPlatform()
    {
        currentTime += Time.deltaTime;
        Vector3 targetPos = target.transform.position;
        targetPos.z = zPos;
        currentTime = currentTime.Min(toPlatformTime);
        transform.position = Vector3.Lerp(cameraStartPlatformPosition,
            targetPos, 
            platformTransionCurve.Evaluate(currentTime / toPlatformTime));
    }

    public void OnEnteredMovingPlatform(Transform platform)
    {
        this.platform = platform;
        cameraStartPlatformPosition = transform.position;
        cameraStartPlatformPosition.z = zPos;
        onMovingPlatform = true;
        currentTime = 0f;
    }

    public void OnExitedMovingPlatform(Transform platform)
    {
        onMovingPlatform = false;
    }

    public void LateUpdate()
    {
        if (player == null)
            return;
        if (target == null)
            return;


        if (onMovingPlatform)
        {
            Debug.Log("Moving Platforms Enabled...");
            MovingPlatform();
            return;
        }

        whiteBox = GetWorldCorners(whiteZone);
        redBox = GetWorldCorners(redZone);
        greenBox = GetWorldCorners(greenZone);
        //blueBox = GetBlueBox();

        Vector3 targetPos = transform.position;

        //Determine if you've left the Green Box's Horizontal Bounds
        if (target.transform.position.x < greenBox.left && (isRedRightTarget == false || isRedRightTarget == null))
        {
            isRedRightTarget = true;
            currentLerpTime = 0;
        }
        else if (target.transform.position.x > greenBox.right && (isRedRightTarget == true || isRedRightTarget == null))
        {
            isRedRightTarget = false;
            currentLerpTime = 0;
        }
        //end

        //Determine if you've left the Green Box's Vertical Bounds
        if ((target.transform.position.y < greenBox.bottom || player.GetAxis("Vertical Movement") == -1)
            && (isRedTopTarget == false || isRedTopTarget == null))
        {
            isRedTopTarget = true;
            currentLerpTime = 0;
        }
        else if ((target.transform.position.y > greenBox.top || player.GetAxis("Vertical Movement") == 1)
            && (isRedTopTarget == true || isRedTopTarget == null))
        {
            isRedTopTarget = false;
            currentLerpTime = 0;
        }
        //end


        //Find how far the Camera should move Horizontally based on Player Position
        //Vector2 clampHorizontal = new Vector2(blueBox.left + whiteBox.width / 2, blueBox.right - whiteBox.width / 2);
        if (isRedRightTarget == true)
        {
            if (target.transform.position.x < redBox.right)
            {
                float distFromAvgPositionToRedBoxLeft = target.transform.position.x.DistanceTo(redBox.left) / 2;
                targetPos.x = (target.transform.position.x - distFromAvgPositionToRedBoxLeft);//.Clamp(clampHorizontal.x, clampHorizontal.y);
                //transform.SetXPosition(targetPos.x);
            }
        }
        else if (isRedRightTarget == false)
        {
            if (target.transform.position.x > redBox.left)
            {
                float distFromAvgPositionToRedBoxRight = target.transform.position.x.DistanceTo(redBox.right)/2;
                targetPos.x = (target.transform.position.x + distFromAvgPositionToRedBoxRight);//.Clamp(clampHorizontal.x, clampHorizontal.y);
               // transform.SetXPosition(targetPos.x);
            }
        }
        //end

        //Find how far the Camera should move Vertically based on Player Position
        //Vector2 clampVertical = new Vector2(blueBox.bottom + whiteBox.height / 2, blueBox.top - whiteBox.height / 2);
        if (isRedTopTarget == true)
        {
            if (target.transform.position.y < redBox.top)
            {
                float distFromAvgPositionToRedBoxBottom = target.transform.position.y.DistanceTo(redBox.bottom)/2;
                targetPos.y = (target.transform.position.y - distFromAvgPositionToRedBoxBottom);//.Clamp(clampVertical.x, clampVertical.y);
                //transform.SetYPosition(targetPos.y);
            }
        }
        else if (isRedTopTarget == false)
        {
            if (target.transform.position.y > redBox.bottom)
            {
                float distFromAvgPositionToRedBoxTop = target.transform.position.y.DistanceTo(redBox.top)/2;
                targetPos.y = (target.transform.position.y + distFromAvgPositionToRedBoxTop);//.Clamp(clampVertical.x, clampVertical.y);
                //transform.SetYPosition(targetPos.y);
            }
        }
        //end
        currentLerpTime += Time.deltaTime;
        currentLerpTime = currentLerpTime.Clamp(currentLerpTime, toRedBoxTime);
        targetPos.z = zPos;
        transform.position = Vector3.Lerp(transform.position, targetPos, redBoxCurve.Evaluate(currentLerpTime / toRedBoxTime));
        //transform.position = Vector3.SmoothDamp(transform.position, targetPos, ref currentVelocity, smoothTime * Time.deltaTime);
        //transform.position = targetPos;
        prevAvgPosition = target.transform.position;
    }

    public void SmoothToWayPoint(FloatRange newFloorAndCeling, FloatRange newLeftAndRight)
    {
        StopAllCoroutines();
        StartCoroutine(SmoothToWayPointRoutine(newFloorAndCeling, newLeftAndRight));
    }

    IEnumerator SmoothToWayPointRoutine(FloatRange newFloorAndCeling, FloatRange newLeftAndRight)
    { 
        float time = 0f;
        float startFloor = floorAndCeling.min;
        float startCeling = floorAndCeling.max;
        float startLeft = leftAndRight.min;
        float startRight = leftAndRight.max;
        while(time < toNewWayPointTime)
        {
            float timeCompleted = time / toNewWayPointTime;
            time += Time.deltaTime;
            timeCompleted = timeCompleted.Clamp01();
            floorAndCeling.min = Mathf.Lerp(startFloor, newFloorAndCeling.min, wayPointCurve.Evaluate(timeCompleted));
            floorAndCeling.max = Mathf.Lerp(startCeling, newFloorAndCeling.max, wayPointCurve.Evaluate(timeCompleted));
            leftAndRight.min = Mathf.Lerp(startLeft, newLeftAndRight.min, wayPointCurve.Evaluate(timeCompleted));
            leftAndRight.max = Mathf.Lerp(startRight, newLeftAndRight.max, wayPointCurve.Evaluate(timeCompleted));
            yield return null;
        }

    }

    //public void OnPreCull()
    //{
    //    //UpdateAveragePosition();
    //    if (prevAvgPosition.x <= redBox.left && target.transform.position.x >= redBox.left)
    //    {
    //        Debug.Log("Manual Setting Of Position");
    //        transform.SetXPosition(redBox.left);
    //    }
    //}

}
