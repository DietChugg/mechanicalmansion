﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using StarterKit;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class CameraWayPointLock : MonoBehaviour 
{
    public AnimationCurve lerpCurve;
    public float returnTime;
    float time;


    public Transform target;
    Rect whiteZone = new Rect(0,0,1,1);
    CameraBounds whiteBox;

    bool leftInBounds, rightInBounds, topInBounds, bottomInBounds,
        bottomLeftInBounds, bottomRightInBounds, topLeftInBounds, topRightInBounds;

    public bool snaps = false;
    BoxCollider2D[] wayPoints;

    Vector3 postRenderPostion;
    Vector3 prevCamPos = Vector3.zero;

    bool prevLeftInBounds, prevRightInBounds, prevTopInBounds, prevBottomInBounds,
        prevBottomLeftInBounds, prevBottomRightInBounds, prevTopLeftInBounds, prevTopRightInBounds;

    Vector3 startLerpPosition;

    void OnEnable()
    {
        if (target != null) 
            target.GetComponent<WayPointTracker>().OnWayPointsChanged += OnWayPointsChanged;
    }

    void OnDisable()
    {
        if(target != null)
            target.GetComponent<WayPointTracker>().OnWayPointsChanged -= OnWayPointsChanged;
    }

    void OnWayPointsChanged(Transform[] newWayPoints)
    {
        ResetSmoothLock();
    }

	void LateUpdate () 
    {
        Debug.DrawLine(prevCamPos, postRenderPostion, Color.red);
        whiteBox = GetWorldCorners(whiteZone);
        DebugBox(whiteBox,ColorX.pink);
	}

    
    public void OnPreCull()
    {
        prevCamPos = transform.position;
        if (snaps)
        {
            InBoundChecks();
            Snap();
        }
        transform.SetZPosition(Camera.main.GetComponent<MainCamera>().zPos);
    }

    public void OnPostRender()
    {
        postRenderPostion = transform.position;
        transform.position = prevCamPos;
    }

    void InBoundChecks()
    {
        if (Time.time == 0f)
            return;
        whiteBox = GetWorldCorners(whiteZone);
        //Debug.Log(whiteBox.width);
        WayPointTracker wayPointTracker = target.GetComponent<WayPointTracker>();
        wayPoints = new BoxCollider2D[target.GetComponent<WayPointTracker>().wayPoints.Count];
        for (int i = 0; i < wayPoints.Length; i++)
        {
            wayPoints[i] = wayPointTracker.wayPoints[i].GetComponent<BoxCollider2D>();
        }

        prevLeftInBounds = leftInBounds;
        prevRightInBounds = rightInBounds;
        prevTopInBounds = topInBounds;
        prevBottomInBounds = bottomInBounds;

        prevBottomLeftInBounds = bottomLeftInBounds;
        prevBottomRightInBounds = bottomRightInBounds;
        prevTopLeftInBounds = topLeftInBounds ;
        prevTopRightInBounds = topRightInBounds;


        //if (wayPoints.Length != wayPoints.Length)
            //Debug.Log("New Waypoint Count ".RTPurple() + wayPoints.Length);
        leftInBounds = false;
        rightInBounds = false;
        topInBounds = false;
        bottomInBounds = false;


        bottomLeftInBounds = false;
        bottomRightInBounds = false;
        topLeftInBounds = false;
        topRightInBounds = false;


        for (int i = 0; i < wayPoints.Length; i++)
        {
            BoxCollider2D currentWayPoint = wayPoints[i];
            float rightSide = currentWayPoint.RightSide();
            float leftSide = currentWayPoint.LeftSide();
            float topSide = currentWayPoint.TopSide();
            float bottomSide = currentWayPoint.BottomSide();


            if (rightInBounds == false)
                rightInBounds = rightSide >= whiteBox.right;
            if (leftInBounds == false)
                leftInBounds = leftSide <= whiteBox.left;
            if (topInBounds == false)
                topInBounds = topSide >= whiteBox.top;
            if (bottomInBounds == false)
                bottomInBounds = bottomSide <= whiteBox.bottom;


            if (bottomLeftInBounds == false)
                bottomLeftInBounds = bottomSide <= whiteBox.bottom && leftSide <= whiteBox.left;
            if (bottomRightInBounds == false)
                bottomRightInBounds = bottomSide <= whiteBox.bottom && rightSide >= whiteBox.right;
            if (topLeftInBounds == false)
                topLeftInBounds = topSide >= whiteBox.top && leftSide <= whiteBox.left;
            if (topRightInBounds == false)
                topRightInBounds = topSide >= whiteBox.top && rightSide >= whiteBox.right;

        }
        
    }

    void ResetSmoothLock()
    {
        startLerpPosition = postRenderPostion;
        time = 0f;
    }

    void Snap()
    {
        if (leftInBounds && rightInBounds && topInBounds && bottomInBounds &&
            bottomLeftInBounds && bottomRightInBounds && topLeftInBounds && topRightInBounds)
        {
            return;
        }

        if (leftInBounds != prevLeftInBounds ||
             rightInBounds != prevRightInBounds ||
                topInBounds != prevTopInBounds ||
                bottomInBounds != prevBottomInBounds ||
            bottomLeftInBounds != prevBottomLeftInBounds ||
            bottomRightInBounds != prevBottomRightInBounds ||
            topLeftInBounds != prevTopLeftInBounds ||
            topRightInBounds != prevTopRightInBounds)
        {
            ResetSmoothLock();
        }
        startLerpPosition = postRenderPostion; 
        Vector3 tempPos = transform.position;
        transform.SetZPosition(tempPos.z);

        if(leftInBounds && rightInBounds && topInBounds && bottomInBounds)
        {
            //if(!bottomLeftInBounds)
            //{

            //    //if(whiteBox.bottom.DistanceTo(Mathf.Max(GetBottomValues())) >= whiteBox.left.DistanceTo(Mathf.Max(GetLeftValues())))
            //        transform.SetXPosition(Mathf.Max(GetLeftValues()) + whiteBox.width / 2);
            //    //else
            //        transform.SetYPosition(Mathf.Max(GetBottomValues()) + whiteBox.height / 2);
            //}
            //else if(!bottomRightInBounds)
            //{
            //    if(whiteBox.bottom.DistanceTo(Mathf.Max(GetBottomValues())) >= whiteBox.right.DistanceTo(Mathf.Min(GetRightValues())))
            //        transform.SetXPosition(Mathf.Max(GetLeftValues()) - whiteBox.width);
            //    else
            //        transform.SetYPosition(Mathf.Max(GetBottomValues()) + whiteBox.height);
            //}
            //else if(!topLeftInBounds)
            //{
            //    if(whiteBox.bottom.DistanceTo(Mathf.Max(GetBottomValues())) > whiteBox.left.DistanceTo(Mathf.Max(GetLeftValues())))
            //        transform.SetXPosition(Mathf.Max(GetLeftValues()) + whiteBox.width);
            //    else
            //        transform.SetYPosition(Mathf.Max(GetBottomValues()) + whiteBox.height);
            //}
            //else if(!topRightInBounds)
            //{
            //    if(whiteBox.bottom.DistanceTo(Mathf.Max(GetBottomValues())) > whiteBox.left.DistanceTo(Mathf.Max(GetLeftValues())))
            //        transform.SetXPosition(Mathf.Max(GetLeftValues()) + whiteBox.width);
            //    else
            //        transform.SetYPosition(Mathf.Max(GetBottomValues()) + whiteBox.height);
            //}
            //return;
        
        }
        else
        {
            if (!rightInBounds)
            {
                transform.SetXPosition(Mathf.Min(GetRightValues()) - whiteBox.width / 2);
            }
            else if (!leftInBounds)
            {
                transform.SetXPosition(Mathf.Max(GetLeftValues()) + whiteBox.width / 2);
            }
            if (!topInBounds)
            {
                transform.SetYPosition(Mathf.Min(GetTopValues()) - whiteBox.height / 2);
            }
            else if (!bottomInBounds)
            {
                transform.SetYPosition(Mathf.Max(GetBottomValues()) + whiteBox.height / 2);
            }
        }

        if (!bottomLeftInBounds && !bottomRightInBounds && !topLeftInBounds && !topRightInBounds)
        {
            transform.position = target.position;
        }
        time += Time.deltaTime;
        time = time.Min(returnTime);
        transform.position = Vector3.Lerp(startLerpPosition, transform.position, lerpCurve.Evaluate(time / returnTime));
        
    }

    public float[] GetRightValues()
    {
        List<float> values = new List<float>();
        for (int i = 0; i < wayPoints.Length; i++)
        {
            values.Add(wayPoints[i].RightSide());
        }
        return values.ToArray();
    }

    public float[] GetLeftValues()
    {
        List<float> values = new List<float>();
        for (int i = 0; i < wayPoints.Length; i++)
        {
            values.Add(wayPoints[i].LeftSide());
        }
        return values.ToArray();
    }

    public float[] GetTopValues()
    {
        List<float> values = new List<float>();
        for (int i = 0; i < wayPoints.Length; i++)
        {
            values.Add(wayPoints[i].TopSide());
        }
        return values.ToArray();
    }

    public float[] GetBottomValues()
    {
        List<float> values = new List<float>();
        for (int i = 0; i < wayPoints.Length; i++)
        {
            values.Add(wayPoints[i].BottomSide());
        }
        return values.ToArray();
    }


//#if UNITY_EDITOR
    void OnDrawGizmos()
    {
        //UpdateAveragePosition();
#if UNITY_EDITOR
        if (EditorApplication.isPlaying)
            return;
#endif

        whiteBox = GetWorldCorners(whiteZone);
        DrawGizmoBox(whiteBox, ColorX.pink);

        Gizmos.color = Color.black;
        //Debug.Log(preCullPosition.ToString().RTAqua());
        //Debug.Log(postRenderPostiion.ToString().RTAqua());
        Gizmos.DrawLine(prevCamPos, postRenderPostion);

        //CameraBounds redBox = GetWorldCorners(redZone);
        //DrawGizmoBox(redBox, Color.red);

        //CameraBounds greenBox = GetWorldCorners(greenZone);
        //DrawGizmoBox(greenBox, Color.green);

        //CameraBounds blueBox = GetBlueBox();
        //DrawGizmoBox(blueBox, Color.blue);

        //Gizmos.color = ColorX.pink;
        //Gizmos.DrawWireSphere(prevTargetPos, .75f);
    }
//#endif

    CameraBounds GetWorldCorners(Rect rect)
    {
        //rect.yMax
        float zDistance = target.position.z.DistanceTo(transform.position.z);

        Vector3 bottomLeft = Camera.main.ScreenToWorldPoint(
            new Vector3(
                rect.xMin * Screen.width,
                rect.yMin * Screen.height,
                zDistance));

        Vector3 topLeft = Camera.main.ScreenToWorldPoint(
            new Vector3(
                rect.xMin * Screen.width,
                rect.yMax * Screen.height,
                zDistance));

        Vector3 bottomRight = Camera.main.ScreenToWorldPoint(
            new Vector3(
                rect.xMax * Screen.width,
                rect.yMin * Screen.height,
                zDistance));

        Vector3 topRight = Camera.main.ScreenToWorldPoint(
            new Vector3(
                rect.xMax * Screen.width,
                rect.yMax * Screen.height,
                zDistance));

        return new CameraBounds() { bottomLeft = bottomLeft, bottomRight = bottomRight, topLeft = topLeft, topRight = topRight };
    }

    void DrawGizmoBox(CameraBounds box, Color color)
    {
        Gizmos.color = color;
        Gizmos.DrawLine(box.topLeft, box.topRight);
        Gizmos.DrawLine(box.bottomLeft, box.bottomRight);
        Gizmos.DrawLine(box.topLeft, box.bottomLeft);
        Gizmos.DrawLine(box.topRight, box.bottomRight);
    }

    void DebugBox(CameraBounds box, Color color)
    {
        Debug.DrawLine(box.topLeft, box.topRight, color);
        Debug.DrawLine(box.bottomLeft, box.bottomRight, color);
        Debug.DrawLine(box.topLeft, box.bottomLeft, color);
        Debug.DrawLine(box.topRight, box.bottomRight, color);
    }
}
