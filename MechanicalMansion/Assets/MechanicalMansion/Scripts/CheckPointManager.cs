using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections;
public class CheckPointManager : SingletonScriptableObject<CheckPointManager> 
{
    public int checkPointID = -1;
#if UNITY_EDITOR
	[MenuItem ("ScriptableObject/CheckPointManager")]
	static void DrawHierarchyDataMenuItem () 
	{
		Selection.activeObject = (UnityEngine.Object)CheckPointManager.Instance;
	}
#endif
}
