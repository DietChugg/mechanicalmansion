﻿using UnityEngine;
using System.Collections;

public class PlaySoundAfterTime : StateMachineBehaviour {

    public FloatClamp timerLeft;
    public FloatClamp timerRIght;
    public AudioClip clip;
    public bool leftPlayed;
    public bool rightPlayed;
	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        timerLeft.Maximize();
        timerRIght.Maximize();
        leftPlayed = false;
        rightPlayed = false;
        animator.GetComponent<YetiBoss>().leftShock.Play();
        animator.GetComponent<YetiBoss>().rightShock.Play();
    }

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        timerLeft -= Time.deltaTime;
        if (timerLeft.IsAtMin() && !leftPlayed)
        {
            leftPlayed = true;
            timerLeft.Maximize();
            AudioSource.PlayClipAtPoint(clip, animator.GetComponent<YetiBoss>().leftHor.transform.position);
            animator.GetComponent<YetiBoss>().leftShock.Stop();
            animator.GetComponent<YetiBoss>().leftShock.Clear();
        }
        timerRIght -= Time.deltaTime;
        if (timerRIght.IsAtMin() && !rightPlayed)
        {
            rightPlayed = true;

            timerRIght.Maximize();
            AudioSource.PlayClipAtPoint(clip, animator.GetComponent<YetiBoss>().rightHor.transform.position);
            animator.GetComponent<YetiBoss>().rightShock.Stop();
            animator.GetComponent<YetiBoss>().rightShock.Clear();
        }
    }

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	//override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
