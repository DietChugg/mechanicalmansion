﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using StarterKit;

public class BeginBossBattle : MonoBehaviourPlus 
{
    public Animator lights;
    public float targetXPos;
    public float snapToRange = .3f;
    GameObject player;

	public IEnumerator StartBossBattle(GameObject player)
	{
        GetComponent<BoxCollider2D>().enabled = false;
        this.player = player;
        PlayerMotor playerMotor = player.GetComponent<PlayerMotor>();
        playerMotor.autoMove = true;
        playerMotor.controllingAutoMove = true;
        playerMotor.lockHorizontal = true;
        bool atTarget = false;
        bool isRightOfTarget = targetXPos < player.transform.position.x;
        if (playerMotor._velocity.y > 0)
            playerMotor._velocity.y *= -1;
        //player.GetComponent<ShootingAimer>().enabled = false;
        Debug.Log("isRightOfTarget");
        while (!atTarget)
        {
            if (isRightOfTarget)
            {
                playerMotor.isFacingRight = !false;
                playerMotor.lockHorizontatTo = new Vector3(-playerMotor.runSpeed,playerMotor._velocity.y, playerMotor._velocity.z);

                if (targetXPos >= player.transform.position.x)
                {
                    atTarget = true;
                }
            }
            else
            {
                playerMotor.isFacingRight = !true;
                playerMotor.lockHorizontatTo = new Vector3(playerMotor.runSpeed, playerMotor._velocity.y, playerMotor._velocity.z);

                if (targetXPos <= player.transform.position.x)
                {
                    atTarget = true;
                }
            }
            if (atTarget)
            {
                player.transform.SetXPosition(targetXPos);
                playerMotor.lockHorizontatTo = Vector3.zero;
                playerMotor.isFacingRight = !true;
                atTarget = true;
            }
            yield return null;
        }
		yield return null;
        YetiBoss.ActivateBoss.Call(player);
        lights.SetTrigger("Activate");
	}

    public void RegainControls()
    {
        Debug.Log("RegainControls()");
        player.GetComponent<ShootingAimer>().enabled = true;
        PlayerMotor playerMotor = player.GetComponent<PlayerMotor>();
        playerMotor.autoMove = false;
        playerMotor.lockHorizontal = true;
        playerMotor.lockHorizontatTo = Vector3.zero;
        gameObject.SetActive(false);
    }

	public new void OnEnable () 
	{
		(this as MonoBehaviourPlus).OnEnable();
        YetiIntro.OnIntroComplete += RegainControls;
	}

	public new void OnDisable () 
	{
		(this as MonoBehaviourPlus).OnDisable();
        YetiIntro.OnIntroComplete -= RegainControls;
	}
	
	public void OnTriggerEnter2D (Collider2D other) 
	{
        if (other.transform.CompareTag("Player"))
        {
            StartCoroutine(StartBossBattle(other.gameObject));
        }
	}
}
