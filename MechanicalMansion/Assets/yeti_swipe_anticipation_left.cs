﻿using UnityEngine;
using System.Collections;

public class yeti_swipe_anticipation_left : StateMachineBehaviour {
    public bool isVertical;
    public bool isHorizontal;
    public bool enterEnableVertical;
    public bool enterDisableVertical;
    public bool exitEnableVertical;
    public bool exitDisableVertical;
    public bool enterEnableHorizontal;
    public bool enterDisableHorizontal;
    public bool exitEnableHorizontal;
    public bool exitDisableHorizontal;
	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (isVertical && enterEnableVertical)
        {
            animator.GetComponent<YetiBoss>().leftVer.GetComponent<BoxCollider2D>().enabled = true;
            animator.GetComponent<YetiBoss>().rightVer.GetComponent<BoxCollider2D>().enabled = true;

        }
        else if (isHorizontal && enterEnableHorizontal)
        {
            animator.GetComponent<YetiBoss>().leftHor.GetComponent<BoxCollider2D>().enabled = true;
            animator.GetComponent<YetiBoss>().rightHor.GetComponent<BoxCollider2D>().enabled = true; 
        }
        if (isVertical && enterDisableVertical)
        {
            animator.GetComponent<YetiBoss>().leftVer.GetComponent<BoxCollider2D>().enabled = false;
            animator.GetComponent<YetiBoss>().rightVer.GetComponent<BoxCollider2D>().enabled = false; 
        }
        else if (isHorizontal && enterDisableHorizontal)
        {
            animator.GetComponent<YetiBoss>().leftHor.GetComponent<BoxCollider2D>().enabled = false;
            animator.GetComponent<YetiBoss>().rightHor.GetComponent<BoxCollider2D>().enabled = false; 
        }

    }

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	//override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (isVertical && exitEnableVertical)
        {
            animator.GetComponent<YetiBoss>().leftVer.GetComponent<BoxCollider2D>().enabled = true;
            animator.GetComponent<YetiBoss>().rightVer.GetComponent<BoxCollider2D>().enabled = true; 
        }
        else if (isHorizontal && exitEnableHorizontal)
        {
            animator.GetComponent<YetiBoss>().leftHor.GetComponent<BoxCollider2D>().enabled = true;
            animator.GetComponent<YetiBoss>().rightHor.GetComponent<BoxCollider2D>().enabled = true; 
        }
        if (isVertical && exitDisableVertical)
        {
            animator.GetComponent<YetiBoss>().leftVer.GetComponent<BoxCollider2D>().enabled = false;
            animator.GetComponent<YetiBoss>().rightVer.GetComponent<BoxCollider2D>().enabled = false; 
        }
        else if (isHorizontal && exitDisableHorizontal)
        {
            animator.GetComponent<YetiBoss>().leftHor.GetComponent<BoxCollider2D>().enabled = false;
            animator.GetComponent<YetiBoss>().rightHor.GetComponent<BoxCollider2D>().enabled = false; 
        }
    }

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
