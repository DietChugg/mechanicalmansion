﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UpdateFog : MonoBehaviour {

    public List<Material> fogMaterials = new List<Material>();

    void OnEnable()
    {
        UpdateFogPlease();
    }


	public void UpdateFogColor (Color col) 
    {
        //Color fogColor = Camera.main.backgroundColor;

        for (int i = 0; i < fogMaterials.Count; i++)
        {
            fogMaterials[i].SetColor("_Fog", col);
        }
	
	}

    [ContextMenu("Update Fog")]
    public void UpdateFogPlease()
    {
        Debug.Log("Here");
        UpdateFogColor(Camera.main.backgroundColor); 
    }
}
