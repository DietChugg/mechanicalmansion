﻿using UnityEngine;
using System.Collections;
using StarterKit;
using Rewired;
using StarterKit.LifeSystem;
using Prime31;


public class PlayerMotor : MonoBehaviourPlus
{
    public CameraKit2D cameraKit2D;
    public Animator animator;
    public bool autoMove;
    public Vector3 moveSpeed;
	public string horizontalInput;
	public string jumpInput;
	public string runInput; 

	// movement config
	public float gravity = -25f;
	public float walkSpeed = 5f;
	public float runSpeed = 8f;
	public float groundDamping = 20f; // how fast do we change direction? higher means faster
	public float inAirDamping = 5f;
	public float jumpHeight = 3f;
	public float runJumpHeight = 5f;
    public float stopRisingSpeed = 3f;

    public float riseGravity;
    public float fallGravity;
    public float minRiseTime = .3f;

	float normalizedHorizontalSpeed = 0;
    [HideInInspector]
	public  CharacterController2D _controller;
//	private Animator _animator;
	private RaycastHit2D _lastControllerColliderHit;
	public Vector3 _velocity;
    bool isJumping;
    float timeSpendJumping = 0f;
    bool releasedJumpButton = true;
    public float minVerticalSpeed;
    public float maxVerticalSpeed;
    public int playerId;
    Player player;
    public float horizontalKickback;
    public float verticalKickback;

    public bool lockWallJump;
    bool canWallJump;
    public bool isFacingRight;
    bool lastWallJumpIsRight;
    private bool isKickback;
    Vector2 applyKickback;
    float timeToWallUnstick;
    public float wallStickTime;
    public float wallSlideSpeedMax;

    public float speedUp;
    public float slowDown;
    //public AnimationCurve speedCurve;
    public float timeBetweenWallJumps = .4f;
    float wallKickCurrent = 0f;

    float lastHorizontalInput;

    public GameObject vaccum;
    public float vaccumDamping = 4f;
    Vector3 vaccumVelocity;
    float kickBackDamping;
    float currentKickBackDamping;
    AnimationCurve kickbackCurve;
    AnimationCurve blendPlayerControls;
    bool rightKickBack;
    public bool lockHorizontal = true;
    public Vector3 lockHorizontatTo;
    public AudioClip[] jumpAudio;
    public AudioClip[] landAudio;
    public AudioClip[] ouchAudio;
    public AudioClip[] healAudio;
    bool wasGrounded;
    public float volume = 0.1f;

    public bool CanWallJump
    {
        get 
        {
            if (lockWallJump == true)
                return false;
            if (_controller.isGrounded)
                return false;
            if (_controller.collisionState.wallSliding || timeToWallUnstick > 0)
            {
                return true;
            }
            return false;
        }
    }

    public ExternalForce externalForce = null;

    [System.Serializable]
    public class ExternalForce
    {
        public Vector3 force;
        public float duration;
        public AnimationCurve curve;
    }

    float currentDuration = 0f;
    public bool controllingAutoMove = true;
        


	void Awake()
	{
//		_animator = GetComponent<Animator>();
		_controller = GetComponent<CharacterController2D>();

		// listen to some events for illustration purposes
        
	}

    void OnEnable()
    {
        player = ReInput.players.GetPlayer(playerId);
        _controller.onStay += onControllerCollider;
        _controller.onEnter += OnControllerColliderEnter;
        _controller.onExit += onTriggerExitEvent;
        GetComponent<Health>().OnTakeDamage += OnTakeDamage;
        GetComponent<Health>().OnDeath += OnDeath;
        GetComponent<Health>().OnRecoveredHealth += OnRecoveredHealth;
    }

    void OnDisable()
    {
        _controller.onStay -= onControllerCollider;
        _controller.onEnter -= OnControllerColliderEnter;
        _controller.onExit -= onTriggerExitEvent;
        GetComponent<Health>().OnTakeDamage -= OnTakeDamage;
        GetComponent<Health>().OnDeath -= OnDeath;
        GetComponent<Health>().OnRecoveredHealth -= OnRecoveredHealth;

    }


	#region Event Listeners

    void OnDeath()
    {
        if (ouchAudio.Length > 0)
            AudioSource.PlayClipAtPoint(ouchAudio.ToList().GetRandom(), transform.position, volume);
    }

    void OnRecoveredHealth(int healthRecovered)
    {
        if (healAudio.Length > 0)
        {
            AudioSource.PlayClipAtPoint(healAudio.ToList().GetRandom(), transform.position, volume*2);
        }
    }

	void onControllerCollider( Character2DHit hit )
	{
        //Debug.Log(hit.transform.gameObject.name);
		// bail out on plain old ground hits cause they arent very interesting
        //if( hit.normal.y == 1f )
        //    return;

		// logs any collider hits if uncommented. it gets noisy so it is commented out for the demo
		//Debug.Log( "flags: " + _controller.collisionState + ", hit.normal: " + hit.normal );
	}

    void OnControllerColliderEnter(Character2DHit col)
	{
        //Debug.Log("onTriggerEnterEvent: " + col.transform.gameObject.name);
	}


    void onTriggerExitEvent(Character2DHit col)
	{
        //Debug.Log( "onTriggerExitEvent: " + col.transform.gameObject.name);
	}

	#endregion

    public void OnTakeDamage(int damageDealt, DamageInflictor damageInflictor)
    {
        if (ouchAudio.Length > 0)
            AudioSource.PlayClipAtPoint(ouchAudio.ToList().GetRandom(), transform.position, volume);

        rightKickBack = transform.position.x <= damageInflictor.transform.position.x;

        if (damageInflictor.overrideKickbackDirection)
            rightKickBack = damageInflictor.isRightKickBack;

        if (rightKickBack)
        {
            applyKickback.x = -damageInflictor.kickback.x;
            applyKickback.y = damageInflictor.kickback.y;
        }
        else
        {
            applyKickback.x = damageInflictor.kickback.x;
            applyKickback.y = damageInflictor.kickback.y;
        }

        kickbackCurve = damageInflictor.kickbackCurve;
        kickBackDamping = damageInflictor.lifeSpan;
        blendPlayerControls = damageInflictor.blendPlayerControls;
    }


	// the Update loop contains a very simple example of moving the character around and controlling the animation
	void Update()
	{
        if (player == null)
            return;


        if (!wasGrounded && _controller.isGrounded && animator.GetFloat("Jumping") > .2f)
        {
            if (landAudio.Length > 0)
            {
                AudioSource.PlayClipAtPoint(landAudio.ToList().GetRandom(), transform.position, volume);//RandomX.Range(.375f, .625f));
            }
        }
        wasGrounded = _controller.isGrounded;
        

        //Debug.Log(_velocity.x);
        bool isForward = isFacingRight == _velocity.x > 0;
        if (autoMove)
            isForward = false;
        //Debug.Log(isForward); 

        float maxSpeed = isFacingRight == _velocity.x > 0 ? walkSpeed : runSpeed; 

        
        
        animator.SetBool("Forward",!isForward);//NOTE THIS IS WRONG BUT IT MOSTLY WORKS JUST LEAVING IT FOR NOW FOR THE SAKE OF TIME
        //if (!isFacingRight == _velocity.x <= 0)
        //{
        //    animator.SetBool("Forward", isForward);
        //}
		// grab our current _velocity to use as a base for all calculations
		_velocity = _controller.velocity;
        if (!autoMove)
        {
            if (applyKickback != Vector2.zero)
            {
                //Debug.Log(applyKickback);
                _controller.collisionState.below = false;
                transform.Translate(0, .001f, 0);
                _velocity.x += applyKickback.x;//isFacingRight ? -1 * applyKickback.x : 
                _velocity.y += applyKickback.y;
                applyKickback = Vector3.Lerp(applyKickback, Vector2.zero, kickbackCurve.Evaluate(currentKickBackDamping / kickBackDamping));
                currentKickBackDamping += Time.deltaTime;
                currentKickBackDamping = currentKickBackDamping.Clamp(0, kickBackDamping);
            }
            else
            {
                currentKickBackDamping = 0f;
            } 
        }


        if (_controller.isGrounded)
        {
            isJumping = false;
            animator.SetFloat("Jumping",0);
            _controller.collisionState.wallSlidingWall = null;
            _controller.collisionState.wallSliding = false;
            _velocity.y = 0;
            timeSpendJumping = 0f;
            //lastWallJumpIsRight = null;
            isKickback = false;
        }
        else
        {
            maxSpeed = runSpeed;
            
            if (_velocity.y > 0 && !releasedJumpButton)
            {
                gravity = riseGravity;
            }
            else
            {
                gravity = fallGravity;
            }
        } 

        float normalizedHorizontalRunSpeed = 0;

        if (!autoMove)
        {
            if (player.GetAxisRaw(horizontalInput) > 0.1f)
		    {
                if (lastHorizontalInput < .1f)
                    normalizedHorizontalSpeed = normalizedHorizontalSpeed * -1;
                normalizedHorizontalSpeed = normalizedHorizontalSpeed.MoveTowards(1, Time.deltaTime * speedUp);


                normalizedHorizontalRunSpeed = 1;

    //			if( _controller.isGrounded )
    //				_animator.Play( Animator.StringToHash( "Run" ) );
                animator.SetFloat("Speed", normalizedHorizontalSpeed.Abs());
		    }
            else if (player.GetAxisRaw(horizontalInput) < -0.1f)
		    {
                if (lastHorizontalInput > -.1f)
                    normalizedHorizontalSpeed = normalizedHorizontalSpeed * -1;
                normalizedHorizontalSpeed = normalizedHorizontalSpeed.MoveTowards(-1, Time.deltaTime * speedUp);

                normalizedHorizontalRunSpeed = -1;

    //			if( _controller.isGrounded )
    //				_animator.Play( Animator.StringToHash( "Run" ) );
                animator.SetFloat("Speed", normalizedHorizontalSpeed.Abs());
		    }
		    else
		    {
                normalizedHorizontalSpeed = normalizedHorizontalSpeed.MoveTowards(0, Time.deltaTime * slowDown);
    //			if( _controller.isGrounded )
    //				_animator.Play( Animator.StringToHash( "Idle" ) );
                animator.SetFloat("Speed", normalizedHorizontalSpeed.Abs());
		    }
            lastHorizontalInput = player.GetAxisRaw(horizontalInput);



            if (_controller.isGrounded && (player.GetButtonDown("Vacuum") || player.controllers.Mouse.GetButtonDown(1)))
            {
                vaccum.GetComponent<Vaccum>().Enable();
            }
            if (player.GetButton("Vacuum") || player.controllers.Mouse.GetButton(1))
            {
                normalizedHorizontalSpeed = normalizedHorizontalSpeed.MoveTowards(0, Time.deltaTime * vaccumDamping);
                _velocity = Vector3.MoveTowards(_velocity, new Vector3(0, _velocity.y, _velocity.z), vaccumDamping * Time.deltaTime);
            }
            else if (player.GetButtonUp("Vacuum") || player.controllers.Mouse.GetButtonUp(1) || !_controller.isGrounded)
            {
                vaccum.GetComponent<Vaccum>().Disable();
            } 
        }


        //if (!_controller.isGrounded)
        //{
        //    normalizedHorizontalSpeed = normalizedHorizontalRunSpeed;
        //}

        

        
        //wallSliding = false;
        //if ((controller.collisions.left || controller.collisions.right) && !controller.collisions.below && velocity.y < 0)
        //{
        //    wallSliding = true;

        //    if (velocity.y < -wallSlideSpeedMax)
        //    {
        //        velocity.y = -wallSlideSpeedMax;
        //    }

        //    if (timeToWallUnstick > 0)
        //    {
        //        velocityXSmoothing = 0;
        //        velocity.x = 0;

        //        if (input.x != wallDirX && input.x != 0)
        //        {
        //            timeToWallUnstick -= Time.deltaTime;
        //        }
        //        else
        //        {
        //            timeToWallUnstick = wallStickTime;
        //        }
        //    }
        //    else
        //    {
        //        timeToWallUnstick = wallStickTime;
        //    }

        //}

        if (_controller.collisionState.wallSliding)
        {
            lastWallJumpIsRight = _controller.collisionState.isWallSlidingRight;
            if (_velocity.y < -wallSlideSpeedMax)
            {
                _velocity.y = -wallSlideSpeedMax;
            }
            if (_controller.collisionState.canWallSlide)
            _velocity.x = 0;
            timeToWallUnstick = wallStickTime;
        }
        else
        {
            timeToWallUnstick -= Time.deltaTime;
            if (timeToWallUnstick < 0)
                _controller.collisionState.wallSliding = false;
        }


        //Debug.Log(_controller.collisionState.wallSliding);

        //if (timeToWallUnstick > 0)


        if (!autoMove)
        {
            animator.SetBool("IsSliding", _controller.collisionState.wallSliding);
            animator.SetBool("IsGrounded", _controller.isGrounded);
            animator.SetBool("IsRising", _velocity.y > 0);
        }
        //if(_controller.collisionState.wallSliding && animator.transform.eulerAngles.y != 90 )
        //{
        //    animator.transform.Rotate(0, 180f, 0);
        //}
        //else if (!_controller.collisionState.wallSliding && animator.transform.localScale.x != 270)
        //{
        //    animator.transform.Rotate(0, -180f, 0);
        //}



		// we can only jump whilst grounded
        //bool wallSlideEnabled = (lastWallJumpIsRight == null || 
        //    (isFacingRight && lastWallJumpIsRight == false || 
        //    !isFacingRight && lastWallJumpIsRight == true));
        //bool justWallJumped = false;
        if (!autoMove)
        {
            if (_controller.isGrounded && player.GetButtonDown(jumpInput))
            {
                //if (!string.IsNullOrEmpty(runInput))
                //    _velocity.y = Mathf.Sqrt(2f * (player.GetButton(runInput) ? runJumpHeight : jumpHeight) * -gravity);
                //else
                _velocity.y = Mathf.Sqrt(2f * (jumpHeight) * -gravity);
                isJumping = true;
                StartCoroutine(JumpAnim());
                releasedJumpButton = false;
                transform.Translate(0, .001f, 0);
                //			_animator.Play( Animator.StringToHash( "Jump" ) );
                if (jumpAudio.Length > 0)
                    AudioSource.PlayClipAtPoint(jumpAudio.ToList().GetRandom(), transform.position, volume);
            }
            else if (CanWallJump && player.GetButtonDown(jumpInput))
            {
                _controller.collisionState.wallSliding = false;
                //if (!string.IsNullOrEmpty(runInput))
                //    _velocity.y = Mathf.Sqrt(2f * verticalKickback * -gravity);
                //else
                _velocity.y = Mathf.Sqrt(2f * (verticalKickback) * -gravity);
                isJumping = true;
                StartCoroutine(JumpAnim());
                isKickback = true;
                releasedJumpButton = false;
                timeToWallUnstick = 0;
                wallKickCurrent = 0;
                //lastWallJumpIsRight = isFacingRight;
                //Debug.Log(lastWallJumpIsRight);
                _velocity.x = !lastWallJumpIsRight ? horizontalKickback : horizontalKickback * -1;

                _controller.collisionState.canWallSlide = false;
                Invoke(timeBetweenWallJumps, delegate()
                {
                    _controller.collisionState.canWallSlide = true;
                });

                transform.Translate(0, .001f, 0);
                if (jumpAudio.Length > 0)
                    AudioSource.PlayClipAtPoint(jumpAudio.ToList().GetRandom(), transform.position, volume);//RandomX.Range(.375f, .625f));
            }

            if (!_controller.isGrounded && !isKickback && !player.GetButton(jumpInput) && _velocity.y > 0 && isJumping && !releasedJumpButton)
            {

                releasedJumpButton = true;
                //Debug.Log(minRiseTime - timeSpendJumping);
                if (minRiseTime <= timeSpendJumping)
                    Release();
                else
                    Invoke("Release", minRiseTime - timeSpendJumping);
            } 
        }

        if (isJumping)
            timeSpendJumping += Time.deltaTime;


		// apply horizontal speed smoothing it
		var smoothedMovementFactor = _controller.isGrounded ? groundDamping : inAirDamping; // how fast do we change direction?
        //if (!string.IsNullOrEmpty(runInput))
        //    _velocity.x = Mathf.Lerp(_velocity.x, normalizedHorizontalSpeed * (player.GetButton(runInput) ? runSpeed : walkSpeed), Time.deltaTime * smoothedMovementFactor);
        //else

        if (_controller.collisionState.canWallSlide && applyKickback == Vector2.zero)
            _velocity.x = Mathf.Lerp(_velocity.x,
                Mathf.Sign(normalizedHorizontalSpeed) * Mathf.Lerp(0, maxSpeed, (normalizedHorizontalSpeed).Abs()),
                Time.deltaTime * smoothedMovementFactor);
        else if (applyKickback != Vector2.zero)
        {
            //float origPlayerControls = _velocity.x = Mathf.Lerp(_velocity.x,
            //    Mathf.Sign(normalizedHorizontalSpeed) * Mathf.Lerp(0, maxSpeed, (normalizedHorizontalSpeed).Abs()),
            //    Time.deltaTime * smoothedMovementFactor);


            if (!autoMove)
            {
                float normalPlayerControls = 0f;

                if (rightKickBack)
                    normalPlayerControls = Mathf.Sign(_velocity.x * -1) *
                        Mathf.Lerp(0, maxSpeed, (normalizedHorizontalSpeed).Abs());
                else
                    normalPlayerControls = Mathf.Sign(_velocity.x * 1) *
                        Mathf.Lerp(0, maxSpeed, (normalizedHorizontalSpeed).Abs());


                _velocity.x = Mathf.Lerp(_velocity.x
                    , normalPlayerControls,
                    blendPlayerControls.Evaluate(currentKickBackDamping / kickBackDamping)); 
            }
                
                
                
                //Mathf.Lerp(rightKickBack ? horizontalKickback * -1 : horizontalKickback, 0, wallKickCurrent / timeBetweenWallJumps) +
                //Mathf.Lerp(_velocity.x,
                //Mathf.Sign(normalizedHorizontalSpeed) * Mathf.Lerp(0, maxSpeed, (normalizedHorizontalSpeed).Abs()),
                //Time.deltaTime * smoothedMovementFactor);


        }
        else
        {
            wallKickCurrent += Time.deltaTime;
            _velocity.x = Mathf.Lerp( lastWallJumpIsRight?horizontalKickback*-1: horizontalKickback,0, wallKickCurrent/timeBetweenWallJumps) + 
                Mathf.Lerp(_velocity.x,
                Mathf.Sign(normalizedHorizontalSpeed) * Mathf.Lerp(0, maxSpeed, (normalizedHorizontalSpeed).Abs()),
                Time.deltaTime * smoothedMovementFactor);
        }

        //animator.SetFloat("Speed", _velocity.x.Abs());
        
        //ORIGIONAL BEFORE MY AJUSTMENTS
        //var smoothedMovementFactor = _controller.isGrounded ? groundDamping : inAirDamping; // how fast do we change direction?
        //_velocity.x = Mathf.Lerp(_velocity.x, normalizedHorizontalSpeed * runSpeed, Time.deltaTime * smoothedMovementFactor);


		// apply gravity before moving
		_velocity.y += gravity * Time.deltaTime;

        _velocity.z = 0;
        _velocity.y = Mathf.Clamp(_velocity.y, minVerticalSpeed, maxVerticalSpeed);

        if (!autoMove)
        {
            if (player.controllers.joystickCount == 0)
            {

                Vector2 screenSpace = Camera.main.WorldToScreenPoint(transform.position).ToVector2();
                isFacingRight = player.controllers.Mouse.screenPosition.x < screenSpace.x;
                //float distance = heading.magnitude;
                //direction = heading / distance; // This is now the normalized direction.


                // && player.controllers.Mouse.GetButtonDown(0)
            }
            else
            {
                //Debug.Log("Controllers Exist and Rock");
                if (player.GetAxisRaw("Horizontal Aim") == 0)
                {
                    if (player.GetAxisRaw("Horizontal Movement") == 1)
                    {
                        isFacingRight = false;
                    }
                    else if (player.GetAxisRaw("Horizontal Movement") == -1)
                    {
                        isFacingRight = true;
                    }
                }
                else if (player.GetAxisRaw("Horizontal Aim") < 0)
                {
                    isFacingRight = true;
                }
                else if (player.GetAxisRaw("Horizontal Aim") > 0)
                {
                    isFacingRight = false;
                }
            }
        }
        if (_controller.collisionState.wallSliding)
        {
            isFacingRight = _controller.collisionState.isWallSlidingRight;
        }


        if(isFacingRight)
        {
            if( transform.localScale.x > 0f )
				transform.localScale = new Vector3( -transform.localScale.x, transform.localScale.y, transform.localScale.z );
        }
        else
	    {
            if( transform.localScale.x < 0f )
				transform.localScale = new Vector3( -transform.localScale.x, transform.localScale.y, transform.localScale.z );
	    }


        if (autoMove && controllingAutoMove)
            _velocity = moveSpeed;

        if (cameraKit2D != null)
            cameraKit2D.isPlayerGrounded = _controller.isGrounded;

        if (!autoMove)
        {
            animator.SetFloat("Speed", Mathf.Lerp(0, maxSpeed, (normalizedHorizontalSpeed).Abs()));
        }
        else
	    {
            animator.SetFloat("Speed", (moveSpeed.x.Sign() * runSpeed).Abs());
            if(moveSpeed.x == 0)
                animator.SetFloat("Speed", 0);
	    }

        
        if (!autoMove)
        {
            if (externalForce != null && externalForce.duration > 0f)
            {

                currentDuration += Time.deltaTime;
                currentDuration = currentDuration.Clamp(0, externalForce.duration);
                isJumping = false;
                //Debug.Log("Applying External Force: " + _velocity);
                _velocity += Vector3.Lerp(externalForce.force, Vector3.zero, externalForce.curve.Evaluate(currentDuration / externalForce.duration));
                //Debug.Log("Result: " + _velocity);

                if (currentDuration / externalForce.duration == 1)
                    externalForce = null;
            }
            else
            {
                currentDuration = 0f;
            } 
        }
        //Debug.Log(_velocity);
        if (lockHorizontal == true && autoMove)
        {
            animator.SetFloat("Speed", lockHorizontatTo.x);
            animator.SetBool("IsGrounded", _controller.isGrounded);
            if (!_controller.isGrounded)
            {
                animator.SetFloat("Jumping", .6f);
            }
            else
            {
                animator.SetFloat("Jumping", 0f);
            }
            _velocity = lockHorizontatTo;
        }
		_controller.move( _velocity * Time.deltaTime );
        

	}

    public void ApplyJump()
    {
        _velocity.y = Mathf.Sqrt(2f * (jumpHeight) * -gravity);
        StartCoroutine(JumpAnim());
        transform.Translate(0, .001f, 0);
    }

    IEnumerator LerpToZeroVelocity()
    { 
        while(_controller.velocity.y > 0)
        {
            _controller.velocity = Vector3.MoveTowards(_controller.velocity, new Vector3(_controller.velocity.x, 0, 0), Time.deltaTime * stopRisingSpeed);
            yield return null;
        }
        yield return null;
    }

    void Release()
    {
        StartCoroutine(LerpToZeroVelocity());
    }

    IEnumerator JumpAnim()
    {
        float jumping = 0;

        while(jumping < 1)
        {
            animator.SetFloat("Jumping", jumping);
            jumping += Time.deltaTime*5;
            yield return null;
        }
    }

}
