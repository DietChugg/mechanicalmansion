﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using StarterKit;

public class MainPlayerTracker : MonoBehaviourPlus 
{
    public bool isPlayerInTrigger;
    public SafeAction OnPlayerEnter;
    public SafeAction OnPlayerStay;
    public SafeAction OnPlayerExit;
    public GameObject player;
         

	public IEnumerator Start()
	{
		yield return null;
	}

	public new void OnEnable () 
	{
		(this as MonoBehaviourPlus).OnEnable();
	}

	public new void OnDisable () 
	{
		(this as MonoBehaviourPlus).OnDisable();
        
	}

    public void Update()
    {
        if (isPlayerInTrigger && GetComponent<Collider2D>().enabled == false)
        {
            isPlayerInTrigger = false;
            OnPlayerExit.Call();
        }
    }
	
	public void OnTriggerEnter2D (Collider2D other) 
	{
        if (other.transform.CompareTag("Player"))
        {
            player = other.gameObject;
            isPlayerInTrigger = true;
            OnPlayerEnter.Call();
        }
	}

    public void OnTriggerStay2D(Collider2D other)
    {
        if (other.transform.CompareTag("Player"))
        {
            player = other.gameObject;
            isPlayerInTrigger = true;
            OnPlayerStay.Call();
        }
    }

    public void OnTriggerExit2D(Collider2D other)
    {
        if (other.transform.CompareTag("Player"))
        {
            isPlayerInTrigger = false;
            OnPlayerExit.Call();
        }
    }
}
