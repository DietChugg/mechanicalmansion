﻿using UnityEngine;
using System.Collections;

public class FlingPlayerBehaviour : StateMachineBehaviour 
{
    //public PlayerMotor.ExternalForce externalForce;
	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        GameObject player = null;
        bool isPlayerdetected = animator.GetComponent<YetiBoss>().leftEyeTrigger.GetComponent<MainPlayerTracker>().isPlayerInTrigger;
        if (isPlayerdetected)
        {
            player = animator.GetComponent<YetiBoss>().leftEyeTrigger.GetComponent<MainPlayerTracker>().player;
        }
        else
        {
            isPlayerdetected = animator.GetComponent<YetiBoss>().rightEyeTrigger.GetComponent<MainPlayerTracker>().isPlayerInTrigger;
            if (isPlayerdetected)
            {
                player = animator.GetComponent<YetiBoss>().rightEyeTrigger.GetComponent<MainPlayerTracker>().player;
            }
        }

        if (player != null)
        {
            //Debug.Log("FWING!!!".RTOrange());
            player.GetComponent<PlayerMotor>().externalForce = animator.GetComponent<YetiBoss>().externalForce;
        }

    }

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	//override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	//override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
