﻿using UnityEngine;
using System.Collections;

public class Gear : MonoBehaviour 
{
    public int gearNumber;
    public GameObject collectedFX;

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.name == "PlayerCol")
        {
            Stats.Instance().FoundGear(gearNumber);
            collectedFX.Instantiate(transform.position, transform.rotation);
            Destroy(this.gameObject);
        }
    }
}
