﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using StarterKit;

public class CheckPointController : MonoBehaviourPlus 
{
    public Transform playerTransform;

	public IEnumerator Start()
	{
        //Transform playerTransform = GameObject.Find("Main Player").transform;
        CheckPoint[] checkPoints = UnityEngine.Object.FindObjectsOfType<CheckPoint>();
        bool foundPlayer = false;
        for (int i = 0; i < checkPoints.Length; i++)
        {
            if (checkPoints[i].id == CheckPointManager.Instance.checkPointID)
            {
                playerTransform.position = checkPoints[i].transform.position;
                Camera.main.transform.position = checkPoints[i].transform.Find("CameraPosition").transform.position;
                playerTransform.gameObject.SetActive(true);
                GameObject intro = GameObject.Find("Intro");
                intro.GetComponent<IntroSequence>().TurnOnTheLights();
                intro.SetActive(false);
                foundPlayer = true;

                StartCoroutine(checkPoints[i].colorSwitch.Switch(true));
            }
        }
        if (!foundPlayer)
            playerTransform.gameObject.SetActive(false);
		yield return new WaitForEndOfFrame();
        //for (int i = 0; i < checkPoints.Length; i++)
        //{
        //    if (checkPoints[i].id == CheckPointManager.Instance.checkPointID)
        //    {
        //        playerTransform.position = checkPoints[i].transform.position;
        //        Camera.main.transform.position = checkPoints[i].transform.Find("CameraPosition").transform.position;
        //    }
        //}
	}
}
