﻿using UnityEngine;
using System.Collections;

public class Flicker : MonoBehaviour 
{
    public Gradient _gradient;
    public FloatRange _randomTime = new FloatRange(0.05f, 0.25f);
    private SpriteRenderer _spriteRenderer;

    void OnEnable()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

	IEnumerator Start () 
    {
        while (true)
        {
            if (_spriteRenderer == null)
                OnEnable();
            _spriteRenderer.color = _gradient.Evaluate(Random.Range(0.0f, 1.0f));
            yield return Random.Range(_randomTime.min, _randomTime.max);
        }
	}
	
}
