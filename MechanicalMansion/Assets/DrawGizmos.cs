﻿using UnityEngine;
using System.Collections;

public class DrawGizmos : MonoBehaviour 
{
    public Color color = Color.blue;
    public Vector3 size = Vector3.one;

	void OnDrawGizmos () 
    {
        Gizmos.color = color;
        Gizmos.DrawWireCube(transform.position, size);
	}
}
