Shader "PlatformToolkit/Sprite" 
{
	Properties
	{
		[PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
		_Color ("Tint", Color) = (1,1,1,1)
		[MaterialToggle] PixelSnap ("Pixel snap", Float) = 0

		_Fog("Fog", Color) = (1,1,1,1)
		_CamDist ("Camera Distance", Float) = 15

		_Lighting ("Lighting", 2D) = "black" {}
		_Edges ("Edges", 2D) = "white" {}
	}

	SubShader
	{
		Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
		Cull Off
		Lighting Off
		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
		CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile DUMMY PIXELSNAP_ON
			#include "UnityCG.cginc"
			//#include "PlatformCG.cginc"

			struct appdata_t
			{
				float4 vertex   : POSITION;
				float4 color    : COLOR;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				fixed4 color    : COLOR;
				half2 texcoord  : TEXCOORD0;
				half4 projPos : TEXCOORD1;
				half4 position_in_screen_space : TEXCOORD2;
			};
			
			fixed4 _Color;
			half4 _Fog;
			float _CamDist;
			sampler2D _Lighting;

			v2f vert(appdata_t IN)
			{
				v2f OUT;
				OUT.vertex = mul(UNITY_MATRIX_MVP, IN.vertex);
				OUT.texcoord = IN.texcoord;
				OUT.color = IN.color * _Color;
				#ifdef PIXELSNAP_ON
				OUT.vertex = UnityPixelSnap (OUT.vertex);
				#endif

				OUT.projPos = ComputeScreenPos(OUT.vertex);

				half4 clipSpace =  mul(UNITY_MATRIX_MVP, IN.vertex);
				clipSpace.xy /= clipSpace.w;
				clipSpace.xy = 0.5*(clipSpace.xy+1.0);// * _ScreenParams.xy;
				OUT.position_in_screen_space = clipSpace;

				return OUT;
			}

			sampler2D _MainTex;

			fixed4 frag(v2f IN) : SV_Target
			{
				half4 lighting = tex2D(_Lighting, IN.position_in_screen_space)*2;
				half depth = (_CamDist * _CamDist)/(UNITY_PROJ_COORD(IN.projPos).z * UNITY_PROJ_COORD(IN.projPos).z);
				if(depth > 1)
					depth = 1;
				if(depth < 0)
					depth = 1;
				//fixed4 c = tex2D(_MainTex, IN.texcoord) * IN.color;
				//c.rgb *= c.a;

				half4 finalTexture = tex2D(_MainTex, IN.texcoord);
				half4 fog = lerp(_Fog,finalTexture * (lighting*finalTexture + lighting) * _Color ,depth);
				finalTexture = float4(fog.rgb,finalTexture.a * _Color.a);



				return finalTexture;
			}
		ENDCG
		}
	}
}
