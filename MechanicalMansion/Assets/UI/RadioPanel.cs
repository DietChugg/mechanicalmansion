﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.IO;
using System;
using StarterKit;

public class RadioPanel : MonoBehaviour 
{
    public static SafeAction RadioStarted;
    public static SafeAction<int> RadioNewLine;
    public static SafeAction RadioEnded;

    public Animator _animator;
    public TextAsset _dialogue;
    public Text _display;
    public Button _goOn;

    private string[] _lines;
    private int _lineIndex = 0;
    private bool _playOnce = true;
    private bool _isPlaying = false;
    //public AudioSource typing;
        

    private static RadioPanel _radioPanel;
    public static RadioPanel Instance()
    {
        if (!_radioPanel)
        {
            _radioPanel = FindObjectOfType(typeof(RadioPanel)) as RadioPanel;
            if (!_radioPanel)
                Debug.LogError("There needs to be one active RadioPanel script on a GameObject in your scene.");
        }

        return _radioPanel;
    }

[ContextMenu("Begin")]
    public void Begin()
    {
        if (_playOnce)
        {
            //typing.Play();
            //typing.Pause();
            RadioStarted.Call();
            _animator.SetTrigger("Display");
            _display.text = "";
            _lines = _dialogue.text.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);
            _goOn.onClick.AddListener(Display);
            Display();
        }
    }

	public void Display () 
    {
        if (_lineIndex < _lines.Length)
        {
            if (!_isPlaying)
            {
                RadioNewLine.Call(_lineIndex);
                StartCoroutine(DisplayLine(_lines[_lineIndex]));
            }
            else
            {
                _isPlaying = false;
                _playOnce = false;
                StopAllCoroutines();
                _display.text = _lines[_lineIndex];
                SetButton();
            }
        }
        else
        {
            RadioEnded.Call();
            _goOn.interactable = false;
            _goOn.transform.GetChild(0).GetComponent<Text>().text = "";
            _animator.SetTrigger("Hide");
            Invoke("DisableGameObject", 3);
        }
	}

    void DisableGameObject()
    {
        this.gameObject.SetActive(false);
    }

    IEnumerator DisplayLine(string line)
    {
        _isPlaying = true;
        _goOn.interactable = false;
        _goOn.transform.GetChild(0).GetComponent<Text>().text = "";
        if(_playOnce)
            yield return new WaitForSeconds(2f);
        int index = 0;

        while (index < line.Length)
        {
            _display.text = line.Substring(0, index);

            yield return new WaitForSeconds(0.05f);
            index++;
        }
        _playOnce = false;
        SetButton();
        _isPlaying = false;
    }

    void SetButton()
    {
        _lineIndex++;
        _goOn.interactable = true;
        _goOn.transform.GetChild(0).GetComponent<Text>().text = "Go On";
    }
}
