﻿using UnityEngine;
using System.Collections;

public class StatsTester : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
    {

        if (Input.GetKeyDown(KeyCode.UpArrow))
            Stats.Instance().Health++;

        if (Input.GetKeyDown(KeyCode.DownArrow))
            Stats.Instance().Health--;

        if (Input.GetKeyDown(KeyCode.RightArrow))
            Stats.Instance().Spirits++;

        if (Input.GetKeyDown(KeyCode.LeftArrow))
            Stats.Instance().Spirits--;

        if (Input.GetKeyDown(KeyCode.Alpha0))
            Stats.Instance().FoundGear(0);

        if (Input.GetKeyDown(KeyCode.Alpha1))
            Stats.Instance().FoundGear(1);

        if (Input.GetKeyDown(KeyCode.Alpha2))
            Stats.Instance().FoundGear(2);

        if (Input.GetKeyDown(KeyCode.Alpha3))
            Stats.Instance().FoundGear(3);

        if (Input.GetKeyDown(KeyCode.Alpha4))
            Stats.Instance().FoundGear(4);
	}
}
