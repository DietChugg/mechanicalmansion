﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using StarterKit;
using System;

public class Stats : MonoBehaviour 
{
    public static SafeAction PlayerDead;
    public Animator gameOver;

    [SerializeField]
    private Animator heartsAnimator;
    [SerializeField]
    private Image[] heartIcons;
    [SerializeField]
    private Animator gearPanel;
    [SerializeField]
    private Image[] gearIcons;
    [SerializeField]
    private Animator spiritsAnimator;
    [SerializeField]
    private Text spiritsText;

    public GameObject levelComplete;
    public Text gearsCollected;
    public Text spiritsCollected;
    public Text completionTime;
    public float timePlayed;

    private List<int> foundGearIDs = new List<int>();
    private int _health = 5;
    public int Health
    {
        get
        {
            return _health;
        }
        set
        {
            string trigger = value > _health ? "Gained" : "Lost";
            heartsAnimator.SetTrigger(trigger);

            _health = value.Clamp(0, 5);
            SetHearts();
            if (_health <= 0)
            {
                PlayerDead.Call();
                gameOver.SetTrigger("Death");
            }
        }
    }

    public void Update()
    {
        timePlayed += Time.deltaTime;
    }

    private int _gears = 0;
    private int Gears
    {
        get
        {
            return _gears;
        }
        set
        {
            _gears = value.Clamp(0, 5);

            if (_gears <= 5)
                AnimateGears();
        }
    }

     private int _spirits = 0;
     public int Spirits
     {
         get
         {
             return _spirits;
         }
         set
         {
             _spirits = value;
             spiritsAnimator.SetTrigger("Gained");
             spiritsText.text = "x  " + _spirits.ToString();
         }
     }

    private static Stats _stats;

    public static Stats Instance()
    {
        if (!_stats)
        {
            _stats = FindObjectOfType(typeof(Stats)) as Stats;
            if (!_stats)
                Debug.LogError("There needs to be one active Stats script on a GameObject in your scene.");
        }

        return _stats;
    }

    public void FoundGear(int id)
    {
        foreach (int alreadyFoundID in foundGearIDs)
        {
            if (alreadyFoundID == id)
                return;
        }

        if (id >= 0 && id <= 4)
        {
            foundGearIDs.Add(id);
            Gears++;
        }
    }


    private void SetHearts()
    {
        for (int i = 0; i < heartIcons.Length; i++)
        {
            if (i < _health)
                heartIcons[i].enabled = true;
            else
                heartIcons[i].enabled = false;
        }
    }

    public void ShowLevelComplete()
    {
        levelComplete.SetActive(true);
        TimeSpan timeSpan = TimeSpan.FromSeconds(timePlayed);
        completionTime.text = "" + timeSpan.Minutes.ToString("D2") + ":" + timeSpan.Seconds.ToString("D2");
        gearsCollected.text = "" + Gears + " / " + "5";
        spiritsCollected.text = "x " + Spirits;
    }

    private void AnimateGears()
    {
        gearPanel.SetTrigger("Show");

        for (int i = 0; i < gearIcons.Length; i++)
        {
            gearIcons[i].enabled = false;
            foreach(int id in foundGearIDs)
            {
                if (i == id)
                    gearIcons[i].enabled = true;
            }
        }
    }
}
