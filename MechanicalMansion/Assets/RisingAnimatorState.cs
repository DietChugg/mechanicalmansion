﻿using UnityEngine;
using System.Collections;
using StarterKit;
using StarterKit.LifeSystem;

public class RisingAnimatorState : StateMachineBehaviour {

	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.speed = 0f;
        Transform parent = animator.transform.parent;
        parent.GetComponent<DamageInflictor>().canDamage = false;
        parent.GetComponent<BoxCollider2D>().isTrigger = true;
        parent.GetComponent<Clown>().hammer.GetComponent<DamageInflictor>().canDamage = false;
        parent.GetComponent<Clown>().hammer.GetComponent<BoxCollider2D>().isTrigger = true;
        //parent.GetComponent<Clown>().hammer.GetComponent<BoxCollider2D>().enabled = true;
        
        parent.GetComponent<Clown>().GetComponent<Health>().invincible = false;

    }

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	//override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Transform parent = animator.transform.parent;
        parent.GetComponent<Clown>().wall.SetActive(true);
        parent.GetComponent<Clown>().swing.SetActive(true);
        parent.GetComponent<Clown>().floor.SetActive(true);
        parent.GetComponent<EnemyMotor>().enabled = true;
        parent.GetComponent<DamageInflictor>().canDamage = true;
        parent.GetComponent<BoxCollider2D>().enabled = true;
        parent.GetComponent<Clown>().hammer.GetComponent<DamageInflictor>().canDamage = true;
        parent.GetComponent<BoxCollider2D>().isTrigger = false;
        parent.GetComponent<Clown>().hammer.GetComponent<BoxCollider2D>().enabled = true;
    }

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
