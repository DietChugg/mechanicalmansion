﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using WayPoints;

public class IntroSequence : MonoBehaviour 
{
    public GameObject player;
    public GameObject ceiling;
    public WayPointSystem whisp;
    public List<ColorTransition> colorTransitionsDoorGate = new List<ColorTransition>();
    public List<ColorTransition> colorTransitions = new List<ColorTransition>();
    public GameObject[] gears;

	IEnumerator Start () 
    {
        ceiling.SetActive(false);
        yield return new WaitForSeconds(3);

        player.GetComponent<PlayerMotor>().autoMove = true;
        player.GetComponent<PlayerMotor>().lockHorizontatTo = new Vector3(0, -10f, 1);
        player.GetComponent<PlayerMotor>().lockHorizontal = true;
        player.GetComponent<PlayerMotor>().controllingAutoMove = false;
        player.SetActive(true);
        
        yield return new WaitForSeconds(1.5f);
        ceiling.SetActive(true);

        RadioPanel.RadioEnded += OnRadioEnded;
        RadioPanel.RadioNewLine += OnRadioNewLine;
        RadioPanel.Instance().Begin();
	}

    void OnRadioEnded()
    {
        player.GetComponent<PlayerMotor>().autoMove = false;
        player.GetComponent<PlayerMotor>().controllingAutoMove = false;

        RadioPanel.RadioEnded -= OnRadioEnded;

        foreach (ColorTransition transition in colorTransitionsDoorGate)
        {
            transition.SetColor();
        }
	}

    void OnRadioNewLine(int lineIndex)
    {
        if(lineIndex == 10)
        {
            whisp._canMove = true;
            whisp.GetComponent<AudioSource>().Play();
            RadioPanel.RadioNewLine -= OnRadioNewLine;
        }
    }

    public void TurnOnTheLights()
    {
        GetComponent<AudioSource>().Stop();
        foreach(ColorTransition transition in colorTransitions)
        {
            transition.SetColor();
        }

        foreach (GameObject gear in gears)
        {
            gear.GetComponent<Rotater>().enabled = true;
        }
        Camera.main.GetComponent<AudioSource>().Play();
    }
}
