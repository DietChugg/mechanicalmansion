﻿using UnityEngine;
using System.Collections;

public class ColorTransition : MonoBehaviour 
{
    public bool isSprite;
    public bool isCamera;

    public UpdateFog updateFog;

    public float time;
    public Gradient gradient;
	
	public void SetColor () 
    {
        StartCoroutine("Set");
	}

    IEnumerator Set () 
    {
        float currentTime = 0;
        while(currentTime < time)
        {
            ApplyColor(gradient.Evaluate(currentTime/time));
            currentTime += Time.deltaTime;
            yield return null;
        }

        ApplyColor(gradient.Evaluate(1));
	}

    void ApplyColor(Color newColor)
    {
        if (isSprite)
            GetComponent<SpriteRenderer>().color = newColor;

        if (isCamera)
        {
            Camera cam = GetComponent<Camera>();
            if (cam != null)
                cam.backgroundColor = newColor;

            if (updateFog != null)
                updateFog.UpdateFogColor(newColor);
        }
    }
}
