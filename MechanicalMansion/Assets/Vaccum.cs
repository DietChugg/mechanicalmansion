﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using StarterKit;

public class Vaccum : MonoBehaviourPlus 
{
    public PlayerMotor playerMotor;
    public ParticleSystem particleSystem;
    public ParticleSystem particleSystemLeft;
    GameObject particleParent;
    public bool active = false;
    ShootingAimer shootingAimer;
    public Vector3 initalEulerAngles;
    public float effectX;
    public AnimationCurve curve;
    public float animatorValue;

    void Start()
    {
        particleParent = new GameObject("Vaccum");
        particleSystem.transform.parent = particleParent.transform;
        particleSystem.transform.localPosition = Vector3.zero;
        particleSystem.Stop();
        particleSystem.Clear();
        shootingAimer = playerMotor.GetComponent<ShootingAimer>();
        GetComponent<PolygonCollider2D>().enabled = false;
    }

    void Update()
    {
        //particleSystemLeft.transform.SetXLocalScale(-1);
        //particleSystem.transform.SetXLocalScale(1);
        //if (playerMotor.isFacingRight)
        //{
        //    particleSystem.Play();
        //    particleSystemLeft.Stop();
        //    particleSystemLeft.Clear();
            
        //}
        //else
        //{
        //    particleSystemLeft.Play();
        //    particleSystem.Stop();
        //    particleSystem.Clear();
            
        //}

        animatorValue = playerMotor.animator.GetFloat("GunDirection2");

        effectX = curve.Evaluate(animatorValue-180);

        if (active)
        {
            if (!particleSystem.isPlaying)
                particleSystem.Play();
            particleSystem.transform.parent.position = this.transform.position = shootingAimer.gunFront.transform.position;
            particleSystem.transform.rotation = Quaternion.Euler(new Vector3(effectX,270,0));
        }
        else
        {
            if (particleSystem.isPlaying)
            {
                particleSystem.Stop();
                particleSystem.Clear();
            }
        }
            
    }

    public void Enable()
    {
        active = true;
        GetComponent<PolygonCollider2D>().enabled = true;
        GetComponent<AudioSource>().Play();
    }

    public void Disable()
    {
        active = false;
        GetComponent<PolygonCollider2D>().enabled = false;
        GetComponent<AudioSource>().Stop();
    }

    //void OnTriggerEnter2D(Collider2D other)
    //{
    //    Debug.Log(other.transform.name.RTOlive());

    //}
    //void OnTriggerStay2D(Collider2D other)
    //{
    //    Debug.Log(other.transform.name.RTOlive());
    //}

    //void OnCollisionEnter2D(Collision2D other)
    //{
    //    Debug.Log(other.transform.name.RTOlive());

    //}
    //void OnCollisionStay2D(Collision2D other)
    //{
    //    Debug.Log(other.transform.name.RTOlive());

    //}
}
