﻿using UnityEngine;
using System.Collections;
using StarterKit;
using StarterKit.LifeSystem;

public class ClownDeath : StateMachineBehaviour {

	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.transform.parent.GetComponent<BoxCollider2D>().enabled = false;
        if (animator.transform.parent.GetComponent<Clown>().hammer != null)
        {
            animator.transform.parent.GetComponent<Clown>().hammer.GetComponent<DamageInflictor>().canDamage = false;
            animator.transform.parent.GetComponent<Clown>().hammer.GetComponent<BoxCollider2D>().enabled = false;
            animator.transform.parent.GetComponent<EnemyMotor>().enabled = false;
        }
            
    }

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	//override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	//override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
