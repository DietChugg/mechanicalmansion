﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using StarterKit;
using StarterKit.LifeSystem;

public class Clown : MonoBehaviourPlus 
{
    public bool rise;
    public bool jumper;
    public bool idles;
    public Animator animator;
    public GameObject throwPosition;
    public SwingDetection swingDetection;
    public GameObject hammer;
    public SafeAction OnRaiseToLife;
    public GameObject wall;
    public GameObject swing;
    public GameObject floor;
    public GameObject clownSpark;
    public AudioClip[] laughs;
    public FloatClamp timer;
    public FloatRange randomReset;



	public IEnumerator Start()
	{
		yield return null;
	}

	public new void OnEnable () 
	{
        timer.Randomize();
		(this as MonoBehaviourPlus).OnEnable();
        if (swingDetection != null)
            swingDetection.OnSwingHit += SwingDetection;
        animator.SetBool("Rise",rise);
        animator.SetBool("Jumping", jumper);
        animator.SetBool("Idle", idles);
        //animator.SetTrigger("Begin");
        Invoke(.02f, delegate() { animator.SetTrigger("Begin"); });
	}

	public new void OnDisable () 
	{
        if(swingDetection != null)
            swingDetection.OnSwingHit -= SwingDetection;
		(this as MonoBehaviourPlus).OnDisable();
	}

    public void SwingDetection(GameObject hit) 
	{
        //Debug.Log("Swipe Detection");
        if(animator.GetCurrentAnimatorStateInfo(0).IsName("Walk"))
            GetComponent<Enemy>().animator.SetTrigger("Swing");
	}

    public void RaiseTheClown()
    {
        OnRaiseToLife.Call();
        animator.speed = 1f;
    }

    void Update()
    {
        if (GetComponent<Health>().renderer.isVisible && !GetComponent<Health>().health.IsAtMin()&&
            !animator.GetCurrentAnimatorStateInfo(0).IsName("Rise") && !animator.GetCurrentAnimatorStateInfo(0).IsName("AriseSwitch"))
        {
            timer -= Time.deltaTime;
            if (timer.IsAtMin())
            {
                timer.AdjustTo(randomReset.GetRandom());
                AudioClip clip = laughs.ToList().GetRandom();
                if(clip != null)
                    AudioSource.PlayClipAtPoint(clip, transform.position);
            }
        }
    }
}
