﻿using UnityEngine;
using System.Collections;
using StarterKit;
using StarterKit.LifeSystem;

public class yeti_head_slam_idle : StateMachineBehaviour {

    public FloatClamp timer;

	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.GetComponent<YetiBoss>().leftHor.GetComponent<DamageInflictor>().canDamage = true;
        animator.GetComponent<YetiBoss>().rightHor.GetComponent<DamageInflictor>().canDamage = true;
        animator.GetComponent<YetiBoss>().jaw.SetActive(false);
    }

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        timer -= Time.deltaTime;
        if (timer.IsAtMin())
        { 
            animator.SetInteger("SlamIndex", RandomX.Range(1,4));
        }
    }

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetInteger("SlamIndex", 0);
    }

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
