﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System;
using System.Collections;
using System.Collections.Generic;
using StarterKit;

public class FloorSwitch : MonoBehaviourPlus 
{
    public LayerMask hitMasks;
    public Sprite pressed;
    public Sprite released;
    public bool singlePress;
    bool isPressed;
    public UnityEvent onButtonPressed;
    public UnityEvent onButtonReleased;
    public UnityEvent onButtonHeld;
    

    List<GameObject> objsOnSwitch = new List<GameObject>();


	public IEnumerator Start()
	{
		yield return null;
	}

	public new void OnEnable () 
	{
		(this as MonoBehaviourPlus).OnEnable();
	}

	public new void OnDisable () 
	{
		(this as MonoBehaviourPlus).OnDisable();
	}
	
	public void OnTriggerEnter2D (Collider2D other) 
	{
        if (other.transform.CompareLayerMask(hitMasks))
        {
            objsOnSwitch.AddIfDoesNotContain(other.gameObject);
            if (objsOnSwitch.Count == 1)
            {
                isPressed = true;
                GetComponent<SpriteRenderer>().sprite = pressed;
                if (onButtonPressed != null)
                {
                    onButtonPressed.Invoke();
                }
            }
        }
	}

    public void Update()
    {
        if (objsOnSwitch.Count > 0  && !singlePress)
        { 
            if(onButtonHeld != null)
                onButtonHeld.Invoke();
        }
            
    }

    public void OnTriggerExit2D(Collider2D other)
    {
        if (objsOnSwitch.Contains(other.gameObject))
        {
            objsOnSwitch.Remove(other.gameObject);
            if (objsOnSwitch.Count == 0 && !singlePress)
            {
                GetComponent<SpriteRenderer>().sprite = released;
                if (onButtonReleased != null)
                    onButtonReleased.Invoke();
            }
        }
    }
}
