﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using StarterKit;
using StarterKit.LifeSystem;

public class YetiBoss : MonoBehaviourPlus 
{
    public int eyesDestoyed = 0;
    public static SafeAction<GameObject> ActivateBoss;
    Animator animator;
    GameObject player;
    public GameObject spiritSpawn,leftHor, rightHor, leftVer, rightVer, rightEye, leftEye, leftEyeTrigger, rightEyeTrigger, fullHead, headSlam, jaw;

    public ParticleSystem leftShock;
    public ParticleSystem rightShock, disabledSpriritFX;

    public PlayerMotor.ExternalForce externalForce;
    bool showLevelComplete = true;
    AudioSource audioSource;
    bool hasPlayedIntroAudio = false;
    public AudioClip roar;
    public AudioClip smallRoar;
    public AudioClip eyeExplosion;
    public AudioClip gears;
    public AudioClip slam;
    public GameObject stoneSmash;



	public new void OnEnable () 
	{
		(this as MonoBehaviourPlus).OnEnable();
        animator = GetComponent<Animator>();
        ActivateBoss += StartBoss;
        Eye.OnEyeDestroyed += OnEyeDestroyed;
        SpiritEnergy.OnSpiritEnergyDepleted += OnSpiritEnergyDepleted;
        audioSource = GetComponent<AudioSource>();

	}

	public new void OnDisable () 
	{
		(this as MonoBehaviourPlus).OnDisable();
        ActivateBoss -= StartBoss;
        Eye.OnEyeDestroyed -= OnEyeDestroyed;
        SpiritEnergy.OnSpiritEnergyDepleted -= OnSpiritEnergyDepleted;

	}

    public void Gears()
    {
        audioSource.clip = gears;
        audioSource.Play();
    }

    public void Roar()
    {
        if (hasPlayedIntroAudio)
            return;
        hasPlayedIntroAudio = true;
        Invoke(.5f, delegate()
        {
            audioSource.clip = roar;
            audioSource.Play();
            Camera.main.GetComponent<CameraShake>().enabled = true;
        });
    }

    public void SmallRoar()
    {
        audioSource.clip = smallRoar;
        audioSource.Play();
        Camera.main.GetComponent<CameraShake>().enabled = true;
    }

    public void BigRoar()
    {
        audioSource.clip = roar;
        audioSource.Play();
        Camera.main.GetComponent<CameraShake>().enabled = true;
    }

    public void EyeExplosion()
    { 
        AudioSource.PlayClipAtPoint(eyeExplosion,transform.position);
    }

    public void StartBoss(GameObject player) 
	{
        this.player = player;
        Gears();
        animator.SetFloat("IntroSpeed", 1f);
	}

    public void OnEyeDestroyed()
    {
        eyesDestoyed++;
        animator.SetTrigger("EyeExploded");
        animator.SetInteger("BossPhase", eyesDestoyed);
        if (eyesDestoyed == 1)
        {
            animator.SetTrigger("PhaseOneComplete");
            animator.speed = 1.15f;
        }
        else
        {
            animator.speed = 1f;
        }
            
    }

    public void PhaseTwoHeadSlam()
    {
        GameObject instance = stoneSmash.Instantiate(spiritSpawn.transform.position, Quaternion.identity);
        instance.transform.parent = spiritSpawn.transform;
        Invoke(.75f, delegate()
        {
            instance.transform.parent = null;
            instance.transform.rotation = Quaternion.identity;
        });
        AudioSource.PlayClipAtPoint(slam, transform.position);
        Camera.main.GetComponent<CameraShake>().enabled = true;
    }

    public void LevelComplete()
    {
        if (showLevelComplete)
        {
            player.SetActive(false);
            Stats.Instance().ShowLevelComplete();
            showLevelComplete = false;
        }
    }

    public void OnSpiritEnergyDepleted()
    {
        animator.SetTrigger("Death");
    }


    /// <summary>
    /// SECTION FOR ANIMATION TRIGGERED METHODS
    /// </summary>










    public void StartLeftHandSlam()
    {
        leftShock.Play();
        leftHor.GetComponent<BoxCollider2D>().enabled = true;
        leftHor.GetComponent<DamageInflictor>().canDamage = true;
        leftHor.GetComponent<AudioSource>().Play();
    }

    public void StartRightHandSlam()
    {
        rightShock.Play();
        rightHor.GetComponent<BoxCollider2D>().enabled = true;
        rightHor.GetComponent<DamageInflictor>().canDamage = true;
        rightHor.GetComponent<AudioSource>().Play();
    }

    public void EndLeftHandSlam()
    {
        leftShock.Clear();
        leftShock.Stop();
        leftHor.GetComponent<DamageInflictor>().canDamage = false;
        AudioSource.PlayClipAtPoint(slam, transform.position);
        leftHor.GetComponent<AudioSource>().Stop();
        stoneSmash.Instantiate(leftHor.transform.position,leftHor.transform.rotation);

    }

    public void EndRightHandSlam()
    {
        rightShock.Clear();
        rightShock.Stop();
        rightHor.GetComponent<DamageInflictor>().canDamage = false;
        AudioSource.PlayClipAtPoint(slam, transform.position);
        rightHor.GetComponent<AudioSource>().Stop();
        stoneSmash.Instantiate(rightHor.transform.position, rightHor.transform.rotation);


    }

    public void FlingPlayer()
    {
        

        GameObject player = null;
        bool isPlayerdetected = animator.GetComponent<YetiBoss>().leftEyeTrigger.GetComponent<MainPlayerTracker>().isPlayerInTrigger;
        if (isPlayerdetected)
        {
            player = animator.GetComponent<YetiBoss>().leftEyeTrigger.GetComponent<MainPlayerTracker>().player;
        }
        else
        {
            isPlayerdetected = animator.GetComponent<YetiBoss>().rightEyeTrigger.GetComponent<MainPlayerTracker>().isPlayerInTrigger;
            if (isPlayerdetected)
            {
                player = animator.GetComponent<YetiBoss>().rightEyeTrigger.GetComponent<MainPlayerTracker>().player;
            }
        }

        if (player != null)
        {
            Debug.Log("FWING!!!".RTOrange());
            player.GetComponent<PlayerMotor>().externalForce = animator.GetComponent<YetiBoss>().externalForce;
        }

        rightHor.GetComponent<BoxCollider2D>().enabled = false;
        leftHor.GetComponent<BoxCollider2D>().enabled = false;
    }


    public void StartSwipe()
    {
        rightShock.Play();
        leftShock.Play();
        rightVer.GetComponent<BoxCollider2D>().enabled = true;
        rightVer.GetComponent<DamageInflictor>().canDamage = true;
        rightVer.GetComponent<AudioSource>().Play();
        leftVer.GetComponent<BoxCollider2D>().enabled = true;
        leftVer.GetComponent<DamageInflictor>().canDamage = true;
        leftVer.GetComponent<AudioSource>().Play();
    }

    public void EndSwipe()
    {
        rightShock.Clear();
        rightShock.Stop();
        leftShock.Clear();
        leftShock.Stop();
        rightVer.GetComponent<BoxCollider2D>().enabled = false;
        rightVer.GetComponent<DamageInflictor>().canDamage = false;
        rightVer.GetComponent<AudioSource>().Stop();
        leftVer.GetComponent<BoxCollider2D>().enabled = false;
        leftVer.GetComponent<DamageInflictor>().canDamage = false;
        leftVer.GetComponent<AudioSource>().Stop();
        AudioSource.PlayClipAtPoint(slam, transform.position);
    }


    public void StartFinalHandSlam()
    {
        StartLeftHandSlam();
        StartRightHandSlam();
    }

    public void EndFinalHandSlamRight()
    {
        AudioSource.PlayClipAtPoint(slam, transform.position);
        stoneSmash.Instantiate(rightHor.transform.position, rightHor.transform.rotation);
        //Play Rock Slam Effect Here
    }

    public void EndFinalHandSlamLeft()
    {
        AudioSource.PlayClipAtPoint(slam, transform.position);
        stoneSmash.Instantiate(leftHor.transform.position, leftHor.transform.rotation);
        //Play Rock Slam Effect Here
    }

    //public void LeftHorizontalShockOff()
    //{

    //}

    //public void RightHorizontalShockOff()
    //{

    //}
}
