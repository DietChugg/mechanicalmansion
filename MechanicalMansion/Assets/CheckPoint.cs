﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using StarterKit;

public class CheckPoint : MonoBehaviourPlus 
{
    public int id;
    public SceneColorSwitch colorSwitch;

	public void OnTriggerEnter2D (Collider2D other) 
	{
        if (other.transform.name == "Main Player")
        {
            CheckPointManager.Instance.checkPointID = id;
        }
	}
}
