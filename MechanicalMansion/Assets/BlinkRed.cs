﻿using UnityEngine;
using System.Collections;
using StarterKit;
using StarterKit.LifeSystem;


public class BlinkRed : MonoBehaviour 
{
    public Health health;
    public Color blinkColor = Color.red;
    public float blinkSpeed;
    public AnimationCurve blinkCurve;
    SkinnedMeshRenderer skinnedMeshRenderer;
    Color startColor; 

	void OnEnable ()
    {
        skinnedMeshRenderer = GetComponent<SkinnedMeshRenderer>();
        startColor = skinnedMeshRenderer.material.GetColor("_Tint"); 
        health.OnTakeDamage += OnTakeDamage;
	}

    void OnDisable()
    {
        health.OnTakeDamage -= OnTakeDamage;
    }

    void OnTakeDamage(int damageDealt, DamageInflictor damageInflictor)
    {
        skinnedMeshRenderer.material.SetColor("_Tint", startColor);
        StopAllCoroutines();
        StartCoroutine(BlinkForTime(damageInflictor.recoverTime));
    }

	IEnumerator BlinkForTime (float recoverTime)
    {
        float curTime = 0;
        while (recoverTime > 0)
        {
            curTime += Time.deltaTime * blinkSpeed;
            float result = Mathf.PingPong(curTime, 1);
            skinnedMeshRenderer.material.SetColor("_Tint", Color.Lerp(startColor, blinkColor, blinkCurve.Evaluate(result)));
            recoverTime -= Time.deltaTime;
            yield return null;
        }
        skinnedMeshRenderer.material.SetColor("_Tint", startColor);
        yield return null;

	}
}
