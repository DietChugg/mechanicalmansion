﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Rewired;

public class ForceOnClick : MonoBehaviour 
{
    public KeyCode keyCode;
    private Button button;
    Player player;


    void OnEnable()
    {
        player = ReInput.players.GetPlayer(0);

    }

	void Update () 
    {
        if (Input.GetKeyDown(keyCode) || player.GetButtonDown("Jump"))
        {
            GetComponent<Button>().onClick.Invoke();
        }
	}
}
