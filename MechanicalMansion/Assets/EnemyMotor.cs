﻿using UnityEngine;
using System.Collections;
using StarterKit;
using Rewired;
using StarterKit.LifeSystem;


public class EnemyMotor : MonoBehaviourPlus
{
    public bool isTraveling = true;
    public Animator animator;
    public bool autoMove;
    public Vector3 moveSpeed;
    public float horizontalInput;
    public bool jumpInput;
    public bool runInput;

    // movement config
    public float gravity = -25f;
    public float walkSpeed = 5f;
    public float runSpeed = 8f;
    public float groundDamping = 20f; // how fast do we change direction? higher means faster
    public float inAirDamping = 5f;
    public FloatRange jumpHeight;
    public float runJumpHeight = 5f;
    public float stopRisingSpeed = 3f;

    public float riseGravity;
    public float fallGravity;
    public float minRiseTime = .3f;

    [HideInInspector]
    private float normalizedHorizontalSpeed = 0;

    private CharacterController2D _controller;
    //	private Animator _animator;
    private RaycastHit2D _lastControllerColliderHit;
    private Vector3 _velocity;
    bool isJumping;
    float timeSpendJumping = 0f;
    bool releasedJumpButton = true;
    public float minVerticalSpeed;
    public float maxVerticalSpeed;
    public int playerId;
    Player player;
    public float horizontalKickback;
    public float verticalKickback;

    public bool lockWallJump;
    bool canWallJump;
    public bool overrideFacingRight;
    public bool randomizeFacingRight;
    public bool isFacingRight;
    bool? lastWallJumpIsRight;
    private bool isKickback;
    Vector2 applyKickback;
    public WallDetection wallDetection;
    public FloorDetection floorDetection;

    public bool isJumper;
    public bool canJump;
    public FloatRange waitToJump;
    float timeToJump;

    public Renderer renderer;
    

    public bool CanWallJump
    {
        get
        {
            if (lockWallJump == true)
                return false;
            if (_controller.isGrounded)
                return false;
            if (_controller.collisionState.wallSliding)
            {
                return true;
            }
            return false;
        }
    }


    void Awake()
    {
        //		_animator = GetComponent<Animator>();
        _controller = GetComponent<CharacterController2D>();

        // listen to some events for illustration purposes
        _controller.onStay += onControllerCollider;
        _controller.onEnter += OnControllerColliderEnter;
        _controller.onExit += onTriggerExitEvent;
        GetComponent<Health>().OnTakeDamage += OnTakeDamage;
    }

    void OnEnable()
    {
        player = ReInput.players.GetPlayer(playerId);
        if (wallDetection)
        {
            wallDetection.OnWallHit += OnWallHit;
        }
        if (randomizeFacingRight && overrideFacingRight)
            isFacingRight = RandomX.value > .5f;
        horizontalInput = 1f;
        timeToJump = waitToJump.GetRandom();
        if (floorDetection != null)
        {
            floorDetection.OnNoFloorsLeft += OnNoFloorsLeft;
        }
    }

    void OnDisable()
    {
        if (wallDetection)
        {
            wallDetection.OnWallHit -= OnWallHit;
        }
        if (floorDetection != null)
        {
            floorDetection.OnNoFloorsLeft -= OnNoFloorsLeft;
        }
    }

    void OnNoFloorsLeft()
    {
        isFacingRight = !isFacingRight;
        horizontalInput *= -1;
    }

    void OnWallHit(GameObject wall)
    {
        isFacingRight = !isFacingRight;
        horizontalInput *= -1;
    }


    #region Event Listeners

    void onControllerCollider(Character2DHit hit)
    {
        //Debug.Log(hit.transform.gameObject.name);
        // bail out on plain old ground hits cause they arent very interesting
        //if( hit.normal.y == 1f )
        //    return;

        // logs any collider hits if uncommented. it gets noisy so it is commented out for the demo
        //Debug.Log( "flags: " + _controller.collisionState + ", hit.normal: " + hit.normal );
    }

    void OnControllerColliderEnter(Character2DHit col)
    {
        //Debug.Log("onTriggerEnterEvent: " + col.transform.gameObject.name);
    }


    void onTriggerExitEvent(Character2DHit col)
    {
        //Debug.Log( "onTriggerExitEvent: " + col.transform.gameObject.name);
    }

    #endregion

    public void OnTakeDamage(int damageDealt, DamageInflictor damageInflictor)
    {
        if (transform.position.x <= damageInflictor.transform.position.x)
        {
            applyKickback.x = -damageInflictor.kickback.x;
            applyKickback.y = damageInflictor.kickback.y;
        }
        else
        {
            applyKickback.x = damageInflictor.kickback.x;
            applyKickback.y = damageInflictor.kickback.y;
        }
    }


    // the Update loop contains a very simple example of moving the character around and controlling the animation
    void Update()
    {
        if (player == null)
            return;

        if (renderer != null && !renderer.isVisible)
            return;

        if (isJumper && canJump)
        {
            jumpInput = false;
            timeToJump -= Time.deltaTime;
            if (timeToJump < 0)
            {
                timeToJump = waitToJump.GetRandom();
                jumpInput = true;
            }
        }

        


        // grab our current _velocity to use as a base for all calculations
        _velocity = _controller.velocity;
        if (applyKickback != Vector2.zero)
        {
            _controller.collisionState.below = false;
            transform.Translate(0, .001f, 0);
            _velocity = applyKickback;
            applyKickback = Vector2.zero;
        }


        if (animator != null)
        {
            animator.SetBool("IsGrounded", _controller.isGrounded);
        }

        if (_controller.isGrounded)
        {
            isJumping = false;
            //animator.SetFloat("Jumping", 0);
            _controller.collisionState.wallSlidingWall = null;
            _velocity.y = 0;
            timeSpendJumping = 0f;
            lastWallJumpIsRight = null;
            isKickback = false;
           
        }
        else
        {
            if (_velocity.y > 0 && !releasedJumpButton)
            {
                gravity = riseGravity;
            }
            else
            {
                gravity = fallGravity;
            }
        }

        if (horizontalInput > 0.1f)
        {
            normalizedHorizontalSpeed = 1;
            if (transform.localScale.x < 0f)
                transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);

            //			if( _controller.isGrounded )
            //				_animator.Play( Animator.StringToHash( "Run" ) );
        }
        else if (horizontalInput < -0.1f)
        {
            normalizedHorizontalSpeed = -1;
            if (transform.localScale.x > 0f)
                transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);

            //			if( _controller.isGrounded )
            //				_animator.Play( Animator.StringToHash( "Run" ) );
        }
        else
        {
            normalizedHorizontalSpeed = 0;

            //			if( _controller.isGrounded )
            //				_animator.Play( Animator.StringToHash( "Idle" ) );
        }


        // we can only jump whilst grounded
        bool wallSlideEnabled = (lastWallJumpIsRight == null || (isFacingRight && lastWallJumpIsRight == false || !isFacingRight && lastWallJumpIsRight == true));

        if (_controller.isGrounded && jumpInput)
        {
            if (!runInput)
                _velocity.y = Mathf.Sqrt(2f * (runInput ? runJumpHeight : jumpHeight.GetRandom()) * -gravity);
            else
                _velocity.y = Mathf.Sqrt(2f * (jumpHeight.GetRandom()) * -gravity);
            isJumping = true;
            StartCoroutine(JumpAnim());
            releasedJumpButton = false;
            transform.Translate(0, .001f, 0);
            jumpInput = false;
            timeToJump = waitToJump.GetRandom();
            //			_animator.Play( Animator.StringToHash( "Jump" ) );
        }
        else if (CanWallJump && jumpInput && _controller.collisionState.wallSliding)
        {
            _controller.collisionState.wallSliding = false;
            if (!runInput)
                _velocity.y = Mathf.Sqrt(2f * verticalKickback * -gravity);
            else
                _velocity.y = Mathf.Sqrt(2f * (verticalKickback) * -gravity);
            isJumping = true;
            StartCoroutine(JumpAnim());
            isKickback = true;
            releasedJumpButton = false;

            lastWallJumpIsRight = isFacingRight;
            _velocity.x += isFacingRight ? horizontalKickback * -1 : horizontalKickback;
            transform.Translate(0, .001f, 0);
            jumpInput = false;
            timeToJump = waitToJump.GetRandom();
        }

        if (!_controller.isGrounded && !isKickback && !jumpInput && _velocity.y > 0 && isJumping && !releasedJumpButton)
        {

            releasedJumpButton = true;
            //Debug.Log(minRiseTime - timeSpendJumping);
            if (minRiseTime <= timeSpendJumping)
                Release();
            else
                Invoke("Release", minRiseTime - timeSpendJumping);
        }

        if (isJumping)
            timeSpendJumping += Time.deltaTime;


        // apply horizontal speed smoothing it
        var smoothedMovementFactor = _controller.isGrounded ? groundDamping : inAirDamping; // how fast do we change direction?
        if (!runInput)
            _velocity.x = Mathf.Lerp(_velocity.x, normalizedHorizontalSpeed * (runInput ? runSpeed : walkSpeed), Time.deltaTime * smoothedMovementFactor);
        else
            _velocity.x = Mathf.Lerp(_velocity.x, normalizedHorizontalSpeed * walkSpeed, Time.deltaTime * smoothedMovementFactor);

        if (!isTraveling)
            _velocity.x = 0f;


        // apply gravity before moving
        _velocity.y += gravity * Time.deltaTime;

        _velocity.z = 0;
        _velocity.y = Mathf.Clamp(_velocity.y, minVerticalSpeed, maxVerticalSpeed);

        if (_velocity.x != 0 && !overrideFacingRight)
            isFacingRight = _velocity.x > 0;

        if (autoMove)
            _velocity += moveSpeed;

        _controller.move(_velocity * Time.deltaTime);
    }

    IEnumerator LerpToZeroVelocity()
    {
        while (_controller.velocity.y > 0)
        {
            _controller.velocity = Vector3.MoveTowards(_controller.velocity, new Vector3(_controller.velocity.x, 0, 0), Time.deltaTime * stopRisingSpeed);
            yield return null;
        }
        yield return null;
    }

    void Release()
    {
        StartCoroutine(LerpToZeroVelocity());
    }

    IEnumerator JumpAnim()
    {
        float jumping = 0;

        while (jumping < 1)
        {
            //animator.SetFloat("Jumping", jumping);
            jumping += Time.deltaTime * 5;
            yield return null;
        }
    }

}
