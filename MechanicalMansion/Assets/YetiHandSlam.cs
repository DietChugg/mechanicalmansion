﻿using UnityEngine;
using System.Collections;
using StarterKit;
using StarterKit.LifeSystem;

public class YetiHandSlam : StateMachineBehaviour {

    public FloatClamp slamDownTime;

	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //animator.GetComponent<YetiBoss>().leftHor.GetComponent<BoxCollider2D>().enabled = true;
        //animator.GetComponent<YetiBoss>().rightHor.GetComponent<BoxCollider2D>().enabled = true;
        animator.GetComponent<YetiBoss>().leftEyeTrigger.GetComponent<BoxCollider2D>().enabled = true;
        animator.GetComponent<YetiBoss>().rightEyeTrigger.GetComponent<BoxCollider2D>().enabled = true;
        animator.GetComponent<YetiBoss>().leftHor.GetComponent<DamageInflictor>().canDamage = true;
        animator.GetComponent<YetiBoss>().rightHor.GetComponent<DamageInflictor>().canDamage = true;
        slamDownTime.Maximize();
    }

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        slamDownTime -= Time.deltaTime * animator.speed;
        if (slamDownTime.IsAtMin())
        {
            slamDownTime.Maximize();
            animator.GetComponent<YetiBoss>().leftHor.GetComponent<DamageInflictor>().canDamage = false;
            animator.GetComponent<YetiBoss>().rightHor.GetComponent<DamageInflictor>().canDamage = false;
            //Debug.Log("Slammed Down".RTOrange());
        }
    }

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.GetComponent<YetiBoss>().leftHor.GetComponent<DamageInflictor>().canDamage = false;
        animator.GetComponent<YetiBoss>().rightHor.GetComponent<DamageInflictor>().canDamage = false;
    }

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
