﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using StarterKit;
using StarterKit.LifeSystem;

public class Crates : MonoBehaviourPlus 
{
    public State[] states;
    public GameObject crateDeathFX;

	public void OnEnable () 
	{
        GetComponent<Health>().health.Randomize();
        GetComponent<Health>().health += 1;
        GetComponent<Health>().OnTakeDamage += OnTakeDamage;
        GetComponent<Health>().OnDeath += OnDeath;
        for (int i = 0; i < states.Length; i++)
        {
            if (states[i].range.IsInRange(GetComponent<Health>().health.current))
            {
                GetComponent<SpriteRenderer>().sprite = states[i].sprite;
            }
        }
	}

    public void OnDisable()
    {
        GetComponent<Health>().OnTakeDamage -= OnTakeDamage;
        GetComponent<Health>().OnDeath -= OnDeath;
    }

    public void OnTakeDamage(int damageDealt, DamageInflictor di)
    {
        for (int i = 0; i < states.Length; i++)
        {
            if (states[i].range.IsInRange(GetComponent<Health>().health.current))
            {
                GetComponent<SpriteRenderer>().sprite = states[i].sprite;
            }
        }
    }

    public void OnDeath()
    {
        crateDeathFX.Instantiate(transform.position, transform.rotation);
    }

    [System.Serializable]
    public class State
    {
        public IntRange range;
        public Sprite sprite;
    }
}


