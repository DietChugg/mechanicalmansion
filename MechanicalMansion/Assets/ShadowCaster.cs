﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using StarterKit;

public class ShadowCaster : MonoBehaviourPlus 
{
    public Transform shadow;
    public LayerMask shadowLandsOn;
    public Transform targetBone;

    public float minDistance;
    public Vector3 minSize;
    public float maxDistance;
    public Vector3 maxSize;
    public AnimationCurve curve;
    public AnimationCurve alphaCurve;
    public SpriteRenderer spriteRenderer;
    public float maxAlpha;

	public new void OnEnable () 
	{
		(this as MonoBehaviourPlus).OnEnable();
        shadow.SetActive(true);
        shadow.parent = null;
        spriteRenderer = shadow.GetComponent<SpriteRenderer>();
	}

	public new void OnDisable () 
	{
        shadow.SetActive(false);
		(this as MonoBehaviourPlus).OnDisable();
	}
	
	public void Update () 
	{
        RaycastHit2D hit = Physics2D.Raycast(targetBone.position, Vector2.up * -1, Mathf.Infinity, (int)shadowLandsOn);
        if (hit)
        {
            
            shadow.position = hit.point;
            Debug.DrawLine(new Vector3(shadow.position.x, shadow.position.y + minDistance, shadow.position.z), 
                new Vector3(shadow.position.x, shadow.position.y + maxDistance ,shadow.position.z), Color.blue);
            Debug.DrawLine(shadow.position, hit.point, Color.red);
            shadow.GetComponent<SpriteRenderer>().enabled = true;

            if (hit.distance < minDistance)
            {
                shadow.localScale = maxSize;
                //Debug.Log("Min Size Hit".RTOrange());
                //spriteRenderer.color = spriteRenderer.color.SetA(1f);
            }
            else if (hit.distance > maxDistance)
            {
                shadow.localScale = minSize;
                //Debug.Log("Min Size Hit".RTMagenta());
                //spriteRenderer.color = spriteRenderer.color.SetA(0f);
            }
            else
            {
                float percent = curve.Evaluate((hit.distance / minDistance.DistanceTo(maxDistance)).Clamp01());
                shadow.localScale = Vector3.Lerp(maxSize, minSize, percent);
                spriteRenderer.color = spriteRenderer.color.SetA(Mathf.Lerp(0, maxAlpha, 1 - percent));
            }
        }
        else
        {
           shadow.GetComponent<SpriteRenderer>().enabled = false;
           
        }
	}
}
