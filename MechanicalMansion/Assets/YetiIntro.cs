﻿using UnityEngine;
using System.Collections;
using StarterKit;

public class YetiIntro : StateMachineBehaviour {
    public static SafeAction OnIntroComplete;
    public FloatClamp lightTheEyes;
	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        lightTheEyes.Maximize();
        //animator.GetComponent<YetiBoss>().Gears();
    }

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (animator.GetFloat("IntroSpeed") == 0)
            return;
        lightTheEyes -= Time.deltaTime;
        if (lightTheEyes.IsAtMin())
        {
            animator.GetComponent<YetiBoss>().leftEye.GetComponent<Eye>().glow.enabled = true;
            animator.GetComponent<YetiBoss>().rightEye.GetComponent<Eye>().glow.enabled = true;
            
            
        }
        else if (lightTheEyes.current < 1f)
        {
            animator.GetComponent<YetiBoss>().Roar();
        }
    }

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        OnIntroComplete.Call();
    }

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
