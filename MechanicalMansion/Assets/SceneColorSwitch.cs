﻿using UnityEngine;
using System.Collections;

public class SceneColorSwitch : MonoBehaviour 
{
    [System.Serializable]
    public class SceneColors
    {
        public Color mainCamera;
        public Color fog;
        public Color ambient;
    }

    public float switchTime;
    public Camera mainCam;
    public Camera lightingCam;
    public UpdateFog updateFog;
    public SceneColors inColors;
    public SceneColors outColors;

	void OnTriggerExit2D (Collider2D col) 
    {
	    if(col.name == "PlayerCol")
        {
            bool isOut = true;// (col.transform.position.x > this.transform.position.x);
            StartCoroutine(Switch(isOut));
        }
	}

    public IEnumerator Switch(bool isOut)
    {
        float currentTime = 0;
        while (currentTime < switchTime)
        {
            float timePercent = currentTime / switchTime;

            if (isOut)
            {
                mainCam.backgroundColor = Color.Lerp(inColors.mainCamera, outColors.mainCamera, timePercent);
                lightingCam.backgroundColor = Color.Lerp(inColors.ambient, outColors.ambient, timePercent);
                updateFog.UpdateFogColor(Color.Lerp(inColors.fog, outColors.fog, timePercent));
            }
            else
            {
                mainCam.backgroundColor = Color.Lerp(outColors.mainCamera, inColors.mainCamera, timePercent);
                lightingCam.backgroundColor = Color.Lerp(outColors.ambient, inColors.ambient, timePercent);
                updateFog.UpdateFogColor(Color.Lerp(outColors.fog, inColors.fog, timePercent));
            }

            currentTime += Time.deltaTime;
            yield return null;
        }
    }
    
}
