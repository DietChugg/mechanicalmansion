﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using StarterKit;
using StarterKit.LifeSystem;
using Random = System.Random;

public class Enemy : MonoBehaviour 
{
    public Health health;
    public List<Drop> drops = new List<Drop>();
    //public int oddsForNoDrop;
    //public int itemsToDrop;
    public bool isSoulSucked;
    public FloatClamp stunnedTimer;
    public SafeAction ReEnable;
    public DamageInflictor damageInflictor;
    public Animator animator;
    public GameObject wispPrefab;
    public FloatClamp soulAbsorb;
    bool soulReleased = false;
    public Renderer renderer;

	public void OnEnable () 
    {
        //health.OnDeath += OnDeath;
	}

    public void OnDisable()
    {
        //health.OnDeath -= OnDeath;
    }
	
    //void OnDeath()
    //{
    //    isStunned = true;
    //    if (damageInflictor != null)
    //        damageInflictor.enabled = false;
    //    StartCoroutine(StunnedTime());
    //}

    //IEnumerator StunnedTime()
    //{
    //    stunnedTimer.Maximize();
    //    if (!stunnedTimer.IsAtMin() && !isSoulSucked)
    //    {
    //        stunnedTimer -= Time.deltaTime;
    //        yield return null;
    //    }
    //    if (!isSoulSucked)
    //    {
    //        isStunned = false;
    //        health.health.Maximize();
    //        if (damageInflictor != null) 
    //            damageInflictor.enabled = true;
    //        ReEnable.Call();
    //    }
    //}

    void OnTriggerEnter2D(Collider2D other)
    {
        //Debug.Log(other.transform.name.RTRed());
        Vaccum vaccum = other.transform.GetComponent<Vaccum>();
        if (vaccum != null && animator != null && !soulReleased && animator.GetCurrentAnimatorStateInfo(0).IsName("Stun"))
        {
            animator.GetBehaviour<StunBehaviour>().stunTime.Maximize();
        }
    
    }

    void OnTriggerStay2D(Collider2D other)
    {
        //Debug.Log(other.transform.name.RTRed());
        Vaccum vaccum = other.transform.GetComponent<Vaccum>();
        if (vaccum != null && animator != null && !soulReleased && animator.GetCurrentAnimatorStateInfo(0).IsName("Stun"))
        {
            //Debug.Log("Staying Here");
            animator.GetBehaviour<StunBehaviour>().stunTime.Maximize();
            soulAbsorb += Time.deltaTime;
            if (soulAbsorb.IsAtMax())
            {
                soulReleased = true;
                Debug.Log("SOUL RELEASED".RTGreen());
                GameObject wispInstance = wispPrefab.Instantiate(transform.position,transform.rotation);
                Wisp wisp = wispInstance.GetComponent<Wisp>();
                wisp.target = other.transform;
                wisp.GoToTarget();
                animator.SetTrigger("OnDeath");
            }
        }

    }

    void OnTriggerExit2D(Collider2D other)
    {
        //Debug.Log(other.transform.name.RTRed());
        Vaccum vaccum = other.transform.GetComponent<Vaccum>();
        if (vaccum != null && animator != null)
        {
            soulAbsorb.Minimize();
        }

    }

    void OnCollisionEnter2D(Collision2D other)
    {
        //Debug.Log(other.transform.name.RTRed());
        Vaccum vaccum = other.transform.GetComponent<Vaccum>();
        if (vaccum != null && animator != null)
        {
            animator.GetBehaviour<StunBehaviour>().stunTime.Maximize();
        }

    }
    void OnCollisionStay2D(Collision2D other)
    {
        //Debug.Log(other.transform.name.RTRed());
        Vaccum vaccum = other.transform.GetComponent<Vaccum>();
        if (vaccum != null && animator != null)
        {
            Debug.Log("Staying Here");
            animator.GetBehaviour<StunBehaviour>().stunTime.Maximize();
        }

    }


}

public class Drop
{
    public GameObject prefab;
    public TransformData transformData;
    public bool alwaysDrops;
    public int odds;


    //Saved in case we bring drops into the picture
    //List<Drop> raffleDrops = new List<Drop>();
    //int totalOdds = oddsForNoDrop;
    //for (int i = 0; i < drops.Count; i++)
    //{
    //    if (drops[i].alwaysDrops)
    //    {
    //        GameObject instance = (GameObject)GameObject.Instantiate(drops[i].prefab);
    //        drops[i].transformData.ApplyTo(instance);
    //    }
    //    else
    //    {
    //        raffleDrops.Add(drops[i]);
    //        totalOdds += drops[i].odds;
    //    }
    //}

    //for (int i = 0; i < itemsToDrop; i++)
    //{
    //    int ticketNumber = RandomX.Range(0, totalOdds);
    //    if (i < totalOdds)
    //        continue;
    //    ticketNumber -= totalOdds;
    //    for (int i = 0; i < raffleDrops.Count; i++)
    //    {

    //    }

    //}
}
