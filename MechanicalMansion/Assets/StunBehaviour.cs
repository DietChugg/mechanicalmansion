﻿using UnityEngine;
using System.Collections;
using StarterKit.LifeSystem;

public class StunBehaviour : StateMachineBehaviour 
{
    public FloatClamp stunTime = new FloatClamp();
    public EnemyMotor enemyMotor;
    public Health health;
    public DamageInflictor damageInflictor;
    public bool prevCanJump = false;
    public bool prevCanTravel = false;
    public bool prevCanDamage = false;
    public Mesh newHead;

	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //Debug.Log("OnStateEnter");
        Transform enemyRoot = animator.transform.parent;
        enemyMotor = enemyRoot.GetComponent<EnemyMotor>();
        health = enemyRoot.GetComponent<Health>();
        damageInflictor = enemyRoot.GetComponent<DamageInflictor>();
        if(enemyRoot.GetComponent<Clown>().hammer != null)
        {
            Debug.Log("Disable Damage from Hammer");
            enemyRoot.GetComponent<Clown>().hammer.GetComponent<DamageInflictor>().canDamage = false;
        }

        enemyRoot.GetComponent<Clown>().clownSpark.SetActive(true);

        //if (newHead != null)
        //{
        //    Debug.Log("REplacing Head");

        //    Transform clownHead = animator.transform.FindChild("_UNKNOWN_REF_NODE_fosterParent2/clown:nose1");
        //    if (clownHead == null)
        //        Debug.Log("That's no head");

        //    clownHead.GetComponent<SkinnedMeshRenderer>().sharedMesh = newHead;
        //    newHead = null;
        //}

        stunTime.Maximize();
        health.invincible = true;
        if (enemyMotor != null)
        {
            prevCanJump = enemyMotor.canJump;
            prevCanTravel = enemyMotor.isTraveling;
            enemyMotor.isTraveling = false;
            //Debug.Log("Lock Enemy Motor Jumping");
            enemyMotor.canJump = false;
            animator.SetBool("Jumping", false);
        }
        if (damageInflictor != null)
        {
            prevCanDamage = damageInflictor.enabled;
            damageInflictor.canDamage = false;
        }
    }

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //if (damageInflictor != null)
        //{
        //    damageInflictor.canDamage = false;
        //}

        //Debug.Log("Stun Time: " + stunTime.current);
        stunTime -= Time.deltaTime;
        if (stunTime.IsAtMin())
        {
            //Debug.Log("Maxing Health");
            health.RestoreAllHealth();
            health.invincible = false;
            health.hasDied = false;

            //if (damageInflictor != null)
            //{
            //    damageInflictor.canDamage = true;
            //}
        }
    }

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Transform enemyRoot = animator.transform.parent;
        enemyMotor = enemyRoot.GetComponent<EnemyMotor>();
        health = enemyRoot.GetComponent<Health>();
        damageInflictor = enemyRoot.GetComponent<DamageInflictor>();

        enemyRoot.GetComponent<Clown>().clownSpark.SetActive(false);

        if (!health.health.IsAtMin())
        {
            enemyMotor.canJump = prevCanJump;
            enemyMotor.isTraveling = prevCanTravel;
            if (enemyMotor != null)
            {
                enemyMotor.isTraveling = prevCanTravel;
                enemyMotor.canJump = prevCanJump;
                animator.SetBool("Jumping", prevCanJump);
            }

            if (damageInflictor != null)
            {
                damageInflictor.canDamage = prevCanDamage;
            }
        }
        if (enemyRoot.GetComponent<Clown>().hammer != null && !stateInfo.IsName("Death"))
        {
            Debug.Log("Enable Damage from Hammer");
            enemyRoot.GetComponent<Clown>().hammer.GetComponent<DamageInflictor>().canDamage = true;
        }
        
    }

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
